// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

#include "stdafx.h"
#include "MainFrm.h"
#include "EdgeView.h"
#include "Resource.h"
#include "Flisr.h"

class CEdgeViewMenuButton : public CMFCToolBarMenuButton
{
	friend class CEdgeView;

	DECLARE_SERIAL(CEdgeViewMenuButton)

public:
	CEdgeViewMenuButton(HMENU hMenu = NULL) : CMFCToolBarMenuButton((UINT)-1, hMenu, -1)
	{
	}

	virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE,
		BOOL bCustomizeMode = FALSE, BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE)
	{
		pImages = CMFCToolBar::GetImages();

		CAfxDrawState ds;
		pImages->PrepareDrawImage(ds);

		CMFCToolBarMenuButton::OnDraw(pDC, rect, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

		pImages->EndDrawImage(ds);
	}
};

IMPLEMENT_SERIAL(CEdgeViewMenuButton, CMFCToolBarMenuButton, 1)

//////////////////////////////////////////////////////////////////////
// �������� � ��������
//////////////////////////////////////////////////////////////////////

CEdgeView::CEdgeView()
{
	m_nCurrSort = ID_SORTING_GROUPBYTYPE;
	SwitchIndexBegin = 2001;  // ����������� 2001, 2002, 2003
	SwitchIndexEnd = 2003;
}

CEdgeView::~CEdgeView()
{
}

BEGIN_MESSAGE_MAP(CEdgeView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_CLASS_ADD_MEMBER_FUNCTION, OnEdgeAddMemberFunction)
	ON_COMMAND(ID_CLASS_ADD_MEMBER_VARIABLE, OnEdgeAddMemberVariable)
	ON_COMMAND(ID_CLASS_DEFINITION, OnEdgeDefinition)
	ON_COMMAND(ID_CLASS_PROPERTIES, OnEdgeProperties)
	ON_COMMAND(ID_NEW_FOLDER, OnNewFolder)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_COMMAND_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnSort)
	ON_UPDATE_COMMAND_UI_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnUpdateSort)
	ON_COMMAND(ID_EXPAND_ALL, &CEdgeView::OnExpandAll)
	ON_COMMAND(ID_COLLAPSE_ALL, &CEdgeView::OnCollapseAll)
	ON_COMMAND(ID_SWITCH_ON, &CEdgeView::OnSwitchOn)
	ON_COMMAND(ID_SWITCH_OFF, &CEdgeView::OnSwitchOff)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ����������� ��������� CEdgeView

int CEdgeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// �������� �������������:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndEdgeView.Create(dwViewStyle, rectDummy, this, 2))
	{
		TRACE0("�� ������� ������� ������������� �������\n");
		return -1;      // �� ������� �������
	}

	// �������� �����������:
	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_SORT);
	m_wndToolBar.LoadToolBar(IDR_SORT, 0, 0, TRUE /* ������������ */);

	OnChangeVisualStyle();

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

	m_wndToolBar.SetOwner(this);

	// ��� ������� ����� �������������� ����� ���� ������� ����������, � �� ����� ������������ �����:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	CMenu menuSort;
	menuSort.LoadMenu(IDR_POPUP_SORT);

	m_wndToolBar.ReplaceButton(ID_SORT_MENU, CEdgeViewMenuButton(menuSort.GetSubMenu(0)->GetSafeHmenu()));
	m_wndToolBar.ReplaceButton(ID_SORT_MENU2, CEdgeViewMenuButton(menuSort.GetSubMenu(1)->GetSafeHmenu()));

	CEdgeViewMenuButton* pButtonSort = DYNAMIC_DOWNCAST(CEdgeViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButtonSort != NULL)
	{
		pButtonSort->m_bText = FALSE;
		pButtonSort->m_bImage = TRUE;
		pButtonSort->SetImage(GetCmdMgr()->GetCmdImage(m_nCurrSort));
		pButtonSort->SetMessageWnd(this);
	}
	CEdgeViewMenuButton* pButtonStage = DYNAMIC_DOWNCAST(CEdgeViewMenuButton, m_wndToolBar.GetButton(2));

	if (pButtonStage != NULL)
	{
		pButtonStage->m_bText = FALSE;
		pButtonStage->m_bImage = TRUE;
		pButtonStage->SetImage(3);
		pButtonStage->SetMessageWnd(this);
	}
	// ������� ��������� ������ ������������ ������������� � ���� ������ (������ ���, ������ �����)
	FillEdgeView();

	return 0;
}

void CEdgeView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CEdgeView::FillEdgeView()
{
	m_wndEdgeView.DeleteAllItems();
	HTREEITEM hRoot, hEdge;
	CString str;
	int i(0);
	hRoot = m_wndEdgeView.InsertItem(_T("�����"), 2, 2);
	m_wndEdgeView.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);
	auto it = theApp.gGraph.m_topology.m_edges.begin();
	for (it; it != theApp.gGraph.m_topology.m_edges.end(); ++it)
	{
		if ((*it).m_property.m_nType <= SwitchIndexEnd && (*it).m_property.m_nType >= SwitchIndexBegin) 
		{
			if ((*it).m_property.m_nState == 0) //0- ����
			{
				str.Format(_T("%d ����������� (%d) %s"), i, (*it).m_property.m_objectIndex, (*it).m_property.m_strName);
				hEdge = m_wndEdgeView.InsertItem(str, 7, 7, hRoot);
			}
			else
			{
				str.Format(_T("%d ����������� (%d) %s"), i, (*it).m_property.m_objectIndex, (*it).m_property.m_strName);
				hEdge = m_wndEdgeView.InsertItem(str, 8, 8, hRoot);
			}
		}
		else
		{
			str.Format(_T("%d  (%d) %s"), i, (*it).m_property.m_objectIndex, (*it).m_property.m_strName);
			hEdge = m_wndEdgeView.InsertItem(str, 8, 8, hRoot);
		}
		str.Format(_T("%d"), (*it).m_source);
		m_wndEdgeView.InsertItem(str, 11, 11, hEdge);
		str.Format(_T("%d"), (*it).m_target);
		m_wndEdgeView.InsertItem(str, 11, 11, hEdge);
		i++;
	}
}

void CEdgeView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndEdgeView;
	ASSERT_VALID(pWndTree);

	if (pWnd != pWndTree)
	{
		CDockablePane::OnContextMenu(pWnd, point);
		return;
	}

	if (point != CPoint(-1, -1))
	{
		// �������� ������� �������:
		CPoint ptTree = point;
		pWndTree->ScreenToClient(&ptTree);

		UINT flags = 0;
		HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
		if (hTreeItem != NULL)
		{
			pWndTree->SelectItem(hTreeItem);
		}
	}
	pWndTree->SetFocus();
	CMenu menu;
	menu.LoadMenu(IDR_MENU_EDGE);
	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}
}

void CEdgeView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndEdgeView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

BOOL CEdgeView::PreTranslateMessage(MSG* pMsg)
{
	return CDockablePane::PreTranslateMessage(pMsg);
}

void CEdgeView::OnSort(UINT id)
{
	if (m_nCurrSort == id)
	{
		return;
	}

	m_nCurrSort = id;

	CEdgeViewMenuButton* pButton = DYNAMIC_DOWNCAST(CEdgeViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->SetImage(GetCmdMgr()->GetCmdImage(id));
		m_wndToolBar.Invalidate();
		m_wndToolBar.UpdateWindow();
	}
}

void CEdgeView::OnUpdateSort(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(pCmdUI->m_nID == m_nCurrSort);
}

void CEdgeView::OnEdgeAddMemberFunction()
{
	AfxMessageBox(_T("�������� �������-����..."));
}

void CEdgeView::OnEdgeAddMemberVariable()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CEdgeView::OnEdgeDefinition()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CEdgeView::OnEdgeProperties()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CEdgeView::OnNewFolder()
{
	AfxMessageBox(_T("������� �����..."));
}

void CEdgeView::OnPaint()
{
	CPaintDC dc(this); // �������� ���������� ��� ���������

	CRect rectTree;
	m_wndEdgeView.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CEdgeView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	m_wndEdgeView.SetFocus();
}

void CEdgeView::OnChangeVisualStyle()
{
	m_EdgeViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_CLASS_VIEW_24 : IDB_CLASS_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("�� ������� ��������� �������� �������: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_EdgeViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_EdgeViewImages.Add(&bmp, RGB(255, 0, 0));

	m_wndEdgeView.SetImageList(&m_EdgeViewImages, TVSIL_NORMAL);

	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_SORT_24 : IDR_SORT, 0, 0, TRUE /* ������������ */);
}

void CEdgeView::OnExpandAll()
{
	HTREEITEM  h = m_wndEdgeView.GetRootItem();
	ExpandBranch(m_wndEdgeView, h, 1);
}


void CEdgeView::OnCollapseAll()
{
	HTREEITEM  h = m_wndEdgeView.GetRootItem();
	ExpandBranch(m_wndEdgeView, h, 0);
}

void CEdgeView::ExpandBranch(CTreeCtrl& tree, HTREEITEM hItem, BOOL bExpand)
{
	if (tree.ItemHasChildren(hItem))
	{
		tree.Expand(hItem, bExpand ? TVE_EXPAND : TVE_COLLAPSE);
		hItem = tree.GetChildItem(hItem);
		do
		{
			ExpandBranch(tree, hItem, 1);
		} while ((hItem = tree.GetNextSiblingItem(hItem)) != NULL);
	}
}

void CEdgeView::OnSwitchOn()
{
	HTREEITEM item = m_wndEdgeView.GetSelectedItem();
	int n_ItemTree = _tstoi(m_wndEdgeView.GetItemText(item));

	auto it = theApp.gGraph.m_topology.m_edges.begin();
	for (it; it != theApp.gGraph.m_topology.m_edges.end(); ++it)
	{
		if ((*it).m_property.m_index == n_ItemTree)
		{
			(*it).m_property.m_nState = 1;
		}
	}
	m_wndEdgeView.SetItemImage(item, 8, 8);
}


void CEdgeView::OnSwitchOff()
{
	HTREEITEM item = m_wndEdgeView.GetSelectedItem();
	int n_ItemTree = _tstoi(m_wndEdgeView.GetItemText(item));

	auto it = theApp.gGraph.m_topology.m_edges.begin();
	for (it; it != theApp.gGraph.m_topology.m_edges.end(); ++it)
	{
		if ((*it).m_property.m_index == n_ItemTree)
		{
			(*it).m_property.m_nState = 0;
		}
	}
	m_wndEdgeView.SetItemImage(item, 7, 7);
}

void CEdgeView::UpdateEdgeView()
{
	FillEdgeView();
}
