// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

#pragma once

/////////////////////////////////////////////////////////////////////////////
// ���� CViewTree

class CViewTree : public CTreeCtrl
{
// ��������
public:
	CViewTree();

// ���������������
protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	void OnPaint();
// ����������
public:
	virtual ~CViewTree();
	void SetItemColor(HTREEITEM, COLORREF);
	BOOL GetItemBold(HTREEITEM);
public:
	struct Color_Font {
		COLORREF color;
		LOGFONT  logfont;
	};
	Color_Font cf;
	CMap <void*, void*, Color_Font, Color_Font&> m_mapColorFont;

	DECLARE_MESSAGE_MAP()
};
