#include "stdafx.h"
//#ifndef COMMON_H
//#define COMMON_H
typedef  complex<double> COMPLEX;
void LFS210();
void INP202(/*int &KST, int &KSN, int &KTR, int MVK[], double ADOP[], int &KODPOT, int &KODPI, double &DELQU,
	int &KODPID, COMPLEX SN[], COMPLEX SG[], double UY[], int M1[], int M2[], int NAME[], int NAME1[], int NAME2[],
	COMPLEX X[], double REPR[], double KT[], double GNN[], double GNV[], double VNN[], double VSN[], int &NMZK, int &NSMK, int &NSML,
	double UOP[], double QMN[], double QMX[], int &IKOY, int &KODPQG, double &KRATG, double QKY[], int &NFP1,
	int PRIZ[], int NOMERY[], int &MAXNOM, int &MKPARV, int &MKEPV, double &KRATQG,
	int &MKSTEK, double &REPRIM, double &UPRMAX, int &NAMET, double SHUNT[], double &YMIN*/);

void IN202(/*int M1[], int M2[], int M3[], int M3M[], int MVK[], int SI[], double QL[], double QGENL, int PRIZ[],
	double ADOP[], COMPLEX X[], int KVIY[], double UOP[], int MYPU[], int MYPQ[], int KOY, int M3V[],
	double KT[], int KTR, double UY[], COMPLEX SG[], COMPLEX SN[], int KST, double PGENS, double PPOTR,
	double GNN[], double GNV[], int NSTN[], int KSN, double REPR[], int NTV[], int IKOY,
	int NYNS1[], int NYNS2[], double VNN[], double KRATG, int KNDE, int KNDEL,
	double PY[], double QY[], COMPLEX IY[], int MYPQ2[], double QGENS, double QPOTR, double QKY[], double QKYSUM,
	int MYY[], COMPLEX DYP[], COMPLEX MYNS1[], COMPLEX MYNS2[], COMPLEX YPH[], int NSMK, int NSML,
	int NYPH[], int IYPH[], int IH[], int IYSTEK[], int IL[], int IYPL[], int NYPL[], int ISSH[],
	COMPLEX DYPE[], int IE[], COMPLEX YPE[], int IYPE[], int NYPE[], int KNDEE, int KSTEK, COMPLEX IYD[],
	int NPARV[], int KPARV, int MKPARV, int KEPV, int MKEPV, int KODPID,
	int M1PV[], int M2PV[], double ADOPPV[], double KTPV[], COMPLEX ZPV[], double YPV[],
	int NAME1[], int NAME2[], int NAME[], double KRATQG, int MKSTEK,
	double REPRIM, double UPRMAX, int NAMET, double QMN[], double QMX[], double SHUNT[], double YMIN*/);

/*CALL out IN202*/
void FMS117();
void PDS015(int M3X[]);
void RZM202();
void MYP015();
void EK013();
void DET117();
void DEE117(int &KY, int &NYNI, BOOL entry_param);
/**/

void PRE015();
void PVREM();
void RTO015();
void PVP118();
void RMV118();
void ZKS016();

#ifndef COMM_
#define COMM_


#endif
//#endif /* COMMON_H */

