﻿#include "stdafx.h"
#include "Common.h"

// namespace support is now required
#ifdef NO_NAMESPACE
#	error namespace support is now required
#endif
#define NAMESPACE_BEGIN(x) namespace x {
#define NAMESPACE_END }

using namespace MSXML2;

MSXML2::IXMLDOMDocumentPtr	 m_plDomDocument;
MSXML2::IXMLDOMElementPtr	 m_pDocRoot;

NAMESPACE_BEGIN(COMM)
COMPLEX UNOM, U0;
double AMAX, UMIN, UMAX, KRAT;
int M0;
double DELU1, DELU2, AMAX1, SQR3, KSQR;
int KPE, NFP, NFR, KIMAX, KPMAX;
double BP, EPSIL;
NAMESPACE_END

NAMESPACE_BEGIN(POR)
int M(0), N(0), NSM, NBU, N1(0), NSMM, NSMV;
NAMESPACE_END

int i, j;
using namespace COMM;
using namespace POR;

#define MAX_ITER_COUNT 20

int IKOY = 1;	//Определение реактивных мощностей в P - U узлах 1-да, 0-нет;
int NMZK = 1;	//Номер итерации, на которой еще пропускается проверка Q в P-U узлах;
const int KODPI = 1;	//Печать информации по узлам на каждой итерации  1-да, 0-нет;
const int KODPOT = 0;	//Печать потерь мощности в элементах сети        1-да, 0-нет;
const int KODPID = 2;	//Печать исходных данных     1-да, 0-нет, 2 - сфоpмиpованных;
const int KODPQG = 1;	//Печать определения реактивных генераций  -     1-да, 0-нет;
int InputNBU = 0;

int MYPU[186], MYPQ[210], MYPQ2[210], NTV[205],
NSTN[211], SI[211], KVIY[211], MYY[210],
NYNS1[211], NYNS2[211], NPB[50],
IH[211], IYSTEK[4360], IL[210],
IE[211], IYPU[211],
IYR, IMON, IDAY, IHR, IMIN, ISEC, I100TH;

int KST(0), KOY,
KTR(0), KSN(0), NVRPV, KNY,
NYNP, NYNQ,
KP, KI, NFP1, NFP2, NFP3, NFP4, NFP5, NFP6,
NFP7, NFP8, NFP9, NFP10, KNDEE, KSTEK, MKSTEK,
KNDE, KNDEL, NSMK, NSML, NS0, MKPARV, KPARV,
MAXNOM, KEPV, MKEPV, NMAX, MMAX, NAMET;

double DELQU, REPRIM,
UPRMAX, YMIN,
VREM0, TVREM, SVREM, QGENS, QGENL, QPOTR, KRATG, KRATQG,
PNEBM, QNEBM, DELPM, DELQM, PGENS, PPOTR, QKYSUM;

COMPLEX SGEN, SNAG, SPOT, SPOTT;

double  PY[211], QY[211], PZ[211], QZ[211],
VNN[10], GNN[10], GNV[10], VSN[10],
UOP[211], QMN[211], QMX[211], UY[211],
QDELX2[186], QDELT[186], QL[211], QKY[211], SHUNT[211];

COMPLEX IY[211], UOT[186], IYD[211],
U[211], UOPOR[186], MYNS2[211], MYNS3[211];

COMPLEX YPH[3000], YPE[9000],
SN[211], SG[211], IT[419],
DYP[211], DYPE[211],
MYNS1[211];

int NYPH[3000], IYPH[3000], NYPE[9000], IYPE[9000],
IYPL[2500], NYPL[2500], ISSH[2500];

int M3[4200], M3M[4200], M3V[3000], NAME[211],
M1[419], M2[419], MVK[419], PRIZ[419], NOMERY[3100],
NAME1[419], NAME2[419], NPARV[200],
M1PV[100], M2PV[100];

double ADOP[419], KT[419], REPR[419],
PVN[419], PVK[419], QVN[419], QVK[419],
KTPV[100], YPV[100], ADOPPV[100];

COMPLEX X[419], ZPV[100];


using namespace COMM;
using namespace POR;

CStdioFile  NFR_File,  //чтение
NFP_File,  //запись
NFP1_File, //запись
NFP2_File, //запись
NFP3_File, //запись
NFP4_File, //запись
NFP5_File, //запись
NFP6_File, //запись
NFP8_File, //запись
NFP7_File, //запись
NFP9_File, //запись
NFP10_File;//запись

bool LFS210()
{
	complex <double> i(0.0, 1.0), i2, c(5.0, 6.0);
	i2 = pow(i, 2);
	CString s;
	NFR = 10;
	NFP = 11;
	NFP1 = 21;
	//	NFP2 = 22; // Файл для вывода 
	NFP3 = 23;
	NFP4 = 24;
	NFP5 = 25;
	NFP6 = 26;
	NFP7 = 27;
	NFP8 = 28;
	NFP9 = 29;
	NFP10 = 30;

	NFR_File.Open(_T("LFS200.25"), CFile::modeRead);
	NFP_File.Open(_T("LIST"), CFile::modeWrite | CFile::modeCreate);
	NFP1_File.Open(_T("LIS200.W01"), CFile::modeWrite | CFile::modeCreate);
	NFP2_File.Open(_T("LIS200.W02"), CFile::modeWrite | CFile::modeCreate);
	NFP3_File.Open(_T("LIS200.W03"), CFile::modeWrite | CFile::modeCreate);
	NFP4_File.Open(_T("LIS200.W04"), CFile::modeWrite | CFile::modeCreate);
	NFP5_File.Open(_T("LIS200.W05"), CFile::modeWrite | CFile::modeCreate);
	NFP6_File.Open(_T("LIS200.W06"), CFile::modeWrite | CFile::modeCreate);
	NFP7_File.Open(_T("LIS200.W07"), CFile::modeWrite | CFile::modeCreate);
	NFP8_File.Open(_T("LIS200.W08"), CFile::modeWrite | CFile::modeCreate);
	NFP9_File.Open(_T("LIS200.W09"), CFile::modeWrite | CFile::modeCreate);
	NFP10_File.Open(_T("LIS200.W10"), CFile::modeWrite | CFile::modeCreate);


	CTime t = CTime::GetCurrentTime();
	IYR = t.GetYear();
	IMON = t.GetMonth();
	IDAY = t.GetDay();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	I100TH = 0;
	VREM0 = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	TVREM = VREM0;
	NVRPV = 0;

	s.Format(_T("%sДата: %3d%3d%5d Время :%4d час%4d мин%4d.%2d с\n"), CString(_T(' '), 12), IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH);
	NFP2_File.WriteString(s);
	s.Format(_T("\n%s┌%s┐\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP2_File.WriteString(s);
	theApp.WriteToLog(s);


	s.Format(_T("%s└%s┘\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP2_File.WriteString(s);
	theApp.WriteToLog(s);

	EPSIL = 0.00001;
	KPE = 0;
	/*Чтение данных из входного потока:*/
	theApp.WriteToLog(_T(" Ввод исходных данных.\n"));

	OnInput();

	NS0 = NSM;
	SN[N1] = COMPLEX(0.0, 0.0);
	SG[N1] = COMPLEX(0.0, 0.0);
	QKY[N1] = 0.0;
	QGENL = 0.0;

	IN202();

	U[N1] = U0;
	/*	Цикл расчета установившегося режима*/

	PRE015();

	KNY = N - IKOY;

	CLF210();

	/*	PVREM();*/
	/*	Заключительный расчет параметров режима
	Расчет токов по ветвям сети	*/
	RTO015();
	PVP118();

	/*Расчет мощностей по ветвям сети*/
	KI = 0;

	RMV118();

	/*Расчет небалансов мощностей в узлах сети*/
	ZKS016();

	t = CTime::GetCurrentTime();
	IYR = t.GetYear();
	IMON = t.GetMonth();
	IDAY = t.GetDay();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	TVREM = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	SVREM = TVREM - VREM0;
	s.Format(_T(" Суммарное время счета составило: %10.2lf c.\n"), SVREM);
	NFP2_File.WriteString(s);
	s.Format(_T("   NSM, KNDE, KNDEL, KNDEE, KSTEK %7d%7d%7d%7d%7d\n"), NSM, KNDE, KNDEL, KNDEE, KSTEK);
	NFP_File.WriteString(s);
	s.Format(_T("   NS0, NSMK, NSML, NSMM, MKSTEK  %7d%7d%7d%7d%7d\n"), NS0, NSMK, NSML, NSMM, MKSTEK);
	NFP_File.WriteString(s);
	s.Format(_T(" Суммарное время счета составило: %10.2lf c.\n"), SVREM);
	NFP_File.WriteString(s);
	s.Format(_T("            Дата: %3d%3d%5d Время :%4d час%4d мин%4d.%2d c\n"), IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH);
	theApp.WriteToLog(s);
	NFP_File.WriteString(s);
	s.Format(_T("\n%s%2d.%2d.%4d\n%sCopyright (c) 1988-1993\n%sХозяинов М.А.\n%sVersion 1.00 \n"), CString(_T(" "), 28), IDAY, IMON, IYR, CString(_T(" "), 18), CString(_T(" "), 18), CString(_T(" "), 18));
	NFP_File.WriteString(s);

	NFP_File.Close();
	NFR_File.Close();
	NFP1_File.Close();
	NFP2_File.Close();
	NFP3_File.Close();
	NFP4_File.Close();
	NFP5_File.Close();
	NFP6_File.Close();
	NFP7_File.Close();
	NFP8_File.Close();
	NFP9_File.Close();
	NFP10_File.Close();

	return true;
}

bool OnInput()
{
	/*
	======================================================================
	ЧTEHИE И ПEЧATЬ ИCXOДHЫX ДAHHЫX
	========================================================================
	*/
	CString strFileName(_T(/*"GraphData.xml"*/"FlisrData.xml"));
	CString FNAME, sOutput, sInput, tmpStr, sEdgeSwitсhes;
	_bstr_t	bstrFileName;
	COMPLEX SNI, SGI;
	/*double  U00, UOPI, QMNI, QMXI, QKYI, UYI, dReal*/;
	/*int		NAMEI, I, J, J1, J2, iEdgesNum*/;

	KIMAX = MAX_ITER_COUNT;

	//Инициализация COM
	::CoInitialize(NULL);
	HRESULT hr = m_plDomDocument.CreateInstance(MSXML2::CLSID_DOMDocument);
	if (FAILED(hr))
	{
		_com_error er(hr);
		AfxMessageBox(er.ErrorMessage());
	}
	//Преобразовать строку имени файла XML в нечто понятное модели COM (в тип bstr)
	bstrFileName = strFileName.AllocSysString();
	//Обратиться к функции load объекта IXMLDOMDocumentPtr и загрузить документ XML
	variant_t vResult = m_plDomDocument->load(bstrFileName);

	if (!((bool)vResult)) return false;
	m_pDocRoot = m_plDomDocument->documentElement;
	theApp.vecVertexes.push_back(CVertex(0, 0));
	theApp.vecVertexeStage1.push_back(0);
	theApp.vecEdgeStage1.push_back(CEdge(0, CVertex(0, 0), CVertex(0, 0), 0));
	if (!DisplayChildren(m_pDocRoot)) return false; //чтение исходных данных, заполнение структур
	int vecEdgeStage1Size = theApp.vecEdgeStage1.size();
	for (int jInd = 0; jInd < vecEdgeStage1Size; jInd++)
	{
		theApp.vecEdgeStage2.push_back(CEdge(jInd, CVertex(M1[jInd], 0), CVertex(M2[jInd], 0), 0));
	}
	/*-----------------------------------------------------------------------
		NFR_File.ReadString(FNAME);
		sOutput.Format(_T("\
		%sР е ж и м : %s \n\
		%sИсходные данные. \n\
		%sK О Н С Т А Н Т Ы   И   П Е Р Е М Е Н Н Ы Е\n"),
		CString(_T(' '), 20), CString(_T(' '), 25), CString(_T(' '), 15), FNAME);
		NFP1_File.WriteString(sOutput);
	-------------------------------------------------------------------------*/
	DisplayInputData();
	/*-----------------------------------------------------------------------
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	N = _tstoi(tmpStr);
	N1 = N + 1;
	sOutput.Format(_T("     Число узлов сети ..............................%s     %5d\n"), CString(_T('.'), 12), N1);
	NFP1_File.WriteString(sOutput);
	-------------------------------------------------------------------------*/

	/*-----------------------------------------------------------------------
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	M = _tstoi(tmpStr);
	sOutput.Format(_T("     Число ветвей сети .............................%s     %5d\n"), CString(_T('.'), 12), M);
	NFP1_File.WriteString(sOutput);
	-------------------------------------------------------------------------*/

	/*-------------------------------------------------------------------------
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	iEdgesNum = _tstoi(tmpStr);
	sOutput.Format(_T("     Число ветвей выключателей......................%s     %5d\n"), CString(_T('.'), 12), iEdgesNum);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%s"), sInput);
	tmpStr.Trim();
	sEdgeSwitсhes = tmpStr;
	CString str;
	int ind(0), iEdge(0);
	A:
	ind = tmpStr.Find('|');
	if (ind > 0)
	{
	str = tmpStr.Left(ind);
	iEdge = _tstoi(str);
	vecEdgesSwitches.push_back(iEdge);
	tmpStr = tmpStr.Right(tmpStr.GetLength() - ind - 1);
	goto A;
	}
	else
	{
	iEdge = _tstoi(tmpStr);
	vecEdgesSwitches.push_back(iEdge);
	}

	sOutput.Format(_T("     Номера ветвей выключателей......................%s     %s\n"), CString(_T('.'), 12), sEdgeSwitсhes);
	NFP1_File.WriteString(sOutput);


	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KST = _tstoi(tmpStr);
	sOutput.Format(_T("     Число станций системы .........................%s     %5d\n"), CString(_T('.'), 12), KST);
	NFP1_File.WriteString(sOutput);


	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KSN = _tstoi(tmpStr);
	sOutput.Format(_T("     Число ступеней напряжения системы .............%s     %5d\n"), CString(_T('.'), 12), KSN);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KTR = _tstoi(tmpStr);
	sOutput.Format(_T("     Число трансформаторов сети ....................%s     %5d\n"), CString(_T('.'), 12), KTR);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MAXNOM = _tstoi(tmpStr);
	sOutput.Format(_T("     Максимальный номеp узла .......................%s     %5d\n"), CString(_T('.'), 12), MAXNOM);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKPARV = _tstoi(tmpStr);
	sOutput.Format(_T("     Максимальное число паpаллельных ветвей.........%s     %5d\n"), CString(_T('.'), 12), MKPARV);
	NFP1_File.WriteString(sOutput);


	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKEPV = _tstoi(tmpStr);
	sOutput.Format(_T("     Максимальное число эквив. паpаллельных ветвей..%s     %5d\n"), CString(_T('.'), 12), MKEPV);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.7s"), sInput); //формат F7.2
	U00 = dReal = _tstof(sInput);
	U0 = U00;
	sOutput.Format(_T("     Напряжение балансирующего узла, кВ...%s      %7.2lf%7.2lf\n"), CString(_T('.'), 12), U0.real(), U0.imag());
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	NBU = _tstoi(tmpStr);
	sOutput.Format(_T("     Номер балансирующего узла .....................%s     %5d\n"), CString(_T('.'), 12), NBU);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	IKOY = _tstoi(tmpStr);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	NMZK = _tstoi(tmpStr);
	sOutput.Format(_T("     Итерация с пропуском проверки Q в P-U узлах....%s     %5d\n"), CString(_T('.'), 12), NMZK);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KIMAX = _tstoi(tmpStr);
	sOutput.Format(_T("     Заданное число итераций .......................%s     %5d\n"), CString(_T('.'), 12), KIMAX);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KRATG = _tstof(tmpStr);
	sOutput.Format(_T("     Кратность генерации ..........................%s     %6.3lf\n"), CString(_T('.'), 12), KRATG);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KRAT = _tstof(tmpStr);
	sOutput.Format(_T("     Кратность нагрузки ...........................%s     %6.3lf\n"), CString(_T('.'), 12), KRAT);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	KRATQG = _tstof(tmpStr);
	sOutput.Format(_T("     Кратность генеpации pеактивных лин. мощностей.%s    %7.4lf\n"), CString(_T('.'), 12), KRATQG);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	YMIN = _tstof(tmpStr);
	sOutput.Format(_T("     Удаляемая из эквивалента проводимость, См.....%s   %8.7lf\n"), CString(_T('.'), 11), YMIN);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	UPRMAX = _tstof(tmpStr);
	sOutput.Format(_T("     Напpяжение деления pеактивных пpоводимостей линий%s    %9.4lf\n"), CString(_T('.'), 7), UPRMAX);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	REPRIM = _tstof(tmpStr);
	sOutput.Format(_T("     Значение деления pеактивных пpоводимостей, См %s    %7.4lf\n"), CString(_T('.'), 12), REPRIM);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	NAMET = _tstoi(tmpStr);
	sOutput.Format(_T("     Возможные значения новых номеpов узлов.........%s     %5d\n"), CString(_T('.'), 12), NAMET);
	NFP1_File.WriteString(sOutput);


	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	KPMAX = _tstoi(tmpStr);
	sOutput.Format(_T("     Максимальное число перегрузок..................%s     %5d\n"), CString(_T('.'), 12), KPMAX);
	NFP1_File.WriteString(sOutput);


	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELU1 = _tstof(tmpStr);
	sOutput.Format(_T("     Точность сходимости по напряжению, о.е. %s     %8.4lf\n"), CString(_T('.'), 16), DELU1);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELU2 = _tstof(tmpStr);
	sOutput.Format(_T("     Точность сходимости по мощности, МВА  .. %s    %8.4lf\n"), CString(_T('.'), 16), DELU2);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELQU = _tstof(tmpStr);
	sOutput.Format(_T("     Точность выбора Q в P-U узлах, МВар.....%s     %8.4lf\n"), CString(_T('.'), 16), DELQU);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	//tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	BP = _tstof(tmpStr);
	sOutput.Format(_T("     Множитель изменения длительно-допустимых токов%s     %6.2lf\n"), CString(_T('.'), 12), BP);
	NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NMAX = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	MMAX = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSM = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSMK = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSML = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSMM = _tstoi(tmpStr);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	MKSTEK = _tstoi(tmpStr);
	//sOutput.Format(_T("     Pазмерность стека..............................%s       %d\n"), CString(_T('.'), 12), MKSTEK);
	//NFP1_File.WriteString(sOutput);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPI = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPOT = _tstoi(tmpStr);
	//sOutput.Format(_T("%d\n"), KODPOT);
	//NFP1_File.WriteString(sOutput);

	KIMAX = MAX_ITER_COUNT;	//Предельное число итераций;

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPID = _tstoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput); //I6/////////
	KODPQG = _tstoi(tmpStr);

	for (I = 0; I < 9; I++)
	NFR_File.ReadString(sInput); //I6///////// - 9 строк пропуска

	//1.2  MACCИBOB ПO УЗЛAM: -------------------------------------------

	CString sVal;
	for (I = 1; I <= N1; I++)
	{
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.100s"), sInput);
	//tmpStr.Replace(_T("."), _T(","));

	sVal = tmpStr.Left(4); // I4
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 4);
	NAME[I] = _tstoi(sVal);
	v.m_Index = NAME[I];
	theApp.vecVertexes.push_back(v);
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 8); //8X

	sVal = tmpStr.Left(9); //F9.6
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
	SHUNT[I] = _tstof(sVal);

	//----complex------
	sVal = tmpStr.Left(7);//F7.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	SN[I].real(_tstof(sVal));

	double dd = _tstof(sVal);
	sVal = tmpStr.Left(7);//F7.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	SN[I].imag(_tstof(sVal));

	sVal = tmpStr.Left(7);//F7.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	SG[I].real(_tstof(sVal));

	sVal = tmpStr.Left(7);//F7.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	SG[I].imag(_tstof(sVal));
	//-----------------

	sVal = tmpStr.Left(6);//F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	UOP[I] = _tstof(sVal);

	sVal = tmpStr.Left(6);//F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	UY[I] = _tstof(sVal);

	tmpStr = tmpStr.Right(tmpStr.GetLength() - 22); //22X

	sVal = tmpStr.Left(8);//F8.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 8);
	QMN[I] = _tstof(sVal);

	sVal = tmpStr.Left(9);//F9.1
	QMX[I] = _tstof(sVal);
	}

	QKY[1] = QKY[1]; //???

	if (N1 != NBU)
	{
	NAMEI = NAME[N1];
	SNI = SN[N1];
	SGI = SG[N1];
	UYI = UY[N1];
	UOPI = UOP[N1];
	QMNI = QMN[N1];
	QMXI = QMX[N1];
	QKYI = QKY[N1];
	NAME[N1] = NAME[NBU];
	SN[N1] = SN[NBU];
	SG[N1] = SG[NBU];
	UY[N1] = UY[NBU];
	UOP[N1] = UOP[NBU];
	QMN[N1] = QMN[NBU];
	QMX[N1] = QMX[NBU];
	QKY[N1] = QKY[NBU];
	NAME[NBU] = NAMEI;
	SN[NBU] = SNI;
	SG[NBU] = SGI;
	UY[NBU] = UYI;
	UOP[NBU] = UOPI;
	QMN[NBU] = QMNI;
	QMX[NBU] = QMXI;
	QKY[NBU] = QKYI;
	NBU = N1;
	}


	for (I = 1; I <= N1; I++)
	{
	J = NAME[I];
	if (J > MAXNOM)
	{
	sOutput.Format(_T("Максимальный номеp узла %d пpевышает заданный %d\n"), J, MAXNOM);
	NFP_File.WriteString(sOutput);
	theApp.STOP();
	}
	NOMERY[J] = I;
	}


	for (I = 0; I < 8; I++)
	{
	NFR_File.ReadString(sInput); //A1/////// - пропуcк 8ми строк
	}
	double dbb;
	CString sVal;

	for (I = 1; I <= M; I++)
	{
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.71s"), sInput);
	//tmpStr.Replace(_T("."), _T(","));

	sVal = tmpStr.Left(5); // I5
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
	NAME1[I] = _tstoi(sVal);

	sVal = tmpStr.Left(5); // I5
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
	NAME2[I] = _tstoi(sVal);

	//----complex------
	sVal = tmpStr.Left(9);//F9.5
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
	X[I].real(_tstof(sVal));

	sVal = tmpStr.Left(9);//F9.5
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
	X[I].imag(_tstof(sVal));
	//-----------------

	sVal = tmpStr.Left(10); //F10.6
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 10);


	REPR[I] = _tstof(sVal);

	//tmpStr.Format(_T("%.8g"), dbb);

	sVal = tmpStr.Left(7); //F7.6
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	KT[I] = _tstof(sVal);

	tmpStr = tmpStr.Right(tmpStr.GetLength() - 14); //14X

	sVal = tmpStr.Left(2); //I2
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 2);
	PRIZ[I] = _tstoi(sVal);
	sVal = tmpStr.Left(7); //F7.3
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
	ADOP[I] = _tstof(sVal);

	sVal = tmpStr.Left(3); //I3
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 3);
	MVK[I] = _tstoi(sVal);
	}

	CEdge vEdge(0, v0, v0, 0);
	theApp.vecEdges.push_back(vEdge);

	for (i = 1; i <= M; i++)
	{
	J1 = NAME1[i];
	J2 = NAME2[i];
	M1[i] = NOMERY[J1];
	M2[i] = NOMERY[J2];
	theApp.vecEdges.push_back(CEdge(i, CVertex(M1[i], 0), CVertex(M2[i], 0), 0));
	}


	CString sVal;
	for (I = 1; I <= KSN; I++)
	{
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%24s"), sInput);
	//tmpStr.Replace(_T("."), _T(","));

	sVal = tmpStr.Left(6); //F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	GNN[I] = _tstof(sVal);

	sVal = tmpStr.Left(6); //F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	VNN[I] = _tstof(sVal);

	sVal = tmpStr.Left(6); //F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	GNV[I] = _tstof(sVal);

	sVal = tmpStr.Left(6); //F6.1
	tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
	VSN[I] = _tstof(sVal);
	}
	------------------------------------------------------------------------*/
	//sOutput.Format(_T("%d\n"), KODPID); //I6
	//NFP1_File.WriteString(sOutput);
	if (KODPID == 0) return true;
	DisplayInitialData();
	/*
	2.   ПEЧATЬ  ИCXOДHЫX ДAHHЫX --------------------------------------
	C
	WRITE ( NFP1,35 )
	35 FORMAT(/23X,' Д А Н Н Ы Е   П О   У З Л А М'/)
	WRITE ( NFP1, 210 )
	210 FORMAT ('┌',77('─'),'┐')
	WRITE ( NFP1, 220 )
	220 FORMAT ('│    │    │             Мощности              ',
	*'│ Номи-│ За-  │   Пределы по │Мощ- │')
	WRITE ( NFP1, 230 )
	230 FORMAT ('│    │    │───────────────────────────────────',
	*'│ наль-│ дан- │   реактивной │ность│')
	WRITE ( NFP1, 240 )
	240 FORMAT ('│Но- │Име-│     Нагрузки    │   Генерации     ',
	*'│ ное  │ ное  │    мощности, │ком- │')
	WRITE ( NFP1, 250 )
	250 FORMAT ('│мера│на  │───────────────────────────────────',
	*'│ нап- │ нап- │      МВАр    │пенс.│')
	WRITE ( NFP1, 260 )
	260 FORMAT ('│    │    │ Актив- │ Реакти-│ Актив- │ Реакти-',
	*'│ ряже-│ ряже-│ ──────────── │устр.│')
	WRITE ( NFP1, 270 )
	270 FORMAT ('│    │    │ ная,   │ вная,  │ ная,   │ вная,  ',
	*'│ ние, │ ние, │  Ниж- │Верх- │МВАр │')
	WRITE ( NFP1, 280 )
	280 FORMAT ('│    │    │ МВт    │ МВАр   │ МВт    │ МВАр   ',
	*'│ кВ   │ кВ   │  ний  │ний   │     │')
	WRITE ( NFP1, 215 )
	215 FORMAT ('├',80('─'),'┤')
	*/
	/*------------------------------------------------------------------------------------
	sOutput.Format(_T("%sД А Н Н Ы Е   П О   У З Л А М\n+%s+\n\
	│    │    │             Мощности              │ Номи-│ За-  │   Пределы по │Мощ- │\n\
	│    │    │───────────────────────────────────│ наль-│ дан- │   реактивной │ность│\n\
	│Но- │Име-│     Нагрузки    │   Генерации     │ ное  │ ное  │    мощности, │ком- │\n\
	│мера│на  │───────────────────────────────────│ нап- │ нап- │      МВАр    │пенс.│\n\
	│    │    │ Актив- │ Реакти-│ Актив- │ Реакти-│ ряже-│ ряже-│ ──────────── │устр.│\n\
	│    │    │ ная,   │ вная,  │ ная,   │ вная,  │ ние, │ ние, │  Ниж- │Верх- │МВАр │\n\
	│    │    │ МВт    │ МВАр   │ МВт    │ МВАр   │ кВ   │ кВ   │  ний  │ний   │     │\n\
	+%s+\n"), CString(_T(' '), 23), CString(_T('─'), 80), CString(_T('─'), 80));

	NFP1_File.WriteString(sOutput);

	for (I = 1; I <= N; I++)
	{
	//sOutput.Format(_T("%4d│%4d│%8g│%8g│%8g│%8g│%6g│%6g│%7g│%6g│%5g\n"),
	sOutput.Format(_T("%4d│%4d│%8.2lf│%8.2lf│%8.2lf│%8.2lf│%6.1lf│%6.1lf│%7.1lf│%6.1lf│%5.1lf\n"),
	I, NAME[I], SN[I].real(), SN[I].imag(), SG[I].real(),
	SG[I].imag(), UY[I], UOP[I], QMN[I], QMX[I], QKY[I]);
	NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+\n"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	sOutput.Format(_T("%s Д А Н Н Ы Е   П О   В Е Т В Я М\n+%s+\n\
	│    │        Узлы         │  Сопротивления,   │  Прово-  │ Коэф-│ Допу-│Сос- │\n\
	│    │─────────────────────│       Ом          │  димо-   │ фици-│ сти- │тоя- │\n\
	│Но- │  Начала  │  Конца   │───────────────────│  сти,    │ енты │ мые  │ние  │\n\
	│мера│──────────│──────────│ Актив-   Реактив- │  См      │ тран-│ токи,│     │\n\
	│    │ Но-│ Имя │ Но-│ Имя │ ное      ное      │          │ сфор-│  А   │     │\n\
	│    │ мер│     │ мер│     │                   │          │ мации│      │     │\n\
	+%s+\n"), CString(_T(' '), 22), CString(_T('─'), 80), CString(_T('─'), 80));

	NFP1_File.WriteString(sOutput);

	double dn;
	for (I = 1; I <= M; I++)
	{
	dn = ADOP[I] * 1000;
	dn = REPR[I];
	sOutput.Format(_T("%4d│%4d│ %4d│%4d| %4d|%9.3lf%9.3lf |%9.6lf |%5.3lf |%6.0lf|%3d  |\n"),
	I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(),
	REPR[I], KT[I], ADOP[I] * 1000, MVK[I]);
	NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);


	sOutput.Format(_T("\n\n\n%s Д А Н Н Ы Е   П О   В Е Т В Я М (vecM1, vecM2)\n+%s+\n\
	│    │        Узлы         │  Сопротивления,   │  Прово-  │ Коэф-│ Допу-│Сос- │\n\
	│    │─────────────────────│       Ом          │  димо-   │ фици-│ сти- │тоя- │\n\
	│Но- │  Начала  │  Конца   │───────────────────│  сти,    │ енты │ мые  │ние  │\n\
	│мера│──────────│──────────│ Актив-   Реактив- │  См      │ тран-│ токи,│     │\n\
	│    │ Но-│ Имя │ Но-│ Имя │ ное      ное      │          │ сфор-│  А   │     │\n\
	│    │ мер│     │ мер│     │                   │          │ мации│      │     │\n\
	+%s+\n"), CString(_T(' '), 22), CString(_T('─'), 80), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	for (i = 1; i < theApp.vecEdges.size(); i++)
	{
	dn = ADOP[I] * 1000;
	dn = REPR[I];
	sOutput.Format(_T("%4d│%4d│ %4d│%4d| %4d|%9.3lf%9.3lf |%9.6lf |%5.3lf |%6.0lf|%3d  |\n"),
	i, theApp.vecEdges[i].m_Index, theApp.vecEdges[i].m_Begin.m_Index, theApp.vecEdges[i].m_End.m_Index, X[i].real(), X[i].imag(),
	REPR[i], KT[i], ADOP[i] * 1000, MVK[i]);
	NFP1_File.WriteString(sOutput);
	}
	-----------------------------------------------------------------------------------*/
	return true;
}

void IN202()
{
	int NYNI(0), KY(0), KYN, IND, KNV,
		NNY, NKY,
		J, I, NNN,
		NTPV, L,
		NNTY, NKTY, JT, IT, ITPV, NNPV, MNEW,
		NAME1X, NAME2X, MVKX, M1X, M2X, NPARVX, NPARVT,
		MM1, NN1, M12T, MOLD, NBUX, NSTNN,
		KUOP;

	double UYI, REPRX, UYNNY,
		KTX,
		ADOPX, REPRI,
		KTI, ADOPI, KNYR;

	COMPLEX SNAG, SGEN, XX;
	CString str;
	theApp.WriteToLog(_T("Формирование вспомогательных данных.\n"));

	EPSIL = 0.000001;
	KPE = 0;

	SQR3 = sqrt(3.0);
	KSQR = 1.0 / SQR3;
	double r_SQR3 = SQR3;
	double r_KSQR = KSQR;
	/*
		Формирование : - номеров ступеней напряжений: (65 line IN202.FOR)
	*/
	if (N1 > NMAX)
	{
		str.Format(_T("\nНовое число узлов%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), N1, NMAX);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	UY[N1] = U0.real();
	for (J = 1; J <= KSN; J++)
	{
		if (UY[N1] > GNV[J]) continue;
		if (UY[N1] < GNN[J])
		{
			NFP2_File.WriteString(_T("\nДля базисного узла не определена ступень напряжения.\n Расчет закончен для корректировки исходных данных.\n"));
			theApp.WriteToLog(_T("Для базисного узла не определена ступень напряжения. Расчет закончен для корректировки исходных данных.\n"));
			theApp.STOP();
			Sleep(10000);
			return;
		}
		NSTN[N1] = J;
		goto L20;
	}
	/*
		ФОРМИРОВАНИЕ НОМЕРОВ ГЕНЕРАТОРНЫХ УЗЛОВ) : (89 line IN202.FOR)
	*/
L20:
	SNAG = COMPLEX(0.0, 0.0);
	SGEN = COMPLEX(0.0, 0.0);
	QGENL = 0.0;
	QKYSUM = 0.0;
	KUOP = 0;
	for (I = 1; I <= N1; I++)
	{
		QL[I] = 0.0;
		IY[I] = COMPLEX(0.0, 0.0);
		IYD[I] = COMPLEX(0.0, 0.0);
		PY[I] = 0.0;
		QY[I] = 0.0;
		if (UOP[I] >= EPSIL) KUOP = KUOP + 1;
		SN[I] = SN[I] * KRAT;
		SG[I] = SG[I] * KRATG;
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QKYSUM = QKYSUM + QKY[I];
		QKY[I] = SHUNT[I] * (pow(UY[I], 2));
		if (IKOY != 0 && UOP[I] >= EPSIL)
		{
			SG[I] = COMPLEX(SG[I].real(), 0.0);
		}
		UYI = UY[I];
		for (J = 1; J <= KSN; J++)
		{
			if (UYI > GNV[J]) continue;
			if (UYI < GNN[J])
			{
				str.Format(_T("\nДля узла %5d не определена ступень напряжения.\n Расчет закончен для корректировки данных.%5d\n"), I, I);
				NFP2_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			NSTN[I] = J;
			goto L67;
		}
	L67:;
	}
	if (KUOP > KST)
	{
		str.Format(_T("\n Число генеpатоpных узлов%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), KUOP, KST);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	QGENS = SGEN.imag();
	QPOTR = SNAG.imag();
	PGENS = SGEN.real();
	PPOTR = SNAG.real();
	/*
		ФОРМИРОВАНИЕ НОМЕРОВ ТРАНСФОРМАТОРНЫХ ВЕТВЕЙ: (139 line IN202.FOR)
	*/
	KTR = 0;
	KPARV = 0;
	if (M > MMAX)
	{
		str.Format(_T("\n Новое число ветвей%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), M, MMAX);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	/*
		Обработка параллельных ветвей: формирование, сортировка, схлопывание и определение места эквивалентам параллельных ветвей:
	*/
	for (I = 1; I <= M; I++)
	{
		if (PRIZ[I])
		{
			/*
				Фоpмиpование NPARV(KPARV)
			*/
			if (KPARV < MKPARV)
			{
				KPARV = KPARV + 1;
				NPARV[KPARV] = I;
			}
			else
			{
				str.Format(_T("\n Число паpаллельных ветвей%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), KPARV, MKPARV);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
		}
		ADOP[I] = ADOP[I] * BP;
		REPR[I] = REPR[I] * KRATQG;
		if (KT[I] <= EPSIL) continue;
		KTR = KTR + 1;
		NTV[KTR] = I;
		NNY = M1[I];
		NKY = M2[I];

		NNY = theApp.vecEdges[I].m_Begin.m_Index;
		NKY = theApp.vecEdges[I].m_End.m_Index;

		if (UY[NKY] >= UY[NNY]) goto L60;
		M1[I] = NKY;
		M2[I] = NNY;

		theApp.vecEdges[I].m_Begin.m_Index = NKY;
		theApp.vecEdges[I].m_End.m_Index = NNY;

		NNN = NNY;
		NNY = NKY;
		NKY = NNN;
	L60:
		if (KT[I] >= 1.0) continue;
		KT[I] = 1.0 / KT[I];
	}
	//hhh
	int vecEdgesSize = theApp.vecEdges.size();
	for (int iInd = 0; iInd < vecEdgesSize; iInd++)
	{
		theApp.vecEdgeStage3.push_back(CEdge(iInd, CVertex(theApp.vecEdges[iInd].m_Begin.m_Index, 0), CVertex(theApp.vecEdges[iInd].m_End.m_Index, 0), 0));
		theApp.vecEdgeStage3[iInd].m_End.m_Index = theApp.vecEdges[iInd].m_End.m_Index;
	}
	if (KPARV == 0) goto L3500;
	KEPV = 0;
	/*
		Соpтиpовка   NPARV(KPARV)
	*/
	JT = 0;
L1500:;
	if (KEPV >= MKEPV)
	{
		str.Format(_T("\n Число эквивалентных паpаллельных ветвей%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), KEPV, MKEPV);
		NFP_File.WriteString(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	KEPV = KEPV + 1;
	JT = JT + 1;
	J = JT + 1;
	I = NPARV[JT];
	NNTY = M1[I];
	NKTY = M2[I];

	NNTY = theApp.vecEdges[I].m_Begin.m_Index;
	NKTY = theApp.vecEdges[I].m_End.m_Index;

	ZPV[KEPV] = X[I];
	YPV[KEPV] = REPR[I];
	KTPV[KEPV] = KT[I];
	ADOPPV[KEPV] = ADOP[I];
	M1PV[KEPV] = NNTY;
	M2PV[KEPV] = NKTY;
L1600:;
	IT = NPARV[J];
	NNY = M1[IT];
	NKY = M2[IT];

	NNY = theApp.vecEdges[IT].m_Begin.m_Index;
	NKY = theApp.vecEdges[IT].m_End.m_Index;

	if ((NNY == NNTY) && (NKY == NKTY))
	{
		/*
			Опеpации по эквивалентиpованию ветвей NPARV(JT)=I и NPARV(J)=IT
		*/
		ZPV[KEPV] = X[IT] * ZPV[KEPV] / (X[IT] + ZPV[KEPV]);
		YPV[KEPV] = REPR[IT] + YPV[KEPV];
		KTPV[KEPV] = (KT[IT] + KTPV[KEPV]) / 2.0;
		ADOPPV[KEPV] = ADOP[IT] + ADOPPV[KEPV];
		if ((JT + 1) == J)
		{
			JT = JT + 1;
		}
		else
		{
			JT = JT + 1;
			NPARVX = NPARV[J];
			NPARVT = NPARV[JT];

			NAME1X = NAME1[NPARVX];
			NAME2X = NAME2[NPARVX];
			XX = X[NPARVX];
			REPRX = REPR[NPARVX];
			KTX = KT[NPARVX];
			ADOPX = ADOP[NPARVX];
			MVKX = MVK[NPARVX];
			M1X = M1[NPARVX];
			M2X = M2[NPARVX];

			M1X = theApp.vecEdges[NPARVX].m_Begin.m_Index;
			M2X = theApp.vecEdges[NPARVX].m_End.m_Index;

			NAME1[NPARVX] = NAME1[NPARVT];
			NAME2[NPARVX] = NAME2[NPARVT];
			X[NPARVX] = X[NPARVT];
			REPR[NPARVX] = REPR[NPARVT];
			KT[NPARVX] = KT[NPARVT];
			ADOP[NPARVX] = ADOP[NPARVT];
			MVK[NPARVX] = MVK[NPARVT];
			M1[NPARVX] = M1[NPARVT];
			M2[NPARVX] = M2[NPARVT];

			theApp.vecEdges[NPARVX].m_Begin.m_Index = theApp.vecEdges[NPARVT].m_Begin.m_Index;
			theApp.vecEdges[NPARVX].m_End.m_Index = theApp.vecEdges[NPARVT].m_End.m_Index;

			NAME1[NPARVT] = NAME1X;
			NAME2[NPARVT] = NAME2X;
			X[NPARVT] = XX;
			REPR[NPARVT] = REPRX;
			KT[NPARVT] = KTX;
			ADOP[NPARVT] = ADOPX;
			MVK[NPARVT] = MVKX;
			M1[NPARVT] = M1X;
			M2[NPARVT] = M2X;

			theApp.vecEdges[NPARVT].m_Begin.m_Index = M1X;
			theApp.vecEdges[NPARVT].m_End.m_Index = M2X;
		}
		if (JT >= KPARV) goto L2500;
	}
	else
	{
		if ((NNY == NKTY) && (NKY == NNTY))
		{
			/*
				Опеpации по эквивалентиpованию ветвей NPARV(JT) и NPARV(J)
			*/
			if ((JT + 1) == J)
			{
				JT = JT + 1;
			}
			else
			{
				JT = JT + 1;
				NPARVX = NPARV[J];
				NPARVT = NPARV[JT];

				NAME1X = NAME1[NPARVX];
				NAME2X = NAME2[NPARVX];
				XX = X[NPARVX];
				REPRX = REPR[NPARVX];
				KTX = KT[NPARVX];
				ADOPX = ADOP[NPARVX];
				MVKX = MVK[NPARVX];
				M1X = M1[NPARVX];
				M2X = M2[NPARVX];

				M1X = theApp.vecEdges[NPARVX].m_Begin.m_Index;
				M2X = theApp.vecEdges[NPARVX].m_End.m_Index;

				NAME1[NPARVX] = NAME1[NPARVT];
				NAME2[NPARVX] = NAME2[NPARVT];
				X[NPARVX] = X[NPARVT];
				REPR[NPARVX] = REPR[NPARVT];
				KT[NPARVX] = KT[NPARVT];
				ADOP[NPARVX] = ADOP[NPARVT];
				MVK[NPARVX] = MVK[NPARVT];
				M1[NPARVX] = M1[NPARVT];
				M2[NPARVX] = M2[NPARVT];

				theApp.vecEdges[NPARVX].m_Begin.m_Index = theApp.vecEdges[NPARVT].m_Begin.m_Index;
				theApp.vecEdges[NPARVX].m_End.m_Index = theApp.vecEdges[NPARVT].m_End.m_Index;

				NAME1[NPARVT] = NAME1X;
				NAME2[NPARVT] = NAME2X;
				X[NPARVT] = XX;
				REPR[NPARVT] = REPRX;
				KT[NPARVT] = KTX;
				ADOP[NPARVT] = ADOPX;
				MVK[NPARVT] = MVKX;
				M1[NPARVT] = M1X;
				M2[NPARVT] = M2X;

				theApp.vecEdges[NPARVT].m_Begin.m_Index = M1X;
				theApp.vecEdges[NPARVT].m_End.m_Index = M2X;
			}
			if (JT >= KPARV) goto L2500;
		}
		else
		{
			goto L2000;
		}
	}
L2000:;
	if (J == KPARV) goto L1500;
	J = J + 1;
	goto L1600;
L2500:;
	/*
		Схлопывание паpаллельных ветвей
	*/
	ITPV = 1;
	NNPV = NPARV[ITPV];
	NTPV = NNPV;
	I = NTPV;
	for (J = NTPV; J <= M; J++)
	{
		if (J == NTPV)
		{
			ITPV = ITPV + 1;
			NTPV = NPARV[ITPV];
			continue;
		}
		else
		{
			NAME1[I] = NAME1[J];
			NAME2[I] = NAME2[J];
			X[I] = X[J];
			REPR[I] = REPR[J];
			KT[I] = KT[J];
			ADOP[I] = ADOP[J];
			MVK[I] = MVK[J];
			M1[I] = M1[J];
			M2[I] = M2[J];

			theApp.vecEdges[I].m_Begin.m_Index = theApp.vecEdges[J].m_Begin.m_Index;
			theApp.vecEdges[I].m_End.m_Index = theApp.vecEdges[J].m_End.m_Index;

			I = I + 1;
		}
	}
	/*
		Определение места эквивалентам параллельных ветвей.
		(Добавление в конец списка ветвей эквивалентиpованных)
	*/
	for (J = 1; J <= KEPV; J++)
	{
		M1[I] = M1PV[J];
		M2[I] = M2PV[J];

		theApp.vecEdges[I].m_Begin.m_Index = M1PV[J];
		theApp.vecEdges[I].m_End.m_Index = M2PV[J];

		NAME1[I] = NAME[M1PV[J]];
		NAME2[I] = NAME[M2PV[J]];
		X[I] = ZPV[J];
		REPR[I] = YPV[J];
		KT[I] = KTPV[J];
		ADOP[I] = ADOPPV[J];
		MVK[I] = 1;
		I = I + 1;
	}

	M = I - 1;
L3500:;
	MNEW = 0;
	MOLD = M;
	NBUX = -100;
	NBU = N1;
	for (I = 1; I <= M; I++)
	{
		if (M1[I] == NBU) M1[I] = NBUX;
		if (M2[I] == NBU) M2[I] = NBUX;

		if (theApp.vecEdges[I].m_Begin.m_Index == NBU)
		{
			theApp.vecEdges[I].m_Begin.m_Index = NBUX;
		}
		if (theApp.vecEdges[I].m_End.m_Index == NBU)
		{
			theApp.vecEdges[I].m_End.m_Index = NBUX;
		}
	}
	for (I = 1; I <= MOLD; I++)
	{
		NNY = M1[I];
		NKY = M2[I];

		NNY = theApp.vecEdges[I].m_Begin.m_Index;
		NKY = theApp.vecEdges[I].m_End.m_Index;

		if (MVK[I] == 0 || KT[I] >= EPSIL || UY[NNY] <= UPRMAX || abs(REPR[I]) <= REPRIM)
		{
			if (MNEW > MMAX)
			{
				str.Format(_T("\n Новое число ветвей%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), M, MMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			MNEW = MNEW + 1;
			M1[MNEW] = NNY;
			M2[MNEW] = NKY;

			theApp.vecEdges[MNEW].m_Begin.m_Index = NNY;
			theApp.vecEdges[MNEW].m_End.m_Index = NKY;

			NAME1[MNEW] = NAME1[I];
			NAME2[MNEW] = NAME2[I];
			X[MNEW] = X[I];
			REPR[MNEW] = REPR[I];
			KT[MNEW] = KT[I];
			ADOP[MNEW] = ADOP[I];
			MVK[MNEW] = MVK[I];
		}
		else
		{
			REPRI = REPR[I];
			KNYR = abs(REPRI / REPRIM);
			KNY = (int)KNYR;
			NN1 = N + 1;
			N = N + KNY;
			N1 = N + 1;
			if (N1 > NMAX)
			{
				str.Format(_T("\n Новое число узлов%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), N1, NMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			NAME[N1] = NAME[NBU];
			SN[N1] = SN[NBU];
			SG[N1] = SG[NBU];
			UY[N1] = UY[NBU];
			UOP[N1] = UOP[NBU];
			QMN[N1] = QMN[NBU];
			QMX[N1] = QMX[NBU];
			QKY[N1] = QKY[NBU];
			QL[N1] = 0.0;
			IY[N1] = COMPLEX(0.0, 0.0);
			IYD[N1] = COMPLEX(0.0, 0.0);
			PY[N1] = 0.0;
			QY[N1] = 0.0;
			NSTN[N1] = NSTN[NBU];
			UYNNY = UY[NNY];
			NSTNN = NSTN[NNY];
			for (J = NN1; J <= N; J++) //line 411    Do 5100
			{
				NAMET = NAMET + 1;
				QL[J] = 0.0;
				IY[J] = COMPLEX(0.0, 0.0);
				IYD[J] = COMPLEX(0.0, 0.0);
				PY[J] = 0.0;
				QY[J] = 0.0;
				SN[J] = COMPLEX(0.0, 0.0);
				SG[J] = COMPLEX(0.0, 0.0);
				NAME[J] = NAMET;
				UOP[J] = 0.0;
				UY[J] = UYNNY;
				QMN[J] = 0.0;
				QMX[J] = 0.0;
				QKY[J] = 0.0;
				NSTN[J] = NSTNN;
			}
			NBU = N1;
			KNV = KNY + 1;
			REPRI = REPRI / KNV;
			XX = X[I] / (double)KNV;
			KNV = KNV - 1;
			KTI = KT[I];
			ADOPI = ADOP[I];

			M12T = NN1;

			MM1 = M + 1;
			M = M + KNV;

			MNEW = I;
			M1[MNEW] = NNY;
			M2[MNEW] = M12T;

			theApp.vecEdges[MNEW].m_Begin.m_Index = NNY;
			theApp.vecEdges[MNEW].m_End.m_Index = M12T;

			X[MNEW] = XX;
			REPR[MNEW] = REPRI;
			KT[MNEW] = 0.0;
			ADOP[MNEW] = ADOPI;
			MVK[MNEW] = 1;
			NAME1[MNEW] = NAME[NNY];
			NAME2[MNEW] = NAME[M12T];
			if (MNEW > MMAX)
			{
				str.Format(_T("\n Новое число ветвей%5d пpевышает заданный пpедел.%5d\n Расчет закончен для корректировки исходных данных.\n"), M, MMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			for (MNEW = MM1; MNEW <= M; MNEW++)
			{
				if (MNEW == M)
				{
					M1[MNEW] = M12T;
					M2[MNEW] = NKY;

					theApp.vecEdges[MNEW].m_Begin.m_Index = M12T;
					theApp.vecEdges[MNEW].m_End.m_Index = NKY;

					NAME1[MNEW] = NAME[M12T];
					NAME2[MNEW] = NAME[NKY];
				}
				else
				{
					M1[MNEW] = M12T;

					theApp.vecEdges[MNEW].m_Begin.m_Index = M12T;

					NAME1[MNEW] = NAME[M12T];
					M12T = M12T + 1;
					M2[MNEW] = M12T;

					theApp.vecEdges[MNEW].m_End.m_Index = M12T;

					NAME2[MNEW] = NAME[M12T];
				}

				X[MNEW] = XX;
				REPR[MNEW] = REPRI;
				KT[MNEW] = 0.0;
				ADOP[MNEW] = ADOPI;
				MVK[MNEW] = 1;
			}
		}
	}
	for (I = 1; I <= M; I++)
	{
		if (M1[I] == NBUX) M1[I] = N1;
		if (M2[I] == NBUX) M2[I] = N1;

		if (theApp.vecEdges[I].m_Begin.m_Index == NBUX)
		{
			theApp.vecEdges[I].m_Begin.m_Index = N1;
		}
		if (theApp.vecEdges[I].m_End.m_Index == NBUX)
		{
			theApp.vecEdges[I].m_End.m_Index = N1;
		}
	}
	if (KODPID == 2)
	{
		for (I = 1; I <= N1; I++)
		{
			str.Format(_T("│%4d│%4d│%8.2lf│%8.2lf│%8.2lf│%8.2lf│%6.1lf│%6.lf│%7.1lf│%6.1lf│%5.1lf│\n"),
				I, NAME[I], SN[I].real(), SN[I].imag(), SG[I].real(), SG[I].imag(), UY[I], UOP[I], QMN[I], QMX[I], QKY[I]);
			NFP_File.WriteString(str);
		}
		for (I = 1; I <= M; I++)
		{
			str.Format(_T("│%4d│%4d│%4d│%4d│%4d│%9.3lf%9.3lf  │%9.6lf │%5.2lf │%6.1lf│%3d │\n"),
				I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(), REPR[I], KT[I], ADOP[I] * 1000., MVK[I]);
			NFP_File.WriteString(str);
		}
	}

	/*
		ДАЛЕЕ ОСУЩЕСТВЛЯЕТСЯ ПРОВЕРКА ИСХОДНОЙ СЕТИ НА СВЯЗНОСТЬ - ОТДЕЛЬНЫХ УЗЛОВ С СЕТЬЮ И
		НА ФОРМИРОВАННОСТЬ СЕТИ ДВУМЯ ОТДЕЛЬНЫМИ ФУНКЦИЯМИ:
		Проверка отсутствия потери связности исходной сети:
	*/
	FMS117();
	PDS015_new(M3M); //переписанная функция PDS015(M3M);
	PDS015(M3M);

	theApp.WriteToLog(_T(" Инициализация счета.\n"));
	/*
		Определение части расчетных нагрузок от генераций линий и потерь х.х.в трансформаторах :
	*/
	RZM202();

	MYP015();
	/*
		Определение адреса и самого элемента матрицы YPH по N1 :
	*/
	I = IH[N1];
L755:;
	if (I)
	{
		L = NYPH[I];
		J = I;
		I = IYPH[I];
		IYD[L] = YPH[J] * U0 * KSQR;
		goto L755;
	}

	EK013();

	KNDEE = KNDE;

	DET117();
	if (IKOY)
	{
		DEE117(KY, NYNI, IND, KYN, 0);
	}
}

void FMS117()
{
	int I, II, IKO, INA, N01, K, K1, KJ, L, J;
	CString sOutput;
	N01 = N + 1;
	K1 = N01 * 2 + 1;
	K = K1;
	KJ = 1;
	vector<int> v_M3, v_M3V;
	v_M3.push_back(0);
	theApp.vecM3.clear();
	theApp.vecM3.push_back(v_M3);

	theApp.vecVertexeStage3.push_back(v_M3);

	v_M3V.push_back(0);
	theApp.vecM3V.clear();
	theApp.vecM3V.push_back(v_M3V);


	for (I = 1; I <= N01; I++)
	{
		v_M3.clear();
		v_M3V.clear();
		II = I;
		IKO = I * 2;
		INA = IKO - 1;
		L = 0;
		for (J = 1; J <= M; J++)
		{
			//if (M1[J] != I) goto L2;
			if (theApp.vecEdges[J].m_Begin.m_Index != I) goto L2;

			M3[K] = M2[J];
			v_M3.push_back(theApp.vecEdges[J].m_End.m_Index);

			M3V[KJ] = J;
			v_M3V.push_back(J);

			L = L + 1;
			if (L == 1)
			{
				M3[INA] = K;
			}
			K = K + 1;
			KJ = KJ + 1;
			if (K > NSM)
			{
				sOutput.Format(_T("\nHеобходимо увеличить pазмеpность базового списка связности свыше %6d\n"), NSM);
				NFP2_File.WriteString(sOutput);
				theApp.STOP();
			}
			else continue;
		L2:
			//if (M2[J] != I) continue;
			if (theApp.vecEdges[J].m_End.m_Index != I) continue;

			M3[K] = M1[J];
			v_M3.push_back(theApp.vecEdges[J].m_Begin.m_Index);

			M3V[KJ] = J;
			v_M3V.push_back(J);

			L = L + 1;
			if (L == 1)
			{
				M3[INA] = K;
			}
			K = K + 1;
			KJ = KJ + 1;
			if (K > NSM)
			{
				sOutput.Format(_T("Hеобходимо увеличить pазмеpность базового списка связности свыше %6d\n"), NSM);
				NFP2_File.WriteString(sOutput);
				theApp.STOP();
			}
		}
		theApp.vecM3.push_back(v_M3);
		theApp.vecM3V.push_back(v_M3V);
		theApp.vecVertexeStage3.push_back(v_M3);
		if (L != 0)
		{
			M3[IKO] = K - 1;
		}
		else
		{
			sOutput.Format(_T("\n HET CBЯЗHOCTИ C УЗЛOM ПOД HOMEPOM%6d\n BЫПOЛHEHИE ЗAДAHИЯ ПPEKPAЩEHO ДЛЯ УCTPAHEHИЯ OШИБKИ\n"), II);
			NFP2_File.WriteString(sOutput);
			theApp.STOP();
		}
	}

	NSM = K - 1;
	NSMV = KJ - 1;
}

void PDS015_new(int M3X[]) //с векторами 
{
	int nVertexNum, iq, ia, i, ii, j, EdgeCount, mna, mko, m4, m4v, m11, m12, nisi, m3j;
	int koy, ki, si[211] = { 0 };
	EdgeCount = M;
	koy = KOY;
	ki = KI;
	vector <vector<int> > vecM3X;
	CString sOutput;
	/*
	1.1.KOPPEKTИPOBKA MACCИBA CBЯЗHOCTИ УДAЛEHИEM
	HOPMAЛЬHO OTKЛЮЧEHHЫX BETBEЙ :
	CПEPECTAHOBKA ЭЛEMEHTOB MACCИBA CBЯЗHOCTИ УЗЛOB B CЛУЧAE OTЛИЧИЯ
	CИCTEMЫ ПO TOПOЛOГИИ OT БAЗOBOГO COCTOЯHИЯ(ПEPEДAETCЯ MACCИB MVK)
	-------------------------------------------------------------------- -
	1.1.2.ИHИЦИAЛИЗAЦИЯ ДЛЯ MACCИBA CBЯЗHOCTИ УЗЛОВ С ВЕТВЯМИ --
	*/
	for (iq = 1; iq <= EdgeCount; iq++)
	{
		if (MVK[iq] != 0) continue;
		//	OПEPAЦИИ C MACCИBOM M3 ДЛЯ OTKЛЮЧEHHЫX BETBEЙ : OПPEДEЛEHИE HAЧAЛЬHЫX И KOHEЧHЫX HOMEPOB OTKЛЮЧEHHOЙ BETBИ iq 
		m11 = theApp.vecEdges[iq].m_Begin.m_Index;
		m12 = theApp.vecEdges[iq].m_End.m_Index;
		int vecM3_m11_size = theApp.vecM3[m11].size();
		// OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO ИHДEKCA CBЯЗHOCTИ УЗЛA HAЧAЛA BETBИ I		 
		mna = theApp.vecM3[m11].at(0);
		mko = theApp.vecM3[m11].at(vecM3_m11_size - 1);
		//	OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO HAЧAЛЬHЫM  ИHДEKCAM MACCИBA CBЯЗHOCTИ----       
		//		if (mna >= mko) goto L55_new; //?

		if (mko == m12) goto L3_new;

		for (ia = 0; ia < vecM3_m11_size; ++ia)
		{
			if (theApp.vecM3[m11][vecM3_m11_size - 1] != m12) continue;
			m4 = theApp.vecM3[m11][ia];
			m4v = theApp.vecM3V[m11][ia];
			theApp.vecM3[m11][ia] = theApp.vecM3[m11][vecM3_m11_size - 1];
			theApp.vecM3V[m11][ia] = theApp.vecM3V[m11][theApp.vecM3V[m11].size() - 1];
			theApp.vecM3[m11][vecM3_m11_size - 1] = m4;
			theApp.vecM3V[m11][theApp.vecM3V[m11].size() - 1] = m4v;
			goto L3_new;
		}
	L3_new:
		theApp.vecM3[m11].erase(theApp.vecM3[m11].begin() + vecM3_m11_size - 1);

		//		OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO КОНЕЧНЫМ ИHДEKCAM MACCИBA CBЯЗHOCTИ----		
		mna = theApp.vecM3[m12].at(0);
		mko = theApp.vecM3[m12].at(theApp.vecM3[m12].size() - 1);

		//if (mna >= mko) goto L55; //?

		if (mko == m11) goto L4_new;
		for (ia = 0; ia < vecM3_m11_size; ++ia)
		{
			if (theApp.vecM3[m11][vecM3_m11_size - 1] != m11) continue;
			m4 = theApp.vecM3[m11][ia];
			m4v = theApp.vecM3V[m11][ia];
			theApp.vecM3[m11][ia] = theApp.vecM3[m11][vecM3_m11_size - 1];
			theApp.vecM3V[m11][ia] = theApp.vecM3V[m11][theApp.vecM3V[m11].size() - 1];
			theApp.vecM3[m11][vecM3_m11_size - 1] = m4;
			theApp.vecM3V[m11][theApp.vecM3V[m11].size() - 1] = m4v;
			goto L4_new;
		}
	L4_new:
		theApp.vecM3[m12].erase(theApp.vecM3[m12].begin() + theApp.vecM3[m12].size() - 1);
	}
	int vecM3Size = theApp.vecM3.size();
	for (ii = 0; ii < vecM3Size; ii++)
	{
		theApp.vecVertexeStage4.push_back(theApp.vecM3[ii]);
	}

	// --- 1.2. ИHИЦИAЛИЗAЦИЯ BCПOMOГATEЛЬHOГO MACCИBA CBЯЗHOCTИ --------- 

	for (i = 0; i < vecM3Size; ++i)
	{
		vecM3X.push_back(theApp.vecM3[i]);
	}
	//  ----- 1.3 ИHИЦИAЛИЗAЦИЯ KOЛИЧECTBEHHЫX XAPAKTEPИCTИK: -------------- 
	koy = 0;
	ki = 1;
	nVertexNum = 1;
	si[ki] = 1;
	nisi = 1;

	// ----- 2. БЛOK OПPOCA ИCTOKA ------------------------------------------ 
L60_new:
	nVertexNum = si[koy + 1];
	if (vecM3X[nVertexNum].size() != 0) goto L10_new;
	// ----- 2.1. ИCTOK HE TPEБУET OПPOCA ----------------------------------- 
L30_new:
	koy += 1;
	nisi += 1;
	if (ki > koy) goto L60_new;
	if (ki == (N + 1)) goto L200_new;
	// ---------- BЫXOД ИЗ П/ПP. C ПEЧATЬЮ O HAPУШEHИИ CBЯЗHOCTИ ------------ 
	sOutput.Format(_T("\n\nCETЬ PAЗДEЛEHA ПO KPAЙHEЙ MEPE HA ДBA УЧACTKA. \nBЫПOЛHEHИE ЗAДAHИЯ ЗABEPШEHO ДЛЯ ИCПPABЛEHИЯ ИCXOДHЫX ДAHHЫX.\n\n\n"));
	NFP2_File.WriteString(sOutput);
	theApp.STOP();
	//----- 2.2. ИCTOK OПPAШИBAETCЯ ----------------------------------------
L10_new:
	int vecM3Xsize = vecM3X[nVertexNum].size();
	for (j = 0; j < vecM3Xsize; j++)
	{
		m3j = vecM3X[nVertexNum][j];
		for (int jx = nisi; jx <= ki; jx++)
		{
			if (si[jx] == m3j) goto L80_new;
		}
		ki += 1;
		si[ki] = m3j;

		// 2.2.1.ИCKЛЮЧEHИE CMEЖHOCTИ УЗЛA m3j C OПPAШИBAEMЫM УЗЛOM nVertexNum :
		//   2.2.1.1.OПPEДEЛEHИE УKAЗATEЛEЙ HA KOHEЧHЫЙ И HAЧAЛЬHЫЙ УЗEЛ	CMEЖHOCTИ :		
	L80_new:
		// 2.2.1.2.OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO УЗЛOB	CMEЖHЫX C УЗЛOM m3j :		
		mna = vecM3X[m3j][0];
		mko = vecM3X[m3j].at(vecM3X[m3j].size() - 1);

		if (mko == nVertexNum) goto L40_new;
		int vecM3XSize = vecM3X[m3j].size();
		for (ia = 0; ia < vecM3XSize; ia++)
		{
			if (vecM3X[m3j][ia] != nVertexNum) continue;
			m4 = vecM3X[m3j][ia];
			vecM3X[m3j][ia] = mko;
			vecM3X[m3j][vecM3X[m3j].size() - 1] = m4;
			goto L40_new;
		}
	L40_new:
		vecM3X[m3j].erase(vecM3X[m3j].begin() + vecM3X[m3j].size() - 1);
	}
	vecM3X[nVertexNum].clear();//erase(vecM3X[nVertexNum].begin() + vecM3X[nVertexNum].size() - 1);
	goto L30_new;
	/*
	ПЕРЕПИСАНЫЙ КОД ДО ЗАПИСИ "КОНЕЦ"
	*/
	/*	for (IQ = 1; IQ <= M; IQ++)
	{
	if (MVK[IQ] != 0) continue;
	//	OПEPAЦИИ C MACCИBOM M3 ДЛЯ OTKЛЮЧEHHЫX BETBEЙ : OПPEДEЛEHИE HAЧAЛЬHЫX И KOHEЧHЫX HOMEPOB OTKЛЮЧEHHOЙ BETBИ IQ
	M11 = theApp.vecEdges[IQ].m_Begin.m_Index;
	M12 = theApp.vecEdges[IQ].m_End.m_Index;

	// OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO ИHДEKCA CBЯЗHOCTИ УЗЛA HAЧAЛA BETBИ I
	MNA = theApp.vecM3[M11].at(0);
	MKO = theApp.vecM3[M11].at(theApp.vecM3[M11].size() - 1);
	//	OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO HAЧAЛЬHЫM  ИHДEKCAM MACCИBA CBЯЗHOCTИ----
	//		if (MNA >= MKO) goto L55; //?

	if(MKO == M12) goto L3_22;

	for (int ia = 0; ia < theApp.vecM3[M11].size(); ++ia)
	{
	if (theApp.vecM3[M11][theApp.vecM3[M11].size() - 1] != M12) continue;
	M4 = theApp.vecM3[M11][ia];
	M4V = theApp.vecM3V[M11][ia];
	theApp.vecM3[M11][ia] = theApp.vecM3[M11][theApp.vecM3[M11].size()-1];
	theApp.vecM3V[M11][ia] = theApp.vecM3V[M11][theApp.vecM3V[M11].size() - 1];
	theApp.vecM3[M11][theApp.vecM3[M11].size()-1] = M4;
	theApp.vecM3V[M11][theApp.vecM3V[M11].size() - 1] = M4V;
	goto L3_22;
	}
	L3_22:
	theApp.vecM3[M11].erase(theApp.vecM3[M11].begin() + theApp.vecM3[M11].size()-1);

	//		OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO КОНЕЧНЫМ ИHДEKCAM MACCИBA CBЯЗHOCTИ----
	MNA = theApp.vecM3[M12].at(0);
	MKO = theApp.vecM3[M12].at(theApp.vecM3[M12].size() - 1);

	//if (MNA >= MKO) goto L55; //?

	if (MKO == M11) goto L4_22;
	for (int ia = 0; ia < theApp.vecM3[M11].size(); ++ia)
	{
	if (theApp.vecM3[M11][theApp.vecM3[M11].size() - 1] != M11) continue;
	M4 = theApp.vecM3[M11][ia];
	M4V = theApp.vecM3V[M11][ia];
	theApp.vecM3[M11][ia] = theApp.vecM3[M11][theApp.vecM3[M11].size()-1];
	theApp.vecM3V[M11][ia] = theApp.vecM3V[M11][theApp.vecM3V[M11].size() - 1];
	theApp.vecM3[M11][theApp.vecM3[M11].size()-1] = M4;
	theApp.vecM3V[M11][theApp.vecM3V[M11].size() - 1] = M4V;
	goto L4_22;
	}
	L4_22:
	theApp.vecM3[M12].erase(theApp.vecM3[M12].begin() + theApp.vecM3[M12].size() - 1);
	}
	theApp.vecM3;
	theApp.vecM3V;
	// --- 1.2. ИHИЦИAЛИЗAЦИЯ BCПOMOГATEЛЬHOГO MACCИBA CBЯЗHOCTИ ---------
	for (int i = 0; i < theApp.vecM3.size(); ++i)
	{
	vecM3X.push_back(theApp.vecM3[i]);
	}
	//  ----- 1.3 ИHИЦИAЛИЗAЦИЯ KOЛИЧECTBEHHЫX XAPAKTEPИCTИK: --------------
	KOY = 0;
	KI = 1;
	I = 1;
	SI[KI] = 1;
	NISI = 1;

	// ----- 2. БЛOK OПPOCA ИCTOKA ------------------------------------------
	L60_22:
	I = SI[KOY + 1];
	if (vecM3X[I].size() != 0) goto L10_22;
	// ----- 2.1. ИCTOK HE TPEБУET OПPOCA -----------------------------------
	L30_22:
	KOY += 1;
	NISI += 1;
	if (KI > KOY) goto L60_22;
	if (KI == (N + 1)) goto L200;
	// ---------- BЫXOД ИЗ П/ПP. C ПEЧATЬЮ O HAPУШEHИИ CBЯЗHOCTИ ------------
	sOutput.Format(_T("\n\nCETЬ PAЗДEЛEHA ПO KPAЙHEЙ MEPE HA ДBA УЧACTKA. \nBЫПOЛHEHИE ЗAДAHИЯ ЗABEPШEHO ДЛЯ ИCПPABЛEHИЯ ИCXOДHЫX ДAHHЫX.\n\n\n"));
	NFP2_File.WriteString(sOutput);
	theApp.STOP();
	//----- 2.2. ИCTOK OПPAШИBAETCЯ ----------------------------------------
	L10_22:
	for (int j = 0; j < vecM3X[I].size(); j++)
	{
	M3J = vecM3X[I][j];
	for (int jx = NISI; jx <= KI; jx++)
	{
	if (SI[jx] == M3J) goto L80_22;
	}
	KI += 1;
	SI[KI] = M3J;

	// 2.2.1.ИCKЛЮЧEHИE CMEЖHOCTИ УЗЛA M3J C OПPAШИBAEMЫM УЗЛOM I :
	//   2.2.1.1.OПPEДEЛEHИE УKAЗATEЛEЙ HA KOHEЧHЫЙ И HAЧAЛЬHЫЙ УЗEЛ	CMEЖHOCTИ :
	L80_22:
	// 2.2.1.2.OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO УЗЛOB	CMEЖHЫX C УЗЛOM M3J :
	MNA = vecM3X[M3J][0];
	MKO = vecM3X[M3J].at(vecM3X[M3J].size() - 1);

	if (MKO == I) goto L40_22;
	for (int ia=0; ia < vecM3X[M3J].size(); ia++)
	{
	if (vecM3X[M3J][ia] != I) continue;
	M4 = vecM3X[M3J][ia];
	vecM3X[M3J][ia] = MKO;
	vecM3X[M3J][vecM3X[M3J].size() - 1] = M4;
	goto L40_22;
	}
	L40_22:
	vecM3X[M3J].erase(vecM3X[M3J].begin() + vecM3X[M3J].size() - 1);
	}
	vecM3X[I].clear();//erase(vecM3X[I].begin() + vecM3X[I].size() - 1);
	goto L30_22;
	*/

	//L55_new:
	sOutput.Format(_T("\n\n OБHAPУЖEH ИЗOЛИPOBAHHЫЙ OT CETИ УЗEЛ, ИHЦИДEHTHЫЙ HOPMAЛЬHO OTKЛЮЧEHHOЙ BETBИ %s%6d .\n\
 BЫПOЛHEHИE ЗAДAHИЯ ПPEKPAЩEHO ДЛЯ KOPPEKTИPOBKИ ИCXOДHЫX ДAHHЫX.\n\n"), CString(_T(' '), 33), iq);
	NFP2_File.WriteString(sOutput);
	AfxMessageBox(sOutput);
	theApp.STOP();//return
L200_new:
	return;
}

void PDS015(int M3X[])
{
	int I, YN, YK, _IN, IK, M3J, J, NJ, KJ, MNA, MKO, IA, M4, M11, M12,
		IQ, LN, LN1, LK, LK1, NNYV, IANNYV, MKNNYV, M4V, NISI, JX;
	CString sOutput;
	/*
	1.1.KOPPEKTИPOBKA MACCИBA CBЯЗHOCTИ УДAЛEHИEM
	HOPMAЛЬHO OTKЛЮЧEHHЫX BETBEЙ :
	CПEPECTAHOBKA ЭЛEMEHTOB MACCИBA CBЯЗHOCTИ УЗЛOB B CЛУЧAE OTЛИЧИЯ
	CИCTEMЫ ПO TOПOЛOГИИ OT БAЗOBOГO COCTOЯHИЯ(ПEPEДAETCЯ MACCИB MVK)
	-------------------------------------------------------------------- -
	1.1.2.ИHИЦИAЛИЗAЦИЯ ДЛЯ MACCИBA CBЯЗHOCTИ УЗЛОВ С ВЕТВЯМИ --
	*/
	NNYV = N1 * 2;
	for (IQ = 1; IQ <= M; IQ++)
	{
		if (MVK[IQ] != 0) continue;
		//  OПEPAЦИИ C MACCИBOM M3 ДЛЯ OTKЛЮЧEHHЫX BETBEЙ : ------ OПPEДEЛEHИE HAЧAЛЬHЫX И KOHEЧHЫX HOMEPOB OTKЛЮ - ------	ЧEHHOЙ BETBИ IQ------

		M11 = M1[IQ];
		M12 = M2[IQ];

		//  OПPEДEЛEHИE KOHEЧHOГO УKAЗATEЛЯ ИHДEKCA CBЯЗHOCTИ ------			УЗЛA HAЧAЛA И KOHЦA BETBИ I:
		LN1 = M11 * 2;
		LK1 = M12 * 2;

		//	OПPEДEЛEHИE HAЧAЛЬHOГO УKAЗATEЛЯ ИHДEKCA CBЯЗHOCTИ ------			УЗЛA HAЧAЛA И KOHЦA BETBИ I:		
		LN = LN1 - 1;
		LK = LK1 - 1;

		//	OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO ИHДEKCA CBЯЗHOCTИ ------			УЗЛA HAЧAЛA BETBИ I
		MNA = M3[LN];
		MKO = M3[LN1];

		//	OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO HAЧAЛЬHЫM----			ИHДEKCAM MACCИBA CBЯЗHOCTИ----
		if (MNA >= MKO) goto L55; //?

		if (M3[MKO] == M12) goto L3;

		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3[IA] != M12)continue;
			IANNYV = IA - NNYV;
			MKNNYV = MKO - NNYV;
			M4 = M3[IA];
			M4V = M3V[IANNYV];
			M3[IA] = M3[MKO];
			M3V[IANNYV] = M3V[MKNNYV];
			M3[MKO] = M4;
			M3V[MKNNYV] = M4V;
			goto L3;
		}
	L3:
		M3[LN1] = M3[LN1] - 1;
		//	OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO КОНЕЧНЫМ----	ИHДEKCAM MACCИBA CBЯЗHOCTИ----
		MNA = M3[LK];
		MKO = M3[LK1];

		if (MNA >= MKO) goto L55; //?

		if (M3[MKO] == M11) goto L4;

		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3[IA] != M11) continue;
			IANNYV = IA - NNYV;
			MKNNYV = MKO - NNYV;
			M4 = M3[IA];
			M4V = M3V[IANNYV];
			M3[IA] = M3[MKO];
			M3V[IANNYV] = M3V[MKNNYV];
			M3[MKO] = M4;
			M3V[MKNNYV] = M4V;
			goto L4;
		}
	L4:
		M3[LK1] = M3[LK1] - 1;
	}
	//	--- 1.2. ИHИЦИAЛИЗAЦИЯ BCПOMOГATEЛЬHOГO MACCИBA CBЯЗHOCTИ ---------	
	int copM3X[4200];
	copM3X[0] = 0;
	for (I = 1; I <= NSM; I++)
	{
		M3X[I] = M3[I];
		copM3X[I] = M3[I];
	}
	//	----- 1.3 ИHИЦИAЛИЗAЦИЯ KOЛИЧECTBEHHЫX XAPAKTEPИCTИK: --------------
	KOY = 0;
	KI = 1;
	I = 1;
	SI[KI] = 1;
	NISI = 1;

	//  ----- 2. БЛOK OПPOCA ИCTOKA ------------------------------------------
L60:
	I = SI[KOY + 1];
	YK = I * 2;
	YN = YK - 1;
	_IN = M3X[YN];
	IK = M3X[YK];

	if (_IN <= IK) goto L10;
	//	----- 2.1. ИCTOK HE TPEБУET OПPOCA -----------------------------------
L30:
	KOY = KOY + 1;
	NISI = NISI + 1;
	if (KI > KOY) goto L60;
	int ffff = N;
	if (KI == (N + 1)) goto L200;
	//		---------- BЫXOД ИЗ П/ПP. C ПEЧATЬЮ O HAPУШEHИИ CBЯЗHOCTИ ------------
	sOutput.Format(_T("\n\nCETЬ PAЗДEЛEHA ПO KPAЙHEЙ MEPE HA ДBA УЧACTKA. \nBЫПOЛHEHИE ЗAДAHИЯ ЗABEPШEHO ДЛЯ ИCПPABЛEHИЯ ИCXOДHЫX ДAHHЫX.\n\n\n"));
	NFP2_File.WriteString(sOutput);
	AfxMessageBox(sOutput);
	theApp.STOP();
	//		2.2. ИCTOK OПPAШИBAETCЯ ----------------------------------------
L10:
	for (J = _IN; J <= IK; J++)
	{
		M3J = M3X[J];
		for (JX = NISI; JX <= KI; JX++)
		{
			if (SI[JX] == M3J) goto L80;
		}
		KI = KI + 1;
		SI[KI] = M3J;

		//		2.2.1.ИCKЛЮЧEHИE CMEЖHOCTИ УЗЛA M3J C OПPAШИBAEMЫM УЗЛOM I :
		//		2.2.1.1.OПPEДEЛEHИE УKAЗATEЛEЙ HA KOHEЧHЫЙ И HAЧAЛЬHЫЙ УЗEЛ	CMEЖHOCTИ :	
	L80:
		KJ = M3J * 2;
		NJ = KJ - 1;

		//		2.2.1.2.OПPEДEЛEHИE ИHДEKCOB HAЧAЛЬHOГO И KOHEЧHOГO УЗЛOB	CMEЖHЫX C УЗЛOM M3J :
		MNA = M3X[NJ];
		MKO = M3X[KJ];

		if (MNA > MKO) continue;
		if (M3X[MKO] == I) goto L40;
		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3X[IA] != I)
			{
				continue;
			}
			M4 = M3X[IA];
			M3X[IA] = M3X[MKO];
			copM3X[IA] = copM3X[MKO];
			M3X[MKO] = M4;
			copM3X[MKO] = M4;
			goto L40;
		}
	L40:
		M3X[KJ] = M3X[KJ] - 1;
		copM3X[KJ] = copM3X[KJ] - 1;
	}
	M3X[YK] = _IN - 1;
	copM3X[YK] = _IN - 1;
	goto L30;
L55:
	sOutput.Format(_T("\n\n OБHAPУЖEH ИЗOЛИPOBAHHЫЙ OT CETИ УЗEЛ, ИHЦИДEHTHЫЙ HOPMAЛЬHO OTKЛЮЧEHHOЙ BETBИ %s%6d .\
\n BЫПOЛHEHИE ЗAДAHИЯ ПPEKPAЩEHO ДЛЯ KOPPEKTИPOBKИ ИCXOДHЫX ДAHHЫX.\n\n"), CString(_T(' '), 33), IQ);
	NFP2_File.WriteString(sOutput);
	AfxMessageBox(sOutput);
	theApp.STOP();//return
L200:
	return;
}

void RZM202()
{
	int NNY, NKY, I;
	double KOEF, EPS(0.00000001), QLG;
	QGENL = 0;
	for (I = 1; I <= M; I++)
	{
		if (MVK[I])
		{
			if (abs(KT[I]) < EPS) goto L45;
			if (abs(REPR[I]) < EPS) continue;
			NKY = M2[I];

			NKY = theApp.vecEdges[I].m_End.m_Index;

			QLG = REPR[I] * (pow(VNN[NSTN[NKY]], 2));
			QGENL = QGENL + QL[NKY];
			QL[NKY] = QL[NKY] + QLG;
			continue;
		L45:
			if (abs(REPR[I]) < EPS) continue;
			NNY = M1[I];
			NKY = M2[I];

			NNY = theApp.vecEdges[I].m_Begin.m_Index;
			NKY = theApp.vecEdges[I].m_End.m_Index;

			KOEF = REPR[I] / 2.0;
			QLG = KOEF * (pow(VNN[NSTN[NNY]], 2));
			QGENL = QGENL + QLG;
			QL[NNY] = QL[NNY] + QLG;
			QLG = KOEF * (pow(VNN[NSTN[NKY]], 2));
			QGENL = QGENL + QLG;
			QL[NKY] = QL[NKY] + QLG;
		}
	}
}

void MYP015()
{
	int N12, NV1, NV2, I, NNV, NKV, J, NV;
	COMPLEX YF, YFKT, YDIAG;

	KNDE = 0;
	N12 = N1 * 2;
	for (I = 1; I <= N1; I++)
	{
		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		YDIAG = COMPLEX(0.0, 0.0);
		for (J = NNV; J <= NKV; J++)
		{
			NV = M3V[J];
			if (MVK[NV] == 0) continue;
			KNDE = KNDE + 1;
			NYPH[KNDE] = M3[J + N12];
			if (J == NNV)
			{
				IH[I] = KNDE;
			}
			if (J == NKV)
			{
				IYPH[KNDE] = 0;
			}
			if (J != NKV)
			{
				IYPH[KNDE] = KNDE + 1;
			}
			YF = COMPLEX(1.0, 0.0) / X[NV];
			if (KT[NV] < EPSIL) goto L5;
			YFKT = -YF * KT[NV];
			YPH[KNDE] = YFKT;
			/*if (M1[NV] == I)
			{
				YDIAG = YDIAG - YFKT*KT[NV];//0.37672826753374494  -9.5312251686037470
			}*/
			if (theApp.vecEdges[NV].m_Begin.m_Index == I)
			{
				YDIAG = YDIAG - YFKT*KT[NV];
			}

			/*if (M2[NV] == I)
			{
				YDIAG = YDIAG + YF;
			}*/

			if (theApp.vecEdges[NV].m_End.m_Index == I)
			{
				YDIAG = YDIAG + YF;
			}
			continue;
		L5:
			YPH[KNDE] = -YF;
			YDIAG = YDIAG + YF;
		}
		DYP[I] = YDIAG;
	}
}

void EK013()
{
	int I;
	for (I = 1; I <= N1; I++)
	{
		DYPE[I] = DYP[I];
		IE[I] = IH[I];
		YPE[I] = YPH[I];
		IYPE[I] = IYPH[I];
		NYPE[I] = NYPH[I];
	}

	for (I = (N1 + 1); I <= KNDE; I++)
	{
		YPE[I] = YPH[I];
		IYPE[I] = IYPH[I];
		NYPE[I] = NYPH[I];
	}
}

void DET117()
{
	int N12, I, II, I1, J, JJ, NKY, NNY, KYN, NYY, NMIN, L, NYNI, IND, JLL;
	int KYNS1, KYNS2, IS1, JS2, NTY, NTY2, ISMAX, ISMAXN, INNDE, ILL;
	COMPLEX YKK, MYP1, MYP2, YKKM;
	CString sOutput;
	/*
		Манипуляции с эквивалентом по генераторным узлам.

		МАССИВЫ:
		--------
		NYNS1(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ОДНОГО УЗЛА НОВОЙ СВ;
		NYNS2(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ДРУГОГО УЗЛА НОВОЙ СВ;

		ПЕРЕМЕННЫЕ:
		----------
		KYNS1     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ЕДИНИЦЫ;
		KYNS2     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ДВУХ;
		KYN       - КОЛИЧЕСТВО НАГРУЗОЧНЫХ УЗЛОВ ЭКВИВАЛЕНТА;
		NTY       - НОМЕР ТЕКУЩЕГО УЗЛА СЕТИ В ПОДСПИСКЕ СВЯЗНОСТИ
		УЗЛА 'NYNI';
		NTY2      - ТО ЖЕ, НО ВО ВНУТРЕННЕМ ЦИКЛЕ;
		N12     - ИНДЕКС УКАЗАТЕЛЯ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
		ISMAX     - ИНДЕКС СВЯЗНОСТИ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
		NYNI      - НОМЕР УЗЛА СРЕДИ НАГРУЗОЧНЫХ С НАИМЕНЬШЕЙ
		ИНЦИДЕНТНОСТЬЮ;
		IND       - ИНДЕКС ЭТОГО УЗЛА.
		NYY       - НОМЕР упорядоченного УЗЛА;
	*/


	N12 = N1 * 2;
	KNDEL = 0;
	/*
		Обнуление указателя индексов IL прямой подстановки и инициализация
		списка негенераторных узлов:
	*/
	for (I = 1; I <= N; I++)
	{
		MYPQ[I] = I;
		IL[I] = 0;
	}

	/*
		ИНИЦИАЛИЗАЦИЯ МАССИВА КОЛИЧЕСТВА ВЕТВЕЙ ИНЦИДЕНТНЫХ УЗЛАМ:
		ЦИКЛ ПО УКАЗАТЕЛЯМ МАССИВА М3(NBU)
	*/
	J = 0;
	for (I = 2; I <= N12; I += 2)
	{
		J = J + 1;
		NKY = M3[I];
		NNY = M3[I - 1];
		KVIY[J] = NKY - NNY + 1;
	}

	KYN = N;
	KSTEK = 0;
	NYY = 0;

L1000:
	//ПОИСК НОМЕРА И ИНДЕКСА УЗЛА С НАИМЕНЬШЕЙ ИНЦИДЕНТНОСТЬЮ
	NMIN = M;
	NYY = NYY + 1;
	for (I = 1; I <= KYN; I++)
	{
		J = MYPQ[I];
		L = KVIY[J];
		if (L >= NMIN) continue;
		NMIN = L;
		NYNI = J;
		IND = I;
	}
	MYY[NYY] = NYNI;

	/*
		Пересчет диагональных элементов матрицы 'DYP'

	*/
	YKK = COMPLEX(1.0, 0.0) / DYP[NYNI];
	I = IH[NYNI];

L60:
	L = NYPH[I];
	if (L != N1)
	{
		DYP[L] = DYP[L] - YPH[I] * YKK * YPH[I];
	}
	I = IYPH[I];
	if (I != 0) goto L60;

	/*
		Проверка на единственность инциденции
	*/
	I = IH[NYNI];
	L = IYPH[I];
	if (L == 0) goto L900;

	/*
		Формирование вспомогательных массивов фиксации связности
		для коррекции списков связности и недиагональных элементов:
	*/
	KYNS1 = 0;
L70:
	KYNS1 += 1;
	NYNS1[KYNS1] = NYPH[I];
	MYNS1[KYNS1] = YPH[I];
	I = IYPH[I];
	if (IYPH[I] != 0) goto L70;
	II = I;
	KYNS2 = KYNS1 - 1;
	if (KYNS2 == 0) goto L90;
	for (I = 1; I <= KYNS2; I++)
	{
		NYNS2[I] = NYNS1[I + 1];
		MYNS2[I] = MYNS1[I + 1];
	}
L90:
	KYNS2 += 1;
	NYNS2[KYNS2] = NYPH[II];
	MYNS2[KYNS2] = YPH[II];

	/*
		Расширение  списков  связности  в соответствии с увеличением связей
		ВНЕШНИЙ ЦИКЛ ПО УЗЛАМ ( - 1 ) , ДЛЯ КОТОРЫХ ДОБАВЛЯЮТСЯ НОВЫЕ СВЯЗИ
	*/
	for (IS1 = 1; IS1 <= KYNS1; IS1++)
	{
		NTY = NYNS1[IS1];
		MYP1 = MYNS1[IS1];

		/*
			ВНУТРЕННИЙ ЦИКЛ ПО ОСТАВШИМСЯ УЗЛАМ, С КОТОРЫМИ ПОЯВЛЯЮТСЯ НОВЫЕ СВЯЗИ
		*/
		for (JS2 = IS1; JS2 <= KYNS2; JS2++)
		{
			NTY2 = NYNS2[JS2];
			MYP2 = MYNS2[JS2];
			YKKM = YKK * MYP1 * MYP2;
			/*
						Проверка на наличие ветви, параллельной новой связи :
						Определение адреса и самого элемента матрицы YPH по NTY :
			*/
			I = IH[NTY];
			if (IYPH[I] == 0) goto L758;
		L755:
			if (I == 0) goto L756;
			L = NYPH[I];
			J = I;
			I = IYPH[I];
			if (L != NTY2) goto L755;

			/*
				Новой связи нет, т.к. существует ветвь параллельная ей:
			*/
			YPH[J] = YPH[J] - YKKM;
			/*
				Определение адреса и самого элемента матрицы YPH по NTY2:
			*/
			I = IH[NTY2];
		L78:
			if (I == 0) goto L756;
			L = NYPH[I];
			J = I;
			I = IYPH[I];
			if (L != NTY) goto L78;

			/*
				Новой связи нет, т.к. существует ветвь параллельная ей:
			*/
			YPH[J] = YPH[J] - YKKM;
			continue;
		L756:
		L758:

			/*
				Добавление пары новых связей:
			*/
			ISMAX = KNDE;
			ISMAXN = ISMAX + 2;
			if (ISMAXN >= NSMK)
			{
				sOutput.Format(_T("\nУвеличьте размерность производных списков связности более заданной - %5d\n\n"), NSMK);
				NFP2_File.WriteString(sOutput);
				theApp.STOP();
			}

			/*
				1. Добавление к узлу 'NTY' новой связи с узлом 'NTY2'
			*/
			KVIY[NTY] = KVIY[NTY] + 1;
			if (KSTEK <= 0) goto L765;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L766;
		L765:
			KNDE = KNDE + 1;
			INNDE = KNDE;
		L766:
			NYPH[INNDE] = NTY2;
			YPH[INNDE] = -YKKM;
			IYPH[INNDE] = 0;

			/*
				Определение адреса и заполнение указателя на новый узел:
			*/
			I = IH[NTY];
		L760://line 214
			J = I;
			I = IYPH[I];
			if (I != 0) goto L760;
			IYPH[J] = INNDE;

			/*
						2. Добавление к узлу 'NTY2' новой связи с узлом 'NTY'
			*/
			KVIY[NTY2] = KVIY[NTY2] + 1;
			if (KSTEK <= 0) goto L775;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L776;
		L775:
			KNDE = KNDE + 1;
			INNDE = KNDE;
		L776:
			NYPH[INNDE] = NTY;
			YPH[INNDE] = -YKKM;
			IYPH[INNDE] = 0;

			/*
				Определение адреса и заполнение указателя на новый узел:
			*/
			I = IH[NTY2];
		L770:
			J = I;
			I = IYPH[I];
			if (I != 0) goto L770;
			IYPH[J] = INNDE;
		}
	}
L900:
	/*
		Удаление узла 'NYNI' из списков связности:
	*/	if (KYN == IND) goto L550;
	MYPQ[IND] = MYPQ[KYN]; //Коррекция элемента массива индексов негенераторных узлов

L550:
	KYN = KYN - 1;
	/*
		Определение адреса и заполнение указателя на новый узел:
	*/
	II = IH[NYNI];
L780:
	JJ = II;
	II = IYPH[II];
	NTY = NYPH[JJ];
	if (NTY == N1) goto L610;
	KVIY[NTY] = KVIY[NTY] - 1;

	/*
		Формирование недиагональных элементов для исключаемого узла:
		1/ для обратной подстановки:
	*/
	YPH[JJ] = YPH[JJ] * YKK;
	/*
		2/ для прямой подстановки:
	*/
	ILL = IL[NTY];
	KNDEL = KNDEL + 1;
	if (KNDEL >= NSML)
	{
		sOutput.Format(_T("\nУвеличьте размерность дополнительных списков связности\n нижнего треугольника более заданной - %5d\n\n"), NSML);
		NFP2_File.WriteString(sOutput);
		theApp.STOP();//return
	}

	if (ILL != 0) goto L600;
	IL[NTY] = KNDEL;
	IYPL[KNDEL] = 0;
	NYPL[KNDEL] = NYNI;
	ISSH[KNDEL] = JJ;
	goto L610;
L600:
	JLL = ILL;
	ILL = IYPL[JLL];
	if (ILL != 0) goto L600;
	IYPL[JLL] = KNDEL;
	IYPL[KNDEL] = 0;
	NYPL[KNDEL] = NYNI;
	ISSH[KNDEL] = JJ;

	/*
		Схлопывание списков связности в цепочке узла с указателем JJ:
		Определение адреса и удаленение указателя на старый узел NYNI:

	*/
L610:
	I = IH[NTY];
	L = NYPH[I];
	if (L != NYNI) goto L785;
	I1 = IYPH[I];
	IH[NTY] = I1;
	goto L786;

	/*
		Схлопывание I-го элемента списков связности :
	*/
L785:
	J = I;
	I = IYPH[J];
	L = NYPH[I];
	if (L != NYNI) goto L785;
	I1 = IYPH[I];
	if (I1 != 0)
	{
		IYPH[J] = I1;
	}
	if (I1 == 0)
	{
		IYPH[J] = 0;
	}
L786:;
	if (KSTEK > MKSTEK)
	{
		sOutput.Format(_T("\n Увеличьте размерность стека дополнительных списков\n связности левого треугольника более заданной -%5d\n"), MKSTEK);
		NFP2_File.WriteString(sOutput);
		theApp.STOP();
	}
	KSTEK = KSTEK + 1;
	IYSTEK[KSTEK] = I;
	if (II != 0) goto L780;
	if (KYN > 0) goto L1000;
}

void DEE117(int &KY, int &NYNI, int &IND, int &KYN, BOOL entry_param) //CALL DEP117( KY,  NYNI, 1)
{
	/*
	C --------------------------------------------------------------------
	C            Манипуляции с эквивалентом по генераторным узлам.
	C
	C     МАССИВЫ:
	C     --------
	C     NYNS1(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ОДНОГО УЗЛА НОВОЙ СВ;
	C     NYNS2(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ДРУГОГО УЗЛА НОВОЙ СВ;
	C
	C     ПЕРЕМЕННЫЕ:
	C     ----------
	C     KYNS1     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ЕДИНИЦЫ;
	C     KYNS2     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ДВУХ;
	C     KYN       - КОЛИЧЕСТВО НАГРУЗОЧНЫХ УЗЛОВ ЭКВИВАЛЕНТА;
	C     NTY       - НОМЕР ТЕКУЩЕГО УЗЛА СЕТИ В ПОДСПИСКЕ СВЯЗНОСТИ
	C                 УЗЛА 'NYNI';
	C     NTY2      - ТО ЖЕ, НО ВО ВНУТРЕННЕМ ЦИКЛЕ;
	C     N12     - ИНДЕКС УКАЗАТЕЛЯ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	C     ISMAX     - ИНДЕКС СВЯЗНОСТИ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	C     NYNI      - НОМЕР УЗЛА СРЕДИ НАГРУЗОЧНЫХ С НАИМЕНЬШЕЙ
	C                 ИНЦИДЕНТНОСТЬЮ;
	C     IND       - ИНДЕКС ЭТОГО УЗЛА.
	C
	C -------------------------------------------------------------------
	*/

	int N12, KYNS1, KYNS2, I, KNOY, J, NKY, NNY, NMIN, L, II, IS1, NTY, JS2,
		NTY2, ISMAX, ISMAXN, INNDE, JJ, I1, KYMIN;
	COMPLEX YKK, MYP1, MYP2, YKKM;
	CString sOutput;

	if (entry_param) goto ENTRY_DEP117;
	//	ИНИЦИАЛИЗАЦИЯ СПИСКА НЕ / ГЕНЕРАТОРНЫХ УЗЛОВ И ИНДЕКСОВ УЗЛОВ :
	KOY = 0;
	KYMIN = 0;
	KNOY = 0;
	for (I = 1; I <= N; I++)
	{
		if (UOP[I] == 0) goto L310;
		KOY = KOY + 1;
		MYPU[KOY] = I;
		continue;
	L310:
		KNOY = KNOY + 1;
		MYPQ[KNOY] = I;
		MYPQ2[KNOY] = I;
	}
	IKOY = KOY;
	if (KOY == 0) goto L990;

	/*	ИНИЦИАЛИЗАЦИЯ МАССИВА КОЛИЧЕСТВА ВЕТВЕЙ ИНЦИДЕНТНЫХ УЗЛАМ:
		ЦИКЛ ПО УКАЗАТЕЛЯМ МАССИВА М3(NBU)
	*/
	N12 = N1 * 2;
	J = 0;
	for (I = 2; I <= N12; I += 2)
	{
		J = J + 1;
		NKY = M3[I];
		NNY = M3[I - 1];
		KVIY[J] = NKY - NNY + 1;
	}
	KY = N;
	KYN = KNOY;
	KSTEK = 0;

L1000:
	/*
		ПОИСК НОМЕРА И ИНДЕКСА УЗЛА С НАИМЕНЬШЕЙ ИНЦИДЕНТНОСТЬЮ
	*/

	NMIN = NSMM;
	for (I = 1; I <= KYN; I++)
	{
		J = MYPQ[I];
		L = KVIY[J];
		if (L >= NMIN) continue;
		NMIN = L;
		NYNI = J;
		IND = I;
	}
	/*
		Вход для определения текущего эквивалента
	*/

ENTRY_DEP117:

	/*
		Пересчет диагональных элементов оставшейся части матрицы 'DYPE':
	*/

	YKK = COMPLEX(1.0, 0.0) / DYPE[NYNI];
	I = IE[NYNI];
L60:
	L = NYPE[I];
	DYPE[L] = DYPE[L] - YPE[I] * YKK * YPE[I];
	//L65:;
	I = IYPE[I];
	if (I != 0) goto L60;

	/*
	Проверка на единственность инциденции узла NYNI
	*/
	I = IE[NYNI];
	L = IYPE[I];
	if (L == 0) goto L900;

	/*
		Формирование вспомогательных массивов фиксации связности
		для коррекции списков связности и недиагональных элементов:
	*/
	KYNS1 = 0;
L70_2:
	KYNS1 = KYNS1 + 1;
	NYNS1[KYNS1] = NYPE[I];
	MYNS1[KYNS1] = YPE[I];
	I = IYPE[I];
	if (IYPE[I] != 0) goto L70_2;
	II = I;
	KYNS2 = KYNS1 - 1;
	if (KYNS2 == 0) goto L90_1;
	for (I = 1; I <= KYNS2; I++)
	{
		NYNS2[I] = NYNS1[I + 1];
		MYNS2[I] = MYNS1[I + 1];
	}
L90_1:
	KYNS2 = KYNS2 + 1;
	NYNS2[KYNS2] = NYPE[II];
	MYNS2[KYNS2] = YPE[II];

	/*
		Расширение  списков  связности  в 	соответствии с увеличением связей
		ВНЕШНИЙ ЦИКЛ ПО УЗЛАМ(-1), ДЛЯ КОТОРЫХ ДОБАВЛЯЮТСЯ НОВЫЕ СВЯЗИ
	*/
	for (IS1 = 1; IS1 <= KYNS1; IS1++)
	{
		NTY = NYNS1[IS1];
		MYP1 = MYNS1[IS1];
		/*
			ВНУТРЕННИЙ ЦИКЛ ПО ОСТАВШИМСЯ УЗЛАМ, С КОТОРЫМИ ПОЯВЛЯЮТСЯ НОВЫЕ СВЯЗИ
		*/
		for (JS2 = IS1; JS2 <= KYNS2; JS2++)
		{
			NTY2 = NYNS2[JS2];
			MYP2 = MYNS2[JS2];
			YKKM = YKK * MYP1 * MYP2;

			/*
				Проверка на наличие ветви, параллельной новой связи:
				Определение адреса и самого элемента матрицы YPE по NTY:
			*/
			I = IE[NTY];
			if (IYPE[I] == 0) goto L758;
		L755:
			if (I == 0) goto L756;
			L = NYPE[I];
			J = I;
			I = IYPE[I];
			if (L != NTY2) goto L755;

			/*
				Новой связи нет, т.к. существует ветвь параллельная ей:
			*/
			YPE[J] = YPE[J] - YKKM;

			/*
				Определение адреса и самого элемента матрицы YPE по NTY2:
			*/
			I = IE[NTY2];
		L78:
			if (I == 0) goto L756;
			L = NYPE[I];
			J = I;
			I = IYPE[I];
			if (L != NTY) goto L78;
			/*
				Новой связи нет, т.к. существует ветвь параллельная ей:
			*/
			YPE[J] = YPE[J] - YKKM;
			continue;
		L756:;
		L758:;
			if (abs(YKKM) <= YMIN) continue;

			/*
				Добавление пары новых связей:
			*/
			ISMAX = KNDEE;
			ISMAXN = ISMAX + 2;
			if (ISMAXN >= NSMM)
			{
				sOutput.Format(_T("\n Увеличьте размерность списков связности эквивалента более заданной - %5d\n\n"), NSMM);
				NFP_File.WriteString(sOutput);
				theApp.STOP();//return
			}

			/*
				1. Добавление к узлу 'NTY' новой связи с узлом 'NTY2'
			*/
			KVIY[NTY] = KVIY[NTY] + 1;
			if (KSTEK <= 0) goto L765;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L766;
		L765:
			KNDEE = KNDEE + 1;
			INNDE = KNDEE;
		L766:
			NYPE[INNDE] = NTY2;
			YPE[INNDE] = -YKKM;
			IYPE[INNDE] = 0;
			/*
				Определение адреса и заполнение указателя на новый узел:
			*/
			I = IE[NTY];
		L760:
			J = I;
			I = IYPE[I];
			if (I != 0) goto L760;
			IYPE[J] = INNDE;

			/*
				2. Добавление к узлу 'NTY2' новой связи с узлом 'NTY'
			*/
			KVIY[NTY2] = KVIY[NTY2] + 1;
			if (KSTEK <= 0) goto L775;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L776;
		L775:
			KNDEE = KNDEE + 1;
			INNDE = KNDEE;
		L776:
			NYPE[INNDE] = NTY;
			YPE[INNDE] = -YKKM;
			IYPE[INNDE] = 0;

			I = IE[NTY2];
		L770://line 260
			J = I;
			I = IYPE[I];
			if (I != 0) goto L770;
			IYPE[J] = INNDE;
		}
	}
L900:

	/*
		Удаление узла 'NYNI' из списков связности:
	*/
	KY = KY - 1;
	if (KYN == IND) goto L550;
	MYPQ[IND] = MYPQ[KYN];//Коррекция элемента массива индексов негенераторных узлов
L550:
	KYN = KYN - 1;
	/*
		Определение адреса и заполнение указателя на новый узел:
	*/
	II = IE[NYNI];
L780:
	JJ = II;
	II = IYPE[II];
	NTY = NYPE[JJ];
	KVIY[NTY] = KVIY[NTY] - 1;

	/*
		Схлопывание списков связности в цепочке узла с указателем JJ:
		Определение адреса и удаленение указателя на старый узел NYNI:
	*/
	I = IE[NTY];
	L = NYPE[I];
	if (L != NYNI) goto L785;
	I1 = IYPE[I];
	IE[NTY] = I1;
	goto L786;

	/*
		Схлопывание I-го элемента списков связности :
	*/
L785:
	J = I;
	I = IYPE[J];
	L = NYPE[I];
	if (L != NYNI) goto L785;
	I1 = IYPE[I];
	if (I1 != 0)
	{
		IYPE[J] = I1;
	}
	else IYPE[J] = 0;
L786:
	if (KSTEK > MKSTEK)
	{
		sOutput.Format(_T("\n Увеличьте размерность стека дополнительных списков\n связности эквивалента более заданной - %5d \n\n"), MKSTEK);
		NFP2_File.WriteString(sOutput);
		theApp.STOP();
	}

	KSTEK = KSTEK + 1;
	IYSTEK[KSTEK] = I;
	if (II != 0) goto L780;
	if (KYN > 0) goto L1000;
L990:;
}

void PRE015()
{
	/*
		C----------------------------------------------------------------------
		C     Предварительный расчет баланса мощности по системе вцелом
		C----------------------------------------------------------------------
	*/
	double Q0(0.0), P0(0.0), QBAL, PBAL;
	CString sOutput;

	QBAL = QGENS - QGENL - QPOTR + QKYSUM;

	sOutput.Format(_T("\
%s\nПредварительный расчет баланса реактивной мощности системы:\n\
%sГенерация реактивной мощности ............%10.3lf МВар\n\
%sРеактивная мощность компенсирующих устр...%10.3lf МВар\n\
%sРеактивные мощности линий и потерь тр. ...%10.3lf МВар\n\
%sПотребление реактивной мощности ..........%10.3lf МВар\n\
%sПотери реактивной мощности ...............%10.3lf МВар\n\
%sНебаланс реактивной мощности .............%10.3lf МВар\n"),
CString(_T(' '), 10), CString(_T(' '), 10),
QGENS, CString(_T(' '), 10), QKYSUM, CString(_T(' '), 10), -QGENL, CString(_T(' '), 10), QPOTR, CString(_T(' '), 10), Q0, CString(_T(' '), 10), QBAL);
	NFP2_File.WriteString(sOutput);

	PBAL = PGENS - PPOTR;
	sOutput.Format(_T("\
%s\nПредварительный расчет баланса активной мощности системы:\n\
%sГенерация активной мощности ..............%10.3lf МВар\n\
%sПотребление активной мощности ............%10.3lf МВар\n\
%sПотери активной мощности .................%10.3lf МВар\n\
%sНебаланс активной мощности ...............%10.3lf МВар\n"),
CString(_T(' '), 10), CString(_T(' '), 10), PGENS, CString(_T(' '), 10),
PPOTR, CString(_T(' '), 10), P0, CString(_T(' '), 10), PBAL);

	NFP2_File.WriteString(sOutput);
}

void CLF210()
{
	/*
		C ======================================================================
		C    ИTEPAЦИOHHЫЙ PACЧET УЗЛOBOГO УPABHEHИЯ C OПOPHЫMИ ПO HAПPЯЖEHИЮ
		C                              УЗЛAMИ:        04-14-93 06:35pm
		C ======================================================================
	*/
	int N12, NQ, FPQ, FEND, I, II, J, JJ, JJ2, JJ3, IJ, L, KSTI, KYPQ3, NYMRQ, KNYS, KIQ(0), NYNI, KY, KYN, IND, NYMRN;
	double EPS(0.0000001), KOEF, DELTMQ, QDELTQ, DELTQ, QX2, DELTM, DELTU, NEBU, NEBS;
	CString sOutput;
	COMPLEX ZERO(0.0, 0.0), IYJ, UK, UK1, UDI;

	KOEF = EPS;
	N12 = N1 * 2;
	sOutput.Format(_T(" -\n\n\n\n\n\n"));
	NFP4_File.WriteString(sOutput);
	sOutput.Format(_T("-\n"));
	NFP3_File.WriteString(sOutput);
	KI = 0;
	NQ = 1;
	FPQ = 0;
	FEND = 0;

	if (IKOY == 0)
	{
		theApp.WriteToLog(_T("Расчет установившегося режима по P-Q узлам:\n"));
		sOutput.Format(_T("\n  Расчет установившегося режима по P-Q узлам:\n\n"));
		NFP2_File.WriteString(sOutput);
	}
	if (IKOY > 0)
	{
		theApp.WriteToLog(_T("  Расчет установившегося режима по P-Q и P-U узлам:\n"));
		sOutput.Format(_T("\n  Расчет установившегося режима по P-Q и P-U узлам:\n\n"));
		NFP2_File.WriteString(sOutput);
	}
	/*
		1. Инициализация мощностей, напряжений, задающих токов.
	*/
	if (IKOY != 0) goto L50;
	KNY = N;
	FPQ = 1;
	/*
		1.1. Инициализация узловых параметров по всем узлам (типа P-Q )
	*/
	for (I = 1; I <= N; I++)
	{
		PZ[I] = SG[I].real() - SN[I].real();
		QZ[I] = SG[I].imag() - SN[I].imag();
		PY[I] = PZ[I];
		QY[I] = QZ[I] - QL[I] + QKY[I];
		U[I] = VSN[NSTN[I]];
		IY[I] = COMPLEX(PY[I], -QY[I]) * KSQR / conj(U[I]) - IYD[I];
	}
	goto L90_2;

L50:
	/*
		1.2.1. Инициализация узловых параметров по узлам типа P-Q
	*/
	if (KNY != 0)
	{
		for (II = 1; II <= KNY; II++)
		{
			I = MYPQ2[II];
			PZ[I] = SG[I].real() - SN[I].real();
			QZ[I] = SG[I].imag() - SN[I].imag();
			PY[I] = PZ[I];
			QY[I] = QZ[I] - QL[I] + QKY[I];
			if (KI == 0)
			{
				U[I] = VSN[NSTN[I]];
			}
			IY[I] = COMPLEX(PY[I], -QY[I]) * KSQR / conj(U[I]) - IYD[I];
		}
	}

	if (FPQ != 1)
	{
		sOutput.Format(_T("\n%s ═══════   Старт   ═══════\n\n\n\n"), CString(_T(' '), 24));
		NFP3_File.WriteString(sOutput);
	}

	/*
		1.2.2. Инициализация узловых параметров по узлам типа P-U: мощностей, напряжений и задающих токов
		Печать исходных условий распределения реактивных мощностей с расчетом отклонений напряжений в узлах
		типа P - U, ликвидируемых выбором имеющихся реактивных мощностей в этих узлах.
	*/
	KSTI = IKOY;
	KYPQ3 = 0;
	KIQ = 0;

	if (NQ > 0 && KODPQG >= 1)
	{
		sOutput.Format(_T(" Для генераторных узлов определяются реактивные мощности:\n"));
		NFP9_File.WriteString(sOutput);
	}
	if (KODPQG != 0)
	{
		sOutput.Format(_T("┌%s┐\n\
│     │Пределы по Q, MBар│   Hапряжение, кB   │  Oпорное   │Требуемое падение │\n\
│Hомер│──────────────────│────────────────────│  напря-    │напряжения от гене│\n\
│узла │ Hижний   Bерхний │   Составляющие:    │  жение,    │раторных узлов, кB│\n\
│     │                  │ Продольн. Поперечн.│  кB        │ Продол. Поперечн.│\n\
├%s┤\n"), CString(_T('-'), 77), CString(_T('-'), 77));

		NFP9_File.WriteString(sOutput);
	}

	/*
		1.2.2.1. Определение активных мощностей, напряжений,
		корректируемых векторов опорных напряжений,
		задающих токов P - U узлов от собственных
		активных мощностей.
	*/

	for (I = 1; I <= IKOY; I++)
	{
		II = MYPU[I];
		IYPU[II] = I;
		PZ[II] = SG[II].real() - SN[II].real();
		PY[II] = PZ[II];
		if (KI == 0)
		{
			U[II] = UOP[II];
		}
		UOPOR[I] = U[II];
		QDELX2[I] = 0.0;
	}

	RML202();

	/*
		1.2.2.2. Определение полных и реактивных мощностей, напряжений,
		отклонений от корректируемых векторов опорных напряжений.
	*/
	if (IKOY == 0) goto L421;
	for (J = 1; J <= IKOY; J++)
	{
		JJ = MYPU[J];
		MYNS3[JJ] = ZERO;
	}
L421:
	if (KNY == 0) goto L422;
	for (J = 1; J <= KNY; J++)
	{
		JJ = MYPQ2[J];
		MYNS3[JJ] = IY[JJ];
	}
L422:
	SYS013(MYNS3, MYNS2); //1

	for (I = 1; I <= IKOY; I++)
	{
		II = MYPU[I];
		UOT[I] = UOPOR[I] - MYNS2[II] * SQR3;
		QZ[II] = -SN[II].imag();
		QY[II] = QZ[II] - QL[II] + QKY[II];
		if (KODPQG == 0) goto L426;
		sOutput.Format(_T("│%4d│%8.1lf %8.1lf │%8.2lf %8.2lf │%9.2lf   │%7.2lf  %7.2lf  │\n"),
			II, QMN[II], QMX[II], U[II].real(), U[II].imag(), abs(UOPOR[I]),
			UOT[I].real(), UOT[I].imag());
		NFP9_File.WriteString(sOutput);
	}

L426:
	if (KODPQG == 0) goto L395;
	sOutput.Format(_T("└%s┘\n"), CString(_T('─'), 77));
	NFP9_File.WriteString(sOutput);
L395://line 194
	KIQ += 1;
	if (KNY == N)goto L140;
	DELTMQ = 0.0;
	NYMRQ = 0;

	/*
		2. Расчет итерации по определению узловых параметров режима:
		2.1.1. Расчет требуемых реактивных мощностей P-U узлов.
	*/
	if (KODPQG != 0)
	{
		sOutput.Format(_T("\n Итерация %3d\n┌%s┐\n\
│      │Пределы по Q, MBAр│ Требуемая │  Oпорное  │  Модуль  Угол    │\n\
│Hомер │──────────────────│ реактивная│  напря-   │  напpя-  напpя-  │\n\
│узла  │ Hижний   Bерхний │ мощность, │  жение,   │  жения,  жения,  │\n\
│      │                  │   МВАр    │    кB     │  кВ      гpад    │\n\
├%s┤\n"), KI, CString(_T('-'), 67), CString(_T('-'), 67));

		NFP9_File.WriteString(sOutput);
	}

	for (II = 1; II <= KSTI; II++)
	{
		IYJ = COMPLEX(0.0, 0.0);
		I = MYPU[II];
		IJ = IYPU[I];
		IYJ = IYJ + DYPE[I] * UOT[IJ];
		JJ = IE[I];

		if (JJ == 0) goto L455;
	L450:
		JJ2 = JJ;
		JJ = IYPE[JJ2];
		JJ3 = NYPE[JJ2];
		if (JJ3 == N1) goto L454;
		IJ = IYPU[JJ3];
		IYJ = IYJ + YPE[JJ2] * UOT[IJ];
	L454:
		if (JJ != 0)goto L450;
	L455:
		/*
			Требуемый задающий ток узла I(II)
		*/
		IY[I] = KSQR*IYJ;
		QDELTQ = -((conj(U[I])*(IY[I] + IYD[I])*SQR3).imag()) + QL[I] + SN[I].imag() - QKY[I];
		DELTQ = abs(QDELTQ - QDELX2[II]);
		QDELT[II] = DELTQ;
		if (DELTMQ < DELTQ)
		{
			DELTMQ = DELTQ;
			NYMRQ = I;
		}
		QDELX2[II] = QDELTQ;
		if (KODPQG != 0)
		{
			sOutput.Format(_T("│%4d  │%8.1lf %8.1lf │ %8.2lf   │%9.2lf  │%8.2lf %8.2lf │\n"),
				I, QMN[I], QMX[I], QDELX2[II], abs(UOPOR[II]), abs(U[I]),
				atan(U[I].imag() / U[I].real())*57.3);
			NFP9_File.WriteString(sOutput);
		}
	}
	if (KODPQG != 0)
	{
		sOutput.Format(_T("└%s┘\n"), CString(_T('-'), 67));
		NFP9_File.WriteString(sOutput);
	}
	/*
		2.1.2. Перевод части P-U узлов в разряд P-Q
	*/
	KNYS = KNY;
	NQ = 0;
L460:
	if (NQ >= KSTI) goto L140;
	if (KI <= NMZK)goto L140;
	NQ = NQ + 1;
L465:
	I = MYPU[NQ];
	if (QDELX2[NQ] <= 0.0) goto L415;
	if (QDELX2[NQ] <= QMX[I]) goto L460;
	if (QDELT[NQ] > DELQU) goto L460;
	QX2 = QMX[I];
	goto L459;
L415:
	if (QDELX2[NQ] >= QMN[I]) goto L460;
	if (QDELT[NQ] > DELQU) goto L460;
	QX2 = QMN[I];
L459:
	NYNI = MYPU[NQ];
	sOutput.Format(_T(" Узел %5d типа P-U с номеpом %5d стал узлом типа P-Q\n"), NYNI, NAME[NYNI]);
	NFP9_File.WriteString(sOutput);
	L = NQ;
L610:
	if (L == KSTI) goto L620;

	MYPU[L] = MYPU[L + 1];
	IYPU[MYPU[L]] = L;
	QDELX2[L] = QDELX2[L + 1];
	UOPOR[L] = UOPOR[L + 1];
	QDELT[L] = QDELT[L + 1];
	L = L + 1;
	goto L610;
L620:
	KSTI = KSTI - 1;
	KY = KSTI + 1;
	KYN = 1;
	IND = 1;

	/*
		2.1.2.1. Расчет нового эквивалента сети
	*/
	DEE117(KY, NYNI, IND, KYN, true);

	KNY = KNY + 1;
	MYPQ2[KNY] = I;
	QY[I] = QY[I] + QX2;
	SG[I] = SG[I] + COMPLEX(0.0, QX2);
	QZ[I] = QZ[I] + QX2;
	if (NQ <= KSTI) goto L465;

L90_2:
L140:

	/*
		2.2.Определение узловых падений и самих напряжений.
	*/
	KI = KI + 1;
	sOutput.Format(_T(" Итерация %3d\n\n"), KI);
	theApp.WriteToLog(sOutput);
	if (FPQ == 1) goto L318;
	if (KNY != N)
	{
		sOutput.Format(_T(" Число новых нагрузочных узлов %5d\n"), KNY - KNYS);
		NFP3_File.WriteString(sOutput);
	}
	else
	{
		sOutput.Format(_T(" Все узлы - нагрузочные типа P-Q.\n"));
		NFP3_File.WriteString(sOutput);
	}
	if (KI > 1) goto L155;
L318:
	DELTM = 0.0;
	if (FPQ == 1)
	{
		sOutput.Format(_T("\n%s═════  Итерация %3d  ═════\n\n"), CString(_T(' '), 26), KI);
		NFP3_File.WriteString(sOutput);
	}
	NYMRN = 0;
	if (FPQ == 0)
	{
		/*
			2.2.1. Пересчет задающих токов P-U узлов.
		*/
		for (II = 1; II <= KSTI; II++)
		{
			I = MYPU[II];
			IY[I] = COMPLEX(PY[I], -(QY[I] + QDELX2[II])) * KSQR / conj(U[I]) - IYD[I];
		}
	}

	SYS013(IY, MYNS2); //2

/*
	2.2.2. Пересчет напpяжений
*/
	for (I = 1; I <= N; I++)
	{
		UK = MYNS2[I] * SQR3;
		UK1 = U[I];
		U[I] = UK;
		DELTU = abs(abs(UK) - abs(UK1));
		DELTU = DELTU / UY[I];
		if (DELTU <= DELTM)
		{
			continue;
		}
		DELTM = DELTU;
		NYMRN = I;
	}

	RML202();
	if (FPQ == 1) goto L316;
L155:
	/*
		2.3. Расчет токов по ветвям сети
	*/
	RT015();
	/*
		Расчет мощностей по ветвям сети
	*/
	RMV118();

	/*
		Расчет напряжений по небалансам мощностей
	*/
	ZKM210(KSTI, NEBS);

	if (KI > KIMAX) return;
	if ((DELPM > DELU2) || (DELQM > DELU2) || (DELTMQ > DELU2)) goto L247;
	FEND = 1;
	goto L142;
L247:
	/*
		2.6. Расчет корректирующих падений напряжений и самих напряжений по небалансу мощностей
	*/
	sOutput.Format(_T("\n%s═════  Итерация %3d  ═════\n\n\n"), CString(_T(' '), 26), KI);
	NFP3_File.WriteString(sOutput);
	DELTM = 0.0;
	NYMRN = 0;

	SYS013(IY, MYNS2);//3  
	for (I = 1; I <= N; I++)
	{
		UDI = MYNS2[I] * SQR3;
		U[I] = U[I] + UDI;
		DELTU = abs(UDI / UY[I]);
		if (DELTU <= DELTM) continue;
		DELTM = DELTU;
		NYMRN = I;
	}

	RML202();

	/*
		2.7. Пересчет задающих токов.
	*/
	if (FPQ == 0) goto L314;
	/*
		2.7.0. Пересчет задающих токов всех узлов ( типа P-Q ).
	*/
L316:
	for (I = 1; I <= N; I++)
	{
		QY[I] = QZ[I] - QL[I] + QKY[I];
		IY[I] = COMPLEX(PY[I], -QY[I]) * KSQR / conj(U[I]) - IYD[I];
	}

	sOutput.Format(_T("%s Итерация %3d, наибольшая разница напряжений: %10.6lf о.е. в узле %4d\n"), CString(_T(' '), 5), KI, DELTM, NYMRN);
	NFP4_File.WriteString(sOutput);

	sOutput.Format(_T("%s  Наибольшая разница напряжений: %10.6lf о.е. в узле %4d%s\n"), CString(_T(' '), 11), DELTM, NYMRN, CString(_T('\n'), 15));
	NFP3_File.WriteString(sOutput);

	sOutput.Format(_T("  Наибольшая разница напряжений: %10.6lf о.е. в узле %4d\n"), DELTM, NYMRN);
	theApp.WriteToLog(sOutput);

	goto L327;

L314:
	/*
		2.7.1.Пересчет задающих токов P - Q узлов.
	*/
	for (J = 1; J <= KNY; J++)
	{
		I = MYPQ2[J];
		QY[I] = QZ[I] - QL[I] + QKY[I];
		IY[I] = COMPLEX(PY[I], -QY[I]) * KSQR / conj(U[I]) - IYD[I];
	}
	if (KNY == N) goto L326;
	/*
		2.7.2. Пересчет заданных напряжений P-U узлов.
	*/
	NEBU = 0.0;
	for (II = 1; II <= KSTI; II++)
	{
		I = MYPU[II];
		UOPOR[II] = U[I] * UOP[I] / abs(U[I]);
		NEBU = NEBU + pow((UOP[I] - abs(U[I])), 2);
	}
L326:
	sOutput.Format(_T(" Итерация, %3d небаланс по U: %8.4lf о.е., по Q %8.2lf MBAp, по P %8.2lf МВт,\n\
кв. невязки по S: %12.0lf МВА**2 по U %12.0lf В**2\n"), KI, DELTM, DELQM, DELPM, NEBS, NEBU);
	NFP4_File.WriteString(sOutput);

	sOutput.Format(_T("  небаланс по U: %8.4lf о.е., по Q %8.2lf МВАр, по P %8.2lf МВт,\n\
кв. невязки по S: %12.0lf МВА**2  по U %12.0lf В**2\n"), DELTM, DELQM, DELPM, NEBS, NEBU);
	theApp.WriteToLog(sOutput);

	sOutput.Format(_T(" Наибольшая разница напряжений: %10.6lf о.е. в узле %4d\n"), DELTM, NYMRN);
	NFP3_File.WriteString(sOutput);

	if (KODPI == 1) goto L142;
	if (DELTM > DELU1) goto L141;
	FEND = 1;
	goto L142;
L327:
	if (KODPI == 1) goto L142;
	if (DELTM > DELU1) goto L141;
	/*
		Печать параметров режима по всем узлам сети.
	*/

L142:
	sOutput.Format(_T("%s\n"), CString(_T('-'), 79));
	NFP7_File.WriteString(sOutput);

	if (FPQ == 0)
	{
		sOutput.Format(_T("\
      │ Наз-│Mощность, MBT/MBAP│Небаланс тока,кA│  Mодуль  │   Угол   \n\
 Hомер│ ва- │──────────────────│────────────────│  напря-  │  напря-  \n\
 узла │ ние │ Aктив- Pеак.без Q│ Составляющие:  │  жения,  │  жения,  \n\
      │     │ ная    P-U узлов │действит. мнимая│  кB      │  гpад    \n%s\n"), CString(_T('-'), 79));
	}
	else if (FPQ == 1)
	{
		sOutput.Format(_T("\
      │ Наз-│Mощность, MBT/MBAP│    Ток, кA     │  Mодуль  │   Угол   \n\
 Hомер│ ва- │──────────────────│────────────────│  напря-  │  напря-  \n\
 узла │ ние │ Aктив-   Pеакти- │ Составляющие:  │  жения,  │  жения,  \n\
      │     │ ная      вная    │действит. мнимая│  кB      │  гpад    \n%s\n"), CString(_T('-'), 79));
	}
	NFP7_File.WriteString(sOutput);

	for (I = 1; I <= N; I++)
	{
		sOutput.Format(_T(" %4d │%4d │%7.1lf  %7.1lf  │%7.3lf %7.3lf │%8.2lf  │%8.2lf  \n"),
			I, NAME[I], PY[I], QY[I], IY[I].real(), IY[I].imag(), abs(U[I]), atan((U[I].imag()) / (U[I]).real())*57.3);
		NFP7_File.WriteString(sOutput);
	}

L141:
	if (FPQ == 1) goto L970;
	/*
		2.8. Коррекция отклонений напряжений от опорных, диапазонов реактивных мощностей во всех текущих узлах типа P - U
	*/
	sOutput.Format(_T(" Наибольшая разница по искомой Q: %10.3lf МВАр в узле %4d\n"), DELTMQ, NYMRQ);
	NFP3_File.WriteString(sOutput);
	if (FEND == 1) goto L980;
	if (KNY == N) return;
	if (DELTMQ > DELU2) goto L982;
	if (DELTMQ <= DELU2) goto L982;
L982:
	if (KSTI != 0)
	{
		for (J = 1; J <= KSTI; J++)
		{
			JJ = MYPU[J];
			MYNS3[JJ] = ZERO;
		}
	}
	if (KNY != 0)
	{
		for (J = 1; J <= KNY; J++)
		{
			JJ = MYPQ2[J];
			MYNS3[JJ] = IY[JJ];
		}
	}

	SYS013(MYNS3, MYNS2);//4

	for (I = 1; I <= KSTI; I++)
	{
		II = MYPU[I];
		UOT[I] = UOPOR[I] - MYNS2[II] * SQR3;
		if (KODPQG == 0) continue;
	}
L980:
	if (KI > KIMAX) goto L985;
	if (FEND == 0) goto L395;
L985:
	/*
		3. Завершение расчета по параметрам узлов типа P - U
	*/
	if (KNY == N) return;
	for (II = 1; II <= KSTI; II++)
	{
		I = MYPU[II];
		QX2 = QDELX2[II];
		QY[I] = QY[I] + QX2;
		SG[I] = SG[I] + COMPLEX(0.0, QX2);
		QZ[I] = QZ[I] + QX2;
	}
	return;
L970:
	if (KI > KIMAX) return;
	if (DELTM > DELU1) goto L140;
}

void RML202()
{
	/*
		SUBROUTINE RML202 ( M1, M2, QL, REPR, KT, MVK, U, QKY, SHUNT )
		C ======================================================================
		C    РАСЧЕТ РЕАКТИВНЫХ МОЩНОСТЕЙ УЗЛОВ, ТРАКТУЕМЫХ КАК НАГРУЗКИ УЗЛОВ,
		C    ОТ ЕМКОСТЕЙ ЛИНИЙ ПОТЕРЬ Х.Х. ТРАНСФОРМАТОРОВ НА ОБМОТКЕ ВЫСШЕГО
		C    НАПРЯЖЕНИЯ.
		C    РАСЧЕТ РЕАКТИВНЫХ МОЩНОСТЕЙ ШУНТОВ.
		C ======================================================================
	*/
	int I, NNY, NKY;
	double EPS(0.000001), KOEF, QLG;
	for (I = 1; I <= N1; I++)
	{
		QL[I] = 0.;
		QKY[I] = SHUNT[I] * (pow(abs(U[I]), 2));
	}

	for (I = 1; I <= M; I++)
	{
		if (MVK[I] == 0) continue;
		if (abs(REPR[I]) < EPS) continue;
		if (abs(KT[I]) >= EPS)
		{
			NKY = M2[I];

			NKY = theApp.vecEdges[I].m_End.m_Index;

			QLG = REPR[I] * (pow(abs(U[NKY]), 2));
			QL[NKY] = QL[NKY] + QLG;
			continue;
		}
		else
		{
			NNY = M1[I];
			NKY = M2[I];

			NNY = theApp.vecEdges[I].m_Begin.m_Index;
			NKY = theApp.vecEdges[I].m_End.m_Index;

			KOEF = REPR[I] / 2.;
			QLG = KOEF * (pow(abs(U[NNY]), 2));
			QL[NNY] = QL[NNY] + QLG;
			QLG = KOEF * (pow(abs(U[NKY]), 2));
			QL[NKY] = QL[NKY] + QLG;
		}
	}
}

void SYS013(COMPLEX PARAM1[], COMPLEX PARAM2[])
{
	/*
		SUBROUTINE SYS013 ( MYY, DYP, IL,IYPL,NYPL,ISSH, IH,IYPH,NYPH,YPH,
		*                    MYNS1, IY, U )
	*/
	int I, J, JJ, NTY, NYZ;
	COMPLEX LS, LS0(0.0, 0.0);
	/*
		Прямая подстановка
	*/
	I = 1;
	NTY = MYY[I];
	MYNS1[NTY] = PARAM1[NTY];

L100:
	I = I + 1;
	NTY = MYY[I];
	LS = LS0;
	J = IL[NTY];

L20:
	if (J == 0) goto L10;
	JJ = J;
	J = IYPL[JJ];
	LS = LS + MYNS1[NYPL[JJ]] * YPH[ISSH[JJ]];
	goto L20;
L10:
	MYNS1[NTY] = PARAM1[NTY] - LS;
	if (I < N) goto L100;

	/*
		Доопределение вспомогательного вектора 'MYNS1'
	*/
	for (I = 1; I <= N; I++)
		MYNS1[I] = MYNS1[I] / DYP[I];
	/*
		Обратная подстановка
	*/
	I = N;
	NTY = MYY[I];
	PARAM2[NTY] = MYNS1[NTY];

L200:

	I = I - 1;
	NTY = MYY[I];
	LS = LS0;
	J = IH[NTY];

L220:

	if (J == 0) goto L210;
	JJ = J;
	J = IYPH[JJ];
	NYZ = NYPH[JJ];
	if (NYZ == N1) goto L220;
	LS = LS + PARAM2[NYZ] * YPH[JJ];
	goto L220;

L210:
	PARAM2[NTY] = MYNS1[NTY] - LS;
	if (I > 1) goto L200;
}

void RT015()
{
	/*     SUBROUTINE RT015  ( IT, U, X, MVK, M1, M2, KT )
		C ---------------------------------------------------------------------
		C                CALCULATION OF BRANCH CURRENTS
		C ---------------------------------------------------------------------
	*/
	int I;
	COMPLEX IT0(0.0, 0.0), XI;

	for (I = 1; I <= M; I++)
	{
		if (MVK[I] == 0)
		{
			IT[I] = IT0;
			continue;
		}
		XI = X[I] * SQR3;
		if (KT[I] > EPSIL)
		{
			//IT[I] = (U[M1[I]] * KT[I] - U[M2[I]]) / XI;
			IT[I] = (U[theApp.vecEdges[I].m_Begin.m_Index] * KT[I] - U[theApp.vecEdges[I].m_End.m_Index]) / XI;
			continue;
		}
		//IT[I] = (U[M1[I]] - U[M2[I]]) / XI;
		IT[I] = (U[theApp.vecEdges[I].m_Begin.m_Index] - U[theApp.vecEdges[I].m_End.m_Index]) / XI;
	}
}

void PVP118()
{
	/*  SUBROUTINE PVP118 ( ADOP, IT, MVK, M1, M2, NAME1, NAME2, NFP5 )
		======================================================================
			  ПEЧATЬ  PEЗУЛЬTATOB  PACЧETA  PEЖИMA  ПO BETBЯM  CETИ
		======================================================================
	*/
	CString sOutput;
	double PEROT;
	int I;
	sOutput.Format(_T("%s\n\
 Hомер│ Cос-│   Узел   │  Имя узла │Предел │   Tок, кA     │  Перегрузка   \n\
 ветви│ тоя-│──────────│───────────│ тока, │───────────────│────────────── \n\
      │ ние │ нач. кон.│ нач.  кон.│  кA   │ Актив. Pеакт. │     о.е.      \n%s\n"), CString(_T('-'), 79), CString(_T('-'), 79));

	NFP5_File.WriteString(sOutput);

	for (I = 1; I <= M; I++)
	{
		PEROT = abs(IT[I]) / ADOP[I];
		sOutput.Format(_T(" %4d │%4d %4d %4d │ %4d %4d │%6.3lf │%7.3lf%7.3lf │ %6.2lf\n"),
			I, MVK[I], M1[I], M2[I], NAME1[I], NAME2[I], ADOP[I], IT[I].real(),
			IT[I].imag(), PEROT, CString(_T(' '), 5));
		NFP5_File.WriteString(sOutput);
	}
	sOutput.Format(_T("└%s┘\n"), CString(_T('-'), 73));
	NFP5_File.WriteString(sOutput);
}

void RMV118()
{
	int I, NNY, NKY;
	double EPS(0.00000001), REP, QLN, QLK, QTK, RSVSV, ASVSV;
	COMPLEX CNUL(0.0, 0.0), SVN, SVK, CIT, SVSV, SPOT0;
	CString str_Output;
	SPOT = CNUL;
	SPOTT = CNUL;
	if (KODPOT != 0)
	{
		str_Output.Format(_T("%s\n\
		%s         │        Узлы         │  Коэффи- │    Потери мощности     \n\
		%s   Номер │─────────────────────│   циент  │─────────────────────── \n\
		%s   ветви │  начала  │  конца   │  транс-  │ Активной, │Реактивной, \n\
		%s         │──────────│──────────│ формации │   МВт     │   МВАр     \n\
		%s         │ N  │ Имя │ N  │ Имя │          │           │            \n\
		%s"), CString(_T('-'), 79), CString(_T(' '), 6), CString(_T(' '), 6),
			CString(_T(' '), 6), CString(_T(' '), 6), CString(_T('-'), 79));

		NFP8_File.WriteString(str_Output);
	}
	for (I = 1; I <= M; I++)
	{
		NNY = M1[I];
		NKY = M2[I];

		NNY = theApp.vecEdges[I].m_Begin.m_Index;
		NKY = theApp.vecEdges[I].m_End.m_Index;

		if (MVK[I] == 1) goto L100_2;
		//L110:  //нет указателя на L110
		PVN[I] = 0.0;
		PVK[I] = 0.0;
		QVN[I] = 0.0;
		QVK[I] = 0.0;
		continue;
	L100_2:
		CIT = conj(IT[I]);
		REP = REPR[I];
		if (KT[I] < EPSIL) goto L130;
		SVK = SQR3 * U[NKY] * CIT;
		PVK[I] = SVK.real();
		QTK = REP * (pow(abs(U[NKY]), 2));
		QVK[I] = SVK.imag() - QTK;
		SVN = SQR3 * U[NNY] * CIT * KT[I];
		PVN[I] = SVN.real();
		QVN[I] = SVN.imag();
		goto L140;
	L130:
		if (abs(REP) > EPS) goto L120;
		SVN = SQR3 * U[NNY] * CIT;
		SVK = SQR3 * U[NKY] * CIT;
		PVN[I] = SVN.real();
		PVK[I] = SVK.real();
		QVN[I] = SVN.imag();
		QVK[I] = SVK.imag();
		goto L140;
	L120:
		QLN = REP * (pow(abs(U[NNY]), 2)) / 2.0;
		QLK = REP * (pow(abs(U[NKY]), 2)) / 2.0;
		SVN = SQR3 * U[NNY] * CIT;
		SVK = SQR3 * U[NKY] * CIT;
		PVN[I] = SVN.real();
		PVK[I] = SVK.real();
		QVN[I] = SVN.imag() + QLN;
		QVK[I] = SVK.imag() - QLK;
	L140:
		SVSV = SVN - SVK;
		RSVSV = abs(SVSV.real());
		ASVSV = abs(SVSV.imag());
		SPOT0 = COMPLEX(RSVSV, ASVSV);
		if (KODPOT == 1)
		{
			str_Output.Format(_T("%s    %4d │%4d %4d │%4d %4d │ %7.3lf  │  %7.3lf  │  %7.3lf   "), CString(_T(' '), 6), I, M1[I], NAME1[I], M2[I], NAME2[I], KT[I], SPOT0.real(), SPOT0.imag());
			NFP8_File.WriteString(str_Output);
		}
		SPOT = SPOT + SPOT0;
		if (KT[I] >= EPSIL) SPOTT = SPOTT + COMPLEX(RSVSV, ASVSV);
	}
	if (KI != 0) goto L500;
	/*
		ПЕЧАТЬ МОЩНОСТЕЙ ВЕТВЕЙ
	*/
	str_Output.Format(_T("%s\n"), CString(_T('-'), 79));
	NFP6_File.WriteString(str_Output);
	NFP6_File.WriteString(_T("            │        Узлы         │            Mощности ветвей               \n\
      Hомер │─────────────────────│───────────────────────────────────────── \n\
      ветви │  начала  │  конца   │      Активные      │     Реактивные      \n\
            │──────────│──────────│   начала,  конца,  │   начала,  конца,   \n\
            │ N  │ Имя │ N  │ Имя │   МВт      МВт     │   МВар     МВар     \n"));
	str_Output.Format(_T("%s\n"), CString(_T('-'), 79));
	NFP6_File.WriteString(str_Output);
	for (I = 1; I <= M; I++)
	{
		str_Output.Format(_T("     %5d  │%4d %4d │%4d %4d │%8.1lf  %8.1lf  │%8.2lf  %8.2lf   \n"), I, theApp.vecEdges[I].m_Begin.m_Index, NAME1[I], theApp.vecEdges[I].m_End.m_Index, NAME2[I], PVN[I], PVK[I], QVN[I], QVK[I]);
		NFP6_File.WriteString(str_Output);
	}
	str_Output.Format(_T("%s└%s┘"), CString(_T(' '), 4), CString(_T('-'), 71));
	NFP6_File.WriteString(str_Output);
L500:;
}

void ZKM210(int &KSTI, double &NEBS)
{
	/*
	======================================================================
	C  Расчет небалансов мощностей и токов в узлах сети   25-03-92 07:58pm
	C ======================================================================
	*/
	CString sOutput;
	double 	QET, QKYS = 0.0, PNEB, QNEB, DELP, DELQ, QNEBX2, QLN, QTK, QLK;
	int N12, IYPQ, I, J, NV2, NV1, NNV, NKV, NBP, _IYPU;
	COMPLEX SPOTL;
	if (KODPQG == 2)
	{
		sOutput.Format(_T("\n Небалансы мощностей в P-Q узлах:\n"));
		NFP3_File.WriteString(sOutput);
	}

	SNAG = COMPLEX(0.0, 0.0);
	SGEN = COMPLEX(0.0, 0.0);
	N12 = N1 * 2;
	NEBS = 0.0;
	QET = 0.0;
	QKYS = 0.0;
	DELPM = 0.001;
	DELQM = 0.001;
	NYNP = 0;
	NYNQ = 0;

	for (IYPQ = 1; IYPQ <= KNY; IYPQ++)
	{
		I = MYPQ2[IYPQ];
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QET = QET - QL[I];
		QKYS = QKYS + QKY[I];
		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		PNEB = -PZ[I];
		QNEB = -QZ[I] - QKY[I];

		for (J = NNV; J <= NKV; J++)
		{
			NBP = M3V[J];
			//if (M1[NBP] != I)
			if (theApp.vecEdges[NBP].m_Begin.m_Index != I)
			{
				PNEB = PNEB - PVK[NBP];
				QNEB = QNEB - QVK[NBP];
			}
			else
			{
				PNEB = PNEB + PVN[NBP];
				QNEB = QNEB + QVN[NBP];
			}
		}
		NEBS = NEBS + pow(PNEB, 2) + pow(QNEB, 2);
		IY[I] = -COMPLEX(PNEB, -QNEB) * KSQR / conj(U[I]);
		if (KODPQG == 2)
		{
			sOutput.Format(_T(" В узле %4d небаланс мощности: %10.2lf %10.2lf МВA, тока: %8.2lf %8.2lf кA\n"),
				I, PNEB, QNEB, IY[I].real(), IY[I].imag());
			NFP3_File.WriteString(sOutput);
		}
		DELP = abs(PNEB);
		if (DELP <= DELPM) goto L30_1;
		DELPM = DELP;
		PNEBM = PNEB;
		NYNP = I;
	L30_1:
		DELQ = abs(QNEB);
		if (DELQ > DELQM)
		{
			DELQM = DELQ;
			QNEBM = QNEB;
			NYNQ = I;
		}
	}

	if (KODPQG == 2)
	{
		sOutput.Format(_T("\n Небалансы мощностей в P-U узлах:\n"));
		NFP3_File.WriteString(sOutput);
	}
	QNEBX2 = 0.0;
	for (_IYPU = 1; _IYPU <= KSTI; _IYPU++)
	{
		I = MYPU[_IYPU];
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QET = QET - QL[I];
		QKYS = QKYS + QKY[I];
		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		PNEB = -PZ[I];
		QNEB = -QZ[I] - QDELX2[_IYPU] - QKY[I];
		QNEBX2 = QNEBX2 + QDELX2[_IYPU];
		for (J = NNV; J <= NKV; J++)
		{
			NBP = M3V[J];
			//if (M1[NBP] != I)
			if (theApp.vecEdges[NBP].m_Begin.m_Index != I)
			{
				PNEB = PNEB - PVK[NBP];
				QNEB = QNEB - QVK[NBP];
			}
			else
			{
				PNEB = PNEB + PVN[NBP];
				QNEB = QNEB + QVN[NBP];
			}
		}
		NEBS = NEBS + pow(PNEB, 2) + pow(QNEB, 2);
		IY[I] = -COMPLEX(PNEB, -QNEB) * KSQR / conj(U[I]);
		if (KODPQG == 2)
		{
			sOutput.Format(_T(" В узле %4d небаланс мощности: %10.2lf %10.2lf МВA, тока: %8.2lf %8.2lf кA\n"),
				I, PNEB, QNEB, IY[I].real(), IY[I].imag());
			NFP3_File.WriteString(sOutput);
		}
		DELP = abs(PNEB);
		if (DELP > DELPM)
		{
			DELPM = DELP;
			PNEBM = PNEB;
			NYNP = I;
		}

		DELQ = abs(QNEB);
		if (DELQ > DELQM)
		{
			DELQM = DELQ;
			QNEBM = QNEB;
			NYNQ = I;
		}
	}

	sOutput.Format(_T("\
 Максимальный небаланс активной мощности %10.2lf МВт в узле %5d\n\
 Максимальный небаланс реактивной мощности %10.2lf МВт в узле %5d\n\
\n"), PNEBM, NYNP, QNEBM, NYNQ);
	NFP3_File.WriteString(sOutput);

	N12 = N1 * 2;
	NV2 = N1 * 2;
	NV1 = NV2 - 1;
	NNV = M3[NV1] - N12;
	NKV = M3[NV2] - N12;
	PNEB = 0.0;
	QNEB = 0.0;

	for (J = NNV; J <= NKV; J++)
	{
		NBP = M3V[J];
		//if (M1[NBP] != NBU)
		if (theApp.vecEdges[NBP].m_Begin.m_Index != NBU)
		{
			PNEB = PNEB - PVK[NBP];
			if (KT[NBP] > EPSIL)
			{
				QTK = REPR[NBP] * (pow(abs(U[N1]), 2));
				QNEB = QNEB - QVK[NBP] - QTK;
				continue;
			}
			QLK = REPR[NBP] * (pow(abs(U[N1]), 2)) / 2.0;
			QNEB = QNEB - QVK[NBP] - QLK;
			continue;
		}
		else
		{
			PNEB = PNEB + PVN[NBP];
			if (KT[NBP] <= EPSIL)
			{
				QLN = REPR[NBP] * (pow(abs(U[N1]), 2)) / 2.0;
				QNEB = QNEB + QVN[NBP] - QLN;
				continue;
			}
			QNEB = QNEB + QVN[NBP];
		}
	}

	SPOTL = SPOT - SPOTT;

	sOutput.Format(_T("\
 Генерируемая мощность системы...................%10.1lf MВт %10.1lf MВар\n\
 Генерация реактивной мощности линий и потреб.тр.%s %10.1lf MВар\n\
 Генерация реактивной мощности компенсир. устр...%s %10.1lf MВар\n\
 Потребляемая мощность в системе................. %10.1lf MВт %10.1lf MВар\n\
 Потери мощности в системе ...................... %10.1lf MВт %10.1lf MВар\n\
 Потери мощности в линиях ..................... %10.1lf MВт %10.1lf MВар\n\
 Потери мощности в трансформаторах ............ %10.1lf MВт %10.1lf MВар\n\
 Мощность в балансирующем узле .................. %10.1lf MВт %10.1lf MВар\n"),
		(SGEN + COMPLEX(0., QNEBX2)).real(), (SGEN + COMPLEX(0., QNEBX2)).imag(),
		CString(_T(' '), 14), QET, CString(_T(' '), 14), QKYS, SNAG.real(),
		SNAG.imag(), SPOT.real(), SPOT.imag(), SPOTL.real(), SPOTL.imag(),
		SPOTT.real(), SPOTT.imag(), PNEB, QNEB);

	PNEB = PNEB + SGEN.real() - SNAG.real() - SPOT.real();
	QNEB = QNEB + SGEN.imag() - SNAG.imag() - SPOT.imag() + QET + QKYS;

	sOutput.AppendFormat(_T(" Небаланс мощности по системе................... %10.1lf MВт %10.1lf MВар\n"), PNEB, QNEB + QNEBX2);
	NFP3_File.WriteString(sOutput);
}

void PVREM()
{
	double TVR, RVR;

	CTime t = CTime::GetCurrentTime();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	TVR = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	RVR = TVR - TVREM;
	TVREM = TVR;
	//NVRPV = nvrpv;
}

void RTO015()
{
	int I;
	double A;
	CString sOutput;
	COMPLEX IT0(0.0, 0.0), XI;
	for (I = 1; I <= M; I++)
	{
		if (MVK[I] != 0) goto L100;
		IT[I] = IT0;
		continue;
	L100:
		XI = X[I] * SQR3;
		if (KT[I] <= EPSIL) goto L200;
		//IT[I] = (U[M1[I]] * KT[I] - U[M2[I]]) / XI;
		IT[I] = (U[theApp.vecEdges[I].m_Begin.m_Index] * KT[I] - U[theApp.vecEdges[I].m_End.m_Index]) / XI;
		continue;
	L200:
		//IT[I] = (U[M1[I]] - U[M2[I]]) / XI;
		IT[I] = (U[theApp.vecEdges[I].m_Begin.m_Index] - U[theApp.vecEdges[I].m_End.m_Index]) / XI;
		A = abs(IT[I]);
		if (A / ADOP[I] <= 1.) continue;
		KP = KP + 1;
		if (KP > KPMAX) continue;
		NPB[KP] = I;
	}
	if (KP > KPMAX)
	{
		sOutput.Format(_T(" Число перегрузок -%5d превышает заданное ограничение:%5d\n"), KP, KPMAX);
		NFP_File.WriteString(sOutput);
	}
}

void ZKS016()
{
	double DELP, DELQ, PNEB, QNEB, QET, QKYS;
	int NV1, NV2, NNV, NKV, N12, I, J, NBP;
	COMPLEX SPOTL;
	CString str_Output;

	SNAG = COMPLEX(0.0, 0.0);
	SGEN = COMPLEX(0.0, 0.0);
	N12 = N1 * 2;
	QET = 0.0;
	QKYS = 0.0;
	DELPM = 0.001;
	DELQM = 0.001;
	NYNP = 0;
	NYNQ = 0;
	for (I = 1; I <= N1; I++)
	{
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QET = QET - QL[I];
		QKYS = QKYS + QKY[I];
		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		PNEB = -PZ[I];
		QNEB = -QZ[I] - QKY[I];
		for (J = NNV; J <= NKV; J++)
		{
			NBP = M3V[J];
			//if(M1[NBP] == I) goto L20_2;
			if (theApp.vecEdges[NBP].m_Begin.m_Index == I) goto L20_2;
			PNEB = PNEB - PVK[NBP];
			QNEB = QNEB - QVK[NBP];
			continue;
		L20_2:
			PNEB = PNEB + PVN[NBP];
			QNEB = QNEB + QVN[NBP];
		}
		IY[I] = -COMPLEX(PNEB, -QNEB) * KSQR / conj(U[I]); //IY(I) = -CMPLX( PNEB,-QNEB ) * KSQR / CONJG(U(I))
		DELP = abs(PNEB);
		if (I == N1) continue;
		if (DELP <= DELPM) goto L30;
		DELPM = DELP;
		PNEBM = PNEB;
		NYNP = I;
	L30:
		DELQ = abs(QNEB);
		if (DELQ <= DELQM) continue;
		DELQM = DELQ;
		QNEBM = QNEB;
		NYNQ = I;
	}
	str_Output.Format(_T("\n\n\n\n Генерируемая мощность системы...................%10.1lf MВт%10.1lf MВар\n"), SGEN.real(), SGEN.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Генерация реактивной мощности линий и потреб.тр.%s%10.1lf MВар\n"), CString(_T(' '), 14), QET);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Генерация реактивной мощности компенсир. устр...%s%10.1lf MВар\n"), CString(_T(' '), 14), QKYS);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потребляемая мощность в системе.................%10.1lf MВт%10.1lf MВар\n"), SNAG.real(), SNAG.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потери мощности в системе ......................%10.1lf MВт%10.1lf MВар\n"), SPOT.real(), SPOT.imag());
	NFP10_File.WriteString(str_Output);
	SPOTL = SPOT - SPOTT;
	str_Output.Format(_T(" Потери мощности в линиях .....................%10.1lf MВт%10.1lf MВар\n"), SPOTL.real(), SPOTL.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потери мощности в трансформаторах ............%10.1lf MВт%10.1lf MВар\n"), SPOTT.real(), SPOTT.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Мощность в балансирующем узле ..................%10.1lf MВт%10.1lf MВар\n"), PNEB, QNEB);
	NFP10_File.WriteString(str_Output);
	PNEB = PNEB + SGEN.real() - SNAG.real() - SPOT.real();
	QNEB = QNEB + SGEN.imag() - SNAG.imag() - SPOT.imag() + QET + QKYS;
	str_Output.Format(_T(" Небаланс мощности по системе...................%10.1lf MВт%10.1lf MВар\n\n"), PNEB, QNEB);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Максимальный небаланс активной мощности  %10.2lf МВт  в узле%5d\n"), PNEBM, NYNP);
	NFP_File.WriteString(str_Output);
	str_Output.Format(_T(" Максимальный небаланс реактивной мощности%10.2lf МВар в узле%5d\n\n"), QNEBM, NYNQ);
	NFP_File.WriteString(str_Output);
	str_Output.Format(_T("\n%s Максимальный небаланс узловой мощности : \n%s активной -%10.2lf МВт, реактивной -%10.2lf МВар.\n"), CString(_T(' '), 17), CString(_T(' '), 10), DELPM, DELQM);
	NFP10_File.WriteString(str_Output);

}

bool DisplayChildren(MSXML2::IXMLDOMNodePtr pParent)
{
	DisplayChild(pParent);
	MSXML2::IXMLDOMNodePtr pChild;
	for (pChild = pParent->firstChild; pChild != NULL; pChild = pChild->nextSibling)
		if (!DisplayChildren(pChild)) return false;

	return true;
}

bool DisplayChild(MSXML2::IXMLDOMNodePtr pChild)
{
	MSXML2::IXMLDOMNodePtr	pNext, pNew;
	COMPLEX					SNI, SGI;
	CString					strAttrVal, sOutput, strElement = (LPCTSTR)pChild->nodeName;
	CVertex					vVertex, vVertexBegin, vVertexEnd;
	CEdge					eEdge;
	double					UOPI, QMNI, QMXI, QKYI, UYI;
	auto					iterVertex = theApp.vecVertexes.begin();
	int						iIsBalanse(0);
	int						NAMEI, J, J1, J2;

	/*
		Максимальное число узлов
	*/

	if (strElement == _T("MaxVertexCount")) NMAX = _wtoi((LPCTSTR)pChild->text);
	//Вершина
	else if (strElement == _T("vertexes"))
	{
		for (pNext = pChild->firstChild; pNext != NULL; pNext = pNext->nextSibling)
		{
			strElement = (LPCTSTR)pNext->nodeName;
			if (strElement == _T("vertex"))
			{
				N1++; //счётчик ветвей
				MSXML2::IXMLDOMNamedNodeMapPtr pXMLAttributeMap = pNext->Getattributes();
				if (pXMLAttributeMap)
				{
					MSXML2::IXMLDOMNodePtr pXMLEncodNodePlId = pXMLAttributeMap->getNamedItem(_T("index"));
					if (pXMLEncodNodePlId) vVertex.m_Index = _wtoi((CString)pXMLEncodNodePlId->GetnodeValue());
					pXMLEncodNodePlId = pXMLAttributeMap->getNamedItem(_T("balanse"));
					if (pXMLEncodNodePlId) iIsBalanse = _wtoi((CString)pXMLEncodNodePlId->GetnodeValue());
					if (iIsBalanse == 1) //если узел балансирующи (базисный), то читаем напряжение
					{
						vVertex.m_Data.m_IsBalanse = true;
						pXMLEncodNodePlId = pXMLAttributeMap->getNamedItem(_T("voltage"));
						if (pXMLEncodNodePlId)
						{
							strAttrVal = pXMLEncodNodePlId->GetnodeValue();
							InputNBU = NBU = vVertex.m_Index;
							U0 = _tstof(strAttrVal);
						}
					}
				}
				for (pNew = pNext->firstChild; pNew != NULL; pNew = pNew->nextSibling)
				{
					strElement = (LPCTSTR)pNew->nodeName;
					if (strElement == _T("ActiveLoadPower")) vVertex.m_Data.m_LoadPower.real(_tstof((LPCTSTR)pNew->text));
					else if (strElement == _T("ReactiveLoadPower")) vVertex.m_Data.m_LoadPower.imag(_tstof((LPCTSTR)pNew->text));
					else if (strElement == _T("ActiveGeneratePower")) vVertex.m_Data.m_GenPower.real(_tstof((LPCTSTR)pNew->text));
					else if (strElement == _T("ReactiveGeneratePower")) vVertex.m_Data.m_GenPower.imag(_tstof((LPCTSTR)pNew->text));
					else if (strElement == _T("Shunt")) vVertex.m_Data.m_Shunt = _tstof((LPCTSTR)pNew->text);
					else if (strElement == _T("SpecVoltage")) vVertex.m_Data.m_SpecVoltage = _tstof((LPCTSTR)pNew->text);
					else if (strElement == _T("NomVoltage")) vVertex.m_Data.m_NomVoltage = _tstof((LPCTSTR)pNew->text);
					else if (strElement == _T("ReactivePowerLowLimit")) vVertex.m_Data.m_ReactPowerLowLim = _tstof((LPCTSTR)pNew->text);
					else if (strElement == _T("ReactivePowerUpperLimit")) vVertex.m_Data.m_ReactPowerUpLim = _tstof((LPCTSTR)pNew->text);
				}
				theApp.vecVertexes.push_back(vVertex);
				//блок заполнения массивов (после обновления кода с векторами - удалить)
				//---------------------------------------------------------
				NAME[N1] = vVertex.m_Index;
				SHUNT[N1] = vVertex.m_Data.m_Shunt;
				SN[N1].real(vVertex.m_Data.m_LoadPower.real());
				SN[N1].imag(vVertex.m_Data.m_LoadPower.imag());
				SG[N1].real(vVertex.m_Data.m_GenPower.real());
				SG[N1].imag(vVertex.m_Data.m_GenPower.imag());
				UOP[N1] = vVertex.m_Data.m_SpecVoltage;
				UY[N1] = vVertex.m_Data.m_NomVoltage;
				QMN[N1] = vVertex.m_Data.m_ReactPowerLowLim;
				QMX[N1] = vVertex.m_Data.m_ReactPowerUpLim;
				if (SG[N1].real() > 0) KST++; //счётчик генерирующих узлов
											  //---------------------------------------------------------
				theApp.vecVertexeStage1.push_back(vVertex.m_Index);
			}
		}
		if (N1 > 0) N = N1 - 1;

		if (N1 != NBU)
		{
			NAMEI = NAME[N1];
			SNI = SN[N1];
			SGI = SG[N1];
			UYI = UY[N1];
			UOPI = UOP[N1];
			QMNI = QMN[N1];
			QMXI = QMX[N1];
			QKYI = QKY[N1];
			NAME[N1] = NAME[NBU];
			SN[N1] = SN[NBU];
			SG[N1] = SG[NBU];
			UY[N1] = UY[NBU];
			UOP[N1] = UOP[NBU];
			QMN[N1] = QMN[NBU];
			QMX[N1] = QMX[NBU];
			QKY[N1] = QKY[NBU];
			NAME[NBU] = NAMEI;
			SN[NBU] = SNI;
			SG[NBU] = SGI;
			UY[NBU] = UYI;
			UOP[NBU] = UOPI;
			QMN[NBU] = QMNI;
			QMX[NBU] = QMXI;
			QKY[NBU] = QKYI;
			NBU = N1;
		}

		for (int I = 1; I <= N1; I++)
		{
			J = NAME[I];
			if (J > MAXNOM)
			{
				sOutput.Format(_T("Максимальный номеp узла %d пpевышает заданный %d\n"), J, MAXNOM);
				NFP_File.WriteString(sOutput);
				AfxMessageBox(sOutput);
				//theApp.STOP();
				return false;
			}
			NOMERY[J] = I;
		}
		int vecVertexeStage1_size = theApp.vecVertexeStage1.size();
		for (int iInd = 0; iInd < vecVertexeStage1_size; iInd++)
		{
			theApp.vecVertexeStage2.push_back(NOMERY[iInd]);
		}
	}
	//Максимальное число ветвей
	else if (strElement == _T("MaxEdgeCount")) MMAX = _wtoi((LPCTSTR)pChild->text);
	//Ветвь
	else if (strElement == _T("edge"))
	{
		if (theApp.vecEdges.empty()) theApp.vecEdges.push_back(CEdge(0, CVertex(0, 0), CVertex(0, 0), 0));
		M++;
		MSXML2::IXMLDOMNamedNodeMapPtr pXMLAttributeMap = pChild->Getattributes();
		if (pXMLAttributeMap)
		{
			MSXML2::IXMLDOMNodePtr pXMLEncodNodePlId = pXMLAttributeMap->getNamedItem(_T("index"));
			if (pXMLEncodNodePlId) eEdge.m_Index = _wtoi((LPCTSTR)pXMLEncodNodePlId->text);
			pXMLEncodNodePlId = pXMLAttributeMap->getNamedItem(_T("type"));
			if (pXMLEncodNodePlId) eEdge.m_Data.m_bType = _wtoi((LPCTSTR)pXMLEncodNodePlId->text);
		}
		for (pNext = pChild->firstChild; pNext != NULL; pNext = pNext->nextSibling)
		{
			strElement = (LPCTSTR)pNext->nodeName;

			if (strElement == _T("begin")) eEdge.m_Begin.m_Index = _wtoi((LPCTSTR)pNext->text);
			else if (strElement == _T("end")) eEdge.m_End.m_Index = _wtoi((LPCTSTR)pNext->text);
			else if (strElement == _T("ActiveResistance")) eEdge.m_Data.m_Resist.real(_tstof((LPCTSTR)pNext->text));
			else if (strElement == _T("ReactiveResistance")) eEdge.m_Data.m_Resist.imag(_tstof((LPCTSTR)pNext->text));
			else if (strElement == _T("AllowableAmperage")) eEdge.m_Data.m_AmperAllow = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("Conduction")) eEdge.m_Data.m_Conduct = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("TransformCoeff")) eEdge.m_Data.m_KT = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("ParallelismMark")) eEdge.m_Data.m_ParallMark = _wtoi((LPCTSTR)pNext->text);
			else if (strElement == _T("State")) eEdge.m_Data.m_State = _wtoi((LPCTSTR)pNext->text);
		}
		//блок заполнения массивов (после обновления кода с векторами - удалить)
		//---------------------------------------------------------
		NAME1[M] = eEdge.m_Begin.m_Index;
		NAME2[M] = eEdge.m_End.m_Index;
		X[M].imag(eEdge.m_Data.m_Resist.real());
		X[M].real(eEdge.m_Data.m_Resist.imag());
		REPR[M] = eEdge.m_Data.m_Conduct;
		KT[M] = eEdge.m_Data.m_KT;
		PRIZ[M] = eEdge.m_Data.m_ParallMark;
		ADOP[M] = eEdge.m_Data.m_AmperAllow;
		MVK[M] = eEdge.m_Data.m_State;
		if (KT[M] > 0) KTR++; //счётчик трансформаторных ветвей
							  //---------------------------------------------------------
		theApp.vecEdgeStage1.push_back(eEdge);
		J1 = NAME1[M];
		J2 = NAME2[M];
		M1[M] = NOMERY[J1];
		M2[M] = NOMERY[J2];
		while (iterVertex != theApp.vecVertexes.end())
		{
			if (iterVertex->m_Index == M1[M]) eEdge.m_Begin = *iterVertex;
			else if (iterVertex->m_Index == M2[M]) eEdge.m_End = *iterVertex;
			++iterVertex;
		}
		theApp.vecEdges.push_back(eEdge);
	}
	//максимальный номер узла
	else if (strElement == _T("MaxVertexNum")) MAXNOM = _wtoi((LPCTSTR)pChild->text);
	//Максимальное число параллельных ветвей
	else if (strElement == _T("MaxParallelEdges")) MKPARV = _wtoi((LPCTSTR)pChild->text);
	//Максимальное число эквивалентный параллельных
	else if (strElement == _T("MaxEqParallelEdges")) MKEPV = _wtoi((LPCTSTR)pChild->text);
	//класс напряжения (ступень)
	else if (strElement == _T("level"))
	{
		KSN++;
		for (pNext = pChild->firstChild; pNext != NULL; pNext = pNext->nextSibling)
		{
			strElement = (LPCTSTR)pNext->nodeName;
			if (strElement == _T("LowLimit")) GNN[KSN] = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("NomVoltage")) VNN[KSN] = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("UpperLimit")) GNV[KSN] = _tstof((LPCTSTR)pNext->text);
			else if (strElement == _T("LevelVoltage")) VSN[KSN] = _tstof((LPCTSTR)pNext->text);
		}
	}
	//Размерности
	else if (strElement == _T("BasicConArDim")) NSM = _wtoi((LPCTSTR)pChild->text);
	else if (strElement == _T("DerivConArDim")) NSMK = _wtoi((LPCTSTR)pChild->text);
	else if (strElement == _T("AddConLowArDim")) NSML = _wtoi((LPCTSTR)pChild->text);
	else if (strElement == _T("AddConEqArDim")) NSMM = _wtoi((LPCTSTR)pChild->text);
	else if (strElement == _T("StackDim")) MKSTEK = _wtoi((LPCTSTR)pChild->text);
	//Множитель нагрузки по системе в целом (кратность нагрузки)
	else if (strElement == _T("SysLoadFactor")) KRAT = _tstof((LPCTSTR)pChild->text);
	//Множитель генерации по системе в целом (кратность генерации)
	else if (strElement == _T("SysGenFactor")) KRATG = _tstof((LPCTSTR)pChild->text);
	//Множитель pеактивной генерации линий по системе в целом
	else if (strElement == _T("SysRLGenFactor")) KRATQG = _tstof((LPCTSTR)pChild->text);
	//Удаляемая из эквивалента проводимость, См
	else if (strElement == _T("RemovConduct")) YMIN = _tstof((LPCTSTR)pChild->text);
	//Напpяжение деления pеактивных пpоводимостей линий
	else if (strElement == _T("FisRLConVolt")) UPRMAX = _tstof((LPCTSTR)pChild->text);
	//Значение деления pеактивных пpоводимостей линий, См
	else if (strElement == _T("FisRLConVal")) REPRIM = _tstof((LPCTSTR)pChild->text);
	//Возможные значения новых номеpов узлов
	else if (strElement == _T("NewVertexNumVal")) NAMET = _wtoi((LPCTSTR)pChild->text);
	//Предельное число перегрузок в сети
	else if (strElement == _T("MaxOverLoad")) KPMAX = _wtoi((LPCTSTR)pChild->text);
	//Точность сходимости итерационного процесса по напряжению, о.е.
	else if (strElement == _T("VoltAccConv")) DELU1 = _tstof((LPCTSTR)pChild->text);
	//Точность сходимости итерационного процесса по мощности, МВА
	else if (strElement == _T("PowerAccConv")) DELU2 = _tstof((LPCTSTR)pChild->text);
	//Точность определения реактивной мощности в P-U узлах, МВаp
	else if (strElement == _T("PowerAccDet")) DELQU = _tstof((LPCTSTR)pChild->text);
	//Множитель изменения длительно-допустимых токов
	else if (strElement == _T("LongTimeFactor")) BP = _tstof((LPCTSTR)pChild->text);
	return true;
}

void DisplayInputData()
{
	CString sOutput;

	sOutput.Format(_T("\
%sИсходные данные. \n\
%sK О Н С Т А Н Т Ы   И   П Е Р Е М Е Н Н Ы Е\n"),
CString(_T(' '), 25), CString(_T(' '), 15));

	NFP1_File.WriteString(sOutput);

	sOutput.Format(_T("\
     Число узлов сети ................................     %5d\n\
     Число ветвей сети ...............................     %5d\n\
     Число станций системы............................     %5d\n\
     Число ступеней напряжения системы ...............     %5d\n\
     Число трансформаторов сети ......................     %5d\n\
     Максимальный номеp узла .........................     %5d\n\
     Максимальное число паpаллельных ветвей...........     %5d\n\
     Максимальное число эквив. паpаллельных ветвей....     %5d\n\
     Напряжение балансирующего узла, кВ...............     %7.2lf%7.2lf\n\
     Номер балансирующего узла .......................     %5d\n\
     Итерация с пропуском проверки Q в P-U узлах......     %5d\n\
     Заданное число итераций .........................     %5d\n\
     Кратность генерации .............................     %6.3lf\n\
     Кратность нагрузки ..............................     %6.3lf\n\
     Кратность генеpации pеактивных лин. мощностей....     %7.4lf\n\
     Удаляемая из эквивалента проводимость, См........     %8.7lf\n\
     Напpяжение деления pеактивных пpоводимостей линий     %9.4lf\n\
     Значение деления pеактивных пpоводимостей, См....     %7.4lf\n\
     Возможные значения новых номеpов узлов...........     %5d\n\
     Максимальное число перегрузок....................     %5d\n\
     Точность сходимости по напряжению, о.е...........     %8.4lf\n\
     Точность сходимости по мощности, МВА.............     %8.4lf\n\
     Точность выбора Q в P-U узлах, МВар..............     %8.4lf\n\
     Множитель изменения длительно-допустимых токов        %6.2lf\n\
\n"), N1, M, KST, KSN, KTR, MAXNOM, MKPARV, MKEPV, U0.real(), U0.imag(),
InputNBU, NMZK, KIMAX, KRATG, KRAT, KRATQG, YMIN, UPRMAX, REPRIM,
NAMET, KPMAX, DELU1, DELU2, DELQU, BP);

	NFP1_File.WriteString(sOutput);
}

void DisplayInitialData()
{
	CString sOutput;
	int		I(0), sizeVecEdges;
	sizeVecEdges = theApp.vecEdges.size();
	sOutput.Format(_T("%sД А Н Н Ы Е   П О   У З Л А М\n+%s+\n\
│    │    │             Мощности              │ Номи-│ За-  │   Пределы по │Мощ- │\n\
│    │    │───────────────────────────────────│ наль-│ дан- │   реактивной │ность│\n\
│Но- │Име-│     Нагрузки    │   Генерации     │ ное  │ ное  │    мощности, │ком- │\n\
│мера│на  │───────────────────────────────────│ нап- │ нап- │      МВАр    │пенс.│\n\
│    │    │ Актив- │ Реакти-│ Актив- │ Реакти-│ ряже-│ ряже-│ ──────────── │устр.│\n\
│    │    │ ная,   │ вная,  │ ная,   │ вная,  │ ние, │ ние, │  Ниж- │Верх- │МВАр │\n\
│    │    │ МВт    │ МВАр   │ МВт    │ МВАр   │ кВ   │ кВ   │  ний  │ний   │     │\n\
+%s+\n"), CString(_T(' '), 23), CString(_T('─'), 80), CString(_T('─'), 80));

	NFP1_File.WriteString(sOutput);

	for (I = 1; I <= N; I++)
	{
		//sOutput.Format(_T("%4d│%4d│%8g│%8g│%8g│%8g│%6g│%6g│%7g│%6g│%5g\n"),
		sOutput.Format(_T("%4d│%4d│%8.2lf│%8.2lf│%8.2lf│%8.2lf│%6.1lf│%6.1lf│%7.1lf│%6.1lf│%5.1lf\n"),
			I, NAME[I], SN[I].real(), SN[I].imag(), SG[I].real(),
			SG[I].imag(), UY[I], UOP[I], QMN[I], QMX[I], QKY[I]);
		NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+\n"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	sOutput.Format(_T("%s Д А Н Н Ы Е   П О   В Е Т В Я М\n+%s+\n\
│    │        Узлы         │  Сопротивления,   │  Прово-  │ Коэф-│ Допу-│Сос- │\n\
│    │─────────────────────│       Ом          │  димо-   │ фици-│ сти- │тоя- │\n\
│Но- │  Начала  │  Конца   │───────────────────│  сти,    │ енты │ мые  │ние  │\n\
│мера│──────────│──────────│ Актив-   Реактив- │  См      │ тран-│ токи,│     │\n\
│    │ Но-│ Имя │ Но-│ Имя │ ное      ное      │          │ сфор-│  А   │     │\n\
│    │ мер│     │ мер│     │                   │          │ мации│      │     │\n\
+%s+\n"), CString(_T(' '), 22), CString(_T('─'), 80), CString(_T('─'), 80));

	NFP1_File.WriteString(sOutput);

	for (I = 1; I <= M; I++)
	{
		sOutput.Format(_T("%4d│%4d│ %4d│%4d| %4d|%9.3lf%9.3lf |%9.6lf |%5.3lf |%6.0lf|%3d  |\n"),
			I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(),
			REPR[I], KT[I], ADOP[I] * 1000, MVK[I]);
		NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	sOutput.Format(_T("\n\n\n%s Д А Н Н Ы Е   П О   В Е Т В Я М (vecM1, vecM2)\n+%s+\n\
│    │        Узлы         │  Сопротивления,   │  Прово-  │ Коэф-│ Допу-│Сос- │\n\
│    │─────────────────────│       Ом          │  димо-   │ фици-│ сти- │тоя- │\n\
│Но- │  Начала  │  Конца   │───────────────────│  сти,    │ енты │ мые  │ние  │\n\
│мера│──────────│──────────│ Актив-   Реактив- │  См      │ тран-│ токи,│     │\n\
│    │ Но-│ Имя │ Но-│ Имя │ ное      ное      │          │ сфор-│  А   │     │\n\
│    │ мер│     │ мер│     │                   │          │ мации│      │     │\n\
+%s+\n"), CString(_T(' '), 22), CString(_T('─'), 80), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	for (int i = 1; i < sizeVecEdges; i++)
	{
		//for (int j = 1; j < theApp.vecVertexes.size; j++)
		sOutput.Format(_T("%4d│%4d│ %4d│%4d| %4d|%9.3lf%9.3lf |%9.6lf |%5.3lf |%6.0lf|%3d  |\n"),
			i, theApp.vecEdges[i].m_Begin.m_Index, NAME1[i], theApp.vecEdges[i].m_End.m_Index,
			NAME2[i], X[i].real(), X[i].imag(), REPR[i], KT[i], ADOP[i] * 1000, MVK[i]);
		NFP1_File.WriteString(sOutput);
	}
}