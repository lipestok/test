// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

#pragma once

#include "ViewTree.h"

class CEdgeToolBar : public CMFCToolBar
{
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*)GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CEdgeView : public CDockablePane
{
public:
	CEdgeView();
	virtual ~CEdgeView();

	void AdjustLayout();
	void OnChangeVisualStyle();
	int SwitchIndexBegin, SwitchIndexEnd;
protected:
	CEdgeToolBar m_wndToolBar;
	CViewTree m_wndEdgeView;
	CImageList m_EdgeViewImages;
	UINT m_nCurrSort;
	void FillEdgeView();

	// ���������������
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnExpandAll();
	afx_msg void OnCollapseAll();
	void ExpandBranch(CTreeCtrl& tree, HTREEITEM hItem, BOOL bExpand);

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEdgeAddMemberFunction();
	afx_msg void OnEdgeAddMemberVariable();
	afx_msg void OnEdgeDefinition();
	afx_msg void OnEdgeProperties();
	afx_msg void OnNewFolder();
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//afx_msg LRESULT OnChangeActiveTab(WPARAM, LPARAM);
	afx_msg void OnSort(UINT id);
	afx_msg void OnUpdateSort(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSwitchOn();
	afx_msg void OnSwitchOff();
	void UpdateEdgeView();
};

