#pragma once

#include "stdafx.h"
#include "Flisr.h"

typedef  complex<double> COMPLEX;

#import <msxml3.dll> named_guids

bool SW00W21_MAIN();
bool RE00W21();  //������ ������ �� �������� ������
//bool CreateBoostGraf();
bool IM00W21();  //������������� �������� � ��������������� ��������
void FM00W21(); //������������ ������� ��������� ����
void MY00W1();
void OFM0W1();
void IYS0W1();
void RAV0W1();
void RAN0W1(COMPLEX IY[]);
void IYV0W21();
void PIY0W21();

void DE00W1(int(&IH)[77], int(&IYPHrg)[290], int(&NYPH)[290], int(&KVIYrg)[77], COMPLEX(&IY)[77], COMPLEX(&IT)[130], int(&NVPH)[290]);
void CH00W1(COMPLEX(&ITrg)[130], COMPLEX(&IYrg)[77]);
void CR30W21(COMPLEX(&ITrg)[130], int &KP, int(&MVKrg)[130], int(&NPB)[20]);
void CB00W1(COMPLEX(&ITrg)[130], int MVK[130]);
#ifndef COMM_
#define COMM_


#endif
//#endif /* COMMON_H */