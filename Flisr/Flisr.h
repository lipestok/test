// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

// Flisr.h : ������� ���� ��������� ��� ���������� Flisr
//
#pragma once

#ifndef __AFXWIN_H__
#error "�������� stdafx.h �� ��������� ����� ����� � PCH"
#endif

#include "resource.h"       // �������� �������


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/breadth_first_search.hpp> 
#include <boost/graph/iteration_macros.hpp> 
#include <boost/graph/visitors.hpp>

#include <boost/graph/adj_list_serialize.hpp>

#include <boost/property_map/property_map.hpp>
/*#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <vector>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/graph/metric_tsp_approx.hpp>*/

#include "ODBCRecordset.h"
typedef  complex<double> COMPLEX;
enum DB_TYPE { ACCESS, SQL, PostgreSQL };

struct VertexProp
{
	long m_index;
	long m_objectIndex;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & m_index;
		ar & m_objectIndex;
	}
	CString m_strName;
	int m_nType;
	int m_nPower;
	int m_nState;
	int color;
	
	VertexProp()
	{
		m_index = 0;
		m_objectIndex = 0;
		m_strName = _T("");
		m_nType = 0;
		m_nPower = 0;
		m_nState = 1;
		color = 0;
	}
	VertexProp(long index, long objectIndex, CString strName, int nType, int nPower, int nState, int ncolor)
	{
		m_index = index;
		m_objectIndex = objectIndex;
		m_strName = strName;
		m_nType = nType;
		m_nPower = nPower;
		m_nState = nState;
		color = ncolor;
	}
	VertexProp& operator=(const VertexProp& from)
	{
		m_index = from.m_index;
		m_objectIndex = from.m_objectIndex;
		m_strName = from.m_strName;
		m_nType = from.m_nType;
		m_nPower = from.m_nPower;
		m_nState = from.m_nState;
		color = from.color;
		return *this;
	}
};

struct EdgeProp
{
	long m_index;
	long m_objectIndex;
	long m_regionIndex, m_dataIndex;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & m_index;
		ar & m_objectIndex;
		ar & m_regionIndex;
		ar & m_dataIndex;
	}
	CString m_strName;
	int m_nType;
	int m_nPower;
	int m_nState;

	EdgeProp()
	{
		m_index = 0;
		m_objectIndex = 0;
		m_regionIndex = 0;
		m_dataIndex = 0;
		m_strName = _T("");
		m_nType = 0;
		m_nPower = 0;
		m_nState = 1;
	}
	EdgeProp(long index, long objectIndex, long regionIndex, long dataIndex, CString strName, int nType, int nPower, int nState)
	{
		m_index = index;
		m_objectIndex = objectIndex;
		m_regionIndex = regionIndex;
		m_dataIndex = dataIndex;
		m_strName = strName;
		m_nType = nType;
		m_nPower = nPower;
		m_nState = nState;
	}
	EdgeProp& operator=(const EdgeProp& from)
	{
		m_index = from.m_index;
		m_objectIndex = from.m_objectIndex;
		m_regionIndex = from.m_regionIndex;
		m_dataIndex = from.m_dataIndex;
		m_strName = from.m_strName;
		m_nType = from.m_nType;
		m_nPower = from.m_nPower;
		m_nState = from.m_nState;
		return *this;
	}
};

using namespace boost;

namespace bglex
{
	using namespace boost;

	template <class Visitor, class Graph>
	struct BFSVisitorConceptEx
	{
		void constraints()
		{
			BOOST_CONCEPT_ASSERT((CopyConstructibleConcept<Visitor>));
			vis.initialize_vertex(u, g);
			vis.discover_vertex(u, g);
			vis.examine_vertex_ex(u, g, res);
			vis.examine_edge_ex(e, g, res);
			//			vis.examine_vertex(u, g);
			vis.examine_edge(e, g);
			vis.tree_edge(e, g);
			vis.non_tree_edge(e, g);
			vis.gray_target(e, g);
			vis.black_target(e, g);
			vis.finish_vertex(u, g);
		}
		Visitor vis;
		Graph g;
		bool res;
		typename graph_traits<Graph>::vertex_descriptor u;
		typename graph_traits<Graph>::edge_descriptor e;
	};

	// Multiple-source version
	template <class IncidenceGraph, class Buffer, class BFSVisitor,
		class ColorMap, class SourceIterator>
		void breadth_first_visit_ex
		(const IncidenceGraph& g,
			SourceIterator sources_begin, SourceIterator sources_end,
			Buffer& Q, BFSVisitor vis, ColorMap color)
	{
		BOOST_CONCEPT_ASSERT((IncidenceGraphConcept<IncidenceGraph>));
		typedef graph_traits<IncidenceGraph> GTraits;
		typedef typename GTraits::vertex_descriptor Vertex;
		BOOST_CONCEPT_ASSERT((BFSVisitorConceptEx<BFSVisitor, IncidenceGraph>));
		BOOST_CONCEPT_ASSERT((ReadWritePropertyMapConcept<ColorMap, Vertex>));
		typedef typename property_traits<ColorMap>::value_type ColorValue;
		typedef color_traits<ColorValue> Color;
		typename GTraits::out_edge_iterator ei, ei_end;

		for (; sources_begin != sources_end; ++sources_begin)
		{
			Vertex s = *sources_begin;
			put(color, s, Color::gray());
			vis.discover_vertex(s, g);
			Q.push(s);
		}
		while (!Q.empty())
		{
			Vertex u = Q.top(); Q.pop();
			bool res(true);
			vis.examine_vertex_ex(u, g, res);
			if (res)
			{
				for (boost::tie(ei, ei_end) = out_edges(u, g); ei != ei_end; ++ei)
				{
					Vertex v = target(*ei, g);
					res = true;
					vis.examine_edge_ex(*ei, g, res);
					ColorValue v_color = get(color, v);
					if (v_color == Color::white())
					{
						vis.tree_edge(*ei, g);
						put(color, v, Color::gray());
						vis.discover_vertex(v, g);
						if (res)
							Q.push(v);
					}
					else
					{
						vis.non_tree_edge(*ei, g);
						if (v_color == Color::gray())
							vis.gray_target(*ei, g);
						else
							vis.black_target(*ei, g);
					}
				} // end for
			}
			put(color, u, Color::black());
			vis.finish_vertex(u, g);
		} // end while
	} // breadth_first_visit

	  // Single-source version
	template <class IncidenceGraph, class Buffer, class BFSVisitor,
		class ColorMap>
		void breadth_first_search_ex
		(const IncidenceGraph& g,
			typename graph_traits<IncidenceGraph>::vertex_descriptor s,
			Buffer& Q, BFSVisitor vis, ColorMap color)
	{
		typename graph_traits<IncidenceGraph>::vertex_descriptor sources[1] = { s };
		breadth_first_search_ex(g, sources, sources + 1, Q, vis, color);
	}

	template <class VertexListGraph, class SourceIterator,
		class Buffer, class BFSVisitor,
		class ColorMap>
		void breadth_first_search_ex
		(const VertexListGraph& g,
			SourceIterator sources_begin, SourceIterator sources_end,
			Buffer& Q, BFSVisitor vis, ColorMap color)
	{
		typedef typename property_traits<ColorMap>::value_type ColorValue;
		typedef color_traits<ColorValue> Color;
		typename boost::graph_traits<VertexListGraph>::vertex_iterator i, i_end;
		for (boost::tie(i, i_end) = vertices(g); i != i_end; ++i)
		{
			vis.initialize_vertex(*i, g);
			put(color, *i, Color::white());
		}
		breadth_first_visit_ex(g, sources_begin, sources_end, Q, vis, color);
	}

	namespace graph { struct bfs_visitor_event_not_overridden {}; }

	namespace detail
	{
		template <class VertexListGraph, class ColorMap, class BFSVisitor,
			class P, class T, class R>
			void bfs_helper_ex
			(VertexListGraph& g,
				typename boost::graph_traits<VertexListGraph>::vertex_descriptor s,
				ColorMap color,
				BFSVisitor vis,
				const bgl_named_params<P, T, R>& params,
				boost::mpl::false_)
		{
			typedef graph_traits<VertexListGraph> Traits;
			// Buffer default
			typedef typename Traits::vertex_descriptor Vertex;
			typedef boost::queue<Vertex> queue_t;
			queue_t Q;
			bglex::breadth_first_search_ex
			(g, s,
				choose_param(get_param(params, buffer_param_t()), boost::ref(Q)).get(),
				vis, color);
		}

		template <class ColorMap>
		struct bfs_dispatch_ex
		{
			template <class VertexListGraph, class P, class T, class R>
			static void apply
			(VertexListGraph& g,
				typename boost::graph_traits<VertexListGraph>::vertex_descriptor s,
				const boost::bgl_named_params<P, T, R>& params,
				ColorMap color)
			{
				bfs_helper_ex
				(g, s, color,
					choose_param(get_param(params, graph_visitor),
						make_bfs_visitor_ex(null_visitor())),
					params,
					boost::mpl::bool_<
					boost::is_base_and_derived<
					distributed_graph_tag,
					typename boost::graph_traits<VertexListGraph>::traversal_category>::value>());
			}
		};
	}

	// Named Parameter Variant
	template <class VertexListGraph, class P, class T, class R>
	void breadth_first_search_ex
	(const VertexListGraph& g,
		typename boost::graph_traits<VertexListGraph>::vertex_descriptor s,
		const boost::bgl_named_params<P, T, R>& params)
	{
		// The graph is passed by *const* reference so that graph adaptors
		// (temporaries) can be passed into this function. However, the
		// graph is not really const since we may write to property maps
		// of the graph.
		VertexListGraph& ng = const_cast<VertexListGraph&>(g);
		typedef typename get_param_type< vertex_color_t, bgl_named_params<P, T, R> >::type C;
		detail::bfs_dispatch_ex<C>::apply(ng, s, params,
			get_param(params, vertex_color));
	}

	template <class Visitors = null_visitor>
	class bfs_visitor_ex : public bfs_visitor<Visitors>
	{
	public:
		bfs_visitor_ex() {}
		bfs_visitor_ex(Visitors vis) : bfs_visitor(vis) {}

		template <class Vertex, class Graph>
		graph::bfs_visitor_event_not_overridden
			examine_vertex_ex(Vertex u, Graph& g, bool& res)
		{
			invoke_visitors(m_vis, u, g, ::boost::on_examine_vertex());
			return graph::bfs_visitor_event_not_overridden();
		}

		template <class Edge, class Graph>
		graph::bfs_visitor_event_not_overridden
			examine_edge_ex(Edge e, Graph& g, bool& res)
		{
			invoke_visitors(m_vis, e, g, ::boost::on_examine_edge());
			return graph::bfs_visitor_event_not_overridden();
		}

	};

	template <class Visitors>
	bfs_visitor_ex<Visitors>
		make_bfs_visitor_ex(Visitors vis)
	{
		return bfs_visitor_ex<Visitors>(vis);
	}
	typedef bfs_visitor_ex<> default_bfs_visitor_ex;
}

// create a typedef for the Graph type
typedef adjacency_list<vecS, vecS, undirectedS, VertexProp, EdgeProp> Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef std::vector<Vertex> Vertices;  //������- ���������� ������
typedef std::vector<Edge> Edges;      //������- ���������� ���� 

/*����� ���������� ��� ������ � ������*/
class CBFS : public bglex::default_bfs_visitor_ex
{
public:
	CBFS();
	CBFS(Vertices* v, ULONG selectedVertex, HWND pwnd) : v(v), startVertex(selectedVertex), m_hwnd(pwnd) {}

	template <typename Vertex, typename Graph>
	void examine_vertex_ex(Vertex u, const Graph& g, bool& res) const;

	template <typename Edge, typename Graph>
	void examine_edge_ex(Edge e, const Graph& g, bool& res) const;
	Vertices* v;
private:

	ULONG startVertex;
	HWND m_hwnd;

};

class BFSVisitor : public default_bfs_visitor
{
public:
	explicit BFSVisitor(Vertices* v) : v(v) {};
	~BFSVisitor() = default;

	Vertices* v;

public:

	void discover_vertex(Vertex u, const Graph & g)
	{
		v->push_back(u);
	}
};
class TopologyGraph
{
public:
	Graph m_topology;                                         //����������� ����
	Vertex AddVertex(const VertexProp& prop);                 //�������� �������
//	void RemoveVertex(Vertex v);                              //������� �������
	Edge AddEdge(Vertex u, Vertex v, const EdgeProp& prop);   //�������� �����
//	void RemoveEdge(Edge e);					              //������� �����

// algorithms
	class SearchParams
	{
	public:
		SearchParams() :startVertex(0) {}
		unsigned long startVertex;
	};

	void WidthSearch(Vertices &vecVertexChain, int paramVertex);
	void saveGraph(std::ofstream & ar);
	void loadGraph(std::ifstream  & ar);
};

//--------------------------------------ObjectTable------------------------------------------//
class CObjectTableElement
{
public:
	long m_ObjectIndex;
	long m_ObjectTypeIndex;
	CString m_ObjectName;
	long m_ModelNumber;
	long m_Reserv;
	long m_PicNumber;
public:
	CObjectTableElement();
	virtual ~CObjectTableElement();

	CObjectTableElement& operator=(const CObjectTableElement& from)
	{
		m_ObjectIndex = from.m_ObjectIndex;
		m_ObjectTypeIndex = from.m_ObjectTypeIndex;
		m_ObjectName = from.m_ObjectName;
		m_ModelNumber = from.m_ModelNumber;
		m_Reserv = from.m_Reserv;
		m_PicNumber = from.m_PicNumber;
		return *this;
	}
};

// CFlisrApp:
// � ���������� ������� ������ ��. Flisr.cpp
//

class CFlisrApp : public CWinAppEx
{
public:
	CFlisrApp();
	map<long, CObjectTableElement > m_MapObjectTable;
	map<long, CObjectTableElement >::iterator it_MapObjectTable;

	int ReadObjectTable();
	CString m_strZerverDB;
	DB_TYPE m_dbType;
	bool flagConnectPostgre, flagLoadGraphFile;

	// ���������������
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	void WriteToLog(CString s);
	void STOP();
	CString m_sDirName, m_VersionNumber, m_DateCreation;
	vector <vector<int> > vecM3;
	vector <vector<int> > vecM3V;

	vector <int> vecVertexeStage1;
	vector <int> vecVertexeStage2;
	vector <vector<int> > vecVertexeStage3;
	vector <vector<int> > vecVertexeStage4;
	/*T*/
	int arrayM1[1000], arrayM2[1000], arrayEdgeState[10000], arrayEdgeType[10000], arrayVertexType[1000];
	int m_nVertexT, m_nEdgeT;
	TopologyGraph gGraph;
	bool OnSlice();
	bool readDateFile();
	bool CreateBoostGraf();

	// ����������
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;
	bool bClassRegistered;


	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();
	int PutToLogWnd(const CString& Message);
	afx_msg void OnAppAbout();
	//	bool testBoostGraph();
protected:
	TCHAR m_szExecDir[MAX_PATH];
	CStdioFile m_LogFile,
		NFR_FileT,  //������
		NFP_FileT;  //������
	bool m_bLogFile;
	void TrimLogFile(CString path, ULONGLONG m_MinLogSize, ULONGLONG m_MaxLogSize);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnButton2();
	void ConnectPostgre(CString ConnectSql);
	bool SendWMCOPYDATA(CString ClassName, BYTE bt, BYTE Message[], int cb);
	int LoadGraphFile(CString strFilePath);
	void FillInGraph();
	void TestRepository();
};

extern CFlisrApp theApp;
