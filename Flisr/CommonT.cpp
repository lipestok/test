﻿#include "stdafx.h"
#include "CommonT.h"

// namespace support is now required
#ifdef NO_NAMESPACE
#	error namespace support is now required
#endif
#define NAMESPACE_BEGIN(x) namespace x {
#define NAMESPACE_END }

CStdioFile NFR_FileT,  //чтение
NFP_FileT;  //запись
CString strOutputT;

bool OnSlice()
{
	int i, kkkk, ihr, imin, isec, i100th, nvrpv, iyr, imon, iday;
	double vrem0, tvrem;
	int gfgfgfgfg;
	NFR_FileT.Open(_T("dateGrid.W02"), CFile::modeRead);
	NFP_FileT.Open(_T("outputFile.W00"), CFile::modeWrite | CFile::modeCreate);
	
	//дата , время в лог файл
	CTime t = CTime::GetCurrentTime();
	iyr = t.GetYear();
	imon = t.GetMonth();
	iday = t.GetDay();
	ihr = t.GetHour();
	imin = t.GetMinute();
	isec = t.GetSecond();

	i100th = 0;
	vrem0 = ihr*3600.0 + imin*60.0 + isec + i100th / 100.0;
	tvrem = vrem0;
	nvrpv = 0;

	strOutputT.Format(_T("%sДата: %3d%3d%5d Время :%4d час%4d мин.-%4d.%2d сек.\n"), CString(_T(' '), 12), iday, imon, iyr, ihr, imin, isec, i100th);
	NFP_FileT.WriteString(strOutputT);
	strOutputT.Format(_T("\n%s┌%s┐\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP_FileT.WriteString(strOutputT);
	theApp.WriteToLog(strOutputT);

	/*	Чтение данных из входного потока	*/
	if (!readDateFile()) AfxMessageBox(_T("ошибка. не прочитался файл sd3098.W02"));
	
	NFR_FileT.Close();
	NFP_FileT.Close();
	return TRUE;
}

bool readDateFile()
{
	CString sVal;
	int i, j;
/*	Код режима сужения поиска по МО хар ПП ДК макс: 1-да, 0-нет;*/
	CString sInput, tmpStr;
	
	NFR_FileT.ReadString(sInput);
	strOutputT.Format(_T("%sХарактеристики сети%s\n"), CString(_T('─'), 26), CString(_T('─'), 26));
	NFP_FileT.WriteString(strOutputT);

	NFR_FileT.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	theApp.m_nEdgeT = _tstoi(tmpStr);
	strOutputT.Format(_T(" %10d - чиcло ветвей\n"), theApp.m_nEdgeT);
	NFP_FileT.WriteString(strOutputT);

	NFR_FileT.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	theApp.m_nVertexT = _tstoi(tmpStr);
	strOutputT.Format(_T(" %10d - чиcло узлов без базисного\n"), theApp.m_nVertexT);
	NFP_FileT.WriteString(strOutputT);
	
	//данные по ветвям
	for (i = 0; i < 9; i++)
	{
		NFR_FileT.ReadString(sInput); /////////// - 9 строк пропуска
	}
	for (i = 0; i < theApp.m_nEdgeT; i++)
	{
		NFR_FileT.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		sVal = tmpStr.Left(13);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 14);
		theApp.arrayM1[i] = _tstoi(sVal.Right(6));
		theApp.arrayM2[i] = _tstoi(tmpStr.Left(6));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 36);
		theApp.arrayEdgeState[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 20);
		theApp.arrayEdgeType[i] = _tstoi(tmpStr.Left(2));
	}
	//данные по узлам
	for (i = 0; i < 9; i++)
	{
		NFR_FileT.ReadString(sInput); /////////// - 9 строк пропуска
	}
	for (i = 0; i < theApp.m_nVertexT; i++)
	{
		NFR_FileT.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 42);
		theApp.arrayVertexType[i] = _tstoi(tmpStr.Left(4));
	}
	return true;
}