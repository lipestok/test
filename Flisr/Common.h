#pragma once

#include "stdafx.h"
#include "Flisr.h"

typedef  complex<double> COMPLEX;

#import <msxml3.dll> named_guids

bool LFS210();

bool OnInput();
void IN202();

void FMS117();
void PDS015_new(int M3X[]); ////������������ ������� PDS015(int M3X[]);
void PDS015(int M3X[]);
void RZM202();
void MYP015();
void EK013();
void DET117();
void DEE117(int &KY, int &NYNI, int &IND, int &KYN, BOOL entry_param /*TRUE-goto entrypoint*/);

void PRE015();
void PVREM();
void ZKS016();
void CLF210();
void RML202();
void SYS013(COMPLEX PARAM1[], COMPLEX PARAM2[]);
void RT015();
void PVP118();
void ZKM210(int &KSTI, double &NEBS);
void RMV118();
void RTO015();

bool DisplayChildren(MSXML2::IXMLDOMNodePtr pParent);
bool DisplayChild(MSXML2::IXMLDOMNodePtr pChild);
void DisplayInputData();
void DisplayInitialData();

#ifndef COMM_
#define COMM_


#endif
//#endif /* COMMON_H */