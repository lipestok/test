// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

#include "stdafx.h"
#include "MainFrm.h"
#include "VertexView.h"
#include "Resource.h"
#include "Flisr.h"

class CVertexViewMenuButton : public CMFCToolBarMenuButton
{
	friend class CVertexView;

	DECLARE_SERIAL(CVertexViewMenuButton)

public:
	CVertexViewMenuButton(HMENU hMenu = NULL) : CMFCToolBarMenuButton((UINT)-1, hMenu, -1)
	{
	}

	virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE,
		BOOL bCustomizeMode = FALSE, BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE)
	{
		pImages = CMFCToolBar::GetImages();

		CAfxDrawState ds;
		pImages->PrepareDrawImage(ds);

		CMFCToolBarMenuButton::OnDraw(pDC, rect, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

		pImages->EndDrawImage(ds);
	}
};

IMPLEMENT_SERIAL(CVertexViewMenuButton, CMFCToolBarMenuButton, 1)

//////////////////////////////////////////////////////////////////////
// �������� � ��������
//////////////////////////////////////////////////////////////////////

CVertexView::CVertexView()
{
	m_nCurrSort = ID_SORTING_GROUPBYTYPE;
	IndexMainSubstation = 2013;
	IndexConsumer = 2009;
}

CVertexView::~CVertexView()
{
}

BEGIN_MESSAGE_MAP(CVertexView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_CLASS_ADD_MEMBER_FUNCTION, OnVertexAddMemberFunction)
	ON_COMMAND(ID_CLASS_ADD_MEMBER_VARIABLE, OnVertexAddMemberVariable)
	ON_COMMAND(ID_CLASS_DEFINITION, OnVertexDefinition)
	ON_COMMAND(ID_CLASS_PROPERTIES, OnVertexProperties)
	ON_COMMAND(ID_NEW_FOLDER, OnNewFolder)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_COMMAND_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnSort)
	ON_UPDATE_COMMAND_UI_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnUpdateSort)
	ON_COMMAND(ID_EXPAND_ALL, &CVertexView::OnExpandAll)
	ON_COMMAND(ID_COLLAPSE_ALL, &CVertexView::OnCollapseAll)
	ON_COMMAND(ID_CONSUMER, &CVertexView::OnListConsumer)
	ON_COMMAND(ID_MAINSUBSTATION, &CVertexView::OnMainsubstation)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ����������� ��������� CVertexView

int CVertexView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// �������� �������������:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndVertexView.Create(dwViewStyle, rectDummy, this, 2))
	{
		TRACE0("�� ������� ������� ������������� �������\n");
		return -1;      // �� ������� �������
	}

	// �������� �����������:
	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_SORT);
	m_wndToolBar.LoadToolBar(IDR_SORT, 0, 0, TRUE /* ������������ */);

	OnChangeVisualStyle();

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// ��� ������� ����� �������������� ����� ���� ������� ����������, � �� ����� ������������ �����:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	CMenu menuSort;
	menuSort.LoadMenu(IDR_POPUP_SORT);

	m_wndToolBar.ReplaceButton(ID_SORT_MENU, CVertexViewMenuButton(menuSort.GetSubMenu(0)->GetSafeHmenu()));
	m_wndToolBar.ReplaceButton(ID_SORT_MENU2, CVertexViewMenuButton(menuSort.GetSubMenu(1)->GetSafeHmenu()));

	CVertexViewMenuButton* pButtonSort = DYNAMIC_DOWNCAST(CVertexViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButtonSort != NULL)
	{
		pButtonSort->m_bText = FALSE;
		pButtonSort->m_bImage = TRUE;
		pButtonSort->SetImage(GetCmdMgr()->GetCmdImage(m_nCurrSort));
		pButtonSort->SetMessageWnd(this);
	}
	CVertexViewMenuButton* pButtonStage = DYNAMIC_DOWNCAST(CVertexViewMenuButton, m_wndToolBar.GetButton(2));

	if (pButtonStage != NULL)
	{
		pButtonStage->m_bText = FALSE;
		pButtonStage->m_bImage = TRUE;
		pButtonStage->SetImage(3);
		pButtonStage->SetMessageWnd(this);
	}
	// ������� ��������� ������ ������������ ������������� � ���� ������ (������ ���, ������ �����)
	FillVertexView();

	return 0;
}

void CVertexView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CVertexView::FillVertexView()
{
	m_wndVertexView.DeleteAllItems();
	HTREEITEM hRoot, hVertex, hVertexM3;
	CString str;
	int i, j;
	hRoot = m_wndVertexView.InsertItem(_T("����"), 0, 0);
	m_wndVertexView.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);
	for (i = 0; i < theApp.gGraph.m_topology.m_vertices.size(); i++)
	{
		str.Format(_T("%d  (%d) %s"), i, theApp.gGraph.m_topology.m_vertices[i].m_property.m_objectIndex, theApp.gGraph.m_topology.m_vertices[i].m_property.m_strName);
		if (theApp.gGraph.m_topology.m_vertices[i].m_property.m_nType == IndexMainSubstation)  // "�������� ��" || �� || �������� �����
		{
			str.Format(_T("%d �������� ����� - (%d) %s"), i, theApp.gGraph.m_topology.m_vertices[i].m_property.m_objectIndex, theApp.gGraph.m_topology.m_vertices[i].m_property.m_strName);
		}
		if (theApp.gGraph.m_topology.m_vertices[i].m_property.m_nType == IndexConsumer)  // "���� ��" || �����������
		{
			str.Format(_T("%d ����������� - (%d) %s"), i, theApp.gGraph.m_topology.m_vertices[i].m_property.m_objectIndex, theApp.gGraph.m_topology.m_vertices[i].m_property.m_strName);
		}
		hVertex = m_wndVertexView.InsertItem(str, 8, 8, hRoot); /*11*/
		//m_wndVertexView.SetItemState(hVertex, TVIS_BOLD, TVIS_BOLD);
		hVertexM3 = m_wndVertexView.InsertItem(_T("���� �������"), 9, 9, hVertex);
		int m_out_edges_size = theApp.gGraph.m_topology.m_vertices[i].m_out_edges.size();
		for (j = 0; j < m_out_edges_size; j++)
		{
			int po = theApp.gGraph.m_topology.m_vertices[i].m_out_edges[j].m_target;
			str.Format(_T("%d"), po);
			m_wndVertexView.InsertItem(str, 11, 11, hVertexM3);
		}
	}
}

void CVertexView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndVertexView;
	ASSERT_VALID(pWndTree);

	if (pWnd != pWndTree)
	{
		CDockablePane::OnContextMenu(pWnd, point);
		return;
	}

	if (point != CPoint(-1, -1))
	{
		// �������� ������� �������:
		CPoint ptTree = point;
		pWndTree->ScreenToClient(&ptTree);

		UINT flags = 0;
		HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
		if (hTreeItem != NULL)
		{
			pWndTree->SelectItem(hTreeItem);
		}
	}

	pWndTree->SetFocus();
	CMenu menu;
	menu.LoadMenu(IDR_MENU_VERTEX);
	CMenu* pSumMenu;
	HTREEITEM item = m_wndVertexView.GetSelectedItem();
	int n_ItemTree = _tstoi(m_wndVertexView.GetItemText(item));

	if (theApp.gGraph.m_topology.m_vertices[n_ItemTree].m_property.m_nType == IndexMainSubstation) //�������� �����
	{
		pSumMenu = menu.GetSubMenu(0);
	}
	else if (theApp.gGraph.m_topology.m_vertices[n_ItemTree].m_property.m_nType == IndexConsumer)
	{
		pSumMenu = menu.GetSubMenu(1); //�����������
	}
	else
	{
		return;
	}


	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}
}

void CVertexView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndVertexView.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

BOOL CVertexView::PreTranslateMessage(MSG* pMsg)
{
	return CDockablePane::PreTranslateMessage(pMsg);
}

void CVertexView::OnSort(UINT id)
{
	if (m_nCurrSort == id)
	{
		return;
	}

	m_nCurrSort = id;

	CVertexViewMenuButton* pButton = DYNAMIC_DOWNCAST(CVertexViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->SetImage(GetCmdMgr()->GetCmdImage(id));
		m_wndToolBar.Invalidate();
		m_wndToolBar.UpdateWindow();
	}
}

void CVertexView::OnUpdateSort(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(pCmdUI->m_nID == m_nCurrSort);
}

void CVertexView::OnVertexAddMemberFunction()
{
	AfxMessageBox(_T("�������� �������-����..."));
}

void CVertexView::OnVertexAddMemberVariable()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CVertexView::OnVertexDefinition()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CVertexView::OnVertexProperties()
{
	// TODO: �������� ���� ��� ����������� ������
}

void CVertexView::OnNewFolder()
{
	AfxMessageBox(_T("������� �����..."));
}

void CVertexView::OnPaint()
{
	CPaintDC dc(this); // �������� ���������� ��� ���������

	CRect rectTree;
	m_wndVertexView.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CVertexView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	m_wndVertexView.SetFocus();
}

void CVertexView::OnChangeVisualStyle()
{
	m_VertexViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_CLASS_VIEW_24 : IDB_CLASS_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("�� ������� ��������� �������� �������: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_VertexViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_VertexViewImages.Add(&bmp, RGB(255, 0, 0));

	m_wndVertexView.SetImageList(&m_VertexViewImages, TVSIL_NORMAL);

	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_SORT_24 : IDR_SORT, 0, 0, TRUE /* ������������ */);
}

void CVertexView::OnExpandAll()
{
	HTREEITEM  h = m_wndVertexView.GetRootItem();
	ExpandBranch(m_wndVertexView, h, 1);
}


void CVertexView::OnCollapseAll()
{
	HTREEITEM  h = m_wndVertexView.GetRootItem();
	ExpandBranch(m_wndVertexView, h, 0);
}

void CVertexView::ExpandBranch(CTreeCtrl& tree, HTREEITEM hItem, BOOL bExpand)
{
	if (tree.ItemHasChildren(hItem))
	{
		tree.Expand(hItem, bExpand ? TVE_EXPAND : TVE_COLLAPSE);
		hItem = tree.GetChildItem(hItem);
		do
		{
			ExpandBranch(tree, hItem, 1);
		} while ((hItem = tree.GetNextSiblingItem(hItem)) != NULL);
	}
}

void CVertexView::OnListConsumer()
{
	HTREEITEM item = m_wndVertexView.GetSelectedItem();
	int n_ItemTree = _tstoi(m_wndVertexView.GetItemText(item));
	Vertices SearchVertices;
	CString s, str, strBuf = (_T(""));
	strBuf.Format(_T("����������� ���� %d: \n"), n_ItemTree);

	int n_paramVertex = n_ItemTree;
	theApp.gGraph.WidthSearch(SearchVertices, n_paramVertex);
	str.Empty();
	str.Format(_T("%d:  "), n_ItemTree);
	for (Vertex j : SearchVertices)
	{
		if (theApp.gGraph.m_topology.m_vertices[j].m_property.m_nType == IndexConsumer)
		{
			s.Format(_T("%d "), j);
			str += s;
		}
	}
	theApp.PutToLogWnd(str);
}


void CVertexView::OnMainsubstation()
{
	HTREEITEM item = m_wndVertexView.GetSelectedItem();
	int n_ItemTree = _tstoi(m_wndVertexView.GetItemText(item));
	Vertices SearchVertices;
	CString s, str, strBuf = (_T(""));
	strBuf.Format(_T("�������� ���� %d: \n"), n_ItemTree);

	int n_paramVertex = n_ItemTree;
	theApp.gGraph.WidthSearch(SearchVertices, n_paramVertex);
	str.Empty();
	str.Format(_T("%d:  "), n_ItemTree);
	for (Vertex j : SearchVertices)
	{
		if (theApp.gGraph.m_topology.m_vertices[j].m_property.m_nType == IndexMainSubstation)
		{
			s.Format(_T("%d "), j);
			str += s;
		}
	}
	theApp.PutToLogWnd(str);
}

void CVertexView::UpdateVertexView()
{
	FillVertexView();
}
