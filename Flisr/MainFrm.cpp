// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// (Fluent UI) � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

// MainFrm.cpp : ���������� ������ CMainFrame
//

#include "stdafx.h"
#include "Flisr.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static const UINT SweepTimer = 0x1E;
static const UINT Start = 0x2E;
static const UINT Sweep = 5000;//3600000;  millisecond 1 ���  5000 = 5���
static UINT FirstStartTimer = 0;
static UINT StartTimer = 0;
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
	ON_WM_CREATE()
	ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
	ON_WM_SETTINGCHANGE()
	ON_COMMAND(ID_VIEW_VERTEXWND, &CMainFrame::OnViewVertex)
	ON_UPDATE_COMMAND_UI(ID_VIEW_VERTEXWND, &CMainFrame::OnUpdateViewVertex)
	ON_COMMAND(ID_VIEW_EDGEWND, &CMainFrame::OnViewEdge)
	ON_UPDATE_COMMAND_UI(ID_VIEW_EDGEWND, &CMainFrame::OnUpdateViewEdge)
	ON_COMMAND(ID_VIEW_FILEWND, &CMainFrame::OnViewFile)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FILEWND, &CMainFrame::OnUpdateViewFile)
	ON_COMMAND(ID_VIEW_CLASSWND, &CMainFrame::OnViewClass)
	ON_UPDATE_COMMAND_UI(ID_VIEW_CLASSWND, &CMainFrame::OnUpdateViewClass)
	ON_COMMAND(ID_VIEW_OUTPUTWND, &CMainFrame::OnViewOutputwnd)
	ON_UPDATE_COMMAND_UI(ID_VIEW_OUTPUTWND, &CMainFrame::OnUpdateViewOutputwnd)
	ON_WM_COPYDATA()
	ON_WM_TIMER()
END_MESSAGE_MAP()

// ��������/����������� CMainFrame

CMainFrame::CMainFrame()
{
	// TODO: �������� ��� ������������� �����
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2008);
	m_bRunTimer = false;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	BOOL bNameValid;

	CMDITabInfo mdiTabParams;
	mdiTabParams.m_style = CMFCTabCtrl::STYLE_3D_ONENOTE; // ������ ��������� �����...
	mdiTabParams.m_bActiveTabCloseButton = TRUE;      // ���������� �������� FALSE, ����� ����������� ������ \"�������\" � ������ ����� ������� �������
	mdiTabParams.m_bTabIcons = FALSE;    // ���������� �������� TRUE, ����� �������� ������ ���������� �� �������� MDI
	mdiTabParams.m_bAutoColor = TRUE;    // ���������� �������� FALSE, ����� ��������� �������������� ��������� ������ ������� MDI
	mdiTabParams.m_bDocumentMenu = TRUE; // �������� ���� ��������� �� ������ ������� ������� �������
	EnableMDITabbedGroups(TRUE, mdiTabParams);

	m_wndRibbonBar.Create(this);
	m_wndRibbonBar.LoadFromResource(IDR_RIBBON);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("�� ������� ������� ������ ���������\n");
		return -1;      // �� ������� �������
	}

	CString strTitlePane1;
	CString strTitlePane2;
	bNameValid = strTitlePane1.LoadString(IDS_STATUS_PANE1);
	ASSERT(bNameValid);
	bNameValid = strTitlePane2.LoadString(IDS_STATUS_PANE2);
	ASSERT(bNameValid);
	m_wndStatusBar.AddElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE1, strTitlePane1, TRUE), strTitlePane1);
	m_wndStatusBar.AddExtendedElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE2, strTitlePane2, TRUE), strTitlePane2);

	// �������� ����� ������ ������������ ���� ������ Visual Studio 2005
	CDockingManager::SetDockingMode(DT_SMART);
	// �������� ����� ������ ��������������� ������� ������������ ���� ������ Visual Studio 2005
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// ��������� ����������� �������� ���� (�� ����������� �� �����-���� ����������� ������� ������������):
	CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

	// ������� ������������ ����
	if (!CreateDockingWindows())
	{
		TRACE0("�� ������� ������� ������������ ����\n");
		return -1;
	}

	m_wndFileView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndClassView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndVertexView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndEdgeView.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndClassView);
	//DockPane(&m_wndFileView);
	//DockPane(&m_wndVertexView);
	//DockPane(&m_wndEdgeView);
	CDockablePane* pTabbedBar = NULL;
	m_wndFileView.AttachToTabWnd(&m_wndClassView, DM_SHOW, TRUE, &pTabbedBar);
	m_wndVertexView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);
	m_wndEdgeView.AttachToTabWnd(&m_wndVertexView, DM_SHOW, TRUE, &pTabbedBar);
	m_wndOutput.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndOutput);
	m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndProperties);

	// ���������� ��������� ��������� � ����� �� ������ ����������� ��������
	OnApplicationLook(theApp.m_nAppLook);

	// �������� ���������� ���� ������������ ���������� ������
	EnableWindowsDialog(ID_WINDOW_MANAGER, ID_WINDOW_MANAGER, TRUE);

	// ����������� ������� ����� ��������� � ����� ���������� � ��������� ����. ���
	// �������� �������� ������������� ������ �����, ��� ��� �� ������ ������������ ��� ���������.
	ModifyStyle(0, FWS_PREFIXTITLE);

	StartTimer = SetTimer(SweepTimer, Sweep, NULL);
//	FirstStartTimer = SetTimer(Start, 2000, NULL);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CMDIFrameWndEx::PreCreateWindow(cs))
		return FALSE;
	// TODO: �������� ����� Window ��� ����� ����������� ���������
	//  CREATESTRUCT cs
	cs.lpszClass = _T("FlisrClass");

	return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
	BOOL bNameValid;

	// ������� ������������� �������
	CString strClassView;
	bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
	ASSERT(bNameValid);
	if (!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"������������� �������\"\n");
		return FALSE; // �� ������� �������
	}

	// ������� ������������� ������
	CString strFileView;
	bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"������������� ������\"\n");
		return FALSE; // �� ������� �������
	}

	// ������� ���� ������
	CString strOutputWnd;
	bNameValid = strOutputWnd.LoadString(IDS_OUTPUT_WND);
	ASSERT(bNameValid);
	if (!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUTWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"�����\"\n");
		return FALSE; // �� ������� �������
	}

	// ������� ���� �������
	CString strPropertiesWnd;
	bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
	ASSERT(bNameValid);
	if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"��������\"\n");
		return FALSE; // �� ������� �������
	}

	// ������� ������������� �����
	CString strVertexView;
	bNameValid = strVertexView.LoadString(IDS_VERTEX_VIEW);
	ASSERT(bNameValid);
	if (!m_wndVertexView.Create(strVertexView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_VERTEXWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"�����\"\n");
		return FALSE; // �� ������� �������
	}
	// ������� ������������� ������
	CString strEdgeView;
	bNameValid = strEdgeView.LoadString(IDS_EDGE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndEdgeView.Create(strEdgeView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_EDGEWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("�� ������� ������� ���� \"������\"\n");
		return FALSE; // �� ������� �������
	}

	SetDockingWindowIcons(theApp.m_bHiColorIcons);
	return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
	HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndFileView.SetIcon(hFileViewIcon, FALSE);

	HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndClassView.SetIcon(hClassViewIcon, FALSE);

	HICON hOutputBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndOutput.SetIcon(hOutputBarIcon, FALSE);

	HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

	UpdateMDITabbedBarsIcons();
}

// ����������� CMainFrame

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// ����������� ��������� CMainFrame

void CMainFrame::OnWindowManager()
{
	ShowWindowsDialog();
}

void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_VS_2008:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(TRUE);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
		m_wndRibbonBar.SetWindows7Look(FALSE);
	}

	m_wndOutput.UpdateFonts();
	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CMDIFrameWndEx::OnSettingChange(uFlags, lpszSection);
	m_wndOutput.UpdateFonts();
}


void CMainFrame::OnViewVertex()
{
	BOOL bShow = m_wndVertexView.IsVisible() ? FALSE : TRUE;
	m_wndVertexView.ShowPane(bShow, FALSE, FALSE);
	RecalcLayout(FALSE);
}


void CMainFrame::OnUpdateViewVertex(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_wndVertexView.IsVisible());
}


void CMainFrame::OnViewEdge()
{
	BOOL bShow = m_wndEdgeView.IsVisible() ? FALSE : TRUE;
	m_wndEdgeView.ShowPane(bShow, FALSE, FALSE);
	RecalcLayout(FALSE);
}


void CMainFrame::OnUpdateViewEdge(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_wndEdgeView.IsVisible());
}

bool CMainFrame::PutToLogWnd(const CString& Message)
{
	//����� ���� ������
	if (m_wndOutput.GetSafeHwnd())
	{
		if (m_wndOutput.m_wndOutputDebug.GetSafeHwnd())
		{
			m_wndOutput.m_wndOutputDebug.AddString(Message);
			m_wndOutput.AdjustHorzScroll(m_wndOutput.m_wndOutputDebug);
			int ind = m_wndOutput.m_wndOutputDebug.GetCount();
			m_wndOutput.m_wndOutputDebug.SetCurSel(ind - 1);
		}
	}
	/*//����� ���� �������
	m_wndOutput.m_wndOutputDebug.AddString(Message);
	//����� ���� ������
	m_wndOutput.m_wndOutputFind.AddString(Message);*/

	return true;
}

void CMainFrame::OnViewFile()
{
	BOOL bShow = m_wndFileView.IsVisible() ? FALSE : TRUE;
	m_wndFileView.ShowPane(bShow, FALSE, FALSE);
	RecalcLayout(FALSE);
}


void CMainFrame::OnUpdateViewFile(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_wndFileView.IsVisible());
}


void CMainFrame::OnViewClass()
{
	BOOL bShow = m_wndClassView.IsVisible() ? FALSE : TRUE;
	m_wndClassView.ShowPane(bShow, FALSE, FALSE);
	RecalcLayout(FALSE);
}


void CMainFrame::OnUpdateViewClass(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_wndClassView.IsVisible());
}


void CMainFrame::OnViewOutputwnd()
{
	BOOL bShow = m_wndOutput.IsVisible() ? FALSE : TRUE;
	m_wndOutput.ShowPane(bShow, FALSE, FALSE);
	RecalcLayout(FALSE);
}

void CMainFrame::OnUpdateViewOutputwnd(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_wndOutput.IsVisible());
}


int CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	CString str;
	ULONG_PTR dwData = pCopyDataStruct->dwData; // ������������� ���������
	int n = pCopyDataStruct->cbData; // ����� ������ � ������
	BYTE* Buf = new BYTE[n];
	memcpy(Buf, pCopyDataStruct->lpData, n);

	if (dwData == 0x0100) // GredRun
	{
		if (n > 0)
		{
			if (Buf[0] == 1)
			{
				str = CString(Buf + 1);	
				PutToLogWnd(str);
				if (str.Find(_T("PostgreSQL")) >= 0)
				{
					theApp.m_dbType = PostgreSQL;
					theApp.ConnectPostgre(str);
				}
				else
				{
					return false;
				}
				theApp.LoadGraphFile(_T("graph.gg"));
			}
			else if (Buf[0] == 10)
			{
				str = CString(Buf + 1);
				PutToLogWnd(str);
				theApp.LoadGraphFile(str);
			}
		}
	}
	delete[] Buf;

	return dwData;
//	return CMDIFrameWndEx::OnCopyData(pWnd, pCopyDataStruct);
}


void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	CTimeSpan ts = CTime::GetCurrentTime() - m_tFirst;
	if (ts.GetSeconds() > 15/*300*/) m_bRunTimer = true;
	if (m_bRunTimer)
	{
		if (nIDEvent == SweepTimer)
		{
			if (theApp.flagConnectPostgre == true && theApp.flagLoadGraphFile == true)
			{
				theApp.FillInGraph();
				m_wndVertexView.UpdateVertexView();
				m_wndEdgeView.UpdateEdgeView();
				theApp.flagConnectPostgre = false;
				theApp.flagLoadGraphFile = false;
			}
		}
		/*else if (nIDEvent == Start)
		{
			if(theApp.flagConnectPostgre == true && theApp.flagLoadGraphFile == true)
			{
				theApp.flagConnectPostgre = false;
				theApp.flagLoadGraphFile = false;
			}
			KillTimer(FirstStartTimer);
		}*/
	}
	CMDIFrameWndEx::OnTimer(nIDEvent);
}
