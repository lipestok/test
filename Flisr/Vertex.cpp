// Vertex.cpp : implementation file
//

#include "stdafx.h"
#include "Flisr.h"
#include "Vertex.h"
#include "afxdialogex.h"


// CVertex dialog

IMPLEMENT_DYNAMIC(CVertex, CDialogEx)

CVertex::CVertex(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_VERTEX_WND, pParent)
{

}

CVertex::~CVertex()
{
}

void CVertex::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CVertex, CDialogEx)
END_MESSAGE_MAP()


// CVertex message handlers
