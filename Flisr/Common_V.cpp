#include "stdafx.h"
#include "Common.h"
#include "StdiofileEx.h"
#include <locale.h>

// namespace support is now required
#ifdef NO_NAMESPACE
#	error namespace support is now required
#endif
#define NAMESPACE_BEGIN(x) namespace x {
#define NAMESPACE_END }

/*
COMMON / COMM / UNOM, U0, AMAX, UMIN, UMAX, KRAT, M0,
*DELU1, DELU2, AMAX1, SQR3, KSQR,
*KPE, NFP, NFR, KIMAX, KPMAX, BP, EPSIL
*/
NAMESPACE_BEGIN(COMM)
	complex <double> UNOM, U0;
	double AMAX, UMIN, UMAX, KRAT;
	int M0;
	double DELU1, DELU2, AMAX1, SQR3, KSQR;
	int KPE, NFP, NFR, KIMAX, KPMAX;
	double BP, EPSIL;
NAMESPACE_END

NAMESPACE_BEGIN(POR)
	int M, N, NSM, NBU, N1, NSMM, NSMV;
NAMESPACE_END

/*	  OPEN (NFR, FILE='LFS200.W00')		//������
      OPEN (NFP, FILE='LIST' )			//
      OPEN (NFP1, FILE='LIS200.W01' )	//������
      OPEN (NFP2, FILE='LIS200.W02' )
      OPEN (NFP3, FILE='LIS200.W03' )
      OPEN (NFP4, FILE='LIS200.W04' )
      OPEN (NFP5, FILE='LIS200.W05' )
      OPEN (NFP6, FILE='LIS200.W06' )
      OPEN (NFP7, FILE='LIS200.W07' )
      OPEN (NFP8, FILE='LIS200.W08' )
      OPEN (NFP9, FILE='LIS200.W09' )
      OPEN (NFP10, FILE='LIS200.W10' )*/

CStdioFile fNFR;  //������
CStdioFile fNFP1; //������

/*void INP202(int KST, int KSN, int KTR, int MVK[], double ADOP[], int KODPOT, int KODPI, double DELQU,
	int KODPID, complex<double> SN[], complex<double> SG[], double UY[], int M1[], int M2[], int NAME[], int NAME1[], int NAME2[],
	complex<double> X[], double REPR[], double KT[], double GNN[], double GNV[], double VNN[], double VSN[], int NMZK, int NSMK, int NSML,
	double UOP[], double QMN[], double QMX[], int IKOY, int KODPQG, double KRATG, double QKY[], CStdioFile NFP1,
	int PRIZ[], int NOMERY[], int MAXNOM, int MKPARV, int MKEPV, double KRATQG,
	int MKSTEK, int NMAX, int MMAX, double REPRIM, double UPRMAX, int NAMET, double SHUNT[], double YMIN);*/

/*
INTEGER * 2 MYPU(186), MYPQ(210), MYPQ2(210), NTV(205),
*NSTN(211), SI(211), KVIY(211), MYY(210),
*NYNS1(211), NYNS2(211), NPB(50),
*IH(211), IYSTEK(4360), IL(210),
*IE(211), IYPU(211),
*IYR, IMON, IDAY, IHR, IMIN, ISEC, I100TH
*/
int MYPU(186), MYPQ(210), MYPQ2(210), NTV(205),
NSTN(211), SI(211), KVIY(211), MYY(210),
NYNS1(211), NYNS2(211), NPB(50),
IH(211), IYSTEK(4360), IL(210),
IE(211), IYPU(211),
IYR, IMON, IDAY, IHR, IMIN, ISEC, I100TH;

/*
INTEGER   KPE, KIMAX, M0, KPMAX, NFP, NFR, KST, KOY, IKOY,
*M, N, NSM, N1, NSMM, KTR, KSN, NSMV, NVRPV, KNY,
*KODPOT, NMZK, KODPID, KODPQG, NYNP, NYNQ, NBU,
*KP, KI, NFP1, NFP2, NFP3, NFP4, NFP5, NFP6,
*NFP7, NFP8, NFP9, NFP10, KODPI, KNDEE, KSTEK, MKSTEK,
*KNDE, KNDEL, NSMK, NSML, NS0, MKPARV, KPARV,
*MAXNOM, KEPV, MKEPV, NMAX, MMAX, NAMET
*/
int KST, KOY, IKOY,
M, N, NSM, N1, NSMM, KTR, KSN, NSMV, NVRPV, KNY,
KODPOT, NMZK, KODPID, KODPQG, NYNP, NYNQ, NBU,
KP, KI, NFP1, NFP2, NFP3, NFP4, NFP5, NFP6,
NFP7, NFP8, NFP9, NFP10, KODPI, KNDEE, KSTEK, MKSTEK,
KNDE, KNDEL, NSMK, NSML, NS0, MKPARV, KPARV,
MAXNOM, KEPV, MKEPV, NMAX, MMAX, NAMET;

/*
REAL * 4
* BP, AMAX, AMAX1, UMIN, UMAX, KRAT, DELQU, REPRIM,
*DELU1, DELU2, KSQR, SQR3, UPRMAX, YMIN,
*VREM0, TVREM, SVREM, QGENS, QGENL, QPOTR, KRATG, KRATQG,
*PNEBM, QNEBM, DELPM, DELQM, PGENS, PPOTR, QKYSUM, EPSIL
*/
double DELQU, REPRIM, UPRMAX, YMIN,
VREM0, TVREM, SVREM, QGENS, QGENL, QPOTR, KRATG, KRATQG,
PNEBM, QNEBM, DELPM, DELQM, PGENS, PPOTR, QKYSUM;

/*
COMPLEX * 8 UNOM, U0, SGEN, SNAG,
*SPOT, SPOTT
*/
complex < double> SGEN, SNAG, SPOT, SPOTT;

/*REAL*4    PY(211), QY(211), PZ(211), QZ(211),
VNN(10),GNN(10),GNV(10),VSN(10),
UOP(211), QMN(211), QMX(211),  UY(211),
QDELX2(186), QDELT(186), QL(211), QKY(211), SHUNT(211)*/

double    PY[], QY[], PZ[], QZ[],
VNN[10], GNN[10], GNV[10], VSN[10],
UOP[211], QMN[211], QMX[211], UY[211],
QDELX2[186], QDELT[186], QL[211], QKY[211], SHUNT[211];

/*COMPLEX*8 IY(211), UOT(186), IYD(211),
 U(211), UOPOR(186), MYNS2(211), MYNS3(211)*/

complex < double> IY[211], UOT[186], IYD[211],
U[211], UOPOR[186], MYNS2[211], MYNS3[211];

/*COMPLEX*8 YPH(3000), YPE(9000),
SN(211), SG(211), IT(419),
DYP(211), DYPE(211),
MYNS1(211)*/

complex < double> YPH[3000], YPE[9000],
SN[211], SG[211], IT[419],
DYP[211], DYPE[211],
MYNS1[211];

/*INTEGER*2 NYPH(3000), IYPH(3000), NYPE(9000), IYPE(9000),
IYPL(2500), NYPL(2500), ISSH(2500)*/

int NYPH[3000], IYPH[3000], NYPE[9000], IYPE[9000],
IYPL[2500], NYPL[2500], ISSH[2500];

/*INTEGER*2 M3(4200), M3M(4200), M3V(3000), NAME(211),
M1(419), M2(419), MVK(419), PRIZ(419), NOMERY(3100),
NAME1(419), NAME2(419), NPARV(200),
M1PV(100), M2PV(100)*/

int M3[4200], M3M[4200], M3V[3000], NAME[211],
M1[419], M2[419], MVK[419], PRIZ[419], NOMERY[3100],
NAME1[419], NAME2[419], NPARV[200],
M1PV[100], M2PV[100];

/*REAL*4    ADOP(419), KT(419), REPR(419),
PVN(419), PVK(419), QVN(419), QVK(419),
KTPV(100), YPV(100), ADOPPV(100)*/

double ADOP[419], KT[419], REPR[419],
PVN[419], PVK[419], QVN[419], QVK[419],
KTPV[100], YPV[100], ADOPPV[100];

/*COMPLEX*8 X(419), ZPV(100)*/

complex < double> X[419], ZPV[100];


using namespace COMM;

void LFS210()
{
	complex <double> i(0.0, 1.0), i2, c(5.0, 6.0);
	i2 = pow(i, 2);
	CString s;
	NFR = 10;
	NFP = 11;
	NFP1 = 21;
//	NFP2 = 22; // ���� ��� ������ 
	NFP3 = 23;
	NFP4 = 24;
	NFP5 = 25;
	NFP6 = 26;
	NFP7 = 27;
	NFP8 = 28;
	NFP9 = 29;
	NFP10 = 30;
/*
	OPEN(NFR, FILE = 'LFS200.W00')
		OPEN(NFP, FILE = 'LIST')
		OPEN(NFP1, FILE = 'LIS200.W01')
		OPEN(NFP2, FILE = 'LIS200.W02')
		OPEN(NFP3, FILE = 'LIS200.W03')
		OPEN(NFP4, FILE = 'LIS200.W04')
		OPEN(NFP5, FILE = 'LIS200.W05')
		OPEN(NFP6, FILE = 'LIS200.W06')
		OPEN(NFP7, FILE = 'LIS200.W07')
		OPEN(NFP8, FILE = 'LIS200.W08')
		OPEN(NFP9, FILE = 'LIS200.W09')
		OPEN(NFP10, FILE = 'LIS200.W10')
*/
	CStdioFile NFP2(_T("LIS200_NEW.W02"), CFile::modeWrite | CFile::modeCreate);

/*
	CALL GETDAT(IYR, IMON, IDAY)
		CALL GETTIM(IHR, IMIN, ISEC, I100TH)
		VREM0 = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.
		TVREM = VREM0
		NVRPV = 0
		WRITE(NFP2, 10)  IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH
		10 FORMAT(12X, '����: ', 2I3, I5, ' ����� :', I4, ' ���', I4, ' ���', I4,
			*'.', I2, ' c')
*/
	CTime t = CTime::GetCurrentTime();
	IYR = t.GetYear();

	I100TH = 0;
	VREM0 = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	TVREM = VREM0;
	NVRPV = 0;
	s.Format(_T("             Date: %3d %3d %10d Time :%3d h %3d m %2d s\n"), IDAY, IMON, IYR, IHR, IMIN, ISEC);
	NFP2.WriteString(s);

	INP202(KST, KSN, KTR, MVK, ADOP, KODPOT, KODPI, DELQU,
		KODPID, SN, SG, UY, M1, M2, NAME, NAME1, NAME2,
		X, REPR, KT, GNN, GNV, VNN, VSN, NMZK, NSMK, NSML,
		UOP, QMN, QMX, IKOY, KODPQG, KRATG, QKY, NFP1,
		PRIZ, NOMERY, MAXNOM, MKPARV, MKEPV, KRATQG,
		MKSTEK, NMAX, MMAX, REPRIM, UPRMAX, NAMET, SHUNT,
		YMIN);

	NFP2.Close();
}

/*C ======================================================================
C                   �TEH�E � �E�AT� �CXO�H�X �AHH�X
C ======================================================================*/
/*      SUBROUTINE INP202( KST, KSN, KTR, MVK, ADOP, KODPOT, KODPI,DELQU,
     *                   KODPID, SN, SG, UY, M1, M2,NAME ,NAME1,NAME2,
     *                   X, REPR, KT, GNN, GNV, VNN,VSN,NMZK,NSMK,NSML,
     *                   UOP, QMN, QMX, IKOY, KODPQG, KRATG, QKY, NFP1,
     *                   PRIZ, NOMERY, MAXNOM, MKPARV, MKEPV, KRATQG,
     *                   MKSTEK, NMAX, MMAX, REPRIM, UPRMAX,NAMET,SHUNT,
     *                   YMIN )*/
int ArSize(0);
void INP202(int KST, int KSN, int KTR, int MVK[], double ADOP[], int KODPOT, int KODPI, double DELQU,
	int KODPID, COMPLEX SN[], COMPLEX SG[], double UY[], int M1[], int M2[], int NAME[], int NAME1[], int NAME2[],
	COMPLEX X[], double REPR[], double KT[], double GNN[], double GNV[], double VNN[], double VSN[], int NMZK, int NSMK, int NSML,
	double UOP[], double QMN[], double QMX[], int IKOY, int KODPQG, double KRATG, double QKY[], int NFP1,
	int PRIZ[], int NOMERY[], int MAXNOM, int MKPARV, int MKEPV, double KRATQG,
	int MKSTEK, int NMAX, int MMAX, double REPRIM, double UPRMAX, int NAMET, double SHUNT[], double YMIN)
{
	/*INTEGER * 4 NAMEI, KST, KSN, KTR, KODPID, KODPQG, IKOY, KODPI,
		*KIMAX, KPMAX, NFP, NFR, M0, KPE, I, NFP1, J, J1, J2,
		*M, N, NSM, NBU, N1, NSMM, NMZK, KODPOT, NSMK, NSML,
		*MAXNOM, MKPARV, MKEPV, MKSTEK, NMAX, MMAX, NAMET
		REAL * 4  DELU1, DELU2, BP, EPSIL, KSQR, SQR3, U00, DELQU,
		*AMAX, UMIN, UMAX, KRAT, AAA, KRATG, YMIN,
		*UOPI, QMNI, QMXI, QKYI, UYI, KRATQG, REPRIM, UPRMAX
		COMPLEX * 8 UNOM, U0, SNI, SGI
		CHARACTER * 40 FNAME
		$LARGE
		REAL * 4  UY(1), UOP(1), QMN(1), QMX(1), QKY(1), SHUNT(1),
		*GNN(1), GNV(1), VNN(1), VSN(1)
		COMPLEX * 8 SN(1), SG(1)
		C
		INTEGER * 2 M1(1), M2(1), MVK(1), PRIZ(1), NOMERY(1), NAME(1),
		*NAME1(1), NAME2(1)
		REAL * 4    ADOP(1), KT(1), REPR(1)
		COMPLEX * 8 X(1)*/

	int NAMEI,I, J, J1, J2/*, M, N, NSM, NBU, N1, NSMM*/;
	double  U00, AAA, UOPI, QMNI, QMXI, QKYI, UYI,f1,f2;
	COMPLEX SNI, SGI;
	CString FNAME, sOutput, sInput, tmpStr;
	char ch_name[40];
	wchar_t ch_name2[40];

	setlocale(LC_ALL, ".1251");
	LPWSTR ss(_T(""));

	/* ======================================================================
C     1.  BBO� � ����� �CXO�H�X �AHH�X �O CET�
C ======================================================================
C
C   1.1  �EPEMEHH�X � KOHCTAHT:  ---------------------------------------
C
      READ (NFR, 126 ) FNAME
  126 FORMAT (A40)
      WRITE (NFP1,127)  FNAME
  127 FORMAT (/20X,'� � � � � : ',A40/)
C      WRITE (NFP1,33)
C   33 FORMAT(25X,' �������� ������.'/)
      WRITE (NFP1,34)
   34 FORMAT(15X,' K � � � � � � � �   �   � � � � � � � � � �'/)*/

	const CString sPoints(_T("............"));
	fNFR.Open(_T("LFS200.25"), CFile::modeRead);
	fNFR.ReadString(FNAME);
	sOutput.Format(_T("                    � � � � � : %s \n                          �������� ������. \n               K � � � � � � � �   �   � � � � � � � � � �\n "), FNAME);
	fNFP1.Open(_T("LIS200.W01"), CFile::modeWrite | CFile::modeCreate);
	fNFP1.WriteString(sOutput);

	/*READ (NFR,141) N //������ I5
      N1 = N + 1
      WRITE ( NFP1,151 ) N1
  151 FORMAT(4x,' ����� ����� ���� ..............................',
     *       12('.'),I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	N = _wtoi(tmpStr);
	N1 = N + 1;
	sOutput.Format(_T("     ����� ����� ���� ..............................%s     %d\n "), sPoints, N1);
	fNFP1.WriteString(sOutput);

	
    /*READ (NFR,141) M //������ I5
      WRITE ( NFP1,152 ) M
  152 FORMAT(4x,' ����� ������ ���� .............................',
     *       12('.'),I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	M = _wtoi(tmpStr);
	sOutput.Format(_T("     ����� ������ ���� .............................%s     %d\n "), sPoints, M);
	fNFP1.WriteString(sOutput);

      /*READ (NFR,141) KST //������ I5
      WRITE ( NFP1,153 ) KST
  153 FORMAT(4x,' ����� ������� ������� .........................',
     *       12('.'),I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KST = _wtoi(tmpStr);
	sOutput.Format(_T("     ����� ������� ������� .........................%s     %d\n "), sPoints, KST);
	fNFP1.WriteString(sOutput);
	
    /*READ (NFR,141) KSN //������ I5
      WRITE ( NFP1,154 ) KSN
  154 FORMAT(4x,' ����� �������� ���������� ������� .............',
     *       12('.'),I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KSN = _wtoi(tmpStr);
	sOutput.Format(_T("     ����� �������� ���������� ������� .............%s     %d\n "), sPoints, KSN);
	fNFP1.WriteString(sOutput);

      /*READ (NFR,141) KTR //������ I5
      WRITE ( NFP1,155 ) KTR
  155 FORMAT(4x,' ����� ��������������� ���� ....................',
     *       12('.'),I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KTR = _wtoi(tmpStr);
	sOutput.Format(_T("     ����� ��������������� ���� ....................%s     %d\n "), sPoints, KTR);
	fNFP1.WriteString(sOutput);

	/*READ(NFR, 141) MAXNOM //������ I5
	WRITE(NFP1, 5155) MAXNOM
	5155 FORMAT(4x, ' ������������ ����p ���� .......................',
		*12('.'), I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MAXNOM = _wtoi(tmpStr);
	sOutput.Format(_T("     ������������ ����p ���� .......................%s     %d\n "), sPoints, MAXNOM);
	fNFP1.WriteString(sOutput);

	/*READ(NFR, 141) MKPARV //������ I5
	WRITE(NFP1, 5156) MKPARV
	5156 FORMAT(4x, ' ������������ ����� ��p��������� ������.........',
		*12('.'), I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKPARV = _wtoi(tmpStr);
	sOutput.Format(_T("     ������������ ����� ��p��������� ������.........%s     %d\n "), sPoints, MKPARV);
	fNFP1.WriteString(sOutput);

	/*READ(NFR, 141) MKEPV //������ I5
	WRITE(NFP1, 5157) MKEPV
	5157 FORMAT(4x, ' ������������ ����� �����. ��p��������� ������..',
		*12('.'), I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKEPV = _wtoi(tmpStr);
	sOutput.Format(_T("     ������������ ����� �����. ��p��������� ������..%s     %d\n "), sPoints, MKEPV);
	fNFP1.WriteString(sOutput);

	/*READ(NFR, 142) U00 //������ F7.2
	142 FORMAT(F7.2)
	U0 = U00
	WRITE(NFP1, 156) U0*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%lf "),f1); //������ F7.2 ????

	U0 = U00 = COMPLEX(f1, 0.0);
	sOutput.Format(_T("     ���������� �������������� ����, ��...%lf.2     %d\n "), sPoints, U0.real()); // ������ 2F7.2 ???
	fNFP1.WriteString(sOutput);

	/*READ(NFR, 141) NBU //������ I5
	WRITE(NFP1, 157) NBU
	157 FORMAT(4x, ' ����� �������������� ���� .....................',
		*12('.'), I5)
	156 FORMAT(4x, ' ���������� �������������� ����, ��... ',
			*12('.'), 2F7.2)
	141 FORMAT(I5)*/

	fNFR.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	NBU = _wtoi(tmpStr);
	sOutput.Format(_T("     ����� �������������� ���� .....................%s     %d\n "), sPoints, NBU);
	fNFP1.WriteString(sOutput);

	fNFR.Close();
	fNFP1.Close();
}
