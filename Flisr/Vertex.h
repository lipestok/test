#pragma once


// CVertex dialog

class CVertex : public CFormView
{
	DECLARE_DYNAMIC(CVertex)

public:
	CVertex(CWnd* pParent = NULL);   // standard constructor
	virtual ~CVertex();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VERTEX_WND };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
