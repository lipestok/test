// ���� �������� ��� MFC Samples ������������� ���������������� ����������������� ���������� Fluent �� ������ MFC � Microsoft Office
// ("Fluent UI") � ��������������� ������������� ��� ���������� �������� � �������� ���������� �
// ����������� �� ������ Microsoft Foundation Classes � ��������� ����������� ������������,
// ���������� � ����������� ����������� ���������� MFC C++.  
// ������� ������������� ���������� �� �����������, ������������� ��� ��������������� Fluent UI �������� ��������.  
// ��� ��������� �������������� �������� � ����� ������������ ��������� Fluent UI �������� ���-����
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) ���������� ���������� (Microsoft Corp.)
// ��� ����� ��������.

#pragma once

#include "ViewTree.h"

class CVertexToolBar : public CMFCToolBar
{
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CVertexView : public CDockablePane
{
public:
	CVertexView();
	virtual ~CVertexView();

	void AdjustLayout();
	void OnChangeVisualStyle();
	CViewTree m_wndVertexView;
	int IndexMainSubstation, IndexConsumer;
protected:
	CVertexToolBar m_wndToolBar;
	
	//CTreeCtrlEx		m_cTree;
	CImageList m_VertexViewImages;
	UINT m_nCurrSort;

	void FillVertexView();

// ���������������
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnExpandAll();
	afx_msg void OnCollapseAll();
	void ExpandBranch(CTreeCtrl& tree, HTREEITEM hItem, BOOL bExpand);

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnVertexAddMemberFunction();
	afx_msg void OnVertexAddMemberVariable();
	afx_msg void OnVertexDefinition();
	afx_msg void OnVertexProperties();
	afx_msg void OnNewFolder();
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//afx_msg LRESULT OnChangeActiveTab(WPARAM, LPARAM);
	afx_msg void OnSort(UINT id);
	afx_msg void OnUpdateSort(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnListConsumer();
	afx_msg void OnMainsubstation();
	void UpdateVertexView();
};

