﻿#include "stdafx.h"
#include "Common.h"
#include "Flisr.h"
// namespace support is now required
#ifdef NO_NAMESPACE
#	error namespace support is now required
#endif
#define NAMESPACE_BEGIN(x) namespace x {
#define NAMESPACE_END }

/*
COMMON / COMM / UNOM, U0, AMAX, UMIN, UMAX, KRAT, M0,
*DELU1, DELU2, AMAX1, SQR3, KSQR,
*KPE, NFP, NFR, KIMAX, KPMAX, BP, EPSIL
*/
NAMESPACE_BEGIN(COMM)
	COMPLEX UNOM, U0;
	double AMAX, UMIN, UMAX, KRAT;
	int M0;
	double DELU1, DELU2, AMAX1, SQR3, KSQR;
	int KPE, NFP, NFR, KIMAX, KPMAX;
	double BP, EPSIL;
NAMESPACE_END

/*
COMMON /POR/  M, N, NSM, NBU, N1, NSMM, NSMV
*/
NAMESPACE_BEGIN(POR)
	int M, N, NSM, NBU, N1, NSMM, NSMV;
NAMESPACE_END

/*
INTEGER * 2 MYPU(186), MYPQ(210), MYPQ2(210), NTV(205),
*NSTN(211), SI(211), KVIY(211), MYY(210),
*NYNS1(211), NYNS2(211), NPB(50),
*IH(211), IYSTEK(4360), IL(210),
*IE(211), IYPU(211),
*IYR, IMON, IDAY, IHR, IMIN, ISEC, I100TH
*/
int MYPU[186], MYPQ[210], MYPQ2[210], NTV[205],
NSTN[211], SI[211], KVIY[211], MYY[210],
NYNS1[211], NYNS2[211], NPB[50],
IH[211], IYSTEK[4360], IL[210],
IE[211], IYPU[211],
IYR, IMON, IDAY, IHR, IMIN, ISEC, I100TH;

/*
INTEGER   KPE, KIMAX, M0, KPMAX, NFP, NFR, KST, KOY, IKOY,
*M, N, NSM, N1, NSMM, KTR, KSN, NSMV, NVRPV, KNY,
*KODPOT, NMZK, KODPID, KODPQG, NYNP, NYNQ, NBU,
*KP, KI, NFP1, NFP2, NFP3, NFP4, NFP5, NFP6,
*NFP7, NFP8, NFP9, NFP10, KODPI, KNDEE, KSTEK, MKSTEK,
*KNDE, KNDEL, NSMK, NSML, NS0, MKPARV, KPARV,
*MAXNOM, KEPV, MKEPV, NMAX, MMAX, NAMET
*/
int KST, KOY, IKOY,
KTR, KSN, NVRPV, KNY,
KODPOT, NMZK, KODPID, KODPQG, NYNP, NYNQ,
KP, KI, NFP1, NFP2, NFP3, NFP4, NFP5, NFP6,
NFP7, NFP8, NFP9, NFP10, KODPI, KNDEE, KSTEK, MKSTEK,
KNDE, KNDEL, NSMK, NSML, NS0, MKPARV, KPARV,
MAXNOM, KEPV, MKEPV, NMAX, MMAX, NAMET;

/*
REAL * 4
* BP, AMAX, AMAX1, UMIN, UMAX, KRAT, DELQU, REPRIM,
*DELU1, DELU2, KSQR, SQR3, UPRMAX, YMIN,
*VREM0, TVREM, SVREM, QGENS, QGENL, QPOTR, KRATG, KRATQG,
*PNEBM, QNEBM, DELPM, DELQM, PGENS, PPOTR, QKYSUM, EPSIL
*/
double DELQU, REPRIM,
UPRMAX, YMIN,
VREM0, TVREM, SVREM, QGENS, QGENL, QPOTR, KRATG, KRATQG,
PNEBM, QNEBM, DELPM, DELQM, PGENS, PPOTR, QKYSUM;

/*
COMPLEX * 8 UNOM, U0, SGEN, SNAG,
*SPOT, SPOTT
*/
COMPLEX SGEN, SNAG, SPOT, SPOTT;

/*
REAL*4    PY(211), QY(211), PZ(211), QZ(211),
VNN(10),GNN(10),GNV(10),VSN(10),
UOP(211), QMN(211), QMX(211),  UY(211),
QDELX2(186), QDELT(186), QL(211), QKY(211), SHUNT(211)
*/

double  PY[211], QY[211], PZ[211], QZ[211],
VNN[10], GNN[10], GNV[10], VSN[10],
UOP[211], QMN[211], QMX[211], UY[211],
QDELX2[186], QDELT[186], QL[211], QKY[211], SHUNT[211];

/*
COMPLEX*8 IY(211), UOT(186), IYD(211),
U(211), UOPOR(186), MYNS2(211), MYNS3(211)
*/
COMPLEX IY[211], UOT[186], IYD[211],
U[211], UOPOR[186], MYNS2[211], MYNS3[211];

/*
COMPLEX*8 YPH(3000), YPE(9000),
SN(211), SG(211), IT(419),
DYP(211), DYPE(211),
MYNS1(211)
*/
COMPLEX YPH[3000], YPE[9000],
SN[211], SG[211], IT[419],
DYP[211], DYPE[211],
MYNS1[211];

/*
INTEGER*2 NYPH(3000), IYPH(3000), NYPE(9000), IYPE(9000),
IYPL(2500), NYPL(2500), ISSH(2500)
*/
int NYPH[3000], IYPH[3000], NYPE[9000], IYPE[9000],
IYPL[2500], NYPL[2500], ISSH[2500];

/*
INTEGER*2 M3(4200), M3M(4200), M3V(3000), NAME(211),
M1(419), M2(419), MVK(419), PRIZ(419), NOMERY(3100),
NAME1(419), NAME2(419), NPARV(200),
M1PV(100), M2PV(100)
*/
int M3[4200], M3M[4200], M3V[3000], NAME[211],
M1[419], M2[419], MVK[419], PRIZ[419], NOMERY[3100],
NAME1[419], NAME2[419], NPARV[200],
M1PV[100], M2PV[100];

/*
REAL*4    ADOP(419), KT(419), REPR(419),
PVN(419), PVK(419), QVN(419), QVK(419),
KTPV(100), YPV(100), ADOPPV(100)
*/
double ADOP[419], KT[419], REPR[419],
PVN[419], PVK[419], QVN[419], QVK[419],
KTPV[100], YPV[100], ADOPPV[100];

/*
COMPLEX*8 X(419), ZPV(100)
*/
COMPLEX X[419], ZPV[100];

using namespace COMM;
using namespace POR;

CStdioFile NFR_File, NFP_File, NFP1_File, NFP2_File, NFP5_File, NFP8_File, NFP6_File, NFP10_File; //запись
void LFS210()
{
	complex <double> i(0.0, 1.0), i2, c(5.0, 6.0);
	i2 = pow(i, 2);
	CString s;
	NFR = 10;
	NFP = 11;
	NFP1 = 21;
//	NFP2 = 22; // Файл для вывода 
	NFP3 = 23;
	NFP4 = 24;
	NFP5 = 25;
	NFP6 = 26;
	NFP7 = 27;
	NFP8 = 28;
	NFP9 = 29;
	NFP10 = 30;
/*
	OPEN(NFR, FILE = 'LFS200.W00')
		OPEN(NFP, FILE = 'LIST')
		OPEN(NFP1, FILE = 'LIS200.W01')
		OPEN(NFP2, FILE = 'LIS200.W02')
		OPEN(NFP3, FILE = 'LIS200.W03')
		OPEN(NFP4, FILE = 'LIS200.W04')
		OPEN(NFP5, FILE = 'LIS200.W05')
		OPEN(NFP6, FILE = 'LIS200.W06')
		OPEN(NFP7, FILE = 'LIS200.W07')
		OPEN(NFP8, FILE = 'LIS200.W08')
		OPEN(NFP9, FILE = 'LIS200.W09')
		OPEN(NFP10, FILE = 'LIS200.W10')
*/
	NFR_File.Open(_T("LFS200.25"), CFile::modeRead);
	NFP_File.Open(_T("LIST"), CFile::modeWrite | CFile::modeCreate);
	NFP1_File.Open(_T("LIS200.W01"), CFile::modeWrite | CFile::modeCreate);
	NFP2_File.Open(_T("LIS200_NEW.W02"), CFile::modeWrite | CFile::modeCreate);
	NFP5_File.Open(_T("LIS200.W05"), CFile::modeWrite | CFile::modeCreate);
	NFP8_File.Open(_T("LIS200.W08"), CFile::modeWrite | CFile::modeCreate);
	NFP6_File.Open(_T("LIS200.W06"), CFile::modeWrite | CFile::modeCreate);
	NFP10_File.Open(_T("LIS200.W10"), CFile::modeWrite | CFile::modeCreate);
/*
	CALL GETDAT(IYR, IMON, IDAY)
		CALL GETTIM(IHR, IMIN, ISEC, I100TH)
		VREM0 = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.
		TVREM = VREM0
		NVRPV = 0
		WRITE(NFP2, 10)  IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH
		10 FORMAT(12X, 'Дата: ', 2I3, I5, ' Время :', I4, ' час', I4, ' мин', I4,
			*'.', I2, ' c')
*/
	CTime t = CTime::GetCurrentTime();
	IYR = t.GetYear();
	IMON = t.GetMonth();
	IDAY = t.GetDay();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	I100TH = 0;
	VREM0 = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	TVREM = VREM0;
	NVRPV = 0;
	s.Format(_T("%sДата: %3d%3d%5d Время :%4d час%4d мин%4d.%2d с\n"), CString(_T(' '), 12), IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH);
	NFP2_File.WriteString(s);
	s.Format(_T("\n%s┌%s┐"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP2_File.WriteString(s);
	theApp.WriteToLog(s);
	s = (_T("     |  Copyright (c) 1988-1993, Хозяинов M.A. All rights reserved  │\n"));
	NFP2_File.WriteString(s);
	theApp.WriteToLog(s);
	s = (_T("     |                        Version 1.00                          │\n"));
	theApp.WriteToLog(s);
	s.Format(_T("%s└%s┘\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP2_File.WriteString(s);
	theApp.WriteToLog(s);
	EPSIL = .00001;
	KPE = 0;
/*
	Чтение данных из входного потока:
*/
	theApp.WriteToLog(_T(" Ввод исходных данных.\n"));

	INP202(/*KST, KSN, KTR, MVK, ADOP, KODPOT, KODPI, DELQU,
		KODPID, SN, SG, UY, M1, M2, NAME, NAME1, NAME2,
		X, REPR, KT, GNN, GNV, VNN, VSN, NMZK, NSMK, NSML,
		UOP, QMN, QMX, IKOY, KODPQG, KRATG, QKY, NFP1,
		PRIZ, NOMERY, MAXNOM, MKPARV, MKEPV, KRATQG,
		MKSTEK, REPRIM, UPRMAX, NAMET, SHUNT,
		YMIN*/);

	IN202(/*ADOP, X, KVIY, UOP, MYPU, MYPQ, KOY,
		KT, KTR, UY, SG, SN, KST, PGENS, PPOTR,
		GNN, GNV, NSTN, KSN, REPR, NTV, IKOY,
		NYNS1, NYNS2, VNN, KRATG, KNDE, KNDEL,
		PY, QY, IY, MYPQ2, QGENS, QPOTR, QKY, QKYSUM,
		MYY, DYP, MYNS1, MYNS2, YPH, NSMK, NSML,
		NYPH, IYPH, IH, IYSTEK, IL, IYPL, NYPL, ISSH,
		DYPE, IE, YPE, IYPE, NYPE, KNDEE, KSTEK, IYD,
		NPARV, KPARV, MKPARV, KEPV, MKEPV, KODPID,
		M1PV, M2PV, ADOPPV, KTPV, ZPV, YPV,
		NAME1, NAME2, NAME, KRATQG, MKSTEK,
		REPRIM, UPRMAX, NAMET, QMN, QMX, SHUNT, YMIN*/);
	U[N1] = U0;
	/*
		Цикл расчета установившегося режима
	*/
	//CALL PRE015(QGENL, QGENS, QPOTR, PGENS, PPOTR, QKYSUM, NFP2)
	PRE015();
	KNY = N - IKOY;
/*	CALL  CLF210(QL, SG, SN, U, MYPU, M3V, M3, NFP6, NFP8,				
		*PY, QY, IY, PZ, QZ, UOP, QMN, QMX, NFP2,
		*MYPQ, KVIY, UY, MYPQ2, KODPI, IYSTEK, NFP7,
		*M1, M2, X, KT, MVK, IKOY, KNY, NFP5,
		*UOT, QDELX2, IT, UOPOR, NAME, NFP9, QDELT,
		*NYNS1, NYNS2, KODPQG, NSTN, NMZK, DELQU,
		*VSN, REPR, PVN, PVK, QVN, QVK, QKY, NAME1, NAME2,
		*MYY, IL, IYPL, NYPL, ISSH, IH, IYPH, NFP3,
		*NYPH, MYNS1, MYNS2, DYP, YPH, MYNS3, NFP4,
		*DYPE, IE, YPE, IYPE, NYPE, KNDEE, KSTEK, IYPU, IYD,
		*MKSTEK, SHUNT, YMIN)
*/
	// CLF210();
	NVRPV = NVRPV + 1;
/*
	CALL PVREM(NVRPV, TVREM);
*/
	PVREM();
/*
	Заключительный расчет параметров режима
	Расчет токов по ветвям сети
*/
/*
	CALL RTO015 ( IT, U, X, MVK, M1, M2, KT, ADOP, KP, NPB )
    CALL PVP118 ( ADOP, IT, MVK, M1, M2, NAME1, NAME2, NFP5 )
*/
	RTO015();
	PVP118();
/*
	Расчет мощностей по ветвям сети
*/
	KI = 0;
/*
	CALL RMV118 ( M1, M2, U, IT, REPR, MVK, PVN, PVK, QVN, QVK,KT,
*              SPOT, SPOTT, KODPOT, KI, NAME1,NAME2,NFP6,NFP8 )
*/
	 RMV118();
/*
	Расчет небалансов мощностей в узлах сети
*/
/*
	CALL ZKS016 ( PZ, QZ, M3V, M3, M1, PVN, PVK, QVN, QVK,
*              SGEN, SNAG, SPOT, QL, SPOTT, SN, SG, IY, U,
*              PNEBM , NYNP, QNEBM , NYNQ, DELPM, DELQM, QKY,
*              NFP10 )
*/
	 ZKS016();
	 NVRPV = NVRPV + 1;
/*
	CALL PVREM ( NVRPV, TVREM )
*/
	PVREM();
/*
	CALL GETDAT ( IYR, IMON, IDAY )
	CALL GETTIM ( IHR, IMIN, ISEC, I100TH )
*/
	t = CTime::GetCurrentTime();
	IYR = t.GetYear();
	IMON = t.GetMonth();
	IDAY = t.GetDay();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	TVREM = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	SVREM = TVREM - VREM0;
	s.Format(_T(" Суммарное время счета составило: %10.2lf c."), SVREM);
	NFP2_File.WriteString(s);
	s.Format(_T("   NSM, KNDE, KNDEL, KNDEE, KSTEK %7d%7d%7d%7d%7d"), NSM, KNDE, KNDEL, KNDEE, KSTEK);
	NFP_File.WriteString(s);
	s.Format(_T("   NS0, NSMK, NSML, NSMM, MKSTEK  %7d%7d%7d%7d%7d"), NS0, NSMK, NSML, NSMM, MKSTEK);
	NFP_File.WriteString(s);
	s.Format(_T(" Суммарное время счета составило: %10.2lf c."), SVREM);
	NFP_File.WriteString(s);
	s.Format(_T("            Дата: %3d%3d%5d Время :%4d час%4d мин%4d.%2d c"), IDAY, IMON, IYR, IHR, IMIN, ISEC, I100TH);
	theApp.WriteToLog(s);
	NFP_File.WriteString(s);
	s.Format(_T("\n%s%2d.%2d.%4d\n%sCopyright (c) 1988-1993\n%sХозяинов М.А.\n%sVersion 1.00 \n"), CString(_T(" "), 28), IDAY, IMON, IYR, CString(_T(" "), 18), CString(_T(" "), 18), CString(_T(" "), 18));
	NFP_File.WriteString(s);

	NFP_File.Close();
	NFR_File.Close();
	NFP1_File.Close();
	NFP2_File.Close();
	NFP5_File.Close();
	NFP8_File.Close();
	NFP6_File.Close();
	NFP10_File.Close();
}

void INP202(/*int &KST, int &KSN, int &KTR, int MVK[], double ADOP[], int &KODPOT, int &KODPI, double &DELQU,
	int &KODPID, COMPLEX SN[], COMPLEX SG[], double UY[], int M1[], int M2[], int NAME[], int NAME1[], int NAME2[],
	COMPLEX X[], double REPR[], double KT[], double GNN[], double GNV[], double VNN[], double VSN[], int &NMZK, int &NSMK, int &NSML,
	double UOP[], double QMN[], double QMX[], int &IKOY, int &KODPQG, double &KRATG, double QKY[], int &NFP1,
	int PRIZ[], int NOMERY[], int &MAXNOM, int &MKPARV, int &MKEPV, double &KRATQG,
	int &MKSTEK, double &REPRIM, double &UPRMAX, int &NAMET, double SHUNT[], double &YMIN*/)
{
/*
	INTEGER * 4 NAMEI, KST, KSN, KTR, KODPID, KODPQG, IKOY, KODPI,
	*KIMAX, KPMAX, NFP, NFR, M0, KPE, I, NFP1, J, J1, J2,
	*M, N, NSM, NBU, N1, NSMM, NMZK, KODPOT, NSMK, NSML,
	*MAXNOM, MKPARV, MKEPV, MKSTEK, NMAX, MMAX, NAMET
	REAL * 4  DELU1, DELU2, BP, EPSIL, KSQR, SQR3, U00, DELQU,
	*AMAX, UMIN, UMAX, KRAT, AAA, KRATG, YMIN,
	*UOPI, QMNI, QMXI, QKYI, UYI, KRATQG, REPRIM, UPRMAX
	COMPLEX * 8 UNOM, U0, SNI, SGI
	CHARACTER * 40 FNAME
	$LARGE
	REAL * 4  UY(1), UOP(1), QMN(1), QMX(1), QKY(1), SHUNT(1),
	*GNN(1), GNV(1), VNN(1), VSN(1)
	COMPLEX * 8 SN(1), SG(1)
	C
	INTEGER * 2 M1(1), M2(1), MVK(1), PRIZ(1), NOMERY(1), NAME(1),
	*NAME1(1), NAME2(1)
	REAL * 4    ADOP(1), KT(1), REPR(1)
	COMPLEX * 8 X(1)
*/

	int NAMEI, I, J, J1, J2/*, M, N, NSM, NBU, N1, NSMM*/;
	double  U00, AAA, UOPI, QMNI, QMXI, QKYI, UYI, dReal, dImag, mmm;
	COMPLEX SNI, SGI;
	CString FNAME, sOutput, sInput, tmpStr;
	char ch_name[40];
	wchar_t ch_name2[40];

	//setlocale(LC_ALL, ".1251");
	LPWSTR ss(_T(""));

/* 
	======================================================================
	C     1.  BBOД И ВЫВОД ИCXOДHЫX ДAHHЫX ПO CETИ
	C ======================================================================
	C
	C   1.1  ПEPEMEHHЫX И KOHCTAHT:  ---------------------------------------
	C
	READ (NFR, 126 ) FNAME
	126 FORMAT (A40)
	WRITE (NFP1,127)  FNAME
	127 FORMAT (/20X,'Р е ж и м : ',A40/)
	C      WRITE (NFP1,33)
	C   33 FORMAT(25X,' Исходные данные.'/)
	WRITE (NFP1,34)
	34 FORMAT(15X,' K О Н С Т А Н Т Ы   И   П Е Р Е М Е Н Н Ы Е'/)
*/

	const CString sPoints12(_T("............"));
	const CString sPoints7(_T("......."));
	const CString sPoints16(_T("................"));
	NFR_File.ReadString(FNAME);
	sOutput.Format(_T("                    Р е ж и м : %s \n                          Исходные данные. \n               K О Н С Т А Н Т Ы   И   П Е Р Е М Е Н Н Ы Е\n"), FNAME);
	NFP1_File.WriteString(sOutput);

/*
	READ (NFR,141) N //формат I5
	N1 = N + 1
	WRITE ( NFP1,151 ) N1
	151 FORMAT(4x,' Число узлов сети ..............................',
	*       12('.'),I5)
*/
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	N = _wtoi(tmpStr);
	N1 = N + 1;
	sOutput.Format(_T("     Число узлов сети ..............................%s     %d\n"), sPoints12, N1);
	NFP1_File.WriteString(sOutput);

	/*
	READ (NFR,141) M //формат I5
	WRITE ( NFP1,152 ) M
	152 FORMAT(4x,' Число ветвей сети .............................',
	*       12('.'),I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	M = _wtoi(tmpStr);
	sOutput.Format(_T("     Число ветвей сети .............................%s     %d\n"), sPoints12, M);
	NFP1_File.WriteString(sOutput);

/*
	READ (NFR,141) KST //формат I5
	WRITE ( NFP1,153 ) KST
	153 FORMAT(4x,' Число станций системы .........................',
	*       12('.'),I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KST = _wtoi(tmpStr);
	sOutput.Format(_T("     Число станций системы .........................%s     %d\n"), sPoints12, KST);
	NFP1_File.WriteString(sOutput);

/*
	READ (NFR,141) KSN //формат I5
	WRITE ( NFP1,154 ) KSN
	154 FORMAT(4x,' Число ступеней напряжения системы .............',
	*       12('.'),I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KSN = _wtoi(tmpStr);
	sOutput.Format(_T("     Число ступеней напряжения системы .............%s     %d\n"), sPoints12, KSN);
	NFP1_File.WriteString(sOutput);

/*
	READ (NFR,141) KTR //формат I5
	WRITE ( NFP1,155 ) KTR
	155 FORMAT(4x,' Число трансформаторов сети ....................',
	*       12('.'),I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	KTR = _wtoi(tmpStr);
	sOutput.Format(_T("     Число трансформаторов сети ....................%s     %d\n"), sPoints12, KTR);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 141) MAXNOM //формат I5
	WRITE(NFP1, 5155) MAXNOM
	5155 FORMAT(4x, ' Максимальный номеp узла .......................',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MAXNOM = _wtoi(tmpStr);
	sOutput.Format(_T("     Максимальный номеp узла .......................%s     %d\n"), sPoints12, MAXNOM);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 141) MKPARV //формат I5
	WRITE(NFP1, 5156) MKPARV
	5156 FORMAT(4x, ' Максимальное число паpаллельных ветвей.........',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKPARV = _wtoi(tmpStr);
	sOutput.Format(_T("     Максимальное число паpаллельных ветвей.........%s     %d\n"), sPoints12, MKPARV);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 141) MKEPV //формат I5
	WRITE(NFP1, 5157) MKEPV
	5157 FORMAT(4x, ' Максимальное число эквив. паpаллельных ветвей..',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	MKEPV = _wtoi(tmpStr);
	sOutput.Format(_T("     Максимальное число эквив. паpаллельных ветвей..%s     %d\n"), sPoints12, MKEPV);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 142) U00 //формат F7.2
	142 FORMAT(F7.2)
	U0 = U00
	WRITE(NFP1, 156) U0
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.7s"), sInput); //формат F7.2
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	U00 = dReal = _wtof(sInput);
	U0 = U00;
	sOutput.Format(_T("     Напряжение балансирующего узла, кВ...%s     %.2lf\n"), sPoints12, U0.real()); // формат 2F7.2 ???
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 141) NBU //формат I5
	WRITE(NFP1, 157) NBU
	157 FORMAT(4x, ' Номер балансирующего узла .....................',
	*12('.'), I5)
	156 FORMAT(4x, ' Напряжение балансирующего узла, кВ... ',
	*12('.'), 2F7.2)
	141 FORMAT(I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.5s"), sInput);
	NBU = _wtoi(tmpStr);
	sOutput.Format(_T("     Номер балансирующего узла .....................%s     %d\n"), sPoints12, NBU);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) IKOY //I6
	READ(NFR, 593) NMZK //I6
	WRITE(NFP1, 461) NMZK
	461 FORMAT(4x, ' Итерация с пропуском проверки Q в P-U узлах....',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	IKOY = _wtoi(tmpStr);

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	NMZK = _wtoi(tmpStr);
	sOutput.Format(_T("     Итерация с пропуском проверки Q в P-U узлах....%s     %d\n"), sPoints12, NMZK);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) KIMAX //I6
	WRITE(NFP1, 161) KIMAX
	161 FORMAT(4x, ' Заданное число итераций .......................',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KIMAX = _wtoi(tmpStr);
	sOutput.Format(_T("     Заданное число итераций .......................%s     %d\n"), sPoints12, KIMAX);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 592) KRATG //F6.3
	WRITE(NFP1, 162) KRATG
	162 FORMAT(4x, ' Кратность генерации ..........................',
	*12('.'), F6.3)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	KRATG = _wtof(tmpStr);
	sOutput.Format(_T("     Кратность генерации ..........................%s     %.3lf\n"), sPoints12, KRATG);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 592) KRAT //F6.3
	WRITE(NFP1, 166) KRAT
	166 FORMAT(4x, ' Кратность нагрузки ...........................',
	*12('.'), F6.3)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	KRAT = _wtof(tmpStr);
	sOutput.Format(_T("     Кратность нагрузки ...........................%s     %.3lf\n"), sPoints12, KRAT);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 572) KRATQG //F8.4
	WRITE(NFP1, 9958) KRATQG
	9958 FORMAT(4x, ' Кратность генеpации pеактивных лин. мощностей.',
	*12('.'), F7.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	KRATQG = _wtof(tmpStr);
	sOutput.Format(_T("     Кратность генеpации pеактивных лин. мощностей.%s     %.4lf\n"), sPoints12, KRATQG);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 572) YMIN //F8.4
	WRITE(NFP1, 9238) YMIN
	9238 FORMAT(4x, ' Удаляемая из эквивалента проводимость, См.....',
	*11('.'), F8.7)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	YMIN = _wtof(tmpStr);
	sOutput.Format(_T("     Удаляемая из эквивалента проводимость, См....%s     %.7lf\n"), sPoints12, YMIN);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 585) UPRMAX //F9.5
	WRITE(NFP1, 9351) UPRMAX
	9351 FORMAT(4x, ' Напpяжение деления pеактивных пpоводимостей линий',
	*7('.'), F9.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	UPRMAX = _wtof(tmpStr);
	sOutput.Format(_T("     Напpяжение деления pеактивных пpоводимостей линий%s     %.4lf\n"), sPoints7, UPRMAX);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 572) REPRIM ////F8.4
	WRITE(NFP1, 9358) REPRIM
	9358 FORMAT(4x, ' Значение деления pеактивных пpоводимостей, См ',
	*12('.'), F7.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	REPRIM = _wtof(tmpStr);
	sOutput.Format(_T("     Значение деления pеактивных пpоводимостей, См %s     %.4lf\n"), sPoints12, REPRIM);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) NAMET //I6
	WRITE(NFP1, 9368) NAMET
	9368 FORMAT(4x, ' Возможные значения новых номеpов узлов.........',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	NAMET = _wtoi(tmpStr);
	sOutput.Format(_T("     Возможные значения новых номеpов узлов.........%s     %d\n"), sPoints12, NAMET);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) KPMAX //I6
	WRITE(NFP1, 163) KPMAX
	163 FORMAT(4x, ' Максимальное число перегрузок..................',
	*12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	KPMAX = _wtoi(tmpStr);
	sOutput.Format(_T("     Максимальное число перегрузок..................%s     %d\n"), sPoints12, KPMAX);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 585) DELU1 //F9.5
	WRITE(NFP1, 164) DELU1
	164 FORMAT(4x, ' Точность сходимости по напряжению, о.е. ',
	*16('.'), F8.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELU1 = _wtof(tmpStr);
	sOutput.Format(_T("     Точность сходимости по напряжению, о.е. %s     %.4lf\n"), sPoints16, DELU1);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 585) DELU2 //F9.5
	WRITE(NFP1, 174) DELU2
	174 FORMAT(4x, ' Точность сходимости по мощности, МВА  ..',
	*16('.'), F8.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELU2 = _wtof(tmpStr);
	sOutput.Format(_T("     Точность сходимости по напряжению, о.е. %s     %.4lf\n"), sPoints16, DELU2);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 585) DELQU //F9.5
	WRITE(NFP1, 474) DELQU
	474 FORMAT(4x, ' Точность выбора Q в P-U узлах, МВар.....',
	*16('.'), F8.4)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.9s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	DELQU = _wtof(tmpStr);
	sOutput.Format(_T("     Точность выбора Q в P-U узлах, МВар.....%s     %.4lf\n"), sPoints16, DELQU);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 583) BP //F6.2
	WRITE(NFP1, 165) BP
	165 FORMAT(4x, ' Множитель изменения длительно-допустимых токов',
	*12('.'), F6.2)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	tmpStr.Replace(_T("."), _T(",")); //необходимо для корректного перевода из строки в вещественное
	BP = _wtof(tmpStr);
	sOutput.Format(_T("     Множитель изменения длительно-допустимых токов%s     %.2lf\n"), sPoints12, BP);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 584) NMAX //I8
	C      WRITE(NFP1, 584) NMAX
	READ(NFR, 584) MMAX //I8
	C      WRITE(NFP1, 584) MMAX
	READ(NFR, 584) NSM //I8
	C      WRITE(NFP1, 584) NSM
	READ(NFR, 584) NSMK //I8
	C      WRITE(NFP1, 584) NSMK
	READ(NFR, 584) NSML //I8
	C      WRITE(NFP1, 584) NSML
	READ(NFR, 584) NSMM //I8
	C      WRITE(NFP1, 584) NSMM
	READ(NFR, 584) MKSTEK //I8
	C      WRITE(NFP1, 5158) MKSTEK
	C 5158 FORMAT(4x, ' Pазмерность стека..............................',
	C * 12('.'), I5)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NMAX = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	MMAX = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSM = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSMK = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSML = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	NSMM = _wtoi(tmpStr);
	sOutput.Format(_T("%d\n%d\n%d\n%d\n%d\n%d\n"), NMAX, MMAX, NSM, NSMK, NSML, NSMM);
	NFP1_File.WriteString(sOutput);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.8s"), sInput);
	MKSTEK = _wtoi(tmpStr);
	sOutput.Format(_T("     Pазмерность стека..............................%s     %d\n"), sPoints12, MKSTEK);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) KODPI
	READ(NFR, 593) KODPOT
	C      WRITE(NFP1, 593) KODPOT
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPI = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPOT = _wtoi(tmpStr);
	sOutput.Format(_T("%d\n"), KODPOT);
	NFP1_File.WriteString(sOutput);

/*
	READ(NFR, 593) KODPID //I6
	READ(NFR, 533) KODPQG
	533    FORMAT(I6/////////)
	583    FORMAT(F6.2)
	585    FORMAT(F9.5)
	584    FORMAT(I8)
	592    FORMAT(F6.3)
	572    FORMAT(F8.4)
	593    FORMAT(I6)
*/

	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput);
	KODPID = _wtoi(tmpStr);
	NFR_File.ReadString(sInput);
	tmpStr.Format(_T("%.6s"), sInput); //I6/////////
	KODPQG = _wtoi(tmpStr);

	for (I = 0; I < 9; I++)
		NFR_File.ReadString(sInput); //I6///////// - 9 строк пропуска

//1.2  MACCИBOB ПO УЗЛAM: -------------------------------------------
/*
	READ(NFR, 123) (NAME(I), SHUNT(I), SN(I), SG(I), UOP(I), UY(I),
	*QMN(I), QMX(I), I = 1, N1, 1)
	123 FORMAT(I4, 8X, F9.6, F7.1, F7.1, F7.1, F7.1,
	*F6.1, F6.1, 22X, F8.1, F9.1)
	QKY(1) = QKY(1)
*/

	CString sVal;
	for (I = 1; I <= N1; I++)
	{
		NFR_File.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		tmpStr.Replace(_T("."), _T(","));

		sVal = tmpStr.Left(4); // I4
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 4);
		NAME[I] = _wtoi(sVal);

		tmpStr = tmpStr.Right(tmpStr.GetLength() - 8); //8X

		sVal = tmpStr.Left(9); //F9.6
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
		SHUNT[I] = _wtof(sVal);

		//----complex------
		sVal = tmpStr.Left(7);//F7.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		SN[I].real(_wtof(sVal));

		double dd = _wtof(sVal);
		sVal = tmpStr.Left(7);//F7.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		SN[I].imag(_wtof(sVal));

		sVal = tmpStr.Left(7);//F7.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		SG[I].real(_wtof(sVal));

		sVal = tmpStr.Left(7);//F7.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		SG[I].imag(_wtof(sVal));
		//-----------------

		sVal = tmpStr.Left(6);//F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		UOP[I] = _wtof(sVal);

		sVal = tmpStr.Left(6);//F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		UY[I] = _wtof(sVal);

		tmpStr = tmpStr.Right(tmpStr.GetLength() - 22); //22X

		sVal = tmpStr.Left(8);//F8.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 8);
		QMN[I] = _wtof(sVal);

		sVal = tmpStr.Left(9);//F9.1
		QMX[I] = _wtof(sVal);
	}

	//QKY(1) = QKY(1) 
	QKY[1] = QKY[1];

/*
	IF(N1.EQ.NBU) GO TO 321
	NAMEI = NAME(N1)
	SNI = SN(N1)
	SGI = SG(N1)
	UYI = UY(N1)
	UOPI = UOP(N1)
	QMNI = QMN(N1)
	QMXI = QMX(N1)
	QKYI = QKY(N1)
	NAME(N1) = NAME(NBU)
	SN(N1) = SN(NBU)
	SG(N1) = SG(NBU)
	UY(N1) = UY(NBU)
	UOP(N1) = UOP(NBU)
	QMN(N1) = QMN(NBU)
	QMX(N1) = QMX(NBU)
	QKY(N1) = QKY(NBU)
	NAME(NBU) = NAMEI
	SN(NBU) = SNI
	SG(NBU) = SGI
	UY(NBU) = UYI
	UOP(NBU) = UOPI
	QMN(NBU) = QMNI
	QMX(NBU) = QMXI
	QKY(NBU) = QKYI
	NBU = N1
	321 CONTINUE
*/

	if (N1 != NBU)
	{
		NAMEI = NAME[N1];
		SNI = SN[N1];
		SGI = SG[N1];
		UYI = UY[N1];
		UOPI = UOP[N1];
		QMNI = QMN[N1];
		QMXI = QMX[N1];
		QKYI = QKY[N1];
		NAME[N1] = NAME[NBU];
		SN[N1] = SN[NBU];
		SG[N1] = SG[NBU];
		UY[N1] = UY[NBU];
		UOP[N1] = UOP[NBU];
		QMN[N1] = QMN[NBU];
		QMX[N1] = QMX[NBU];
		QKY[N1] = QKY[NBU];
		NAME[NBU] = NAMEI;
		SN[NBU] = SNI;
		SG[NBU] = SGI;
		UY[NBU] = UYI;
		UOP[NBU] = UOPI;
		QMN[NBU] = QMNI;
		QMX[NBU] = QMXI;
		QKY[NBU] = QKYI;
		NBU = N1;
	}

/*  
	DO 1000 I = 1, N1, 1
	J = NAME (I)
	IF ( J .LE. MAXNOM ) GO TO 999
	WRITE ( NFP, 998 ) J, MAXNOM
	998             FORMAT (/'Максимальный номеp узла',I5,
	*                     'пpевышает заданный',I5/)
	STOP
	999         CONTINUE
	NOMERY(J) = I
	1000 CONTINUE
*/

	for (I = 1; I <= N1; I++)
	{
		J = NAME[I];
		if (J > MAXNOM)
		{
			sOutput.Format(_T("Максимальный номеp узла %d пpевышает заданный %d\n"), J, MAXNOM);
			NFP_File.WriteString(sOutput);
			theApp.STOP();
		}
		NOMERY[J] = I;
	}

/*
	C   1.3  MACCИBOB ПO BETBЯM: ------------------------------------------
	C
	READ ( NFR,534) AAA
	534    FORMAT(A1///////)
	READ (NFR,124) ( NAME1(I), NAME2(I), X(I), REPR(I),
	*                    KT(I), PRIZ(I), ADOP(I), MVK(I), I = 1, M, 1 )
	124 FORMAT ( I5, I5, F9.5, F9.5,
	*         F10.6, F7.4, 14X, I2, F7.3, I3 )
	DO 2000 I = 1, M, 1
	J1= NAME1(I)
	J2= NAME2(I)
	M1(I) = NOMERY(J1)
	M2(I) = NOMERY(J2)
	2000 CONTINUE
*/

	for (I = 0; I < 8; I++)
	{
		NFR_File.ReadString(sInput); //A1/////// - пропуcк 8ми строк
	}
	double dbb;
	for (I = 1; I <= M; I++)
	{
		NFR_File.ReadString(sInput);
		tmpStr.Format(_T("%.71s"), sInput);
		tmpStr.Replace(_T("."), _T(","));

		sVal = tmpStr.Left(5); // I5
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		NAME1[I] = _wtoi(sVal);

		sVal = tmpStr.Left(5); // I5
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		NAME2[I] = _wtoi(sVal);

		//----complex------
		sVal = tmpStr.Left(9);//F9.5
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
		X[I].real(_wtof(sVal));

		sVal = tmpStr.Left(9);//F9.5
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
		X[I].imag(_wtof(sVal));
		//-----------------

		sVal = tmpStr.Left(10); //F10.6
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 10);
/*		
		sVal.Trim();
		sVal = _T("  42,0256  ");
		dbb = _wtof(sVal);
		sVal = _T("  0,0000 ");
		dbb = _wtof(sVal);
		sVal = _T("0,0200 ");
		dbb = _wtof(sVal);
		sVal = _T("   0,00003");
		dbb = 0.00003;
*/
		dbb = _wtof(sVal);
		REPR[I] = _wtof(sVal);

		//tmpStr.Format(_T("%.8g"), dbb);

		sVal = tmpStr.Left(7); //F7.6
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		KT[I] = _wtof(sVal);

		tmpStr = tmpStr.Right(tmpStr.GetLength() - 14); //14X

		sVal = tmpStr.Left(2); //I2
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 2);
		PRIZ[I] = _wtoi(sVal);
		sVal = tmpStr.Left(7); //F7.3
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		ADOP[I] = _wtof(sVal);

		sVal = tmpStr.Left(3); //I3
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 3);
		MVK[I] = _wtoi(sVal);
	}

/*
	DO 2000 I = 1, M, 1
	J1= NAME1(I)
	J2= NAME2(I)
	M1(I) = NOMERY(J1)
	M2(I) = NOMERY(J2)
	2000 CONTINUE
*/

	for (I = 1; I <= M; I++)
	{
		J1 = NAME1[I];
		J2 = NAME2[I];
		M1[I] = NOMERY[J1];
		M2[I] = NOMERY[J2];
	}

/*
	C   1.4  ГРАНИЦ И НОМИНАЛОВ НАПРЯЖЕНИЙ --------------------------------
	C
	READ (NFR,125) ( GNN(I), VNN(I), GNV(I), VSN(I), I = 1, KSN, 1 )
	125 FORMAT ( 4F6.1 )
	C      WRITE ( NFP1,593) KODPID
	IF ( KODPID .EQ. 0 ) GO TO 600
*/

	for (I = 1; I <= KSN; I++)
	{
		NFR_File.ReadString(sInput);
		tmpStr.Format(_T("%24s"), sInput);
		tmpStr.Replace(_T("."), _T(","));

		sVal = tmpStr.Left(6); //F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		GNN[I] = _wtof(sVal);

		sVal = tmpStr.Left(6); //F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		VNN[I] = _wtof(sVal);

		sVal = tmpStr.Left(6); //F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		GNV[I] = _wtof(sVal);

		sVal = tmpStr.Left(6); //F6.1
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		VSN[I] = _wtof(sVal);
	}

	sOutput.Format(_T("%d\n"), KODPID); //I6
	NFP1_File.WriteString(sOutput);
	if (KODPID == 0) return;

/*
	2.   ПEЧATЬ  ИCXOДHЫX ДAHHЫX --------------------------------------
	C
	WRITE ( NFP1,35 )
	35 FORMAT(/23X,' Д А Н Н Ы Е   П О   У З Л А М'/)
	WRITE ( NFP1, 210 )
	210 FORMAT ('┌',77('─'),'┐')
	WRITE ( NFP1, 220 )
	220 FORMAT ('│    │    │             Мощности              ',
	*'│ Номи-│ За-  │   Пределы по │Мощ- │')
	WRITE ( NFP1, 230 )
	230 FORMAT ('│    │    │───────────────────────────────────',
	*'│ наль-│ дан- │   реактивной │ность│')
	WRITE ( NFP1, 240 )
	240 FORMAT ('│Но- │Име-│     Нагрузки    │   Генерации     ',
	*'│ ное  │ ное  │    мощности, │ком- │')
	WRITE ( NFP1, 250 )
	250 FORMAT ('│мера│на  │───────────────────────────────────',
	*'│ нап- │ нап- │      МВАр    │пенс.│')
	WRITE ( NFP1, 260 )
	260 FORMAT ('│    │    │ Актив- │ Реакти-│ Актив- │ Реакти-',
	*'│ ряже-│ ряже-│ ──────────── │устр.│')
	WRITE ( NFP1, 270 )
	270 FORMAT ('│    │    │ ная,   │ вная,  │ ная,   │ вная,  ',
	*'│ ние, │ ние, │  Ниж- │Верх- │МВАр │')
	WRITE ( NFP1, 280 )
	280 FORMAT ('│    │    │ МВт    │ МВАр   │ МВт    │ МВАр   ',
	*'│ кВ   │ кВ   │  ний  │ний   │     │')
	WRITE ( NFP1, 215 )
	215 FORMAT ('├',80('─'),'┤')
*/

	sOutput.Format(_T("%sД А Н Н Ы Е   П О   У З Л А М\n+%s+\n│    │    │             Мощности              │ Номи-│ За-  │   Пределы по │Мощ- │\n"), CString(_T(' '), 23), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│    │    │───────────────────────────────────│ наль-│ дан- │   реактивной │ность│\n│Но- │Име-│     Нагрузки    │   Генерации     │ ное  │ ное  │    мощности, │ком- │\n"));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│мера│на  │───────────────────────────────────│ нап- │ нап- │      МВАр    │пенс.│\n│    │    │ Актив- │ Реакти-│ Актив- │ Реакти-│ ряже-│ ряже-│ ──────────── │устр.│\n"));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│    │    │ ная,   │ вная,  │ ная,   │ вная,  │ ние, │ ние, │  Ниж- │Верх- │МВАр │\n│    │    │ МВт    │ МВАр   │ МВт    │ МВАр   │ кВ   │ кВ   │  ний  │ний   │     │\n+%s+\n"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	int yy;
	double dbll;
	for (I = 1; I <= N; I++)
	{
		sOutput.Format(_T("%4d│%4d│%8g│%8g│%8g│%8g│%6g│%6g│%7g│%6g│%5g\n"), I, NAME[I], SN[I].real(), SN[I].imag(), SG[I].real(), SG[I].imag(), UY[I], UOP[I], QMN[I], QMX[I], QKY[I]);
		NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+\n"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	sOutput.Format(_T("%s Д А Н Н Ы Е   П О   В Е Т В Я М\n+%s+\n│    │        Узлы         │  Сопротивления,   │  Прово-  │ Коэф-│ Допу-│Сос- │\n"), CString(_T(' '), 22), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│    │─────────────────────│       Ом          │  димо-   │ фици-│ сти- │тоя- │\n│Но- │  Начала  │  Конца   │───────────────────│  сти,    │ енты │ мые  │ние  │\n"));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│мера│──────────│──────────│ Актив-   Реактив- │  См      │ тран-│ токи,│     │\n│    │ Но-│ Имя │ Но-│ Имя │ ное      ное      │          │ сфор-│  А   │     │\n"));
	NFP1_File.WriteString(sOutput);
	sOutput.Format(_T("│    │ мер│     │ мер│     │                   │          │ мации│      │     │\n+%s+\n"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);

	double dn;
	for (I = 1; I <= M; I++)
	{
		dn = ADOP[I] * 1000;
		dn = REPR[I];
		sOutput.Format(_T("%4d│%4d│ %4d│%4d| %4d|%9g%9g |%9lf |%5g |%6g|%3d  |\n"), I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(), REPR[I], KT[I], ADOP[I] * 1000, MVK[I]);
		NFP1_File.WriteString(sOutput);
	}
	sOutput.Format(_T("+%s+"), CString(_T('─'), 80));
	NFP1_File.WriteString(sOutput);
}

void IN202(/*int M1[], int M2[], int M3[], int M3M[], int MVK[], int SI[], double QL[], double QGENL, int PRIZ[], 
	double ADOP[], COMPLEX X[], int KVIY[], double UOP[], int MYPU[], int MYPQ[], int KOY, int M3V[],
	double KT[], int KTR, double UY[], COMPLEX SG[], COMPLEX SN[], int KST, double PGENS, double PPOTR,
	double GNN[], double GNV[], int NSTN[], int KSN, double REPR[], int NTV[], int IKOY,
	int NYNS1[], int NYNS2[], double VNN[], double KRATG, int KNDE, int KNDEL,
	double PY[], double QY[], COMPLEX IY[], int MYPQ2[], double QGENS, double QPOTR, double QKY[], double QKYSUM,
	int MYY[], COMPLEX DYP[], COMPLEX MYNS1[], COMPLEX MYNS2[], COMPLEX YPH[], int NSMK, int NSML,
	int NYPH[], int IYPH[], int IH[], int IYSTEK[], int IL[], int IYPL[], int NYPL[], int ISSH[],
	COMPLEX DYPE[], int IE[], COMPLEX YPE[], int IYPE[], int NYPE[], int KNDEE, int KSTEK, COMPLEX IYD[],
	int NPARV[], int KPARV, int MKPARV, int KEPV, int MKEPV, int KODPID,
	int M1PV[], int M2PV[], double ADOPPV[], double KTPV[], COMPLEX ZPV[], double YPV[],
	int NAME1[], int NAME2[], int NAME[], double KRATQG, int MKSTEK, 
	double REPRIM, double UPRMAX, int NAMET, double QMN[], double QMX[], double SHUNT[], double YMIN*/)
{
/*
	INTEGER * 4 M, N, NSM, N1, NSMM, NYNI, KY, KYN, IND, KOY, KNY, KNV,
	*KTR, NNY, NKY, KST, KSN, IKOY, NSMV, NFP, NFR, NFP2,
	*M0, KPE, KIMX, KPMAX, NBU, J, I, NNN, KNDE, KNDEL,
	*KNDEE, KSTEK, NSMK, NSML, KPARV, MKPARV, NTPV, L,
	*NNTY, NKTY, KEPV, MKEPV, JT, IT, ITPV, NNPV, MNEW,
	*NAME1X, NAME2X, MVKX, M1X, M2X, NPARVX, NPARVT, MKSTEK,
	*NAMET, NMAX, MMAX, MM1, NN1, M12T, MOLD, NBUX, NSTNN,
	*KODPID, KUOP
*/
	int NYNI(0), KY(0), KYN, IND,KNY, KNV,
		NNY, NKY,
		KIMX, J, I, NNN,
		NTPV, L,
		NNTY, NKTY, JT, IT, ITPV, NNPV, MNEW,
		NAME1X, NAME2X, MVKX, M1X, M2X, NPARVX, NPARVT,
		MM1, NN1, M12T, MOLD, NBUX, NSTNN,
		KUOP;

/*
	REAL*4  SQR3, KSQR, BP, KRAT, UYI, QGENL, REPRX, KRATQG, UYNNY,
	*QGENS, QPOTR, PGENS, PPOTR, QKYSUM, KRATG, KTX,
	*AMAX, UMIN, UMAX, DELU1, DELU2, EPSIL, ADOPX, REPRI,
	*REPRIM, UPRMAX, KTI, ADOPI, KNYR, YMIN
*/
	double UYI, REPRX, UYNNY,
		KTX,
		ADOPX, REPRI,
		KTI, ADOPI, KNYR;

/*
	COMPLEX*8 UNOM, U0, SNAG, SGEN, XX
*/
	COMPLEX SNAG, SGEN, XX;
	CString str;
	theApp.WriteToLog(_T("Формирование вспомогательных данных."));
/*
DATA  EPSIL/.000001/, KPE/0/, NFP/11/, NFR/10/
*/
	EPSIL = 0.000001;
	KPE = 0;
	/*NFP = 11;
	NFR = 10;*/

	SQR3 = sqrt(3);
	KSQR = 1 / SQR3;
/*
Формирование : - номеров ступеней напряжений: (65 line IN202.FOR)
*/
	if (N1 > NMAX)
	{
		str.Format(_T("Новое число узлов%5d пpевышает заданный пpедел.%5d Расчет закончен для корректировки исходных данных."), N1, NMAX);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	UY[N1] = U0.real();   
	for (J=1; J<=KSN; J++) // Do 10
	{
		if (UY[N1] > GNV[J]) continue;
		if (UY[N1] < GNN[J]) 
		{
			NFP2_File.WriteString(_T("Для базисного узла не определена ступень напряжения. Расчет закончен для корректировки исходных данных.\n"));
			theApp.WriteToLog(_T("Для базисного узла не определена ступень напряжения. Расчет закончен для корректировки исходных данных."));
			theApp.STOP();
			Sleep(10000);
			return;
		}
		NSTN[N1] = J;
		goto L20;
	}
	// Label_10
/*
Формирование : - номеров станционных узлов : (89 line IN202.FOR)
*/
L20:		
	SNAG = (0., 0.);
	SGEN = (0., 0.);
	QGENL = 0.;
	QKYSUM = 0.;
	KUOP = 0;
	for (I = 1; I <= N1; I++) // DO 90 I = 1, N1, 1
	{
		QL[I] = 0.;
		IY[I] = (.0, .0);
		IYD[I] = (.0, .0);
		PY[I] = 0.;
		QY[I] = 0.;
		if (UOP[I] >= EPSIL) KUOP = KUOP + 1;
		SN[I] = SN[I] * KRAT;
		SG[I] = SG[I] * KRATG;
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QKYSUM = QKYSUM + QKY[I];
		QKY[I] = pow(SHUNT[I] * UY[I], 2);
		if (IKOY != 0 && UOP[I] >= EPSIL) SG[I] = COMPLEX(SG[I].real(), 0.); //SG(I) = CMPLX ( REAL(SG(I)), 0. )
		UYI = UY[I];
		for (J = 1; J <= KSN; J++)  //DO 65 J = 1, KSN, 1
		{
			if (UYI > GNV[J]) continue;   // GO TO 65
			if (UYI < GNN[J]) // GO TO 66
			{
				// Label_66
				str.Format(_T("Для узла %5d не определена ступень напряжения. Расчет закончен для корректировки данных.%5d"), I, I);
				NFP2_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			NSTN[I] = J; 
			break; //goto L67;
		} 
		// Label_65:
//	L67:;
	} 
// Label_90:
	if (KUOP > KST)
	{
		str.Format(_T(" Число генеpатоpных узлов%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), KUOP, KST);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	QGENS = SGEN.imag(); //QGENS = AIMAG(SGEN)
	QPOTR = SNAG.imag(); //QPOTR = AIMAG(SNAG)
	PGENS = SGEN.real();
	PPOTR = SNAG.real();
/*
Формирование : - номеров трансформаторных ветвей: (139 line IN202.FOR)
*/
	KTR = 0;
	KPARV = 0;
	if (M > MMAX) //IF(M.LE.MMAX) GO TO 5020
	{
		str.Format(_T(" Новое число ветвей%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), M, MMAX);
		NFP_File.WriteString(str);
		theApp.WriteToLog(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	for (I = 1; I <= M; I++) //line 151    Do 50 
	{
		if (PRIZ[I]) // не равно 0
		{
			//Фоpмиpование NPARV(KPARV)
			if (KPARV < MKPARV) //IF(KPARV.LT.MKPARV) GO TO 51
			{
				KPARV = KPARV + 1;
				NPARV[KPARV] = I;
			}
			else
			{
				str.Format(_T(" Число паpаллельных ветвей%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), KPARV, MKPARV);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
		}
		ADOP[I] = ADOP[I] * BP;
		REPR[I] = REPR[I] * KRATQG;
		if(KT[I] <= EPSIL) continue; //GO TO 50
		KTR = KTR + 1;
		NTV[KTR] = I;
		NNY = M1[I];
		NKY = M2[I];
		if (UY[NKY] >= UY[NNY]) goto L60;//GO TO 60
		M1[I] = NKY;
		M2[I] = NNY;
		NNN = NNY;
		NNY = NKY;
		NKY = NNN;
	L60:
		if (KT[I] >= 1.0) continue;//GO TO 50
		KT[I] = 1. / KT[I];
	} 
	// Label_50:
	if (KPARV == 0) goto L3500;
	KEPV = 0;
/*
Соpтиpовка   NPARV(KPARV)
*/
	JT = 0;
L1500:;
	if (KEPV >= MKEPV)
	{
		str.Format(_T(" Число эквивалентных паpаллельных ветвей%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), KEPV, MKEPV);
		NFP_File.WriteString(str);
		theApp.STOP();
		Sleep(10000);
		return;
	}
	KEPV = KEPV + 1;
	JT = JT + 1;
	J = JT + 1;
	I = NPARV[JT];
	NNTY = M1[I];
	NKTY = M2[I];
	ZPV[KEPV] = X[I];
	YPV[KEPV] = REPR[I];
	KTPV[KEPV] = KT[I];
	ADOPPV[KEPV] = ADOP[I];
	M1PV[KEPV] = NNTY;
	M2PV[KEPV] = NKTY;
L1600:;
	IT = NPARV[J];
	NNY = M1[IT];
	NKY = M2[IT];
	if ((NNY == NNTY) && (NKY == NKTY))
	{
		//Опеpации по эквивалентиpованию ветвей NPARV(JT)=I и NPARV(J)=IT
		ZPV[KEPV] = X[IT] * ZPV[KEPV] / (X[IT] + ZPV[KEPV]);
		YPV[KEPV] = REPR[IT] + YPV[KEPV];
		KTPV[KEPV] = (KT[IT] + KTPV[KEPV]) / 2.0;
		ADOPPV[KEPV] = ADOP[IT] + ADOPPV[KEPV];
		if (JT + 1 == J)
		{
			JT = JT + 1;
		}
		else
		{
			JT = JT + 1;
			NPARVX = NPARV[J];
			NPARVT = NPARV[JT];
			NAME1X = NAME1[NPARVX];
			NAME2X = NAME2[NPARVX];
			XX = X[NPARVX];
			REPRX = REPR[NPARVX];
			KTX = KT[NPARVX];
			ADOPX = ADOP[NPARVX];
			MVKX = MVK[NPARVX];
			M1X = M1[NPARVX];
			M2X = M2[NPARVX];
			NAME1[NPARVX] = NAME1[NPARVT];
			NAME2[NPARVX] = NAME2[NPARVT];
			X[NPARVX] = X[NPARVT];
			REPR[NPARVX] = REPR[NPARVT];
			KT[NPARVX] = KT[NPARVT];
			ADOP[NPARVX] = ADOP[NPARVT];
			MVK[NPARVX] = MVK[NPARVT];
			M1[NPARVX] = M1[NPARVT];
			M2[NPARVX] = M2[NPARVT];
			NAME1[NPARVT] = NAME1X;
			NAME2[NPARVT] = NAME2X;
			X[NPARVT] = XX;
			REPR[NPARVT] = REPRX;
			KT[NPARVT] = KTX;
			ADOP[NPARVT] = ADOPX;
			MVK[NPARVT] = MVKX;
			M1[NPARVT] = M1X;
			M2[NPARVT] = M2X;
		}
		if (JT >= KPARV) goto L2500;
	}
	else
	{
		if ((NNY == NKTY) && (NKY == NNTY))
		{
			//Опеpации по эквивалентиpованию ветвей NPARV(JT) и NPARV(J)
			if (JT + 1 == J)
			{
				JT = JT + 1;
			}
			else
			{
				JT = JT + 1;
				NPARVX = NPARV[J];
				NPARVT = NPARV[JT];
				NAME1X = NAME1[NPARVX];
				NAME2X = NAME2[NPARVX];
				XX = X[NPARVX];
				REPRX = REPR[NPARVX];
				KTX = KT[NPARVX];
				ADOPX = ADOP[NPARVX];
				MVKX = MVK[NPARVX];
				M1X = M1[NPARVX];
				M2X = M2[NPARVX];
				NAME1[NPARVX] = NAME1[NPARVT];
				NAME2[NPARVX] = NAME2[NPARVT];
				X[NPARVX] = X[NPARVT];
				REPR[NPARVX] = REPR[NPARVT];
				KT[NPARVX] = KT[NPARVT];
				ADOP[NPARVX] = ADOP[NPARVT];
				MVK[NPARVX] = MVK[NPARVT];
				M1[NPARVX] = M1[NPARVT];
				M2[NPARVX] = M2[NPARVT];
				NAME1[NPARVT] = NAME1X;
				NAME2[NPARVT] = NAME2X;
				X[NPARVT] = XX;
				REPR[NPARVT] = REPRX;
				KT[NPARVT] = KTX;
				ADOP[NPARVT] = ADOPX;
				MVK[NPARVT] = MVKX;
				M1[NPARVT] = M1X;
				M2[NPARVT] = M2X;
			}
			if (JT >= KPARV) goto L2500;
		}
		else
		{
			goto L2000;
		}
	}
L2000:;
	if (J == KPARV) goto L1500;
	J = J + 1;
	goto L1600;
L2500:; //line 312
	//Схлопывание паpаллельных ветвей
	ITPV = 1;
	NNPV = NPARV[ITPV];
	NTPV = NNPV;
	I = NTPV;
	for (J = NTPV; J <= M; J++) //line 320    Do 3000 
	{
		if(J == NTPV) 
		{
			ITPV = ITPV + 1;
			NTPV = NPARV[ITPV];
			continue;
		}
		else
		{
			NAME1[I] = NAME1[J];
			NAME2[I] = NAME2[J];
			X[I] = X[J];
			REPR[I] = REPR[J];
			KT[I] = KT[J];
			ADOP[I] = ADOP[J];
			MVK[I] = MVK[J];
			M1[I] = M1[J];
			M2[I] = M2[J];
			I = I + 1;
		}
	}
/*
Добавление в конец списка ветвей эквивалентиpованных
*/
	for (J = 1; J <= KEPV; J++) //line 341    Do 3100 
	{
		M1[I] = M1PV[J];
		M2[I] = M2PV[J];
		NAME1[I] = NAME[M1PV[J]];
		NAME2[I] = NAME[M2PV[J]];
		X[I] = ZPV[J];
		REPR[I] = YPV[J];
		KT[I] = KTPV[J];
		ADOP[I] = ADOPPV[J];
		MVK[I] = 1;
		I = I + 1;
	}
	// Label_3100:
	M = I - 1;
L3500: ; //line 354
	for (I = 1; I <= M; I++)
	{
		str.Format(_T("'│', %4d, '│', %4d, '│ '%4d, '│', %4d, '│ '%4d, '│', %9.3lf, %9.3lf, ' │', %9.6lf, ' │', %5.1lf, ' │', %6.lf, '│', %3d, '  │'"), I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(), REPR[I], KT[I], ADOP[I] * 1000., MVK[I]);
		NFP2_File.WriteString(str);
	}
	MNEW = 0;
	MOLD = M;
	NBUX = -100;
	NBU = N1;
	for (I = 1; I <= M; I++) //line 365    Do 3600 
	{
		if(M1[I] == NBU) M1[I] = NBUX;
		if(M2[I] == NBU) M2[I] = NBUX;
	}
	for (I = 1; I <= MOLD; I++) //line 369    Do 5000 
	{
		NNY = M1[I];
		NKY = M2[I];
		if (MVK[I] == 0 || KT[I] >= EPSIL || UY[NNY] <= UPRMAX || abs(REPR[I]) <= REPRIM)
		{
			if (MNEW > MMAX)
			{
				str.Format(_T(" Новое число ветвей%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), M, MMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			MNEW = MNEW + 1;
			M1[MNEW] = NNY;
			M2[MNEW] = NKY;
			NAME1[MNEW] = NAME1[I];
			NAME2[MNEW] = NAME2[I];
			X[MNEW] = X[I];
			REPR[MNEW] = REPR[I];
			KT[MNEW] = KT[I];
			ADOP[MNEW] = ADOP[I];
			MVK[MNEW] = MVK[I];
		}
		else 
		{
			REPRI = REPR[I];
			KNYR = abs(REPRI / REPRIM);
			KNY = KNYR;
			NN1 = N + 1;
			N = N + KNY;
			N1 = N + 1;
			if (N1 > NMAX) 
			{
				str.Format(_T("Новое число узлов%5d пpевышает заданный пpедел.%5d Расчет закончен для корректировки исходных данных."), N1, NMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			NAME[N1] = NAME[NBU];
			SN[N1] = SN[NBU];
			SG[N1] = SG[NBU];
			UY[N1] = UY[NBU];
			UOP[N1] = UOP[NBU];
			QMN[N1] = QMN[NBU];
			QMX[N1] = QMX[NBU];
			QKY[N1] = QKY[NBU];
			QL[N1] = 0.;
			IY[N1] = (.0, .0);
			IYD[N1] = (.0, .0);
			PY[N1] = 0.;
			QY[N1] = 0.;
			NSTN[N1] = NSTN[NBU];
			UYNNY = UY[NNY];
			NSTNN = NSTN[NNY];
			for (J = NN1; J <= N; J++) //line 411    Do 5100
			{
				NAMET = NAMET + 1;
				QL[J] = 0.;
				IY[J] = (.0, .0);
				IYD[J] = (.0, .0);
				PY[J] = 0.;
				QY[J] = 0.;
				SN[J] = (0., 0.);
				SG[J] = (0., 0.);
				NAME[J] = NAMET;
				UOP[J] = 0.;
				UY[J] = UYNNY;
				QMN[J] = 0.;
				QMX[J] = 0.;
				QKY[J] = 0.;
				NSTN[J] = NSTNN;
			}
			// Label_5100:
			NBU = N1;
			KNV = KNY + 1;
			int jj;
			jj = 15 / 3;
			REPRI = REPRI / KNV;
			//COMPLEX CKNV = COMPLEX((double)KNV, 0.0);
			XX = X[I] / (double)KNV;  //XX = X(I) / KNV
			KNV = KNV - 1;
			KTI = KT[I];
			ADOPI = ADOP[I];

			M12T = NN1;

			MM1 = M + 1;
			M = M + KNV;

			MNEW = I;
			M1[MNEW] = NNY;
			M2[MNEW] = M12T;
			X[MNEW] = XX;
			REPR[MNEW] = REPRI;
			KT[MNEW] = 0.;
			ADOP[MNEW] = ADOPI;
			MVK[MNEW] = 1;
			NAME1[MNEW] = NAME[NNY];
			NAME2[MNEW] = NAME[M12T];
			if(MNEW > MMAX)
			{
				str.Format(_T(" Новое число ветвей%5d пpевышает заданный пpедел.%5d. Расчет закончен для корректировки исходных данных."), M, MMAX);
				NFP_File.WriteString(str);
				theApp.WriteToLog(str);
				theApp.STOP();
				Sleep(10000);
				return;
			}
			for (MNEW = MM1; MNEW <= M; MNEW++) //line 455    Do 5200
			{
				if(MNEW == M)
				{
					M1[MNEW] = M12T;
					M2[MNEW] = NKY;
					NAME1[MNEW] = NAME[M12T];
					NAME2[MNEW] = NAME[NKY];
				}
				else
				{
					M1[MNEW] = M12T;
					NAME1[MNEW] = NAME[M12T];
					M12T = M12T + 1;
					M2[MNEW] = M12T;
					NAME2[MNEW] = NAME[M12T];
				}

				X[MNEW] = XX;
				REPR[MNEW] = REPRI;
				KT[MNEW] = 0.;
				ADOP[MNEW] = ADOPI;
				MVK[MNEW] = 1;
			}
			// Label_5200:
		}
	}
	// Label_5000:
	for (I = 1; I <= M; I++) //line 478    Do 6000 
	{
		if (M1[I] == NBUX) M1[I] = N1;
		if(M2[I] == NBUX) M2[I] = N1;
	}
	// Label_6000:
	if (KODPID == 2)
	{
		for (I = 1; I <= N1; I++)
		{
			str.Format(_T("'│', %4d, '│', %4d, '│', %8.2lf, '│', %8.2lf, '│', %8.2lf, '│', %8.2lf, '│', %6.1lf, '│', %6.lf, '│', %7.0lf, '│', %6.0lf'│', %5.0lf, '│'"), I, NAME[I], SN[I].real(), SN[I].imag(), SG[I].real(), SG[I].imag(), UY[I], UOP[I], QMN[I], QMX[I], QKY[I]);
			NFP_File.WriteString(str);
		}
		for (I = 1; I <= M; I++)
		{
			str.Format(_T("'│', %4d, '│', %4d, '│ '%4d, '│', %4d, '│ '%4d, '│', %9.3lf, %9.3lf, ' │', %9.6lf, ' │', %5.2lf, ' │', %6.0lf, '│', %3d, '  │'"), I, M1[I], NAME1[I], M2[I], NAME2[I], X[I].real(), X[I].imag(), REPR[I], KT[I], ADOP[I]*1000., MVK[I]);
			NFP_File.WriteString(str);
		}
	}
/*
Проверка отсутствия потери связности исходной сети:
*/
	//CALL FMS117(M1, M2, M3, M3V, NFP2)    ВЕРОНИКА
	FMS117();

	//CALL PDS015(M1, M2, M3, M3M, MVK, SI, M3V, NFP2)   НАТАША
	PDS015(M3M);
	theApp.WriteToLog(_T(" Инициализация счета."));
/*
Определение части расчетных нагрузок от генераций линий и потерь х.х.в трансформаторах :
*/
	//CALL RZM202(M1, M2, QL, VNN, REPR, KT, MVK, NSTN, QGENL)  НАТАША
	RZM202();
	//CALL MYP015(M1, M2, MVK, X, KT, M3, M3V, DYP, IH, YPH, IYPH, NYPH, KNDE)  НАТАША
	MYP015();
	str.Format(_T("'NSM: '%5d"), NSM);
	NFP_File.WriteString(str);

	str.Format(_T("'NSMM: '%5d"), NSMM);
	NFP_File.WriteString(str);

	str.Format(_T("'KNDE: '%5d"), KNDE);
	NFP_File.WriteString(str);
	
	//WRITE(NFP, '(''IH(I),I=1,N,1'')')
	//WRITE(NFP, 1112) (IH(I), I = 1, N, 1)
	//1112 FORMAT ( 12I5 )
	NFP_File.WriteString(_T("'IH(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%5d"), IH[I]);   
		NFP_File.WriteString(str);
	}
	
	//WRITE(NFP, '(''DYP(I),I=1,N,1'')')
	//WRITE(NFP, 1113) (DYP(I), I = 1, N, 1)
	//1113 FORMAT ( 10F7.2 )
	NFP_File.WriteString(_T("'DYP(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%7.2lf"), DYP[I]);
		NFP_File.WriteString(str);
	}
	//WRITE(NFP, '(''NYPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (NYPH(I), I = 1, KNDE, 1)
	//1112 FORMAT ( 12I5 )
	NFP_File.WriteString(_T("'NYPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), NYPH[I]);
		NFP_File.WriteString(str);
	}
	//WRITE(NFP, '(''IYPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (IYPH(I), I = 1, KNDE, 1)
	//1112 FORMAT ( 12I5 )
	NFP_File.WriteString(_T("'IYPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), IYPH[I]);
		NFP_File.WriteString(str);
	}
	//WRITE(NFP, '(''YPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1113) (YPH(I), I = 1, KNDE, 1)
	//1113 FORMAT ( 10F7.2 )
	NFP_File.WriteString(_T("'YPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%7.2lf"), YPH[I]);
		NFP_File.WriteString(str);
	}
/*
Определение адреса и самого элемента матрицы YPH по N1 :
*/
	I = IH[N1];
L755:; //line 531
	if (I) // не равно 0
	{
		L = NYPH[I];
		J = I;
		I = IYPH[I];
		IYD[L] = YPH[J] * U0 * KSQR;
		//write(nfp, 6754) l, iyd(l), u0
		//6754 format('l,iyd(l), u0', i5, 4f16.6)
		str.Format(_T("l,iyd(l), u0%5d%16.6lf%16.6lf%16.6lf%16.6lf"), L, IYD[L].real(), IYD[L].imag(), U0.real(), U0.imag());
		goto L755;
	}
	//CALL EK013(DYP, IH, YPH, IYPH, NYPH, KNDE,DYPE, IE, YPE, IYPE, NYPE)  ВЕРОНИКА
	EK013();
	KNDEE = KNDE;
	
	// WRITE(NFP, '(''KNDE: '',I5)') KNDE
	str.Format(_T("'KNDE: '%5d"), KNDE);
	NFP_File.WriteString(str);

	//WRITE(NFP, '(''IE(I),I=1,N1,1'')')
	//WRITE(NFP, 1112) (IE(I), I = 1, N1, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IE(I),I=1,N1,1'"));
	for (I = 1; I <= N1; I++)
	{
		str.Format(_T("%5d"), IE[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''DYPE(I),I=1,N1,1'')')
	//WRITE(NFP, 1113) (DYPE(I), I = 1, N1, 1)
	//1113 FORMAT(10F7.2)
	NFP_File.WriteString(_T("'DYPE(I),I=1,N1,1'"));
	for (I = 1; I <= N1; I++)
	{
		str.Format(_T("%7.2lf"), DYPE[I]);
		NFP_File.WriteString(str);
	}
	//WRITE(NFP, '(''NYPE(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (NYPE(I), I = 1, KNDE, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'NYPE(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), NYPE[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''IYPE(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (IYPE(I), I = 1, KNDE, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IYPE(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), IYPE[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''YPE(I),I=1,KNDE,1'')')
	// WRITE(NFP, 1113) (YPE(I), I = 1, KNDE, 1)
	//1113 FORMAT(10F7.2)
	NFP_File.WriteString(_T("'YPE(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%7.2lf"), YPE[I]);
		NFP_File.WriteString(str);
	}
	/*
	CALL DET117(MYY, DYP, IH, KNDE, YPH, IYPH, NYPH, NSMK, NSML,  ВЕРОНИКА
		*NYNS1, NYNS2, MYNS1, MYNS2, IYSTEK, NFP2,
		*IL, IYPL, NYPL, ISSH, KNDEL, MYPQ, KVIY, M3,
		*MKSTEK)
	*/
	DET117();
	//WRITE(NFP, '(''MYY(I),I=1,N,1'')')
	//WRITE(NFP, 1112) (MYY(I), I = 1, N, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'MYY(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%5d"), MYY[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''IH(I),I=1,N,1'')')
	//WRITE(NFP, 1112) (IH(I), I = 1, N, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IH(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%5d"), IH[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''DYP(I),I=1,N,1'')')
	//WRITE(NFP, 1113) (DYP(I), I = 1, N, 1)
	//1113 FORMAT(10F7.2)
	NFP_File.WriteString(_T("'DYP(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%7.2lf"), DYP[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''NYPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (NYPH(I), I = 1, KNDE, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'NYPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), NYPH[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''IYPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1112) (IYPH(I), I = 1, KNDE, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IYPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%5d"), IYPH[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''YPH(I),I=1,KNDE,1'')')
	//WRITE(NFP, 1113) (YPH(I), I = 1, KNDE, 1)
	//1113 FORMAT(10F7.2)
	NFP_File.WriteString(_T("'YPH(I),I=1,KNDE,1'"));
	for (I = 1; I <= KNDE; I++)
	{
		str.Format(_T("%7.2lf"), YPH[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''IYSTEK(I),I=1,N,1'')')
	//WRITE(NFP, 1112) (IYSTEK(I), I = 1, N, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IYSTEK(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%5d"), IYSTEK[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''IL(I),I=1,N,1'')')
	//WRITE(NFP, 1112) (IL(I), I = 1, N, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IL(I),I=1,N,1'"));
	for (I = 1; I <= N; I++)
	{
		str.Format(_T("%5d"), IL[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''KNDEL: '',I5)') KNDEL
	str.Format(_T("KNDEL: %5d"), KNDEL);
	NFP_File.WriteString(str);

	//WRITE(NFP, '(''IYPL(I),I=1,KNDEL,1'')')
	//WRITE(NFP, 1112) (IYPL(I), I = 1, KNDEL, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'IYPL(I),I=1,KNDEL,1'"));
	for (I = 1; I <= KNDEL; I++)
	{
		str.Format(_T("%5d"), IYPL[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''NYPL(I),I=1,KNDEL,1'')')
	// WRITE(NFP, 1112) (NYPL(I), I = 1, KNDEL, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'NYPL(I),I=1,KNDEL,1'"));
	for (I = 1; I <= KNDEL; I++)
	{
		str.Format(_T("%5d"), NYPL[I]);
		NFP_File.WriteString(str);
	}

	//WRITE(NFP, '(''ISSH(I),I=1,KNDEL,1'')')
	//WRITE(NFP, 1112) (ISSH(I), I = 1, KNDEL, 1)
	//1112 FORMAT(12I5)
	NFP_File.WriteString(_T("'ISSH(I),I=1,KNDEL,1'"));
	for (I = 1; I <= KNDEL; I++)
	{
		str.Format(_T("%5d"), ISSH[I]);
		NFP_File.WriteString(str);
	}
	if (IKOY) // не равно 0
	{
		/*
		CALL       DEE117(DYPE, IE, YPE, IYPE, NYPE, KNDEE, YMIN,
			*NYNS1, NYNS2, MYNS1, MYNS2, IYSTEK,
			*KSTEK, MYPQ, KVIY, M3, UOP, MYPU, NYNI, KY,
			*KYN, IND, KOY, IKOY, MYPQ2, NFP2, MKSTEK)
			*/
		DEE117(KY, NYNI, 0);
		//WRITE(NFP, '(''KNDEE: '',I5)') KNDEE
		str.Format(_T("'KNDEE: '%5d"), KNDEE);
		NFP_File.WriteString(str);

		//WRITE(NFP, '(''KSTEK: '',I5)') KSTEK
		str.Format(_T("'KSTEK: '%5d"), KSTEK);
		NFP_File.WriteString(str);

		//WRITE(NFP, '(''IE(I),I=1,N1,1'')')
		//WRITE(NFP, 1112) (IE(I), I = 1, N1, 1)
		//1112 FORMAT(12I5)
		NFP_File.WriteString(_T("'IE(I),I=1,N1,1'"));
		for (I = 1; I <= N1; I++)
		{
			str.Format(_T("%5d"), IE[I]);
			NFP_File.WriteString(str);
		}

		//WRITE(NFP, '(''DYPE(I),I=1,N1,1'')')
		//WRITE(NFP, 1113) (DYPE(I), I = 1, N1, 1)
		//1113 FORMAT(10F7.2)
		NFP_File.WriteString(_T("'DYPE(I),I=1,N1,1'"));
		for (I = 1; I <= N1; I++)
		{
			str.Format(_T("%7.2lf"), DYPE[I]);
			NFP_File.WriteString(str);
		}

		//WRITE(NFP, '(''NYPE(I),I=1,KNDEE,1'')')
		//WRITE(NFP, 1112) (NYPE(I), I = 1, KNDEE, 1)
		//1112 FORMAT(12I5)
		NFP_File.WriteString(_T("'NYPE(I),I=1,KNDEE,1'"));
		for (I = 1; I <= KNDEE; I++)
		{
			str.Format(_T("%5d"), NYPE[I]);
			NFP_File.WriteString(str);
		}
		
		//WRITE(NFP, '(''IYPE(I),I=1,KNDEE,1'')')
		//WRITE(NFP, 1112) (IYPE(I), I = 1, KNDEE, 1)
		//1112 FORMAT(12I5)
		NFP_File.WriteString(_T("'IYPE(I),I=1,KNDEE,1'"));
		for (I = 1; I <= KNDEE; I++)
		{
			str.Format(_T("%5d"), IYPE[I]);
			NFP_File.WriteString(str);
		}

		//WRITE(NFP, '(''YPE(I),I=1,KNDEE,1'')')
		//WRITE(NFP, 1113) (YPE(I), I = 1, KNDEE, 1)
		//1113 FORMAT(10F7.2)
		NFP_File.WriteString(_T("'YPE(I),I=1,KNDEE,1'"));
		for (I = 1; I <= KNDEE; I++)
		{
			str.Format(_T("%7.2lf"), YPE[I]);
			NFP_File.WriteString(str);
		}
	}
	//RETURN
	//END
}

void FMS117()
{
/*
	COMPLEX*8  UNOM, U0
	REAL   *4  AMAX, UMIN, UMAX, KRAT,
*           DELU1, DELU2, AMAX1, SQR3, KSQR, BP, EPSIL
	INTEGER*4  M, N, NSM, NBU, N1, NSMM, NSMV, M0, I, II, IKO, INA,
*           KPE, NFP, NFR, KIMX, KPMAX, N01, K, K1, KJ, L, J, NFP2
*/
	int I, II, IKO, INA, N01, K, K1, KJ, L, J;
	CString sOutput;

	N01 = N + 1;
	K1 = N01 * 2 + 1;
	K = K1;
	KJ = 1;
	for (I = 1; I <= N01; I++)
	{
		II = I;
		IKO = I * 2;
		INA = IKO - 1;
		L = 0;
		for (J = 1; J <= M;J++)
		{
			if (M1[J] == I)
			{
				M3[K] = M2[J];
				M3V[KJ] = J;
				L = L + 1;
				if (L == 1) M3[INA] = K;
				K = K + 1;
				KJ = KJ + 1;
				if (K > NSM)
				{
					sOutput.Format(_T("Hеобходимо увеличить pазмеpность базового списка связности свыше %d", NSM));
					NFP2_File.WriteString(sOutput);
					theApp.STOP();//return
				}
				else continue;
			}
			else
			{
				if (M2[J] != I) continue;
				M3[K] = M1[J];
				M3V[KJ] = J;
				L = L + 1;
				if (L == 1) M3[INA] = K;
				K = K + 1;
				KJ = KJ + 1;
				if (K > NSM)
				{
					sOutput.Format(_T("Hеобходимо увеличить pазмеpность базового списка связности свыше %d\n", NSM));
					NFP2_File.WriteString(sOutput);
					theApp.STOP();//return
				}
			}
		}
		if (L == 0)
		{
			sOutput.Format(_T("\n\n HET CBЯЗHOCTИ C УЗЛOM ПOД HOMEPOM %d\n BЫПOЛHEHИE ЗAДAHИЯ ПPEKPAЩEHO ДЛЯ УCTPAHEHИЯ OШИБKИ\n\n", II));
			NFP2_File.WriteString(sOutput);
			theApp.STOP(); //return
		}
		else M3[IKO] = K - 1;
	}

	NSM = K - 1;
	NSMV = KJ - 1;
}

void PDS015(int M3X[])
{
	/*      INTEGER*4  KOY, KI, I, YN, YK, IN, IK, NFP, M, N, NSM,
	*           M3J, J, NJ, KJ, MNA, MKO, IA, M4, M11, M12,
	*           IQ, LN, LK, LN1, LK1, NNYV, NSMV, KPMAX, NFP2,
	*           M0, KPE, NFR, KIMX, NBU, N1, NSMM, IANNYV, MKNNYV,
	*           M4V, NISI, JX*/

	int I, YN, YK, _IN, IK, M3J, J, NJ, KJ, MNA, MKO, IA, M4, M11, M12,
		IQ, LN, LN1, LK, LK1, NNYV, KIMX, IANNYV, MKNNYV, M4V, NISI, JX;
	CString sOutput;

/*
	1.1.KOPPEKTИPOBKA MACCИBA CBЯЗHOCTИ УДAЛEHИEM
	    HOPMAЛЬHO OTKЛЮЧEHHЫX BETBEЙ :
		CПEPECTAHOBKA ЭЛEMEHTOB MACCИBA CBЯЗHOCTИ УЗЛOB B CЛУЧAE OTЛИЧИЯ
		CИCTEMЫ ПO TOПOЛOГИИ OT БAЗOBOГO COCTOЯHИЯ(ПEPEДAETCЯ MACCИB MVK)
		-------------------------------------------------------------------- -
	1.1.2.ИHИЦИAЛИЗAЦИЯ ДЛЯ MACCИBA CBЯЗHOCTИ УЗЛОВ С ВЕТВЯМИ --
*/
	NNYV = N1 * 2;

	//DO 1 IQ = 1, M, 1
	for (IQ = 1; IQ <= M; IQ++)
	{
		if (MVK[IQ] != 0) continue;
		/*
			OПEPAЦИИ C MACCИBOM M3 ДЛЯ OTKЛЮЧEHHЫX BETBEЙ : ------
			OПPEДEЛEHИE HAЧAЛЬHЫX И KOHEЧHЫX HOMEPOB OTKЛЮ - ------
			ЧEHHOЙ BETBИ IQ------
		*/
		M11 = M1[IQ];
		M12 = M2[IQ];

		/*
			OПPEДEЛEHИE KOHEЧHOГO УKAЗATEЛЯ ИHДEKCA CBЯЗHOCTИ ------
			УЗЛA HAЧAЛA И KOHЦA BETBИ I:
		*/
		LN1 = M11 * 2;
		LK1 = M12 * 2;

		/*
			OПPEДEЛEHИE HAЧAЛЬHOГO УKAЗATEЛЯ ИHДEKCA CBЯЗHOCTИ ------
			УЗЛA HAЧAЛA И KOHЦA BETBИ I:
		*/
		LN = LN1 - 1;
		LK = LK1 - 1;

		/*
			OПPEДEЛEHИE HAЧAЛЬHOГO И KOHEЧHOГO ИHДEKCA CBЯЗHOCTИ ------
			УЗЛA HAЧAЛA BETBИ I
		*/
		MNA = M3[LN];
		MKO = M3[LN1];
		/*
			OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO HAЧAЛЬHЫM----
			ИHДEKCAM MACCИBA CBЯЗHOCTИ----
		*/
		if (MNA >= MKO) goto L55;
		if (M3[MKO] == M12) goto L3;
		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3[IA] != M12)continue;
			IANNYV = IA - NNYV;
			MKNNYV = MKO - NNYV;
			M4 = M3[IA];
			M4V = M3V[IANNYV];
			M3[IA] = M3[MKO];
			M3V[IANNYV] = M3V[MKNNYV];
			M3[MKO] = M4;
			M3V[MKNNYV] = M4V;
			goto L3;
		}
	L3:
		M3[LN1] = M3[LN1] - 1;
/*
	OПPEДEЛEHИE УCЛOBИЙ ПOTEPИ CBЯЗHOCTИ ПO КОНЕЧНЫМ----
	ИHДEKCAM MACCИBA CBЯЗHOCTИ----
*/
		MNA = M3[LK];
		MKO = M3[LK1];

		if (MNA >= MKO) goto L55;
		if (M3[MKO] == M11) goto L4;
		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3[IA] != M11) continue;
			IANNYV = IA - NNYV;
			MKNNYV = MKO - NNYV;
			M4 = M3[IA];
			M4V = M3V[IANNYV];
			M3[IA] = M3[MKO];
			M3V[IANNYV] = M3V[MKNNYV];
			M3[MKO] = M4;
			M3V[MKNNYV] = M4V;
			goto L4;
		}
		L4:
			M3[LK1] = M3[LK1] - 1;

	}
	
/*
	--- 1.2. ИHИЦИAЛИЗAЦИЯ BCПOMOГATEЛЬHOГO MACCИBA CBЯЗHOCTИ ---------
*/	
	for (I = 1; I <= NSM; I++)
	{
		M3X[I] = M3[I];
	}
/*
	----- 1.3 ИHИЦИAЛИЗAЦИЯ KOЛИЧECTBEHHЫX XAPAKTEPИCTИK: --------------
*/
	KOY = 0;
	KI = 1;
	I = 1;
	SI[KI] = 1;
	NISI = 1;

/*
	//----- 2. БЛOK OПPOCA ИCTOKA ------------------------------------------
*/
L60:
	I = SI[KOY + 1];
	YK = I * 2;
	YN = YK - 1;
	_IN = M3X[YN];
	IK = M3X[YK];
	if (_IN <= IK) goto L10;
/*
		----- 2.1. ИCTOK HE TPEБУET OПPOCA -----------------------------------
*/
L30:   
	KOY = KOY + 1;
	NISI = NISI + 1;
	if (KI > KOY) goto L60;
	if (KI == N + 1) goto L200;
/*
		---------- BЫXOД ИЗ П/ПP. C ПEЧATЬЮ O HAPУШEHИИ CBЯЗHOCTИ ------------
*/
	sOutput.Format(_T("\n\nCETЬ PAЗДEЛEHA ПO KPAЙHEЙ MEPE HA ДBA УЧACTKA. \nBЫПOЛHEHИE ЗAДAHИЯ ЗABEPШEHO ДЛЯ ИCПPABЛEHИЯ ИCXOДHЫX ДAHHЫX.\n\n\n"));
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("\nKI, KOY, I, YN, YK, IN, IK: %5d%5d%5d%5d%5d%5d%5d\n\n"), KI, KOY, I, YN, YK, _IN, IK);
	NFP2_File.WriteString(sOutput);
	for (I = 1;I < KOY;I++)
	{
		sOutput.Format(_T("%d\n"), SI[I]);
		NFP2_File.WriteString(sOutput);
	}
	theApp.STOP();
/*
	----- 2.2. ИCTOK OПPAШИBAETCЯ ----------------------------------------
*/
L10:
	for (J = _IN; J <= IK; J++)
	{
		M3J = M3X[J];
		for (JX = NISI; JX <= KI; JX++)
		{
			if (SI[JX] == M3J) goto L80;
		}
		KI = KI + 1;
		SI[KI] = M3J;

		/*
			2.2.1.ИCKЛЮЧEHИE CMEЖHOCTИ УЗЛA M3J C OПPAШИBAEMЫM УЗЛOM I :
			2.2.1.1.OПPEДEЛEHИE УKAЗATEЛEЙ HA KOHEЧHЫЙ И HAЧAЛЬHЫЙ УЗEЛ	CMEЖHOCTИ :
		*/
	L80:
		KJ = M3J * 2;
		NJ = KJ - 1;
		/*
			 2.2.1.2.OПPEДEЛEHИE ИHДEKCOB HAЧAЛЬHOГO И KOHEЧHOГO УЗЛOB	CMEЖHЫX C УЗЛOM M3J :
		*/
		MNA = M3X[NJ];
		MKO = M3X[KJ];
		if (MNA > MKO) continue;
		if (M3X[MKO] == I) goto L40;
		for (IA = MNA; IA <= MKO; IA++)
		{
			if (M3X[IA] != I) continue;
			M4 = M3X[IA];
			M3X[IA] = M3X[MKO];
			M3X[MKO] = M4;
			goto L40;
		}
	L40:
		M3X[KJ] = M3X[KJ] - 1;
	}
	M3X[YK] = _IN - 1;
	goto L30;
L55:
	sOutput.Format(_T("\n\n OБHAPУЖEH ИЗOЛИPOBAHHЫЙ OT CETИ УЗEЛ, ИHЦИДEHTHЫЙ HOPMAЛЬHO OTKЛЮЧEHHOЙ BETBИ\n%s%d .\n\n BЫПOЛHEHИE ЗAДAHИЯ ПPEKPAЩEHO ДЛЯ KOPPEKTИPOBKИ ИCXOДHЫX ДAHHЫX.\n\n\n"));
	NFP2_File.WriteString(sOutput);
	theApp.STOP();
L200: 
	return;
}

void RZM202()
{
/*
	INTEGER   M, N, NNY, NKY, NBU, NSM, N1, NSMM, I
*/
	int NNY, NKY, I;
/*
	REAL      KOEF, EPS / .00000001 / , QGENL, QLG
*/
	double KOEF, EPS(0.00000001), QLG;
/*
	C     ИНИЦИАЛИЗАЦИЮ(QL(I), I = 1, N, 1) СМОТРИ В П / ПР. 'IN...'
*/
	QGENL = 0;
	for (I = 1; I <= M; I++) //  Do 200 
	{
		if (MVK[I])
		{
			if (abs(KT[I]) < EPS) goto L45;
			if (abs(REPR[I]) < EPS) continue;
			NKY = M2[I];
			QLG = pow(REPR[I] * VNN[NSTN[NKY]], 2);
			QGENL = QGENL + QL[NKY];
			QL[NKY] = QL[NKY] + QLG;
			continue;
	L45:
			if (abs(REPR[I]) < EPS) continue;
			NNY = M1[I];
			NKY = M2[I];
			KOEF = REPR[I] / 2.;
			QLG = pow(KOEF * VNN[NSTN[NNY]], 2);
			QGENL = QGENL + QLG;
			QL[NNY] = QL[NNY] + QLG;
			QLG = pow(KOEF * VNN[NSTN[NKY]], 2);
			QGENL = QGENL + QLG;
			QL[NKY] = QL[NKY] + QLG;
		}
	}
	// Label_200:

}

void MYP015()
{
	/*INTEGER * 4 KNDE, M0, KPE, NFP, NFR, KIMX, KPMAX, M, N, NSM, NBU,
			*N1, NSMM, N12, NV1, NV2, I, NNV, NKV, J, NV
	*/
	int  /*M0, KPE, NFP, NFR,*/ KIMX,/* KPMAX, M, N, NSM, NBU, N1, NSMM,*/ N12, NV1, NV2, I, NNV, NKV, J, NV;
	/*
				COMPLEX   YF, YFKT, YDIAG, U0, UNOM
	*/
	COMPLEX YF, YFKT, YDIAG /*, U0, UNOM*/;

	KNDE = 0;
	N12 = N1 * 2;
	for (I = 1; I <= N1; I++) //  Do 100 
	{

		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		YDIAG = (0., 0.);
		for (J = NNV; J <= NKV; J++) //  Do 10 
		{
			NV = M3V[J];
			if (MVK[NV] == 0) continue;
			KNDE = KNDE + 1;
			NYPH[KNDE] = M3[J + N12];
			if (J == NNV) IH[I] = KNDE;
			if (J == NKV) IYPH[KNDE] = 0;
			if (J != NKV) IYPH[KNDE] = KNDE + 1;
			YF = (1., 0.) / X[NV];
			if (KT[NV] < EPSIL) goto L5;
			YFKT = -YF * KT[NV];
			YPH[KNDE] = YFKT;
			if (M1[NV] == I) YDIAG = YDIAG - YFKT*KT[NV];
			if (M2[NV] == I) YDIAG = YDIAG + YF;
			continue;
		L5:
			YPH[KNDE] = -YF;
			YDIAG = YDIAG + YF;
		}
		DYP[I] = YDIAG;
	}
}

void EK013()
{
	int I;

/*
	DO 100 I = 1, N1, 1
	DYPE(I) = DYP(I)
	IE(I) = IH(I)
	YPE(I) = YPH(I)
	IYPE(I) = IYPH(I)
	NYPE(I) = NYPH(I)
	100 CONTINUE
	DO 200 I = N1+1, KNDE, 1
	YPE(I) = YPH(I)
	IYPE(I) = IYPH(I)
	NYPE(I) = NYPH(I)
	200 CONTINUE
*/
	for (I = 1;I <= N1;I++)
	{
		DYPE[I] = DYP[I];
		IE[I] = IH[I];
		YPE[I] = YPH[I];
		IYPE[I] = IYPH[I];
		NYPE[I] = NYPH[I];
	}

	for (I = (N1 + 1);I <= KNDE;I++)
	{
		YPE[I] = YPH[I];
		IYPE[I] = IYPH[I];
		NYPE[I] = NYPH[I];
	}
}

void DET117(/*IH, KNDE, YPH, IYPH, NYPH, NSMK, NSML,
			NYNS1, NYNS2, MYNS1, MYNS2, IYSTEK, NFP2,
			IL, IYPL, NYPL, ISSH, KNDEL, MYPQ, KVIY, M3,
			MKSTEK*/)
{
/*
	Манипуляции с эквивалентом по генераторным узлам.

	МАССИВЫ:
	--------
	NYNS1(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ОДНОГО УЗЛА НОВОЙ СВ;
	NYNS2(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ДРУГОГО УЗЛА НОВОЙ СВ;

	ПЕРЕМЕННЫЕ:
	----------
	KYNS1     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ЕДИНИЦЫ;
	KYNS2     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ДВУХ;
	KYN       - КОЛИЧЕСТВО НАГРУЗОЧНЫХ УЗЛОВ ЭКВИВАЛЕНТА;
	NTY       - НОМЕР ТЕКУЩЕГО УЗЛА СЕТИ В ПОДСПИСКЕ СВЯЗНОСТИ
	УЗЛА 'NYNI';
	NTY2      - ТО ЖЕ, НО ВО ВНУТРЕННЕМ ЦИКЛЕ;
	N12     - ИНДЕКС УКАЗАТЕЛЯ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	ISMAX     - ИНДЕКС СВЯЗНОСТИ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	NYNI      - НОМЕР УЗЛА СРЕДИ НАГРУЗОЧНЫХ С НАИМЕНЬШЕЙ
	ИНЦИДЕНТНОСТЬЮ;
	IND       - ИНДЕКС ЭТОГО УЗЛА.
	NYY       - НОМЕР упорядоченного УЗЛА;
*/
	int N12, I, II, I1, J, JJ, NKY, NNY, KYN, NYY, NMIN, L, NYNI, IND, JLL;
	int KYNS1, KYNS2, IS1, JS2, NTY, NTY2, ISMAX, ISMAXN, INNDE, ILL;
	COMPLEX YKK, MYP1, MYP2, YKKM;
	CString sOutput;
	MYY, DYP, IH, KNDE, YPH, IYPH, NYPH, NSMK, NSML,
		NYNS1, NYNS2, MYNS1, MYNS2, IYSTEK, NFP2,
		IL, IYPL, NYPL, ISSH, KNDEL, MYPQ, KVIY, M3,
		MKSTEK;
	N12 = N1 * 2;
	KNDEL = 0;
/*
	Обнуление указателя индексов IL прямой подстановки и инициализация
	списка негенераторных узлов:
*/
	for (I = 1;I <= N; I++)
	{
		MYPQ[I] = I;
		IL[I] = 0;
	}

/*
	ИНИЦИАЛИЗАЦИЯ МАССИВА КОЛИЧЕСТВА ВЕТВЕЙ ИНЦИДЕНТНЫХ УЗЛАМ:
	ЦИКЛ ПО УКАЗАТЕЛЯМ МАССИВА М3(NBU)
*/
	J = 0;
	for (I = 2;I <= N12; I += 2)
	{
		J = J + 1;
		NKY = M3[I];
		NNY = M3[I - 1];
		KVIY[J] = NKY - NNY + 1;
	}

	KYN = N;
	KSTEK = 0;
	NYY = 0;

L1000: //line 92
	   //ПОИСК НОМЕРА И ИНДЕКСА УЗЛА С НАИМЕНЬШЕЙ ИНЦИДЕНТНОСТЬЮ
	NYNI;
	NMIN = M;
	NYY = NYY + 1;
	for (I = 1;I <= KYN;I++)
	{
		J = MYPQ[I];
		L = KVIY[J];
		if (L >= NMIN) continue;
		NMIN = L;
		NYNI = J;
		IND = I;
	}
	MYY[NYY] = NYNI;

/*
	Пересчет диагональных элементов матрицы 'DYP'

*/

	YKK = (1., 0.) / DYP[NYNI];
	I = IH[NYNI];

L60: //line 99
	L = NYPH[I];
	if (L != N1) DYP[L] = DYP[L] - YPH[I] * YKK * YPH[I];
	I = IYPH[I];
	if (I != 0) goto L60;

/*
	Проверка на единственность инциденции
*/
	I = IH[NYNI];
	L = IYPH[I];
	if (L == 0) goto L900;

/*
	Формирование вспомогательных массивов фиксации связности
	для коррекции списков связности и недиагональных элементов:
*/

	KYNS1 = 0;

L70: //line 117
	
	KYNS1 = KYNS1 + 1;
	NYNS1[KYNS1] = NYPH[I];
	MYNS1[KYNS1] = YPH[I];
	I = IYPH[I];
	if (IYPH[I] != 0) goto L70;
	II = I;
	KYNS2 = KYNS1 - 1;
	if (KYNS2 != 0)
	{
		for (I = 1; I <= KYNS2; I++)
		{
			NYNS2[I] = NYNS1[I+1];
			MYNS2[I] = MYNS1[I+1];
		}
	}
	KYNS2 = KYNS2 + 1;
	NYNS2[KYNS2] = NYPH[II];
	MYNS2[KYNS2] = YPH[II];

/*
	Расширение  списков  связности  в соответствии с увеличением связей
	ВНЕШНИЙ ЦИКЛ ПО УЗЛАМ ( - 1 ) , ДЛЯ КОТОРЫХ ДОБАВЛЯЮТСЯ НОВЫЕ СВЯЗИ
*/

	for (IS1 = 1; IS1 <= KYNS1; IS1++)
	{
		NTY = NYNS1[IS1];
		MYP1 = MYNS1[IS1];
/*
	ВНУТРЕННИЙ ЦИКЛ ПО ОСТАВШИМСЯ УЗЛАМ, С КОТОРЫМИ ПОЯВЛЯЮТСЯ НОВЫЕ СВЯЗИ
*/
		for (JS2 = IS1; JS2 <= KYNS2; JS2++)
		{
			NTY2 = NYNS2[JS2];
			MYP2 = MYNS2[JS2];
			YKKM = YKK * MYP1 * MYP2;
/*
			Проверка на наличие ветви, параллельной новой связи :
			Определение адреса и самого элемента матрицы YPH по NTY :
*/
			I = IH[NTY];
			if (IYPH[I] == 0) goto L758;
		L755:  //line158
			if (I == 0) goto L756;
			L = NYPH[I];
			J = I;
			I = IYPH[I];
			if (L != NTY2) goto L755;

/*
	Новой связи нет, т.к. существует ветвь параллельная ей:
*/
			YPH[J] = YPH[J] - YKKM;
/*
	Определение адреса и самого элемента матрицы YPH по NTY2:
*/
			I = IH[NTY2];
		L78://line 78
			if (I == 0)goto L756;
			L = NYPH[I];
			J = I;
			I = IYPH[I];
			if (L != NTY) goto L78;

/*
	Новой связи нет, т.к. существует ветвь параллельная ей:
*/
			YPH[J] = YPH[J] - YKKM;
			continue; //goto L750
		L756://line 183
		L758://line 184
/*
	Добавление пары новых связей:
*/
			ISMAX = KNDE;
			ISMAXN = ISMAX + 2;
			if (ISMAXN >= NSMK)
			{
				sOutput.Format(_T("\nУвеличьте размерность производных списков связности более заданной - %5d\n"), NSMK);
				NFP2_File.WriteString(sOutput);
				theApp.STOP();//return
			}
/*	
	1. Добавление к узлу 'NTY' новой связи с узлом 'NTY2'
*/
			KVIY[NTY] = KVIY[NTY] + 1;
			if (KSTEK <= 0) goto L765;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L766;
		L765:
			KNDE = KNDE + 1;
			INNDE = KNDE;
		L766:
			NYPH[INNDE] = NTY2;
			YPH[INNDE] = -YKKM;
			IYPH[INNDE] = 0;
/*
	Определение адреса и заполнение указателя на новый узел:	
*/
			I = IH[NTY];
		L760://line 214
			J = I;
			I = IYPH[I];
			if (I != 0) goto L760;
			IYPH[J] = INNDE;
/*
			2. Добавление к узлу 'NTY2' новой связи с узлом 'NTY'
*/
			KVIY[NTY2] = KVIY[NTY2] + 1;
			if (KSTEK <= 0) goto L775;
			INNDE = IYSTEK[KSTEK];
			KSTEK = KSTEK - 1;
			goto L776;
		L775:
			KNDE = KNDE + 1;
			INNDE = KNDE;
		L776:
			NYPH[INNDE] = NTY;
			YPH[INNDE] = -YKKM;
			IYPH[INNDE] = 0;
/*
	Определение адреса и заполнение указателя на новый узел:
*/
			I = IH[NTY2];
		L770://line 237
			J = I;
			I = IYPH[I];
			if (I != 0) goto L770;
			IYPH[J] = INNDE;
		}//Label750 continue
	}
L900://line 244
/*
	Удаление узла 'NYNI' из списков связности:
*/	if (KYN != IND)
		MYPQ[IND] = MYPQ[KYN]; //Коррекция элемента массива индексов негенераторных узлов
	KYN = KYN - 1;
/*
	Определение адреса и заполнение указателя на новый узел:
*/
	II = IH[NYNI];
L780://line 259
	JJ = II;
	II = IYPH[II];
	NTY = NYPH[JJ];
	if (NTY == N1) goto L610;
	KVIY[NTY] = KVIY[NTY] - 1;
/*
	Формирование недиагональных элементов для исключаемого узла:
	1/ для обратной подстановки:
*/		
	YPH[JJ] = YPH[JJ] * YKK;
/*
	2/ для прямой подстановки:
*/
	ILL = IL[NTY];
	KNDEL = KNDEL + 1;
	if (KNDEL >= NSML)
	{
		sOutput.Format(_T("\nУвеличьте размерность дополнительных списков связности\n нижнего треугольника более заданной - %5d\n"), NSML);
		NFP2_File.WriteString(sOutput);
		theApp.STOP();//return
	}

	if (ILL != 0) goto L600;
	IL[NTY] = KNDEL;
	IYPL[KNDEL] = 0;
	NYPL[KNDEL] = NYNI;
	ISSH[KNDEL] = JJ;
	goto L610;	
L600: //line288
	JLL = ILL;
	ILL = IYPL[JLL];
	if (ILL != 0) goto L600;
	IYPL[JLL] = KNDEL;
	IYPL[KNDEL] = 0;
	NYPL[KNDEL] = NYNI;
	ISSH[KNDEL] = JJ;
/*
	Схлопывание списков связности в цепочке узла с указателем JJ:
	Определение адреса и удаленение указателя на старый узел NYNI:
	
*/
L610:
	I = IH[NTY];
	L = NYPH[I];
	if (L != NYNI) goto L785;
	I1 = IYPH[I];
	IH[NTY] = I1;
	goto L786;
/*
	Схлопывание I-го элемента списков связности :
*/
L785://line311
	J = I;
	I = IYPH[J];
	L = NYPH[I];
	if (L != NYNI) goto L785;
	I1 = IYPH[I];
	if (I1 != 0) IYPH[J] = I1;
	if (I1 == 0) IYPH[J] = 0;
L786:;
	if (KSTEK > MKSTEK)
	{
		sOutput.Format(_T("\nУвеличьте размерность стека дополнительных списков\nсвязности левого треугольника более заданной -%5d\n", MKSTEK));
		NFP2_File.WriteString(sOutput);
		theApp.STOP();//return
	}
	KSTEK = KSTEK + 1;
	IYSTEK[KSTEK] = I;
	if (II != 0) goto L780;
	if (KYN > 0) goto L1000;
}

void DEE117(int &KY, int &NYNI, BOOL entry_param) //CALL DEP117( KY,  NYNI, 1)
{
	/*
	C --------------------------------------------------------------------
	C            Манипуляции с эквивалентом по генераторным узлам.
	C
	C     МАССИВЫ:
	C     --------
	C     NYNS1(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ОДНОГО УЗЛА НОВОЙ СВ;
	C     NYNS2(?)   - ВСПОМОГАТЕЛЬНЫЙ МАССИВ ФИКСАЦИИ ДРУГОГО УЗЛА НОВОЙ СВ;
	C
	C     ПЕРЕМЕННЫЕ:
	C     ----------
	C     KYNS1     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ЕДИНИЦЫ;
	C     KYNS2     - ЧИСЛО УЗЛОВ НОВЫХ СВЯЗЕЙ БЕЗ ДВУХ;
	C     KYN       - КОЛИЧЕСТВО НАГРУЗОЧНЫХ УЗЛОВ ЭКВИВАЛЕНТА;
	C     NTY       - НОМЕР ТЕКУЩЕГО УЗЛА СЕТИ В ПОДСПИСКЕ СВЯЗНОСТИ
	C                 УЗЛА 'NYNI';
	C     NTY2      - ТО ЖЕ, НО ВО ВНУТРЕННЕМ ЦИКЛЕ;
	C     N12     - ИНДЕКС УКАЗАТЕЛЯ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	C     ISMAX     - ИНДЕКС СВЯЗНОСТИ ПОСЛЕДНЕГО НОМЕРА УЗЛА;
	C     NYNI      - НОМЕР УЗЛА СРЕДИ НАГРУЗОЧНЫХ С НАИМЕНЬШЕЙ
	C                 ИНЦИДЕНТНОСТЬЮ;
	C     IND       - ИНДЕКС ЭТОГО УЗЛА.
	C
	C -------------------------------------------------------------------
	*/

	int KYN, N12, KYNS1, KYNS2, IND, I, KNOY, J, NKY, NNY, NMIN, L, II, IS1, NTY, JS2,
		NTY2, ISMAX, ISMAXN, INNDE, JJ, I1, KIMX, KYMIN;
	COMPLEX YKK, MYP1, MYP2, YKKM;
	CString sOutput;

	if (entry_param) goto ENTRY_DEP117;

	/*
	c		 WRITE ( NFP, '(''IE(I),I=1,N1,1'')')
	c		 WRITE ( NFP, 1112 ) (IE(I),I=1,N1,1)
	c 1112	FORMAT ( 20I3 )
	*/
	/*
	sOutput.Format(_T("(''IE(I),I=1,N1,1'')\n"));
	NFP_File.WriteString(sOutput);
	for (I = 1; I <= N1; I++)
	{
	sOutput.Format(_T("%3d"));
	NFP_File.WriteString(sOutput);
	}
	*/
	/*
	ИНИЦИАЛИЗАЦИЯ СПИСКА НЕ/ГЕНЕРАТОРНЫХ УЗЛОВ И ИНДЕКСОВ УЗЛОВ:

	KOY = 0
	KYMIN = 0
	KNOY = 0
	DO 300 I = 1, N, 1
	IF ( UOP(I) .EQ. 0.) GO TO 310
	KOY = KOY + 1
	MYPU(KOY) = I
	GO TO 300
	310         KNOY = KNOY + 1
	MYPQ(KNOY) = I
	MYPQ2(KNOY) = I
	300 CONTINUE
	IKOY = KOY
	IF ( KOY .EQ. 0 ) GO TO 990
	*/

	//ИНИЦИАЛИЗАЦИЯ СПИСКА НЕ / ГЕНЕРАТОРНЫХ УЗЛОВ И ИНДЕКСОВ УЗЛОВ :
	KOY = 0;
	KYMIN = 0;
	KNOY = 0;
	for (I = 1; I <= N; I++)
	{
		if (UOP[I] != 0)
		{
			KOY = KOY + 1;
			MYPU[KOY] = I;
			continue;
		}
		else
		{
			KNOY = KNOY + 1;
			MYPQ[KNOY] = I;
			MYPQ2[KNOY] = I;
		}
	}
	IKOY = KOY;
	if (KOY == 0) return;

	/*     ИНИЦИАЛИЗАЦИЯ МАССИВА КОЛИЧЕСТВА ВЕТВЕЙ ИНЦИДЕНТНЫХ УЗЛАМ:
	ЦИКЛ ПО УКАЗАТЕЛЯМ МАССИВА М3(NBU)

	N12 = N1 * 2
	J = 0
	DO 200 I = 2, N12, 2
	J = J + 1
	NKY = M3(I)
	NNY = M3(I-1)
	KVIY(J) = NKY - NNY + 1
	200 CONTINUE

	KY = N
	KYN = KNOY
	KSTEK = 0
	1000 CONTINUE*/


	//ИНИЦИАЛИЗАЦИЯ МАССИВА КОЛИЧЕСТВА ВЕТВЕЙ ИНЦИДЕНТНЫХ УЗЛАМ:
	//ЦИКЛ ПО УКАЗАТЕЛЯМ МАССИВА М3(NBU)
	N12 = N1 * 2;
	J = 0;
	for (I = 2;I <= N12; I += 2)
	{
		J = J + 1;
		NKY = M3[I];
		NNY = M3[I - 1];
		KVIY[J] = NKY - NNY + 1;
	}
	KY = N;
	KYN = KNOY;
	KSTEK = 0;

L1000://line 1000

	  /*
	  ПОИСК НОМЕРА И ИНДЕКСА УЗЛА С НАИМЕНЬШЕЙ ИНЦИДЕНТНОСТЬЮ

	  NMIN = NSMM
	  DO 400 I = 1, KYN, 1
	  J = MYPQ(I)
	  L = KVIY(J)
	  IF ( L .GE. NMIN ) GO TO 400
	  NMIN = L
	  NYNI = J
	  IND = I
	  400 CONTINUE
	  */

	  //ПОИСК НОМЕРА И ИНДЕКСА УЗЛА С НАИМЕНЬШЕЙ ИНЦИДЕНТНОСТЬЮ
	NMIN = NSMM;
	for (I = 1; I <= KYN; I++)
	{
		J = MYPQ[I];
		L = KVIY[J];
		if (L >= NMIN) continue;
		NMIN = L;
		NYNI = J;
		IND = I;
	}

	/*
	Вход для определения текущего эквивалента

	ENTRY      DEP117 ( DYPE, IE, YPE, IYPE, NYPE, KNDEE, YMIN,
	*                    NYNS1, NYNS2, MYNS1, MYNS2, IYSTEK,
	*                    KSTEK, MYPQ, KVIY, M3, UOP, MYPU, NYNI, KY,
	*                    KYN, IND, KOY, IKOY, MYPQ2, NFP2, MKSTEK )*/

ENTRY_DEP117:

	/* Пересчет диагональных элементов оставшейся части матрицы 'DYPE':

	YKK = (1.,0.) / DYPE (NYNI)
	I = IE(NYNI)
	60 CONTINUE
	L = NYPE(I)
	DYPE(L) = DYPE(L) - YPE(I) * YKK * YPE(I)
	65 CONTINUE
	I = IYPE(I)
	IF ( I .NE. 0 ) GO TO 60

	Проверка на единственность инциденции узла NYNI

	I = IE(NYNI)
	L = IYPE(I)
	IF ( L .EQ. 0 ) GO TO 900
	*/

	//Пересчет диагональных элементов оставшейся части матрицы 'DYPE':
	YKK = (1., 0.) / DYPE[NYNI];
	I = IE[NYNI];
L60://line 114
	L = NYPE[I];
	DYPE[L] = DYPE[L] - YPE[I] * YKK * YPE[I];
	I = IYPE[I];
	if (I != 0) goto L60;

	//Проверка на единственность инциденции узла NYNI
	I = IE[NYNI];
	L = IYPE[I];
	if (L == 0) goto L900;

	/*
	Формирование вспомогательных массивов фиксации связности
	для коррекции списков связности и недиагональных элементов:

	KYNS1 = 0
	70 CONTINUE
	KYNS1 = KYNS1 + 1
	NYNS1(KYNS1) = NYPE(I)
	MYNS1(KYNS1) = YPE(I)
	I = IYPE(I)
	IF ( IYPE(I) .NE. 0 ) GO TO 70
	II = I
	KYNS2 = KYNS1 - 1
	IF ( KYNS2 .EQ. 0 ) GO TO 90
	DO 80 I = 1, KYNS2, 1
	NYNS2(I) = NYNS1(I+1)
	MYNS2(I) = MYNS1(I+1)
	80       CONTINUE
	90    KYNS2 = KYNS2 + 1
	NYNS2(KYNS2) = NYPE(II)
	MYNS2(KYNS2) = YPE(II)
	*/

	//Формирование вспомогательных массивов фиксации связности
	//для коррекции списков связности и недиагональных элементов :
	KYNS1 = 0;
L70://line 131
	KYNS1 = KYNS1 + 1;
	NYNS1[KYNS1] = NYPE[I];
	MYNS1[KYNS1] = YPE[I];
	I = IYPE[I];
	if (IYPE[I] != 0) goto L70;
	II = I;
	KYNS2 = KYNS1 - 1;
	if (KYNS2 == 0)
	{
		for (I = 1; I <= KYNS2; I++)
		{
			NYNS2[I] = NYNS1[I + 1];
			MYNS2[I] = MYNS1[I + 1];
		}
	}
	KYNS2 = KYNS2 + 1;
	NYNS2[KYNS2] = NYPE[II];
	MYNS2[KYNS2] = YPE[II];

	/*
	Расширение  списков  связности  в
	соответствии с увеличением связей

	ВНЕШНИЙ ЦИКЛ ПО УЗЛАМ ( - 1 ) , ДЛЯ КОТОРЫХ ДОБАВЛЯЮТСЯ
	НОВЫЕ СВЯЗИ

	DO 700 IS1 = 1, KYNS1, 1
	NTY  = NYNS1(IS1)
	MYP1 = MYNS1(IS1)

	ВНУТРЕННИЙ ЦИКЛ ПО ОСТАВШИМСЯ УЗЛАМ, С КОТОРЫМИ ПОЯВЛЯЮТСЯ
	НОВЫЕ СВЯЗИ

	DO 750 JS2 = IS1, KYNS2, 1
	NTY2 = NYNS2(JS2)
	MYP2 = MYNS2(JS2)
	YKKM = YKK * MYP1 * MYP2
	*/

	//Расширение  списков  связности  в	соответствии с увеличением связей
	//ВНЕШНИЙ ЦИКЛ ПО УЗЛАМ(-1), ДЛЯ КОТОРЫХ ДОБАВЛЯЮТСЯ НОВЫЕ СВЯЗИ
	for (IS1 = 1; IS1 <= KYNS1; IS1++)
	{
		NTY = NYNS1[IS1];
		MYP1 = MYNS1[IS1];
		// ВНУТРЕННИЙ ЦИКЛ ПО ОСТАВШИМСЯ УЗЛАМ, С КОТОРЫМИ ПОЯВЛЯЮТСЯ НОВЫЕ СВЯЗИ
		for (JS2 = IS1; JS2 <= KYNS2; JS2++)
		{
			NTY2 = NYNS2[JS2];
			MYP2 = MYNS2[JS2];
			YKKM = YKK * MYP1 * MYP2;

			/*
			Проверка на наличие ветви, параллельной новой связи:
			Определение адреса и самого элемента матрицы YPE по NTY:

			I = IE(NTY)
			IF ( IYPE(I) .EQ. 0 ) GO TO 758
			755         CONTINUE
			IF ( I .EQ. 0 ) GO TO 756
			L = NYPE(I)
			J = I
			I = IYPE(I)
			IF ( L .NE. NTY2 ) GO TO 755

			Новой связи нет, т.к. существует ветвь параллельная ей:

			YPE(J) = YPE(J) - YKKM

			Определение адреса и самого элемента матрицы YPE по NTY2:

			I = IE(NTY2)
			78         CONTINUE
			IF ( I .EQ. 0 ) GO TO 756
			L = NYPE(I)
			J = I
			I = IYPE(I)
			IF ( L .NE. NTY ) GO TO 78

			Новой связи нет, т.к. существует ветвь параллельная ей:

			YPE(J) = YPE(J) - YKKM
			GO TO 750
			756         CONTINUE
			758       CONTINUE
			*/

			//Проверка на наличие ветви, параллельной новой связи:
			//Определение адреса и самого элемента матрицы YPE по NTY :

			L = NYPE[I];
			if (IYPE[I] != 0)
			{
			L755://line 172
				if (I != 0)
				{
					L = NYPE[I];
					J = I;
					I = IYPE[I];
					if (L != NTY2) goto L755;
					//Новой связи нет, т.к. существует ветвь параллельная ей:
					YPE[J] = YPE[J] - YKKM;
					//Определение адреса и самого элемента матрицы YPE по NTY2:
					I = IE[NTY2];
				L78://line 186
					if (I != 0)
					{
						L = NYPE[I];
						J = I;
						I = IYPE[I];
						if (L != NTY) goto L78;
						//Новой связи нет, т.к. существует ветвь параллельная ей:
						YPE[J] = YPE[J] - YKKM;
						continue;
					}
				}
			}
			/*
			c	KYMIN = KYMIN + 1
			c	WRITE (NFP,1111) KYMIN, YKKM, CABS(YKKM)
			1111   c   FORMAT (' KYMIN, YMIN:',I6,3F12.7 )
			IF ( CABS(YKKM) .LE. YMIN ) GO TO 750
			*/
			/*
			KYMIN = KYMIN + 1;
			sOutput.Format(_T(" KYMIN, YMIN:'%6d%12lf%12lf%12lf\n"), KYMIN, YKKM.real(), YKKM.imag(),abs(YKKM));
			NFP_File.WriteString(sOutput);
			*/
			if (abs(YKKM) <= YMIN) continue;

			/*
			Добавление пары новых связей:

			ISMAX = KNDEE
			ISMAXN = ISMAX + 2
			if (ismaxn .gt. 2700 ) write (nfp,2112) ismaxn, kndee, kstek
			2112		format (' ismaxn, kndee, kstek:',3i10)
			IF ( ISMAXN .LT. NSMM ) GO TO 751
			753       WRITE ( NFP2 , 752 ) NSMM
			752       FORMAT(/' Увеличьте размерность списков связности ',
			*              'эквивалента более заданной -',I5/)
			STOP
			751       CONTINUE
			*/

			//Добавление пары новых связей:
			ISMAX = KNDEE;
			ISMAXN = ISMAX + 2;
			/*
			if (ISMAXN > 2700)
			{
			sOutput.Format(_T(" ismaxn, kndee, kstek:'%10d%10d%10d\n"), ISMAXN,KNDEE,KSTEK);
			NFP_File.WriteString(sOutput);
			}
			*/
			if (ISMAXN >= NSMM)
			{
				sOutput.Format(_T(" Увеличьте размерность списков связности эквивалента более заданной - %5d\n"), NSMM);
				NFP_File.WriteString(sOutput);
				theApp.STOP();//return
			}

			/*
			1. Добавление к узлу 'NTY' новой связи с узлом 'NTY2'

			KVIY(NTY) = KVIY(NTY) + 1
			IF ( KSTEK .LE. 0 ) GO TO 765
			INNDE = IYSTEK(KSTEK)
			KSTEK = KSTEK - 1
			GO TO 766
			765       KNDEE = KNDEE + 1
			INNDE = KNDEE
			766       CONTINUE
			NYPE(INNDE) = NTY2
			YPE(INNDE) = - YKKM
			IYPE(INNDE) = 0
			*/

			//1. Добавление к узлу 'NTY' новой связи с узлом 'NTY2'
			KVIY[NTY] = KVIY[NTY] + 1;
			if (KSTEK > 0)
			{
				INNDE = IYSTEK[KSTEK];
				KSTEK = KSTEK - 1;
			}
			else
			{
				KNDEE = KNDEE + 1;
				INNDE = KNDEE;
			}
			NYPE[INNDE] = NTY2;
			YPE[INNDE] = -YKKM;
			IYPE[INNDE] = 0;

			/*			if (INNDE > 2700)
			{
			sOutput.Format(_T(" innde, kstek, ismax: %10d%10d%10d\n"), INNDE, KSTEK, ISMAXN);
			NFP_File.WriteString(sOutput);
			}
			*/

			/*
			Определение адреса и заполнение указателя на новый узел:

			I = IE(NTY)
			760       CONTINUE
			J = I
			I = IYPE(I)
			IF ( I .NE. 0 ) GO TO 760
			IYPE(J) = INNDE
			*/

			//Определение адреса и заполнение указателя на новый узел:
			I = IE[NTY];
		L760://line236
			J = I;
			I = IYPE[I];
			if (I != 0) goto L760;
			IYPE[J] = INNDE;

			/*
			C			2. Добавление к узлу 'NTY2' новой связи с узлом 'NTY'
			C
			KVIY(NTY2) = KVIY(NTY2) + 1
			IF ( KSTEK .LE. 0 ) GO TO 775
			INNDE = IYSTEK(KSTEK)
			KSTEK = KSTEK - 1
			GO TO 776
			775       KNDEE = KNDEE + 1
			INNDE = KNDEE
			776       CONTINUE
			NYPE(INNDE) = NTY
			YPE(INNDE) = - YKKM
			IYPE(INNDE) = 0
			C			if (innde .gt. 2700 ) write (nfp,2111) innde, kstek, ismaxn
			*/

			//2. Добавление к узлу 'NTY2' новой связи с узлом 'NTY'
			KVIY[NTY2] = KVIY[NTY2] + 1;
			if (KSTEK > 0)
			{
				INNDE = IYSTEK[KSTEK];
				KSTEK = KSTEK - 1;
			}
			else
			{
				KNDEE = KNDEE + 1;
				INNDE = KNDEE;
			}
			NYPE[INNDE] = NTY;
			YPE[INNDE] = -YKKM;
			IYPE[INNDE] = 0;

			//if (INNDE > 2700)
			//{
			//	sOutput.Format(_T("\n"));
			//	NFP_File.WriteString(sOutput);
			//}

			/*
			C			Определение адреса и заполнение указателя на новый узел:
			C
			I = IE(NTY2)
			770       CONTINUE
			J = I
			I = IYPE(I)
			IF ( I .NE. 0 ) GO TO 770
			IYPE(J) = INNDE
			750 CONTINUE
			700 CONTINUE
			900 CONTINUE
			*/

			I = IE[NTY2];
		L770://line 260

			J = I;
			I = IYPE[I];
			if (I != 0) goto L770;
			IYPE[J] = INNDE;
		}
	}
L900://line 267

	 /*
	 C     Удаление узла 'NYNI' из списков связности:
	 C
	 KY = KY - 1
	 IF ( KYN .EQ. IND ) GO TO 550
	 C
	 C        Коррекция элемента массива индексов негенераторных узлов
	 C
	 MYPQ(IND) = MYPQ(KYN)
	 550 CONTINUE
	 KYN = KYN - 1
	 C
	 */
	 //Удаление узла 'NYNI' из списков связности:
	KY = KY - 1;
	if (KYN != IND)
		MYPQ[IND] = MYPQ[KYN];//Коррекция элемента массива индексов негенераторных узлов
	KYN = KYN - 1;

	/*
	Определение адреса и заполнение указателя на новый узел:
	C
	II = IE(NYNI)
	780 CONTINUE
	JJ = II
	II = IYPE(II)
	NTY = NYPE(JJ)
	KVIY(NTY) = KVIY(NTY) - 1
	*/
	// Определение адреса и заполнение указателя на новый узел :
	II = IE[NYNI];
L780://line 283
	JJ = II;
	II = IYPE[II];
	NTY = NYPE[JJ];
	KVIY[NTY] = KVIY[NTY] - 1;

	/*
	C     Схлопывание списков связности в цепочке узла с указателем JJ:
	C
	C     Определение адреса и удаленение указателя на старый узел NYNI:
	C
	I = IE(NTY)
	L = NYPE(I)
	IF ( L .NE. NYNI ) GO TO 785
	I1 = IYPE(I)
	IE(NTY) = I1
	GO TO 786
	*/

	//Схлопывание списков связности в цепочке узла с указателем JJ :
	//Определение адреса и удаленение указателя на старый узел NYNI :
	I = IE[NTY];
	L = NYPE[I];
	if (L != NYNI) goto L785;
	I1 = IYPE[I];
	IE[NTY] = I1;
	goto L786;

	/*
	C     Схлопывание I-го элемента списков связности :
	C
	785 CONTINUE
	J = I
	I = IYPE(J)
	L = NYPE(I)
	IF ( L .NE. NYNI ) GO TO 785
	I1 = IYPE(I)
	IF ( I1 .NE. 0 ) IYPE(J) = I1
	IF ( I1 .EQ. 0 ) IYPE(J) = 0
	786               CONTINUE
	IF ( KSTEK .LE. MKSTEK ) GO TO 9751
	WRITE ( NFP2 , 9752 ) MKSTEK
	9752 FORMAT(/' Увеличьте размерность стека дополнительных списков'/,
	*   ' связности эквивалента более заданной -',I5/)
	STOP
	9751               CONTINUE
	KSTEK = KSTEK + 1
	IYSTEK(KSTEK) = I
	IF ( II .NE. 0 ) GO TO 780
	IF ( KYN .GT. 0 ) GO TO 1000
	990 RETURN
	END
	*/

	//Схлопывание I-го элемента списков связности :
L785://line 302
	J = I;
	I = IYPE[J];
	L = NYPE[I];
	if (L != NYNI) goto L785;
	I1 = IYPE[I];
	if (I1 != 0) IYPE[J] = I1;
	else IYPE[J] = 0;
L786://line 310
	if (KSTEK > MKSTEK)
	{
		sOutput.Format(_T("\nУвеличьте размерность стека дополнительных списков\n связности эквивалента более заданной - %5d \n\n"), MKSTEK);
		NFP2_File.WriteString(sOutput);
		theApp.STOP();//return
	}

	KSTEK = KSTEK + 1;
	IYSTEK[KSTEK] = I;
	if (II != 0) goto L780;
	if (KYN > 0) goto L1000;
}

void PRE015()
{
	/*
	C----------------------------------------------------------------------
	C     Предварительный расчет баланса мощности по системе вцелом
	C----------------------------------------------------------------------
	*/

	double Q0(0.), P0(0.), QBAL, PBAL;
	CString sOutput;

	/*
	Q0 = 0.
	QBAL = QGENS - QGENL - QPOTR + QKYSUM
	WRITE (NFP2,5)
	5 FORMAT(10X/'Предварительный расчет баланса',
	*           ' реактивной мощности системы:')
	WRITE (NFP2,6) QGENS
	6 FORMAT(10X,'Генерация реактивной мощности ............',F10.3,
	*           ' МВар')
	WRITE (NFP2,26) QKYSUM
	26 FORMAT(10X,'Реактивная мощность компенсирующих устр...',F10.3,
	*           ' МВар')
	WRITE (NFP2,7) -QGENL
	7 FORMAT(10X,'Реактивные мощности линий и потерь тр. ...',F10.3,
	*           ' МВар')
	WRITE (NFP2,8) QPOTR
	8 FORMAT(10X,'Потребление реактивной мощности ..........',F10.3,
	*           ' МВар')
	WRITE (NFP2,9) Q0
	9 FORMAT(10X,'Потери реактивной мощности ...............',F10.3,
	*           ' МВар')
	WRITE (NFP2,4) QBAL
	4 FORMAT(10X,'Небаланс реактивной мощности .............',F10.3,
	*           ' МВар')
	*/

	QBAL = QGENS - QGENL - QPOTR + QKYSUM;

	sOutput.Format(_T("%s\nПредварительный расчет баланса реактивной мощности системы:\n"), CString(_T(' ')), 10);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sГенерация реактивной мощности ............%10.7lf МВар\n"), CString(_T(' ')), 10, QGENS);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sРеактивная мощность компенсирующих устр...%10.7lf МВар\n"), CString(_T(' ')), 10, QKYSUM);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sРеактивные мощности линий и потерь тр. ...%10.7lf МВар\n"), CString(_T(' ')), 10, -QGENL);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sПотребление реактивной мощности ..........%10.7lf МВар\n"), CString(_T(' ')), 10, QPOTR);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sПотери реактивной мощности ...............%10.7lf МВар\n"), CString(_T(' ')), 10, Q0);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sНебаланс реактивной мощности .............%10.7lf МВар\n"), CString(_T(' ')), 10, QBAL);
	NFP2_File.WriteString(sOutput);

	/*
	P0 = 0.
	PBAL = PGENS - PPOTR
	WRITE (NFP2,15)
	15 FORMAT(10X/'Предварительный расчет баланса',
	*           ' активной мощности системы:')
	WRITE (NFP2,16) PGENS
	16 FORMAT(10X,'Генерация активной мощности ..............',F10.3,
	*           ' МВар')
	WRITE (NFP2,18) PPOTR
	18 FORMAT(10X,'Потребление активной мощности ............',F10.3,
	*           ' МВар')
	WRITE (NFP2,19) P0
	19 FORMAT(10X,'Потери активной мощности .................',F10.3,
	*           ' МВар')
	WRITE (NFP2,14) PBAL
	14 FORMAT(10X,'Небаланс активной мощности ...............',F10.3,
	*           ' МВар')
	RETURN
	END
	*/

	PBAL = PGENS - PPOTR;
	sOutput.Format(_T("%s\nПредварительный расчет баланса активной мощности системы:\n"), CString(_T(' ')), 10);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sГенерация активной мощности ..............%10.3lf МВар\n"), CString(_T(' ')), 10, PGENS);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sПотребление активной мощности ............%10.3lf МВар\n"), CString(_T(' ')), 10, PPOTR);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sПотери активной мощности .................%10.3lf МВар\n"), CString(_T(' ')), 10, P0);
	NFP2_File.WriteString(sOutput);
	sOutput.Format(_T("%sНебаланс активной мощности ...............%10.3lf МВар\n"), CString(_T(' ')), 10, PBAL);
	NFP2_File.WriteString(sOutput);
}

void PVREM() 
{
/*
	REAL      TVREM, TVR, RVR
*/
	double TVR, RVR;
/*
		CALL GETTIM(IHR, IMIN, ISEC, I100TH)
*/
	CTime t = CTime::GetCurrentTime();
	IHR = t.GetHour();
	IMIN = t.GetMinute();
	ISEC = t.GetSecond();

	TVR = IHR*3600. + IMIN*60. + ISEC + I100TH / 100.;
	RVR = TVR - TVREM;
	TVREM = TVR;
	//NVRPV = nvrpv;
}

void RTO015()
{
/*
INTEGER*4 M0, KPE, NFP, NFR, KIMAX, KPMAX, I,
*          M, N, NSM, NBU, N1, NSMM, NSMV
*/
	int I;
/*
COMPLEX*8 IT0/(.0,.0)/, UNOM, U0, XI
*/
	COMPLEX IT0(.0, .0), XI;
	for (I = 1; I <= M; I++)
	{
		if (MVK[I] != 0) goto L100;
		IT[I] = IT0;
		continue;
	L100:
		XI = X[I] * SQR3;
		if (KT[I] <= EPSIL) goto L200;
		IT[I] = (U[M1[I]] * KT[I] - U[M2[I]]) / XI;
		continue;
	L200:
		COMPLEX ds1, ds2;
		ds1 = U[M1[I]];
		ds2 = U[M2[I]];
		IT[I] = (U[M1[I]] - U[M2[I]]) / XI;
	}
}

void PVP118()
{
/*
	INTEGER    M, NFP, KPE, NFR, KIMX, N, NSM, NBU, N1, NSMM, M0, I,
     *           KPMAX, NFP5
*/
	int I;
/*
	REAL*4     KRAT, KSQR, PEROT,
*/
	double PEROT;
	CString str_Output;
	str_Output.Format(_T("%sТоки ветвей сети:"), CString(_T(' '), 25));
	NFP5_File.WriteString(str_Output);
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP5_File.WriteString(str_Output);
	NFP5_File.WriteString(_T(" Hомер│ Cос-│   Узел   │  Имя узла │Предел │   Tок, кA     │  Перегрузка   "));
	NFP5_File.WriteString(_T(" ветви│ тоя-│──────────│───────────│ тока, │───────────────│────────────── "));
	NFP5_File.WriteString(_T("      │ ние │ нач. кон.│ нач.  кон.│  кA   │ Актив. Pеакт. │     о.е.      "));
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP5_File.WriteString(str_Output);

	for (I = 1; I <= M; I++) //Do 60
	{
		// PEROT = CABS(IT(I)) / ADOP(I)
		PEROT = abs(IT[I]) / ADOP[I];
		str_Output.Format(_T(" %4d |%4d |%4d %4d | %4d %4d |%6.3lf |%7.3lf%7.3lf | %6.2lf%s"), I, MVK[I], M1[I], M2[I], NAME1[I], NAME2[I],
			ADOP[I], IT[I].real(), IT[I].imag(), PEROT, CString(_T(' '), 5));
		NFP5_File.WriteString(str_Output);
	}
	str_Output.Format(_T("└%s┘"), CString(_T('-'), 73));
	NFP5_File.WriteString(str_Output);
}

void RMV118()
{
/*
INTEGER   M, N, NNY, NKY, KODPOT, KI, M0, NSM, NBU, N1, NSMM,
*          KPE, NFP, NFR, KIMAX, KPMAX, I, NFP6, NFP8
*/
	int I, NNY, NKY;
/*
REAL      EPS/0.00000001/, REP, QLN, QLK, QTK, RSVSV, ASVSV,
*          AMAX, UMIN, UMAX, KRAT,
*          DELU1, DELU2, AMAX1, SQR3, KSQR, BP, EPSIL
*/
	double EPS(0.00000001), REP, QLN, QLK, QTK, RSVSV, ASVSV;
/*
COMPLEX*8 CNUL/(.0,.0)/, SPOT0,
*          SVN, SVK, U0, UNOM, CIT, SPOT, SVSV, SPOTT
*/
	COMPLEX CNUL(.0, .0), SVN, SVK, CIT, SVSV, SPOT0;
	CString str_Output;
	SPOT = CNUL;
	SPOTT = CNUL;
	if (KODPOT == 0) goto L90;
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s         │        Узлы         │  Коэффи- │    Потери мощности     "), CString(_T(' '), 6));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s   Номер │─────────────────────│   циент  │─────────────────────── "), CString(_T(' '), 6));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s   ветви │  начала  │  конца   │  транс-  │ Активной, │Реактивной, "), CString(_T(' '), 6));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s         │──────────│──────────│ формации │   МВт     │   МВАр     "), CString(_T(' '), 6));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s         │ N  │ Имя │ N  │ Имя │          │           │            "), CString(_T(' '), 6));
	NFP8_File.WriteString(str_Output);
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP8_File.WriteString(str_Output);
L90:
	for(I = 1; I <= M; I++)
	{
		NNY = M1[I];
		NKY = M2[I];
		if (MVK[I] == 1) goto L100_2;
	L110:  //нет указателя на L110
		PVN[I] = .0;
		PVK[I] = .0;
		QVN[I] = .0;
		QVK[I] = .0;
		continue;
	L100_2:
		CIT = conj(IT[I]);
		REP = REPR[I];
		if (KT[I] < EPSIL) goto L130;
		SVK = SQR3 * U[NKY] * CIT;
		PVK[I] = real(SVK);
		QTK = pow(REP * abs(U[NKY]), 2);
		QVK[I] = imag(SVK) - QTK;
		SVN = SQR3 * U[NNY] * CIT * KT[I];
		PVN[I] = real(SVN);
		QVN[I] = imag(SVN);
		goto L140;
	L130:
		if (abs(REP) > EPS) goto L120;
		SVN = SQR3 * U[NNY] * CIT;
		SVK = SQR3 * U[NKY] * CIT;
		PVN[I] = real(SVN);
		PVK[I] = real(SVK);
		QVN[I] = imag(SVN);
		QVK[I] = imag(SVK);
		goto L140;
	L120:
		QLN = pow(REP * abs(U[NNY]), 2) / 2.;
		QLK = pow(REP * abs(U[NKY]), 2) / 2.;
		SVN = SQR3 * U[NNY] * CIT;
		SVK = SQR3 * U[NKY] * CIT;
		PVN[I] = real(SVN);
		PVK[I] = real(SVK);
		QVN[I] = imag(SVN) + QLN;
		QVK[I] = imag(SVK) - QLK;
	L140:
		SVSV = SVN - SVK;
		RSVSV = abs(real(SVSV));
		ASVSV = abs(imag(SVSV)); 
		SPOT0 = COMPLEX(RSVSV, ASVSV);
		if (KODPOT == 1)
		{
			str_Output.Format(_T("%s    %4d │%4d %4d │%4d %4d │ %7.3lf  │  %7.3lf  │  %7.3lf   "), CString(_T(' '), 6), I, M1[I], NAME1[I], M2[I], NAME2[I], KT[I], SPOT0.real(), SPOT0.imag());
			NFP8_File.WriteString(str_Output);
		}
		SPOT = SPOT + SPOT0;
		if (KT[I] >= EPSIL) SPOTT = SPOTT + COMPLEX(RSVSV, ASVSV);
	}
	if (KODPOT == 1)
	{
		str_Output.Format(_T("%s└%s┘"), CString(_T(' '), 7), CString(_T('-'), 64));
		NFP8_File.WriteString(str_Output);
	}
	if (KI == 0) goto L500;
/*
	ПЕЧАТЬ МОЩНОСТЕЙ ВЕТВЕЙ
*/
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP6_File.WriteString(str_Output);
	NFP6_File.WriteString(_T("            │        Узлы         │            Mощности ветвей               "));
	NFP6_File.WriteString(_T("      Hомер │─────────────────────│───────────────────────────────────────── "));
	NFP6_File.WriteString(_T("      ветви │  начала  │  конца   │      Активные      │     Реактивные      "));
	NFP6_File.WriteString(_T("            │──────────│──────────│   начала,  конца,  │   начала,  конца,   "));
	NFP6_File.WriteString(_T("            │ N  │ Имя │ N  │ Имя │   МВт      МВт     │   МВар     МВар     "));
	str_Output.Format(_T("%s"), CString(_T('-'), 79));
	NFP6_File.WriteString(str_Output);
	for (I = 1; I <= M; I++)
	{
		str_Output.Format(_T("     %5d  │%4d %4d │%4d %4d │%8.1lf  %8.1lf  │%8.2lf  %8.2lf   "), I, M1[I], NAME1[I], M2[I], NAME2[I],PVN[I], PVK[I], QVN[I], QVK[I]);
		NFP6_File.WriteString(str_Output);
	}
	str_Output.Format(_T("%s└%s┘"), CString(_T(' '), 4), CString(_T('-'), 71));
	NFP6_File.WriteString(str_Output);
L500:;
}

void ZKS016()
{
/*
REAL *  4 DELP, DELPM, DELQ, DELQM,
*          PNEB, PNEBM, QNEB, QNEBM, QET,
*          SQR3, KSQR, QKYS,
*          BP, EPSIL, AMAX, UMN, UMX, KRAT,
*          DELU1, DELU2, AMAX1
*/
	double DELP, DELQ, PNEB, QNEB, QET, QKYS;
/*
INTEGER*4 M, N, NV1, NV2, NNV, NKV, N12, NYNP, NYNQ, NSM, NBU,
*          N1, NSMM, M0, KPE, NFP, NFR, KIMX, KPMAX, I, J, NBP,
*          NFP10
*/
	int NV1, NV2, NNV, NKV, N12, I, J, NBP;
/*
COMPLEX*8 U0, UNOM, SGEN, SNAG, SPOT, SPOTT, SPOTL
*/
	COMPLEX SPOT, SPOTT, SPOTL;
	CString str_Output;
	NFP_File.WriteString(_T(" Небалансы мощностей в узлах:"));
	/*
		MISMATCHS AT THE NODES P-Q, P-U -------------------------------
	*/
	SNAG = (0., 0.);
	SGEN = (0., 0.);
	N12 = N1 * 2;
	QET = 0.;
	QKYS = 0.;
	DELPM = 0.001;
	DELQM = 0.001;
	NYNP = 0;
	NYNQ = 0;
	for (I = 1; I <= N1; I++)
	{
		SNAG = SN[I] + SNAG;
		SGEN = SG[I] + SGEN;
		QET = QET - QL[I];
		QKYS = QKYS + QKY[I];
		NV2 = I * 2;
		NV1 = NV2 - 1;
		NNV = M3[NV1] - N12;
		NKV = M3[NV2] - N12;
		PNEB = -PZ[I];
		QNEB = -QZ[I] - QKY[I];
		for (J = NNV; J <= NKV; J++)
		{
			NBP = M3V[J];
			if(M1[NBP] == I) goto L20_2;
			PNEB = PNEB - PVK[NBP];
			QNEB = QNEB - QVK[NBP];
			continue;
		L20_2:
			PNEB = PNEB + PVN[NBP];
			QNEB = QNEB + QVN[NBP];
		}
		IY[I] = -COMPLEX(PNEB, -QNEB) * KSQR / conj(U[I]); //IY(I) = -CMPLX( PNEB,-QNEB ) * KSQR / CONJG(U(I))
		str_Output.Format(_T(" В узле%5d небаланс мощности:%8.3lf%8.3lf МВA, тока:%8.3lf%8.3lf A"), I, PNEB, QNEB, IY[I].real(), IY[I].imag());
		NFP_File.WriteString(str_Output);
		DELP = abs(PNEB);
		if (I == N1) continue;
		if(DELP <= DELPM) goto L30;
		DELPM = DELP;
		PNEBM = PNEB;
		NYNP = I;
	L30:
		DELQ = abs(QNEB);
		if (DELQ <= DELQM) continue;
		DELQM = DELQ;
		QNEBM = QNEB;
		NYNQ = I;
	}
	str_Output.Format(_T(" Генерируемая мощность системы...................%10.1lf MВт%10.1lf MВар"), SGEN.real(), SGEN.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Генерация реактивной мощности линий и потреб.тр.%s%10.1lf MВар"), CString(_T(' '), 14), QET);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Генерация реактивной мощности компенсир. устр...%s%10.1lf MВар"), CString(_T(' '), 14), QKYS);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потребляемая мощность в системе.................%10.1lf MВт%10.1lf MВар"), SNAG.real(), SNAG.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потери мощности в системе ......................%10.1lf MВт%10.1lf MВар"), SPOT.real(), SPOT.imag());
	NFP10_File.WriteString(str_Output);
	SPOTL = SPOT - SPOTT;
	str_Output.Format(_T(" Потери мощности в линиях .....................%10.1lf MВт%10.1lf MВар"), SPOTL.real(), SPOTL.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Потери мощности в трансформаторах ............%10.1lf MВт%10.1lf MВар"), SPOTT.real(), SPOTT.imag());
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Мощность в балансирующем узле ..................%10.1lf MВт%10.1lf MВар"), PNEB, QNEB);
	NFP10_File.WriteString(str_Output);
	PNEB = PNEB + real(SGEN) - real(SNAG) - real(SPOT);
	QNEB = QNEB + imag(SGEN) - imag(SNAG) - imag(SPOT) + QET + QKYS;
	str_Output.Format(_T(" Небаланс мощности по системе...................%10.1lf MВт%10.1lf MВар"), PNEB, QNEB);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Максимальный небаланс активной мощности  %10.2lf МВт  в узле%5d"), PNEBM, NYNP);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T(" Максимальный небаланс реактивной мощности%10.2lf МВар в узле%5d"), QNEBM, NYNQ);
	NFP10_File.WriteString(str_Output);
	str_Output.Format(_T("%s Максимальный небаланс узловой мощности : %s активной -%10.2lf МВт, реактивной -%10.2lf МВар."), CString(_T(' '), 17), CString(_T(' '), 10), DELPM, DELQM);
	NFP10_File.WriteString(str_Output);

}
