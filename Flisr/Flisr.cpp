﻿// Этот исходный код MFC Samples демонстрирует функционирование пользовательского интерфейса Fluent на основе MFC в Microsoft Office
// ("Fluent UI") и предоставляется исключительно как справочный материал в качестве дополнения к
// справочнику по пакету Microsoft Foundation Classes и связанной электронной документации,
// включенной в программное обеспечение библиотеки MFC C++.  
// Условия лицензионного соглашения на копирование, использование или распространение Fluent UI доступны отдельно.  
// Для получения дополнительных сведений о нашей лицензионной программе Fluent UI посетите веб-узел
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// (C) Корпорация Майкрософт (Microsoft Corp.)
// Все права защищены.

// Flisr.cpp : Определяет поведение классов для приложения.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "Flisr.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "FlisrDoc.h"
#include "FlisrView.h"
#include "Common.h"
#include "updateversion.h"

#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace bglex;

CString LastName(CString& Full)
{
	CString s = Full;
	int k = s.GetLength();
	if (s.Right(1) == _T("\\")) s = Full.Left(k - 1);
	k = s.ReverseFind(_T('\\'));
	int n = s.GetLength();
	n = n - k - 1;
	return s.Right(n);
}

CString GetExecuteDir(TCHAR m_szExecDir[])
{
	//	Получить директорию запуска
	HMODULE hModule = GetModuleHandle(NULL);
	DWORD nSize = MAX_PATH;         // size of buffer, in characters
	DWORD dw = GetModuleFileName(hModule, m_szExecDir, nSize);
	CString fn = CString(m_szExecDir);
	CString rg = LastName(fn);
	CString s;
	int i = fn.GetLength() - 1;
	fn = fn.Left(i - rg.GetLength());
	_tcscpy_s(m_szExecDir, nSize, fn);
	fn = CString(m_szExecDir);
	return fn;
}
// CFlisrApp

BEGIN_MESSAGE_MAP(CFlisrApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CFlisrApp::OnAppAbout)
	// Стандартные команды по работе с файлами документов
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// Стандартная команда настройки печати
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
	ON_COMMAND(ID_BUTTON2, &CFlisrApp::OnButton2)
END_MESSAGE_MAP()


// создание CFlisrApp

CFlisrApp::CFlisrApp()
{
	m_bHiColorIcons = TRUE;

	// поддержка диспетчера перезагрузки
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// Если приложение построено с поддержкой среды Common Language Runtime (/clr):
	//     1) Этот дополнительный параметр требуется для правильной поддержки работы диспетчера перезагрузки.
	//   2) В своем проекте для сборки необходимо добавить ссылку на System.Windows.Forms.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO: замените ниже строку идентификатора приложения строкой уникального идентификатора; рекомендуемый
	// формат для строки: ИмяКомпании.ИмяПродукта.СубПродукт.СведенияОВерсии
	SetAppID(_T("Flisr.AppID.1"));
	LPCTSTR lpsz = _tsetlocale(LC_CTYPE, _T("rus"));
	//setlocale(LC_ALL, ".1251");
	m_sDirName = GetExecuteDir(m_szExecDir);
	CString s, fn = m_sDirName + _T("\\Flisr.log");
	m_bLogFile = m_LogFile.Open(fn, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone) ? true : false;
	if (m_bLogFile)
	{
		m_LogFile.SeekToEnd();
		s.Format(_T("Folder of running %s\n---------------------------------------------------------------------------------"), m_sDirName);
		WriteToLog(s);
		m_DateCreation = _T("Creation date:  ");
		m_DateCreation += VERSION_BUILD_DDATE;
		m_VersionNumber = VERSION_STRING;
		s.Format(_T("\n%s\nVersion number: %s\n"), m_DateCreation, m_VersionNumber);
		WriteToLog(s);
	}

	flagConnectPostgre = false;
	flagLoadGraphFile = false;
//	LoadGraphFile(_T("graph.gg"));
//	ConnectPostgre();
//	if (!OnSlice()) AfxMessageBox(_T("Запуск провалился"));
//	
	//	if (!LFS210()) AfxMessageBox(_T("Запуск провалился"));	
	//	if (!SW00W21_MAIN()) AfxMessageBox(_T("Запуск провалился"));
	//  testBoostGraph();

}
void CFlisrApp::STOP()
{
	//AfxGetMainWnd()->SendMessage(WM_COMMAND, ID_APP_EXIT);
	AfxMessageBox(_T("STOP"));
}
// Единственный объект CFlisrApp

CFlisrApp theApp;


// инициализация CFlisrApp

BOOL CFlisrApp::InitInstance()
{
	// InitCommonControlsEx() требуются для Windows XP, если манифест
	// приложения использует ComCtl32.dll версии 6 или более поздней версии для включения
	// стилей отображения.  В противном случае будет возникать сбой при создании любого окна.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Выберите этот параметр для включения всех общих классов управления, которые необходимо использовать
	// в вашем приложении.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	// Инициализация библиотек OLE
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();

	EnableTaskbarInteraction();

	// Для использования элемента управления RichEdit требуется метод AfxInitRichEdit2()	
	// AfxInitRichEdit2();

	// Стандартная инициализация
	// Если эти возможности не используются и необходимо уменьшить размер
	// конечного исполняемого файла, необходимо удалить из следующего
	// конкретные процедуры инициализации, которые не требуются
	// Измените раздел реестра, в котором хранятся параметры
	// TODO: следует изменить эту строку на что-нибудь подходящее,
	// например на название организации
	SetRegistryKey(_T("Systel"));
	LoadStdProfileSettings(4);  // Загрузите стандартные параметры INI-файла (включая MRU)

// Register our unique class name that we wish to use
	WNDCLASS wndcls;

	memset(&wndcls, 0, sizeof(WNDCLASS));   // start with NULL defaults

	wndcls.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndcls.lpfnWndProc = ::DefWindowProc;
	wndcls.hInstance = AfxGetInstanceHandle();
	wndcls.hIcon = LoadIcon(IDR_MAINFRAME); // or load a different icon
	wndcls.hCursor = LoadCursor(IDC_ARROW);
	wndcls.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndcls.lpszMenuName = NULL;

	// Specify our own class name for using FindWindow later
	wndcls.lpszClassName = _T("FlisrClass");

	//CString str_ClassName = _T("GredClass");
	SendWMCOPYDATA(_T("GredClass"), 1, NULL, 1);
	// Register new class and exit if it fails
	if (!AfxRegisterClass(&wndcls))
	{
		TRACE("Class Registration Failed\n");
		bClassRegistered = false;
	}
	else bClassRegistered = true;

	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Зарегистрируйте шаблоны документов приложения.  Шаблоны документов
	//  выступают в роли посредника между документами, окнами рамок и представлениями
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_FlisrTYPE,
		RUNTIME_CLASS(CFlisrDoc),
		RUNTIME_CLASS(CChildFrame), // настраиваемая дочерняя рамка MDI
		RUNTIME_CLASS(CFlisrView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// создайте главное окно рамки MDI
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;


	// Разрешить использование расширенных символов в горячих клавишах меню
	CMFCToolBar::m_bExtCharTranslation = TRUE;

	// Синтаксический разбор командной строки на стандартные команды оболочки, DDE, открытие файлов
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);



	// Команды диспетчеризации, указанные в командной строке.  Значение FALSE будет возвращено, если
	// приложение было запущено с параметром /RegServer, /Register, /Unregserver или /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// Главное окно было инициализировано, поэтому отобразите и обновите его
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}

bool CFlisrApp::SendWMCOPYDATA(CString ClassName, BYTE bt, BYTE Message[], int cb)
{
	CWnd *pWnd = CWnd::FindWindow(ClassName, NULL);
	LRESULT copyDataResult;
	if (pWnd)
	{
		COPYDATASTRUCT cpd;
		cpd.dwData = 0x0010;  // Код Flisr
		cpd.cbData = cb;
		cpd.lpData = &bt;
		copyDataResult = pWnd->SendMessage(WM_COPYDATA,
			(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
			(LPARAM)&cpd);
		if (copyDataResult != cpd.dwData) PutToLogWnd(_T("Gred doesn't response!"));
	}
	//  Отправка сообщения
	//int i, n = Message.GetLength();
	//COPYDATASTRUCT cd;
	//char* Buf = new char[n + 1];
	//for (i = 0; i < n; i++) Buf[i] = Message[i];
	//Buf[n] = 0;
	//cd.lpData = Buf;
	//cd.cbData = n + 1;
	//SendMessage(hWnd, WM_COPYDATA, (WPARAM)0, (LPARAM)&cd);
	//delete[] Buf;

	return true;
}

CObjectTableElement::CObjectTableElement()
{
	m_ObjectIndex = 0;
	m_ObjectTypeIndex = 0;
	m_ObjectName.Empty();
	m_ModelNumber = 0;
}

CObjectTableElement::~CObjectTableElement()
{
}

int CFlisrApp::ReadObjectTable()
{
	CObjectTableElement de;
	CDatabase* db = NULL;
	CODBCRecordset* pRS;
	CString s, lpszSQL;
	long Index;
	int i;
//	lpszSQL.Format(_T("select * from ObjectTable %s ;"), Where);
	if (m_dbType == PostgreSQL)
	{
		lpszSQL.Format(_T("SELECT  * from \"ObjectTable\" order by ObjectTypeIndex,ObjectIndex;"));
	}
	m_MapObjectTable.clear();
	if (!OpenODBC(m_strZerverDB, db, pRS, lpszSQL)) return false;
	while (!pRS->IsEOF())
	{
		Index = pRS->Field(_T("ObjectIndex")).AsLong();
		de.m_ObjectIndex = Index;
		de.m_ObjectTypeIndex = pRS->Field(_T("ObjectTypeIndex")).AsLong();
		de.m_ObjectName = pRS->Field(_T("ObjectName")).AsString();
		if ((i = pRS->GetFieldID(_T("ModelNumber"))) != -1) de.m_ModelNumber = pRS->Field(i).AsLong(); else de.m_ModelNumber = 0;
		if ((i = pRS->GetFieldID(_T("Reserv"))) != -1) de.m_Reserv = pRS->Field(i).AsLong(); else de.m_Reserv = 0;
		m_MapObjectTable[Index] = de;
		pRS->MoveNext();
	}
	pRS->Close();
	db->Close();
	delete pRS;
	delete db;
	return true;
}

int CFlisrApp::ExitInstance()
{
	//TODO: обработайте дополнительные ресурсы, которые могли быть добавлены
	AfxOleTerm(FALSE);

	return CWinAppEx::ExitInstance();
}

// обработчики сообщений CFlisrApp


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// Команда приложения для запуска диалога
void CFlisrApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CFlisrApp настройка методов загрузки и сохранения

void CFlisrApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
	bNameValid = strName.LoadString(IDS_EXPLORER);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EXPLORER);
}

void CFlisrApp::LoadCustomState()
{
}

void CFlisrApp::SaveCustomState()
{
}
void CFlisrApp::WriteToLog(CString s)
{
	if (m_bLogFile)
	{
		
		CTime t = CTime::GetCurrentTime();
		CString ss;
		long size = (long)m_LogFile.GetPosition();//кол-во байт
		if (size > 1200000)
		{
			m_LogFile.Close();

			CString fn = m_sDirName + _T("\\Flisr.log");
			TrimLogFile(fn, 800000, 1200000);
			m_bLogFile = m_LogFile.Open(fn, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::shareDenyNone) ? true : false;
		}
		ss = t.Format(_T("%d.%m.%Y  %H:%M:%S"));
		ss += _T("  ");
		ss += s;
		ss += _T("\n");
		m_LogFile.WriteString(ss);
		m_LogFile.Flush();
	}
}
int CFlisrApp::PutToLogWnd(const CString& Message)
{
	CMainFrame* pWnd = static_cast<CMainFrame*> (AfxGetMainWnd());
	if (pWnd) pWnd->PutToLogWnd(Message);
	return 1;
}

void CFlisrApp::TrimLogFile(CString path, ULONGLONG m_MinLogSize, ULONGLONG m_MaxLogSize)
{
	CString str;
	if (path.IsEmpty()) return;

	CFileException e;
	CFile LogFile;
	if (!LogFile.Open(path, CFile::modeRead, &e))
	{
		return;
	}
	ULONGLONG count, len = LogFile.GetLength();
	if (len < m_MaxLogSize) { LogFile.Close(); return; }

	// выделяем память
	char* buff = new char[m_MinLogSize];
	LogFile.Seek(0 - m_MinLogSize, CFile::end);
	count = LogFile.Read(buff, m_MinLogSize);
	LogFile.Close();

	if (!LogFile.Open(path, CFile::modeCreate | CFile::modeWrite, &e))
	{
		delete[] buff;
		return;
	}

	// пишем буфер в файл
	LogFile.Write(buff, count);

	LogFile.Flush();
	LogFile.Close();
	delete[] buff;
}

Vertex TopologyGraph::AddVertex(const VertexProp& prop)
{
	Vertex v = 0;
	v = boost::add_vertex(m_topology);
	m_topology[v] = prop;
	return v;
}
void TopologyGraph::saveGraph(std::ofstream & ofs)
{
	boost::archive::text_oarchive oa(ofs); 
	oa << m_topology;
}

void TopologyGraph::loadGraph(std::ifstream & ifs)
{
	m_topology.clear();
	boost::archive::text_iarchive ia(ifs);
	ia >> m_topology;
}

Edge TopologyGraph::AddEdge(Vertex u, Vertex v, const EdgeProp& prop)
{
	//std::pair<Edge, bool> pr;
	//pr = boost::add_edge(u, v, m_topology);
	//Edge eEdge = pr.first;
	Edge eEdge = boost::add_edge(u, v, m_topology).first;
	m_topology[eEdge] = prop;
	return eEdge;
}

void TopologyGraph::WidthSearch(Vertices &vecVertexChain, int paramVertex)
{
	CBFS vis(&vecVertexChain, paramVertex, nullptr);
	bglex::breadth_first_search_ex(m_topology, vertex(paramVertex, m_topology), color_map(get(&VertexProp::color, m_topology)).visitor(vis));
}

bool CFlisrApp::OnSlice()
{
	CString strOutputT;
	int i, ihr, imin, isec, iyr, imon, iday;
	NFR_FileT.Open(_T("dateGrid.W02"), CFile::modeRead);
	NFP_FileT.Open(_T("outputFile.W00"), CFile::modeWrite | CFile::modeCreate);
	//дата , время в лог файл
	CTime t = CTime::GetCurrentTime();
	iyr = t.GetYear();
	imon = t.GetMonth();
	iday = t.GetDay();
	ihr = t.GetHour();
	imin = t.GetMinute();
	isec = t.GetSecond();
	strOutputT.Format(_T("%sДата: %3d%3d%5d Время :%4d час%4d мин.-%4d.%2d\n"), CString(_T(' '), 12), iday, imon, iyr, ihr, imin, isec);
	NFP_FileT.WriteString(strOutputT);
	strOutputT.Format(_T("\n%s┌%s┐\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP_FileT.WriteString(strOutputT);
	theApp.WriteToLog(strOutputT);

	/*	Чтение данных из файла	*/
	if (!readDateFile()) AfxMessageBox(_T("ошибка. не прочитался файл sd3098.W02"));
	NFR_FileT.Close();
	NFP_FileT.Close();
	/*	постоение графа	*/
	if (!CreateBoostGraf()) AfxMessageBox(_T("построение графа провалилось"));
	return TRUE;
}

bool CFlisrApp::readDateFile()
{
	CString sVal, strOutputT;
	/*	Код режима сужения поиска по МО хар ПП ДК макс: 1-да, 0-нет;*/
	CString sInput, tmpStr;
	NFR_FileT.ReadString(sInput);
	strOutputT.Format(_T("%sХарактеристики сети%s\n"), CString(_T('─'), 26), CString(_T('─'), 26));
	NFP_FileT.WriteString(strOutputT);

	NFR_FileT.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	theApp.m_nEdgeT = _tstoi(tmpStr);
	strOutputT.Format(_T(" %10d - чиcло ветвей\n"), theApp.m_nEdgeT);
	NFP_FileT.WriteString(strOutputT);

	NFR_FileT.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	theApp.m_nVertexT = _tstoi(tmpStr);
	strOutputT.Format(_T(" %10d - чиcло узлов без базисного\n"), theApp.m_nVertexT);
	NFP_FileT.WriteString(strOutputT);
	//данные по ветвям
	for (int i = 0; i < 9; i++)
	{
		NFR_FileT.ReadString(sInput); /////////// - 9 строк пропуска
	}
	for (int i = 0; i < theApp.m_nEdgeT; i++)
	{
		NFR_FileT.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		sVal = tmpStr.Left(13);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 14);
		theApp.arrayM1[i] = _tstoi(sVal.Right(6));
		theApp.arrayM2[i] = _tstoi(tmpStr.Left(6));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 36);
		theApp.arrayEdgeState[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 20);
		theApp.arrayEdgeType[i] = _tstoi(tmpStr.Left(2));
	}
	//данные по узлам
	for (int i = 0; i < 9; i++)
	{
		NFR_FileT.ReadString(sInput); /////////// - 9 строк пропуска
	}
	for (int i = 0; i < theApp.m_nVertexT; i++)
	{
		NFR_FileT.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 42);
		theApp.arrayVertexType[i] = _tstoi(tmpStr.Left(4));
	}
	return true;
}

bool CFlisrApp::CreateBoostGraf()
{
	Vertices vecVertices;
	Edges vecEdges;
	VertexProp VertProperty;
	EdgeProp EdgeProperty;
	//TopologyGraph::SearchParams param;
	//добавление вершин
	for (int i = 0; i < m_nVertexT; i++)
	{
		VertProperty.m_index = i;
		VertProperty.m_nState = 0;
		VertProperty.m_nPower = 0;
		VertProperty.m_nType = arrayVertexType[i];
		vecVertices.push_back(gGraph.AddVertex(VertProperty));
	}
	//добавление рёбер
	for (int i = 0; i < m_nEdgeT; i++)
	{
		EdgeProperty.m_index = i;
		EdgeProperty.m_nState = arrayEdgeState[i];
		EdgeProperty.m_nPower = 0;
		EdgeProperty.m_nType = arrayEdgeType[i];
		vecEdges.push_back(gGraph.AddEdge(arrayM1[i], arrayM2[i], EdgeProperty));
	}

	return true;
}

template <typename Vertex, typename Graph>
void CBFS::examine_vertex_ex(Vertex u, const Graph& g, bool& res) const
{
	int a = 0;
	v->push_back(u);
}

template <typename Edge, typename Graph>
void CBFS::examine_edge_ex(Edge e, const Graph& g, bool& res) const
{
	int a = g[e].m_index;
	if (g[e].m_nState == 0)
		res = false;
}

void CFlisrApp::OnButton2()
{
	int paramVertex = 0;
	Vertices SearchVertices;
	CBFS vis(&SearchVertices, paramVertex, nullptr);
	bglex::breadth_first_search_ex(gGraph.m_topology, vertex(0, gGraph.m_topology), color_map(get(&VertexProp::color, gGraph.m_topology)).visitor(vis));
}

void CFlisrApp::ConnectPostgre(CString strConnectSql)
{
	if (strConnectSql.GetLength() == 0) 
	{
		strConnectSql = _T("Driver={PostgreSQL Unicode};Server=172.16.10.7;Port=5432;Database=ZerverDB;Uid=sa;Pwd=sql;");
	}
	m_strZerverDB = strConnectSql;
	if (ReadObjectTable())
	{
		flagConnectPostgre = true;
	}
}

int CFlisrApp::LoadGraphFile(CString strFilePath)
{
	//std::ofstream ofs("graph_1.gg");
	//gGraph.saveGraph(ofs);

	std::ifstream ifs("graph.gg");
	if (!ifs) {   //if (GetFileAttributes(ifs) == 0xffffffff)
		AfxMessageBox(_T("read error!"));
		return false;
	}

	gGraph.loadGraph(ifs);
	if (gGraph.m_topology.m_vertices.size() > 0)
	{
		flagLoadGraphFile = true;
	}
	return true;
}

void CFlisrApp::FillInGraph()
{
	Vertices vecVertices;
	Edges vecEdges;
	VertexProp VertProperty;
	EdgeProp EdgeProperty;
	int m_ObjectIndex;
	for (int i = 0; i <theApp.gGraph.m_topology.m_vertices.size(); i++)
	{
		m_ObjectIndex = theApp.gGraph.m_topology.m_vertices[i].m_property.m_objectIndex;
		if((it_MapObjectTable = m_MapObjectTable.find(m_ObjectIndex)) != m_MapObjectTable.end())
		{
			theApp.gGraph.m_topology.m_vertices[i].m_property.m_nType = (*it_MapObjectTable).second.m_ObjectTypeIndex;
			theApp.gGraph.m_topology.m_vertices[i].m_property.m_strName = (*it_MapObjectTable).second.m_ObjectName;
		}
		
	}
	//добавление рёбер
	for (int i = 0; i < theApp.gGraph.m_topology.m_edges.size(); i++)
	{
		auto it = theApp.gGraph.m_topology.m_edges.begin();
		for (it; it != theApp.gGraph.m_topology.m_edges.end(); ++it)
		{
			m_ObjectIndex = (*it).m_property.m_objectIndex;
			if ((it_MapObjectTable = m_MapObjectTable.find(m_ObjectIndex)) != m_MapObjectTable.end())
			{
				(*it).m_property.m_nType = (*it_MapObjectTable).second.m_ObjectTypeIndex;
				(*it).m_property.m_strName = (*it_MapObjectTable).second.m_ObjectName;
			}			
		}		
	}

}

void CFlisrApp::TestRepository()
{
	CString out;
}