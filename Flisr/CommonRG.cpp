﻿#include "stdafx.h"
#include "CommonRG.h"

// namespace support is now required
#ifdef NO_NAMESPACE
#	error namespace support is now required
#endif
#define NAMESPACE_BEGIN(x) namespace x {
#define NAMESPACE_END }

CString strOutputRG;
COMPLEX Urg[78], U0, UNOM, Xrg[131], IYrg[78], ITrg[131];

int M = 130, N = 76, N1 = 77, NSM = 454, NSMM = 454, NBU, NSMV = 300, KNDErg = 290, KNDEN = 290, KSTEKrg = 290, MMAXrg = 130, N1MAX = 77, NMAXrg = 76, NSMAX = 454, NSMVM = 300, KNDEM = 290, KSTEKM = 390;

double BP, DELU1, DELU2, ADOPrg[130], KVX, BETA1, BETA2, POR[5], UMIN, UMAX, KRAT, SQR3, KSQR;
int IHrg[78], IYPHrg[290], IYSTEKrg[390], MVKrg[131], KPE = 0, NPE[20], NYPHrg[290], KVIYrg[78], MYPQrg[78], MYYrg[78], TIME, KPMAX, MXPPRK, KYP, KR1PM, KR2PM, KR3PM, KR4PM, KK1PM, KK2PM, KK3PM, KK4PM;


int PRKDKS, PERKDK, PTLIAN, KODPC, KODRSP, NSW[131], NYSL, KODYIT, KIMX, KODPTV, KODPIT;
int AVR[131], ZAVR[131], TPRP[78], PORMAX = 5, MD[131];
double LOW1, LOW2, LOW3, LOW4;
CString NAZVY[78];
int M1rg[131], M2rg[131], M3rg[454], M3Vrg[300];
int MVKN[130], NTY, NYNI, KVIYN[78], IHN[78], NYPHN[290], IYPHN[290], KFS, KFOS, KEAVRV, NEAVRV[131], KEAVRN, KYOS;
int STEK[78], INDXY[78], MFSY[78], INDXYK[78], INDXF[78], NVPH[290], NVPHN[290], NEAVRN[78], KESTV, STEKV[650], INDXV[131], INDXVK[131], KVIYX[77], IHX[77], IYPHX[290];
int KMD = 0, KPU, MYV[78], KODSEA = 0;
COMPLEX ITX[130], IYF[78], IYX[77];
double RITX, PEROT[131];

CStdioFile NFR_FileRG,  //чтение
NFP_FileRG,  //запись
NFPU3_FileRG,  //запись
NFPU4_FileRG,  //запись
NFPU7_FileRG,  //запись
NFPU8_FileRG,  //запись
NFPU9_FileRG,  //запись
NFPU12_FileRG;  //запись

bool SW00W21_MAIN()
{
	int i, kkkk, ihr, imin, isec, i100th, nvrpv, iyr, imon, iday;
	double vrem0, tvrem;
	NFR_FileRG.Open(_T("sd3098.W02"), CFile::modeRead);
	NFP_FileRG.Open(_T("sl3098.W00"), CFile::modeWrite | CFile::modeCreate);
	NFPU3_FileRG.Open(_T("sl3098.W03"), CFile::modeWrite | CFile::modeCreate);
	NFPU4_FileRG.Open(_T("sl3098.W04"), CFile::modeWrite | CFile::modeCreate);
	NFPU7_FileRG.Open(_T("sl3098.W07"), CFile::modeWrite | CFile::modeCreate);
	NFPU8_FileRG.Open(_T("sl3098.W08"), CFile::modeWrite | CFile::modeCreate);
	NFPU9_FileRG.Open(_T("sl3098.W09"), CFile::modeWrite | CFile::modeCreate);
	NFPU12_FileRG.Open(_T("sl3098.W12"), CFile::modeWrite | CFile::modeCreate);

	NFPU3_FileRG.WriteString(_T("test"));
	//дата , время в лог файл
	CTime t = CTime::GetCurrentTime();
	iyr = t.GetYear();
	imon = t.GetMonth();
	iday = t.GetDay();
	ihr = t.GetHour();
	imin = t.GetMinute();
	isec = t.GetSecond();

	i100th = 0;
	vrem0 = ihr*3600.0 + imin*60.0 + isec + i100th / 100.0;
	tvrem = vrem0;
	nvrpv = 0;

	strOutputRG.Format(_T("%sДата: %3d%3d%5d Время :%4d час%4d мин.-%4d.%2d сек.\n"), CString(_T(' '), 12), iday, imon, iyr, ihr, imin, isec, i100th);
	NFP_FileRG.WriteString(strOutputRG);
	strOutputRG.Format(_T("\n%s┌%s┐\n"), CString(_T(' '), 5), CString(_T('-'), 62));
	NFP_FileRG.WriteString(strOutputRG);
	theApp.WriteToLog(strOutputRG);

	kkkk = 7900 * 8 * 20 + 10000;
	strOutputRG.Format(_T(" You have been allocated : %10d bytes of memory !!!\n"), kkkk);
	/*	Чтение данных из входного потока	*/
	if (!RE00W21()) AfxMessageBox(_T("ошибка. не прочитался файл sd3098.W02"));
	/*	Инициализация основных и вспомогательных массивов	*/
//	CreateBoostGraf();
	IM00W21();
	for (i = 1; i <= N1; i++)
	{
		KVIYX[i] = KVIYN[i];
		IHX[i] = IHN[i];
		IYPHX[i] = IYPHN[i];
	}
	for (i = N + 1; i <= KNDErg; i++)
	{
		IYPHX[i] = IYPHN[i];
	}

	DE00W1(IHX, IYPHX, NYPHN, KVIYX, IYX, ITX, NVPHN);
	CH00W1(ITX, IYX);

	if (KPU != 0)
	{
		NFP_FileRG.WriteString(_T(" Задание завершено по превышении предела напряжения"));
		goto L_19;
	}
	CR30W21(ITX, KPE, MVKN, NPE);
	CB00W1(ITX, MVKN);

	////	if (kpe == 0)  goto L_500;
	KMD = 0;
	//int NNV, NKV, NYSD, NYKD;
	//for (i = 1; i <= kpe; i++)
	//{
	//	j = npe(i);
	//	//!Определение узла ствола дерева
	//	NNV = M1[j];
	//	NKV = M2[j];
	//	//!NYSD - номер узла ствола дерева
	//	//!NYKD - номер узла корня дерева
	//	RITX = (double)ITX[j].real();
	//	if (RITX >= 0)
	//	{
	//		NYSD = NKV;
	//		NYKD = NNV;
	//	}
	//	else
	//	{
	//		NYSD = NNV;
	//		NYKD = NKV;
	//	}
	//}
//L_500:
L_19:

	NFR_FileRG.Close();
	NFP_FileRG.Close();
	NFPU3_FileRG.Close();
	NFPU4_FileRG.Close();
	NFPU7_FileRG.Close();
	NFPU8_FileRG.Close();
	NFPU9_FileRG.Close();
	NFPU12_FileRG.Close();
	return TRUE;
}

bool RE00W21()
{
	CString sVal;
	int i, j;
	/*	Код режима сужения поиска по МО хар ПП ДК макс: 1-да, 0-нет;*/
	KODRSP = 1;
	CString sInput, tmpStr;
	NFR_FileRG.ReadString(sInput);
	strOutputRG.Format(_T("%sХарактеристики поиска%s\n"), CString(_T('─'), 25), CString(_T('─'), 25));
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	TIME = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10.2lf - ограничение на время счета (c)\n"), TIME);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	MXPPRK = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - мaкcимaльное чиcло устранений перегрузок ( <= 15 )\n"), MXPPRK);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KYP = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - мaкcимaльное чиcло уровней поиcкa ( 0 <= 4 )\n"), KYP);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KPMAX = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - допуcтимое чиcло перегрузок иcходного режимa ( <= 20 )\n"), KPMAX);
	NFP_FileRG.WriteString(strOutputRG);

	KVX = 1000.0; //- множитель перевода хар в INT * 2:
	BETA1 = 3.0;  //- коэффициент при сигма для определения ординат хар :		
	BETA2 = 1.25; // - коэффициент при хар - 1 с одной ПП ДК 1 - го уровня :

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KR1PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - мaкcимaльное чиcло устранений перегрузок нa 1 переключение\n"), KR1PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KR2PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ........................................... 2 переключения\n"), KR2PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KR3PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ........................................... 3 переключения\n"), KR3PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KR4PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ........................................... 4 переключения\n"), KR4PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KK1PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - мaкcимaльное чиcло снижений перегрузок нa 1 переключение\n"), KK1PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KK2PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ......................................... 2 переключения\n"), KK2PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KK3PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ......................................... 3 переключения\n"), KK3PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KK4PM = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - ......................................... 4 переключения\n"), KK4PM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	UMIN = _tstof(tmpStr);
	strOutputRG.Format(_T(" %10.2lf - нижний предел нaпряжений, кВ\n"), UMIN);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	UMAX = _tstof(tmpStr);
	strOutputRG.Format(_T(" %10.2lf - верхний предел нaпряжений, кВ\n"), UMAX);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	strOutputRG.Format(_T("%sХарактеристики сети%s\n"), CString(_T('─'), 26), CString(_T('─'), 26));
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	M = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - чиcло ветвей\n"), M);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	N = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d - чиcло узлов без базисного\n"), N);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	NSM = _tstoi(tmpStr);
	NSMM = NSM;
	NBU = N + 1;
	N1 = NBU;
	strOutputRG.Format(_T(" %10d - рaзмерноcть мaccивa cвязноcти ( <= 420 )\n"), NSM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	U0 = _tstof(tmpStr);
	strOutputRG.Format(_T("%7.2lf %6.2lf    - нaпряжение узлa бaлaнca ( бaзиca ) /кВ/\n"), U0.real(), U0.imag());
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	UNOM = _tstof(tmpStr);
	strOutputRG.Format(_T("%7.2lf %6.2lf    - номинaльное нaпряжение /кВ/\n"), UNOM);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	BP = _tstof(tmpStr);
	strOutputRG.Format(_T(" %10.2lf- множитель изменения длительно-допуcтимых токов\n"), BP);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KRAT = _tstof(tmpStr);
	strOutputRG.Format(_T(" %10.2lf- множитель нaгрузки cиcтемы\n"), KRAT);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.10s"), sInput);
	KODPC = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %10d- сеть замкнутая( 1 ) или разомкнутая( 0 )\n"), KODPC);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	strOutputRG.Format(_T("%sХарактеристики расчета режима%s\n"), CString(_T('─'), 29), CString(_T('─'), 29));
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.9s"), sInput);
	KODYIT = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %9d - итерaционное уточнение режимa потомкa:    1-дa, 0-нет\n"), KODYIT);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.9s"), sInput);
	KIMX = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %9d - мaкcимaльное чиcло итерaций\n"), KIMX);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.9s"), sInput);
	DELU2 = _tstof(tmpStr);
	strOutputRG.Format(_T(" %9.2lf - точность расчета режима по напряжению /кВ/\n"), DELU2);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.9s"), sInput);
	DELU1 = _tstof(tmpStr);
	strOutputRG.Format(_T(" %9.2lf - точночть напряжения для переcчетa узловых токов /кВ/\n"), DELU1);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	strOutputRG.Format(_T("%sПечaть:%s\n"), CString(_T('─'), 33), CString(_T('─'), 33));
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	PRKDKS = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %8d -  отcортировaнных ДК, РК: 1-дa, 0-нет\n"), PRKDKS);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	PERKDK = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %8d -  действительных  ДК, РК: 1-дa, 0-нет\n"), PERKDK);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	KODPTV = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %8d -  токов ветвей :          1-дa, 0-нет\n"), KODPTV);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	KODPIT = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %8d -  итерaций по узлам:      1-дa, 0-нет\n"), KODPIT);
	NFP_FileRG.WriteString(strOutputRG);

	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.8s"), sInput);
	PTLIAN = _tstoi(tmpStr);
	strOutputRG.Format(_T(" %8d -  результатов грубого поиска: 1-дa, 0-нет\n"), PTLIAN);
	NFP_FileRG.WriteString(strOutputRG);

	//данные по ветвям
	for (i = 0; i < 9; i++)
	{
		NFR_FileRG.ReadString(sInput); /////////// - 9 строк пропуска
	}
	int ttt = M;
	for (i = 1; i <= M; i++)
	{
		NFR_FileRG.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		sVal = tmpStr.Left(11);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 6);
		M1rg[i] = _tstoi(sVal.Right(6));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		M2rg[i] = _tstoi(tmpStr.Left(6));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 7);
		ADOPrg[i] = _tstof(tmpStr.Left(8));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 9);
		Xrg[i].real(_tstof(tmpStr.Left(9)));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 10);
		Xrg[i].imag(_tstof(tmpStr.Left(9)));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 10);
		MVKrg[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		NSW[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		AVR[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		ZAVR[i] = _tstoi(tmpStr.Left(4));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 5);
		MD[i] = _tstoi(tmpStr.Left(2));
	}
	//печать данных по ветвям
	strOutputRG.Format(_T("%s Данные по ветвям %s\n\
| По- |    Узлы     | До -   |   Сопротивление,  |Сос-|За- |АВР:|За- |\n\
| ряд-|─────────────| пус -  |        Ом         |тоя-|прет|выс.|прет|\n\
| ко- | На-  | Кон- | ти -   |───────────────────|ние:|пер.|134 |АВР |\n\
| вый | ча-  | ца   | мые    | Актив-  | Реак-   |1-  |1 - |низ.|1-  |\n\
| но- | ла   |      | то -   | ное     | тив-    |вк. |да, |-2  |да, |\n\
| мер |      |      | ки, А  |         | ное     |0-  |0 - |нет |0-  |\n\
|     |      |      |        |         |         |от. |нет |-0  |нет |\n\
+%s+\n"), CString(_T('─'), 26), CString(_T('─'), 26), CString(_T('─'), 70));
	NFP_FileRG.WriteString(strOutputRG);
	for (i = 1; i <= M; i++)
	{
		strOutputRG.Format(_T("|%5d│%6d│%6d│%8.1lf│%9.4lf│%9.1lf│%4d│%4d│%4d│%4d│%2d\n"),
			i, M1rg[i], M2rg[i], ADOPrg[i], Xrg[i].real(), Xrg[i].imag(), MVKrg[i], NSW[i], AVR[i], ZAVR[i], MD[i]);
		NFP_FileRG.WriteString(strOutputRG);
	}
	//данные по узлам
	for (i = 0; i < 9; i++)
	{
		NFR_FileRG.ReadString(sInput); /////////// - 9 строк пропуска
	}
	for (i = 0; i < N; i++)
	{
		NFR_FileRG.ReadString(sInput);
		tmpStr.Format(_T("%.100s"), sInput);
		sVal = tmpStr.Left(18); // I4
		IYrg[i].real(_tstof(sVal.Right(11)));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 19);
		IYrg[i].imag(_tstof(tmpStr.Left(10)));
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 11);
		NAZVY[i] = tmpStr.Left(11);
		tmpStr = tmpStr.Right(tmpStr.GetLength() - 12);
		TPRP[i] = _tstoi(tmpStr.Left(4));
		//время работы ЗМН
	}
	//печать данных по узлам
	strOutputRG.Format(_T("%s Данные по узлам %s\n\
| По- |     Составляющие     |           |Сек-|Вpе- |\n\
| ряд-|         токов        | Назва-    |ция |мя   |\n\
| ко- |        узлов, А      | ния       |РП- |pа-  |\n\
| вый | ─────────────────────| РП и      |1,  |боты |\n\
| но- | Активная  | Реактив- | ТП        |ТП- |ЗМН, |\n\
| мер |           | ная      |           |2,0-|с    |\n\
|     |           |          |           |нет |     |\n\
+%s+\n"), CString(_T('─'), 26), CString(_T('─'), 26), CString(_T('─'), 70));
	NFP_FileRG.WriteString(strOutputRG);
	for (i = 1; i <= N; i++)
	{
		strOutputRG.Format(_T("|%5d│%11.1lf│%10.1lf│%11s│%4d│  ?  │\n"),
			i, IYrg[i].real(), IYrg[i].imag(), NAZVY[i], TPRP[i]);
		NFP_FileRG.WriteString(strOutputRG);
	}
	NFR_FileRG.ReadString(sInput);
	NFR_FileRG.ReadString(sInput);
	tmpStr.Format(_T(" %.7s"), sInput);
	NYSL = _tstoi(tmpStr);
	strOutputRG.Format(_T("%6d -  уровень сложности просматриваемых вариантов\n"), NYSL);
	NFP_FileRG.WriteString(strOutputRG);
	return true;
}

//bool CreateBoostGraf()
//{	
//	Vertices vecVertices;
//	Edges vecEdges;
//	VertexProp VertProperty;
//	EdgeProp EdgeProperty;
//	TopologyGraph gGraph;
//	//добавление вершин
//	for (i = 0; i <= N+1; i++)
//	{
//		VertProperty.m_nVertexIndex = i;
//		vecVertices.push_back(gGraph.AddVertex(VertProperty));
//	}
//	//добавление рёбер
//	for(i=0; i<M; i++)
//	{ 
//		EdgeProperty.m_nEdgeIndex = i+1;
//		vecEdges.push_back(gGraph.AddEdge(M1[i+1], M2[i+1], EdgeProperty));
//	}
//	return true;
//	/*
//	typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;
//
// //   // Make convenient labels for the vertices
// //   enum { A, B, C, D, E, N };
// //   const int num_vertices = N;
// //   const char* name = "ABCDE";
//
// //   // writing out the edges in the graph
// //   typedef std::pair<int, int> Edge;
// //   Edge edge_array[] = 
// //   { Edge(A,B), Edge(A,D), Edge(C,A), Edge(D,C),
// //     Edge(C,E), Edge(B,D), Edge(D,E) };
// //   const int num_edges = sizeof(edge_array)/sizeof(edge_array[0]);
//
// //   // declare a graph object
// //   Graph g(num_vertices);
//
// //   // add the edges to the graph object
// //   for (int i = 0; i < num_edges; ++i)
// //     add_edge(edge_array[i].first, edge_array[i].second, g);*/
//}

bool IM00W21()
{
	int NNYV, NKY, NNY, KV;
	int i, j, iii = 0;
	KSTEKrg = 0;
	SQR3 = sqrt(3.0);
	KSQR = 1.0 / SQR3;
	/*		Формирование списков смежности сети	*/
	FM00W21();

	/*		Инициализация количеств узловых связей	*/
	NNYV = N1 * 2;
	j = 0;
	for (i = 2; i <= NNYV; i += 2)
	{
		j = j + 1;
		NKY = M3rg[i];
		NNY = M3rg[i - 1];
		KVIYrg[j] = NKY - NNY + 1;
		KVIYN[j] = KVIYrg[j];
	}
	MY00W1();

	for (i = 1; i <= N1; i++)
	{
		IHN[i] = IHrg[i];
		IYrg[i] = IYrg[i] * KRAT / 1000.0;
		IYX[i] = IYrg[i];
		MFSY[i] = 0;
	}

	for (i = 1; i <= KNDErg; i++)
	{
		IYPHN[i] = IYPHrg[i];
		NYPHN[i] = NYPHrg[i];
		NVPHN[i] = NVPH[i];
	}

	for (i = 1; i <= M; i++)
	{
		MVKN[i] = MVKrg[i];
	}
	KNDEN = KNDErg;

	/*		Удаление нормально отключенных ветвей	*/
	OFM0W1();

	/*	Формирование изолированных участков сети по узлам:*/
	IYS0W1();

	if (KFS > 1)
	{
		/*	Моделирование работы автоматики в с/в части сети:*/
		RAV0W1();

		if (KFOS >= KFS) goto L_400;
		/*	Моделирование работы автоматики в н / в части сети :*/
		RAN0W1(IYX);

		/*	Формирование изолированных участков сети по ветвям :*/
		IYV0W21();
		/*
			Печать изолированных участков сети по ветвям и узлам :
		*/
		PIY0W21();
	L_400:
		for (i = 1; i <= M; i++)
		{
			ADOPrg[i] = ADOPrg[i] * BP / 1000.0;
		}
		if (KFOS >= KFS)
		{
			strOutputRG.Format(_T("\niii = %4d\n"), iii);
			NFPU7_FileRG.WriteString(strOutputRG);
			strOutputRG.Format(_T("\nKEAVRV = %4d\n"), KEAVRV);
			NFPU8_FileRG.WriteString(strOutputRG);
			for (i = 1; i <= KEAVRV; i++)
			{
				strOutputRG.Format(_T(" %4d "), NEAVRV[i]);
				NFPU8_FileRG.WriteString(strOutputRG);
			}
			NFPU8_FileRG.WriteString(_T("\n"));
			strOutputRG.Format(_T("\niii = %4d\n"), iii);
			NFPU9_FileRG.WriteString(strOutputRG);
		}
	}
	else
	{
		if (KEAVRV <= 0)
		{
			strOutputRG.Format(_T("\iii = %4d\n"), iii);
			NFPU8_FileRG.WriteString(strOutputRG);
		}
		if (KEAVRN <= 0)
		{
			strOutputRG.Format(_T("\niii = %4d\n"), iii);
			NFPU9_FileRG.WriteString(strOutputRG);
		}
		/*		Инициализация списка индексов узлов основной сети:*/
		KYOS = N;
		for (i = 1; i <= N; i++)
		{
			MYPQrg[i] = i;
		}
		strOutputRG.Format(_T("\niii = %4d\n"), iii);
		NFPU7_FileRG.WriteString(strOutputRG);

		KV = 0;
		for (i = 1; i <= M; i++)
		{
			ADOPrg[i] = ADOPrg[i] * BP / 1000.0;
			if (MVKrg[i] != 1) continue;
			KV = KV + 1;
		}
		if (KV != N)
		{
			NFP_FileRG.WriteString(_T(" Сеть замкнута, задание прекращено для коррекции схемы\n"));
			theApp.STOP();
		}
	}
	return true;
}

void FM00W21()
{
	/*
	C   ====================================================================
	C     Формирование массива смежности узлов по узлам M3(1:NSM) и массива
	!     смежности узлов по ветвям M3V(1:NSMV) для исходной сети, где все
	!     ветви включены.

	C     NSM, NSMV - длины массива смежности, оцениваются в N*7 и N*5.
	C   ====================================================================
	*/
	int i, j, ii, N01, K1, K, KJ, IKO, INA, L;
	N01 = N + 1;
	K1 = N01 * 2 + 1;
	K = K1;
	KJ = 1;

	for (i = 1; i <= N01; i++)
	{
		ii = i;
		IKO = i * 2;
		INA = IKO - 1;
		L = 0;
		for (j = 1; j <= M; j++)
		{
			if (M1rg[j] != i) goto L_2;
			M3rg[K] = M2rg[j];
			M3Vrg[KJ] = j;
			L = L + 1;
			if (L == 1) M3rg[INA] = K;
			if (NSMAX <= K) goto L_6;
			K = K + 1;
			if (NSMVM <= KJ) goto L_7;
			KJ = KJ + 1;
			continue;
		L_2:
			if (M2rg[j] != i) continue;
			M3rg[K] = M1rg[j];
			M3Vrg[KJ] = j;
			L = L + 1;
			if (L == 1) M3rg[INA] = K;
			if (NSMAX <= K) goto L_6;
			K = K + 1;
			if (NSMVM <= KJ) goto L_7;
			KJ = KJ + 1;
		}
		if (L != 0) goto L_4;

		strOutputRG.Format(_T("\n Связность узла %5d в схему не заложена\n выполнение задания прекращено для исправления данных."), ii);
		NFP_FileRG.WriteString(strOutputRG);
		theApp.STOP();
	L_6:
		strOutputRG.Format(_T("\n Необходимо увеличить%5d - размерность списка NSM\n выполнение задания прекращено для исправления данных."), K);
		NFP_FileRG.WriteString(strOutputRG);
		theApp.STOP();
	L_7:
		strOutputRG.Format(_T("\n Необходимо увеличить%5d - размерность списка NSMV\n выполнение задания прекращено для исправления данных."), KJ);
		NFP_FileRG.WriteString(strOutputRG);
		theApp.STOP();
	L_4:
		M3rg[IKO] = K - 1;
	}
	NSM = K - 1;
	NSMV = KJ - 1;
}

void MY00W1()
{
	int i, j, N12, NV2, NV1, NNV, NKV, NV;
	KNDErg = 0;
	N12 = N1 * 2;
	for (i = 1; i <= N1; i++)
	{
		NV2 = i * 2;
		NV1 = NV2 - 1;
		NNV = M3rg[NV1] - N12;
		NKV = M3rg[NV2] - N12;
		for (j = NNV; j <= NKV; j++)
		{
			NV = M3Vrg[j];
			if (KNDErg >= KNDEM)
			{
				strOutputRG.Format(_T("\nУвеличить размерность списков св¤зности более заданной - %5d \n"), KNDEM);
				NFP_FileRG.WriteString(strOutputRG);
			}
			KNDErg = KNDErg + 1;
			NYPHrg[KNDErg] = M3rg[j + N12];
			NVPH[KNDErg] = NV;
			if (j == NNV) IHrg[i] = KNDErg;
			if (j == NKV) IYPHrg[KNDErg] = 0;
			if (j != NKV) IYPHrg[KNDErg] = KNDErg + 1;
		}
	}
	return;
}

void OFM0W1()
{
	int i, j, N12, KOLSH, L, i1;
	N12 = N1 * 2;
	int k = M;
	for (int iq = 1; iq <= M; iq++)
	{
		if (MVKN[iq] == 1) continue;
		NTY = M1rg[iq];
		NYNI = M2rg[iq];
		KVIYN[NTY] = KVIYN[NTY] - 1;
		KVIYN[NYNI] = KVIYN[NYNI] - 1;
		KOLSH = 1;
		goto L_100;
		NFP_FileRG.WriteString(_T("\n Monitoring of OFM001\n"));
	L_200:
		NTY = NYNI;
		NYNI = M1rg[iq];
		KOLSH = 2;
	L_100:
		/*	Схлопывание списков связности в цепочке узла с указателем JJ :
			Определение адреса и удаление указателя на старый узел NYNI :		*/
		i = IHN[NTY];
		L = NYPHN[i];
		/*			Проверка ветви на удаление по идентичности L узлу NYNI :		*/
		if (L != NYNI) goto L_785;
		/*	Фиксация выведенного элемента массива IYPHN и массива NYPHN с
						  помощью указателя - элемента массива IYSTEKrg :		*/
		i1 = IYPHN[i];
		IHN[NTY] = i1;
		goto L_786;
		/*			Схлопывание I - го элемента списков связности :		*/
	L_785:
		j = i;
		i = IYPHN[j];
		L = NYPHN[i];
		/*			Проверка ветви на удаление по идентичности L узлу NYNI :		*/
		if (L != NYNI) goto L_785;
		i1 = IYPHN[i];
		if (i1 != 0) IYPHN[j] = i1;
		if (i1 == 0) IYPHN[j] = 0;
	L_786:
		KNDEN = KNDEN - 1;
		/*			Для отладки - просмотр прохождения всех узлов откл - х ветвей :
			фиксация выведенного элемента массива IYPHN и массива NYPHN с
			помощью указателя - элемента массива IYSTEKrg :		*/
		KSTEKrg = KSTEKrg + 1;
		if (KSTEKrg >= KSTEKM)
		{
			strOutputRG.Format(_T("\n Увеличьте размерность списка стека более заданной - %5d\n"), KSTEKM);
			NFP_FileRG.WriteString(strOutputRG);
			theApp.STOP();
		}
		IYSTEKrg[KSTEKrg] = i;
		if (KOLSH != 2) goto L_200;
	}
}

void IYS0W1()
{
	/*	C ---------------------------------------------------------------------
		C           Формирование изолированных участков сети
		C ---------------------------------------------------------------------*/
	int i, IYPY, KEST, NYI, NYP, IJ;
	/*	Количество фрагментов сети*/
	KFS = 0;
	/*	C---------------------------------------------------------------------
		C  Проход от базисного узла 
		C ---------------------------------------------------------------------
		Инициализация указателя-индекса текущего элемента массива STEK,
		C  показывающего положение последнего найденного узла
		C  (индекс-указатель пройденного узла):*/
	IYPY = 0;
	/*	Инициализация номера узла, от которого исходит отсчет:*/
	KEST = 1;
	STEK[KEST] = N1;
	INDXY[1] = 1;
	MFSY[N1] = 1;
	/*	Выделение очередного текущего номера просматриваемого узла элемента массива STEK:*/
L_2100:
	NYI = STEK[IYPY + 1];
	i = IHN[NYI];
	if (i == 0) goto L_3000;
	/*	Определение номеров узлов, которые еще нужно пройти по (номеру) исходного узла NYI*/
L_1100:
	NYP = NYPHN[i];
	IJ = IYPHN[i];
	if (IYPY == 0) goto L_110;
	/*		Проверка на наличие узла NYP в списке уже пройденных узлов с целью возможного увеличения стека STEK:	*/

	for (int j = 1; j <= KEST; j++)
	{
		if (NYP != STEK[j]) continue;
		goto L_1200;
	}
L_110:
	KEST = KEST + 1;
	STEK[KEST] = NYP;
	/*		Коррекция элемента массива фиксации стековых узлов:	*/
	MFSY[NYP] = 1;
L_1200:
	i = IJ;
	if (i != 0) goto L_1100;
	IYPY = IYPY + 1;
	if (IYPY < KEST)  goto L_2100;
L_3000:
	KFS = KFS + 1;
	if (KEST < N1)
	{
		INDXY[KFS + 1] = KEST + 1;
		for (i = 1; i <= N1; i++)
		{
			if (MFSY[i] == 1) continue;
			/*				Инициализация номера узла, от которого исходит отсчет :			*/
			IYPY = KEST;
			KEST = KEST + 1;
			STEK[KEST] = i;
			MFSY[i] = 1;
			goto L_2100;
		}
	}
	else
	{
		/*			Формирование массива индекса концов фрагментов сети :		*/
		for (int IFS = 1; IFS <= KFS; IFS++)
		{
			if (IFS == KFS)
			{
				INDXYK[IFS] = N1;
			}
			else
			{
				INDXYK[IFS] = INDXY[IFS + 1] - 1;
			}
		}
	}
}

void RAV0W1()
{
	/*C-------------------------------------------------------------------- -
	C     Моделирование работы автоматики в средневольтной части сети
	C-------------------------------------------------------------------- -	*/
	;
	int i, j, IX, IFS, IYTYF, IYKYF, KYFS, NYP, IJ, NVP, JK, IJKN, IJKK, IKS;
	KEAVRV = 0;
	/*		Инициализация списка индексов узлов основной сети:	*/
	KYOS = INDXY[2] - 1;
	for (i = 1; i <= KYOS; i++)
	{
		MYPQrg[i] = STEK[i];
	}
	/*		Инициализация индексов фрагментов:	*/
	for (i = 1; i <= KFS; i++)
	{
		INDXF[i] = i;
	}
	/*			KFOS - это число фрагментов основной(опорной или питаемой) сети.
			Включает исходный фрагмент и все остальные фрагменты,
			подключаемые к исходному.	*/
	KFOS = 1;
	/*	Проход по 2-му(IFS=2), 3-му (IFS=3),..., фрагментам сети:	*/
	IX = KFOS + 1;
L_2000:
	IFS = INDXF[IX];
	/*	Просмотр фрагмента на наличие в нем РП
	Индекс-указатель текущего узла фрагмента 	*/
	IYTYF = INDXY[IFS];
	/*	Индекс - указатель конечного узла фрагмента 	*/
	IYKYF = INDXYK[IFS];
	KYFS = IYKYF - IYTYF + 1;
	/*	Выделение очередного текущего номера просматриваемого узла  элемента массива STEK:	*/
	for (j = IYTYF; j <= IYKYF; j++)
	{
		NTY = STEK[j];
		if (TPRP[NTY] != 1) continue;
		/*		Просмотр узлов с возможной подачей напряжения от АВР СН		*/
		i = IHrg[NTY];
		if (i == 0) continue;
		/*		Определение номеров узлов, которые еще нужно пройти по (номеру) исходного узла NTY		*/
	L_1100:
		NYP = NYPHrg[i];
		IJ = IYPHrg[i];
		/*		Проверка узла NYP на возможность подачи от него напряжения по АВР СН :		*/
		if (TPRP[NYP] != 1) goto L_1200;
		NVP = NVPH[i];
		if (MVKrg[NVP] == 1) goto L_1200;
		if (ZAVR[NVP] == 1) goto L_1200;
		if (AVR[NVP] == 0) goto L_1200;
		if ((AVR[NVP] == 1) || (AVR[NVP] == 3) && (NYP == M1rg[NVP]) || (AVR[NVP] == 4) && (NTY == M1rg[NVP]))
		{
			/*			Проверка связности с основной сетью :
			Поиск узла, подающего напряжение на NTY от АВР СН с узла NYP			*/
			for (int jx = 1; jx <= KFOS; jx++)
			{
				JK = INDXF[jx];
				/*				Индексы - указатели начального и конечного узлов фрагмента основной сети :				*/
				IJKN = INDXY[JK];
				IJKK = INDXYK[JK];
				/*				Просмотр STEKа фрагмента основной сети :				*/
				for (int jj = IJKN; jj <= IJKK; jj++)
				{
					/*					Условие фиксации фрагмента сети, как части основной сети :					*/
					if (STEK[jj] == NYP)
					{
						/*						Продолжение формирования списка индексов узлов основной сети :						*/
						IKS = 0;
						for (int is = IYTYF; is <= IYKYF; is++)
						{
							IKS = IKS + 1;
							MYPQrg[KYOS + IKS] = STEK[is];
						}
						/*						Коррекция фрагментированности сети :						*/
						KYOS = KYOS + KYFS;

						MVKN[NVP] = 1;
						KVIYN[M1rg[NVP]] = KVIYN[M1rg[NVP]] + 1;
						KVIYN[M2rg[NVP]] = KVIYN[M2rg[NVP]] + 1;
						/*						Включение ветви NVP по массивам связности :						*/
						//CALL       ON00W1()
						strOutputRG.Format(_T("\n Работа АВР СН ветви%5d - подача напряжения на узел%5d с узла %5d\n"), NVP, NTY, NYP);
						NFP_FileRG.WriteString(strOutputRG);

						KEAVRV = KEAVRV + 1;
						NEAVRV[KEAVRV] = NVP;

						/*						Коррекция индексов фрагментов сети :						*/
						KFOS = KFOS + 1;
						INDXF[IFS] = INDXF[KFOS];
						INDXF[KFOS] = IFS;
						IX = KFOS;
						goto L_1900;
					}
					else
					{

					}
				}
			}
		}
		else
		{
			goto L_1200;
		}
	L_1200:
		i = IJ;
		if (i != 0) goto L_1100;
	}
L_1900:
	if (KFS > IX)
	{
		IX = IX + 1;
		goto L_2000;
	}
	else
	{
		return;
	}
}

void RAN0W1(COMPLEX IY[])
{
	int i, j, IX, IFS, IYTYF, IYKYF, NYP, NVP, IJ, JK, IJKN, IJKK;
	/*C ---------------------------------------------------------------------
	C     Моделирование работы автоматики в низковольтной части сети
	C ---------------------------------------------------------------------
	C
		Проход по второму (IFS=2), третьему (IFS=3),..., дp фрагментам	*/
	IX = KFOS + 1;

	/*	Индекс-указатель текущего узла фрагмента 	*/
L_2000:

	IFS = INDXF[IX];
	IYTYF = INDXY[IFS];
	/*	Индекс-указатель конечного узла фрагмента	*/
	IYKYF = INDXYK[IFS];
	/*	Выделение очередного текущего номера просматриваемого узла элемента массива STEK	*/
	for (j = IYTYF; j <= IYKYF; j++)
	{
		NTY = STEK[j];
		/*		Формирование изолированных участков сети по ветвям		*/
		if (TPRP[NTY] != 2) continue;

		/*		Просмотр смежных узлов с возможным подхватом нагрузки от н / в АВР :
		!Выделение узла с возможным подхватом нагрузки от н / в АВР :		*/
		i = IHrg[NTY];
		if (i == 0) continue;

		/*		Определение номеров узлов которые еще нужно пройти по номеру исходного узла NTY		*/
	L_1100:
		NYP = NYPHrg[i];
		NVP = NVPH[i];
		IJ = IYPHrg[i];
		/*		Проверка ветви NVP на возможность АВР по НН :
		*/
		if (TPRP[NYP] != 2) goto L_1200;
		if (MVKrg[NVP] == 1) goto L_1200;
		if (ZAVR[NVP] == 1) goto L_1200;
		if (AVR[NVP] != 2) goto L_1200;
		/*		Поиск узла, подхватывающего нагрузку, среди отделившихся фрагментов :		*/
		for (int jkx = KFOS + 1; jkx <= KFS; jkx++)
		{
			JK = INDXF[jkx];
			if (JK == IFS) continue;
			IJKN = INDXY[JK];
			/*			Индекс - указатель конечного узла фрагмента :			*/
			if (JK == KFS)
			{
				IJKK = N1;
			}
			else
			{
				IJKK = INDXY[JK + 1] - 1;
			}
			/*				Выделение очередного текущего номера просматриваемого узла элемента массива STEK :			*/
			for (int jj = IJKN; jj <= IJKK; jj++)
			{
				/*				Условие неопределенности - "повисания" обоих узлов :				*/
				if (STEK[jj] == NYP)
				{
					strOutputRG.Format(_T("\n Работа н/в АВР - подхват нагрузки узла %5d' узлом %5d\n"), NTY, NYP);
					NFP_FileRG.WriteString(strOutputRG);
				}
				if (STEK[jj] == NYP) continue;
			}
		}
		/*			Коррекция нагрузок узлов		*/
		IYrg[NYP] = IYrg[NYP] + IYrg[NTY];

		strOutputRG.Format(_T("\n Обе секции ТП - %5d и %5d находятся в обесточенных фрагментах\n"), NTY, NYP);
		NFP_FileRG.WriteString(strOutputRG);

		KEAVRN = KEAVRN + 1;
		NEAVRN[KEAVRN] = NYP;
		continue;
	L_1200:

		i = IJ;
		if (i != 0) goto L_1100;
	}

	/*	Обнуление нагрузок элементов обесточенного фрагмента сети	*/
	for (j = IYTYF; j <= IYKYF; j++)
	{
		NTY = STEK[j];
		IYrg[NTY] = COMPLEX(0.0, 0.0);
	}
	/*	Просмотр смежных узлов с возможным подхватом нагрузки от н / в АВР :	*/
	if (KFS > IX)
	{
		IX = IX + 1;
		goto L_2000;
	}
	else
	{
		return;
	}
}

void IYV0W21()
{
	/*
	C ---------------------------------------------------------------------
	C     Формирование изолированных участков сети по ветвям
	C ---------------------------------------------------------------------
	C
	*/
	int i, j, IFS, IYPV, KESTVF, IYTYF, IYKYF, NYI, NVP, IJ;
	KESTV = 0;
	/*
	Проход по всем отделившимся фрагментам сети
	*/
	for (int ix = KFOS + 1; ix <= KFS; ix++)
	{
		IFS = INDXF[ix];
		/*Инициализация указателя - индекса текущего элемента массива STEKV, показывающего положение последней найденной ветви(индекс - указатель пройденной ветви) :*/
		IYPV = 0;
		/*Инициализация номера ветви, от которой исходит отсчет :		*/
		KESTVF = KESTV + 1;
		STEKV[KESTVF] = NVPH[IHrg[STEK[INDXY[IFS]]]];
		INDXV[IFS] = KESTV + 1;
		/*Индексы - указатели текущего и конечного узла фрагмента :*/
		IYTYF = INDXY[IFS];
		IYKYF = INDXYK[IFS];
		/* Выделение очередного текущего номера узла фрагмента :*/

		for (int indx = IYTYF; indx <= IYKYF; indx++)
		{
			NYI = STEK[indx];
			i = IHrg[NYI];
			/*Определение номеров узлов, которые еще нужно пройти по(номеру] исходного узла NYI :*/
		L_1100:

			NVP = NVPH[i];
			IJ = IYPHrg[i];
			if (IYPV == 0) goto L_110;
			/*Проверка на наличие NVP в списке уже пройденных ветвей с целью возможного увеличения стека STEKV :
			*/
			for (j = KESTVF; j <= KESTV; j++)
			{
				if (NVP != STEKV[j]) continue;
				goto L_1200;
			}
		L_110:
			KESTV = KESTV + 1;
			STEKV[KESTV] = NVP;
			IYPV = IYPV + 1;
		L_1200:
			i = IJ;
			if (i != 0) goto L_1100;
		}
		/* Формирование массива индекса конца фрагментов узлов :*/
		INDXVK[IFS] = INDXV[IFS] + KESTV - KESTVF;
	}
}

void PIY0W21()
{
	/*	C ---------------------------------------------------------------------
	C     Печать изолированных участков сети по ветвям и узлам
	C ---------------------------------------------------------------------	*/
	int i, j = 0, IFS, IYTYF, IYKYF;
	strOutputRG.Format(_T("\n Число изолированных фрагментов сети -%4d\n"), KFS - KFOS);
	NFP_FileRG.WriteString(strOutputRG);
	strOutputRG.Format(_T("\n%4d\n"), KFS - KFOS);
	NFPU7_FileRG.WriteString(strOutputRG);


	/*Проход по всем фрагментам сети*/
	for (int ix = KFOS + 1; ix <= KFS; ix++)
	{
		IFS = INDXF[ix];
		j = j + 1;
		strOutputRG.Format(_T("\n Изолированный фрагмент сети номер%4d\n"), j);
		NFP_FileRG.WriteString(strOutputRG);
		/*Индекс - указатель начального и конечного узла фрагмента :*/
		IYTYF = INDXY[IFS];
		IYKYF = INDXYK[IFS];
		for (i = IYTYF; i <= IYKYF; i++)
		{
			strOutputRG.Format(_T(" %4d "), STEK[i]);
			NFPU7_FileRG.WriteString(strOutputRG);
		}
		NFPU7_FileRG.WriteString(_T(" Узлы фрагмента:"));
		for (i = IYTYF; i <= IYKYF; i++)
		{
			strOutputRG.Format(_T(" %4d "), STEK[i]);
			NFP_FileRG.WriteString(strOutputRG);
		}
		/*Индекс - указатель начальной и конечной ветви фрагмента*/
		IYTYF = INDXV[IFS];
		IYKYF = INDXVK[IFS];
		for (i = IYTYF; i <= IYKYF; i++)
		{
			strOutputRG.Format(_T(" %4d "), STEKV[i]);
			NFPU7_FileRG.WriteString(strOutputRG);
		}
		NFP_FileRG.WriteString(_T(" Ветви фрагмента:"));
		for (i = IYTYF; i <= IYKYF; i++)
		{
			strOutputRG.Format(_T(" %4d "), STEKV[i]);
			NFP_FileRG.WriteString(strOutputRG);
		}
	}
}

void DE00W1(int(&IH)[77], int(&IYPHrg)[290], int(&NYPH)[290], int(&KVIYrg)[77], COMPLEX(&IY)[77], COMPLEX(&IT)[130], int(&NVPH)[290])
{
	/*
	C--------------------------------------------------------------------
	C           Упорядочивание, исключение, токораспределение
	C(токораспределение только для ДЕРЕВА!!!)
	C     Переменные :
	C----------
	C     KYN - Количество узлов нагрузочных(= исключаемых)
	C     NTY - Номер текущего узла сети в подсписке связности узла
	C                 NYNI
	C     NTY2 - То же, но во внутреннем цикле
	C     N12 - Индекс указателя последнего номера узла
	C     NYNI - Номер узла среди нагрузочных(исключаемых) с
	C                 наименьшей инцидентностью
	C     IND - Индекс этого узла(
	C     NYY - Номер упорядоченного узла
	C------------------------------------------------------------------ -*/

	int i, j, NYNI, KYN, L, MINPR, MINP, NYY, NMIN, IND, ii, jj, NYV, I1;
	double th = 1000.0;
	for (i = 1; i <= M; i++)
	{
		ITrg[i] = COMPLEX(0.0, 0.0);
	}
	/*Инициализация списка нагрузочных (исключаемых) узлов:*/

	for (i = 1; i <= N; i++)
	{
		IYF[i] = IYrg[i];
	}
	IYF[N1] = COMPLEX(0.0, 0.0);
	KYN = KYOS;
	if ((KYOS == N) && (KFOS == KFS) || (KFS == 1))
	{
		MINPR = 0;
		MINP = 1;
	}
	else
	{
		MINPR = 1;
		MINP = 2;
	}
	NYY = 0;
L_1000:
	/*Поиск номера и индекса узла с наименьшей инцидентностью*/
	NMIN = M;
	NYY = NYY + 1;
	for (i = MINP; i <= KYN; i++)
	{
		j = MYPQrg[i];
		L = KVIYrg[j];
		if (L >= NMIN) continue;
		NMIN = L;
		NYNI = j;
		IND = i;
	}
	MYYrg[NYY] = NYNI;
	/*Пересчет фиктивных токов нагрузки узла, инцидентного NYNI:*/
	i = IHrg[NYNI];
L_60:
	L = NYPHrg[i];
	IYF[L] = IYF[L] + IYF[NYNI];
	i = IYPHrg[i];
	if (i != 0) goto L_60;
	/*Исключение (удаление) узла NYNI из списков связности:
	Коррекция элемента массива индексов нагрузочных (исключаемых) узлов  */
	MYPQrg[IND] = MYPQrg[KYN];
	KYN = KYN - 1;
	/*Определение адреса и заполнение указателя на новый узел :*/
	ii = IHrg[NYNI];
L_780:
	jj = ii;
	ii = IYPHrg[ii];
	NTY = NYPHrg[jj];
	/*Определение номера удаляемой ветви и ее тока: */
	NYV = NVPH[jj];
	MYV[NYY] = NYV;
	if (M1rg[NYV] == NYNI) ITrg[NYV] = IYF[NYNI];
	if (M1rg[NYV] == NTY) ITrg[NYV] = -IYF[NYNI];

	if (NTY == N1) goto L_610;
	KVIYrg[NTY] = KVIYrg[NTY] - 1;
	/*Схлопывание списков связности в цепочке узла с указателем JJ:
	Определение адреса и удаление указателя на старый узел NYNI:*/
L_610:
	i = IHrg[NTY];
	L = NYPHrg[i];
	if (L != NYNI) goto L_785;
	I1 = IYPHrg[i];
	IHrg[NTY] = I1;
	goto L_786;
	/* Схлопывание I-го элемента списков связности:*/
L_785:
	j = i;
	i = IYPHrg[j];
	L = NYPHrg[i];
	if (L != NYNI) goto L_785;
	I1 = IYPHrg[i];
	if (I1 != 0) IYPHrg[j] = I1;
	if (I1 == 0) IYPHrg[j] = 0;
L_786:

	if (ii != 0) goto L_780;
	if (KYN > MINPR) goto L_1000;
	//L_990:
	strOutputRG = (_T("\n I,     MYY,     MYV\n"));
	NFP_FileRG.WriteString(strOutputRG);
	for (i = 1; i <= KYOS - 1; i++)
	{
		strOutputRG.Format(_T("   %4d   %4d   %4d  %6.1lf   %6.1lf\n"), i, MYYrg[i], MYV[i], (ITrg[MYV[i]] * th).real(), (ITrg[MYV[i]] * th).imag());
		NFP_FileRG.WriteString(strOutputRG);
	}
}

void CH00W1(COMPLEX(&ITrg)[130], COMPLEX(&IYrg)[77])
{
	/*======================================================================
	C       Проверка напряжений узлов на допустимость:
	C         Если KPU = 1, то хотя бы одно напряжение превышает предел,
	C         EC‹€ KPU = 0, то все напряжения в пределах допуска.
	C ======================================================================*/
	int i, j, KYOSP, ii, NNY, NKY, NKYY;
	double UR;
	KPU = 0;
	Urg[N1] = U0;

	if (KYOS == N)
	{
		KYOSP = KYOS + 1;
	}
	else
	{
		KYOSP = KYOS;
	}
	if (KODSEA != 1)
	{
		NFPU3_FileRG.WriteString(_T(" Узел  РП / ТП  Ток, А      Напряжение, кВ\n"));
		MYYrg[KYOSP] = N1;
		for (i = 1; i <= KYOSP - 1; i++)
		{
			ii = KYOSP - i;
			j = MYV[ii];
			NNY = M1rg[j];
			NKY = M2rg[j];
			NKYY = MYYrg[ii];
			if (NKY == NKYY)
			{
				Urg[NKY] = Urg[NNY] - Xrg[j] * ITrg[j] * SQR3;
			}
			else
			{
				Urg[NNY] = Urg[NKY] + Xrg[j] * ITrg[j] * SQR3;
			}
			UR = abs(Urg[NKYY]);
			strOutputRG.Format(_T(" %4d    %8s   %7.1lf     %6.2lf\n"), NKYY, NAZVY[NKYY], abs(IYrg[NKYY]) * 1000, UR);
			NFPU3_FileRG.WriteString(strOutputRG);
			if ((UR >= UMIN) && (UR <= UMAX)) continue;
			KPU = 1;
		}
	}
}

void CR30W21(COMPLEX(&ITrg)[130], int &KP, int(&MVKrg)[130], int(&NPB)[20])
{
	/*C =====================================================================
	C
	C   ѕроверка токов ветвей сети на допустимость по перегрузке
	C
	C   ITrg(M) - токи ветвей,
	!   PEROT(M) - перегрузки ветвей в о.е.,
	C   NPB(KP) - номера перегружаемых ветвей,
	!   KP - количество перегрузок,
	!   MVK(M) - состо¤ние ветвей.
	C ======================================================================*/
	double R0 = 0.0, R1000 = 1000.0, A;
	int i;
	KP = 0;
	for (i = 1; i <= M; i++)
	{
		if (MVKrg[i] == 1) goto L_50;
		PEROT[i] = R0;
		continue;
	L_50:
		A = abs(ITrg[i]);
		PEROT[i] = A / ADOPrg[i];
		if (PEROT[i] <= 1.0) continue;
		KP = KP + 1;
		strOutputRG.Format(_T(" Перегрузка №%5d в ветви %5d составляет%7.1lf %s%5.2lf о.е.\n"), KP, i, A*R1000, PEROT[i]);
		NFP_FileRG.WriteString(strOutputRG);
		if (KP > KPMAX) continue;
		NPB[KP] = i;
	}
}

void CB00W1(COMPLEX(&ITrg)[130], int MVK[130])
{
	/*C ======================================================================
	C     Печать токов по ветвям
	C ======================================================================
	C*/

	CString VKL = (_T("Вкл. ")), OTKL = (_T("Откл. ")), POL;
	int i, K;
	NFPU4_FileRG.WriteString(_T("Ветвь РП/ТП: начала  конца    Положение  Доп I, А   Ток, А   Перегруз, %"));
	NFPU12_FileRG.WriteString(_T("Ветвь Узел: начала       конца  Положение Доп I, А   Ток, А   Перегруз, %"));
	for (i = 1; i <= M - 1; i++)
	{
		K = int(ADOPrg[i] * 1000.0);
		if (MVK[i] == 1)
		{
			POL = VKL;
		}
		else
		{
			POL = OTKL;
		}
		strOutputRG.Format(_T("%4d    %8s    %8s   %5s     %5d     %8.1lf     %7.1lf\n"), i, NAZVY[M1rg[i]], NAZVY[M2rg[i]], POL, K, abs(ITrg[i])*1000.0, PEROT[i] * 100);
		NFPU4_FileRG.WriteString(strOutputRG);

		strOutputRG.Format(_T("%4d    %8d    %8d   %5d     %5d     %8.1lf     %7.1lf\n"), i, M1rg[i], M2rg[i], MVK[i], K, abs(ITrg[i])*1000.0, PEROT[i] * 100);
		NFPU12_FileRG.WriteString(strOutputRG);
	}
}